<?php

namespace App\Classes;

use App\Models\ProductsAliexpressMm;
use App\Models\ProductsAttributesAliexpress;
use App\Models\ProductsShippingAliexpress;
use App\Models\ShopsAliexpressConfig;
use Carbon\Carbon;
use DateTimeZone;

class AliexpressClass
{
    /** CALCULO FRETE INTERNACIONAL */
    public function freeAliexpress($id)
    {
        $result = ProductsShippingAliexpress::where('product_id', $id)->first();
        $array = json_decode($result['shipping']);

        $result_table = [];
        $test_price = 99999;

        foreach ($array as $value) {

            ## Calcular Dias de Entrega
            $deadline = ($value->deliveryTimeInDays->min < $value->deliveryTimeInDays->max)
                ? $value->deliveryTimeInDays->min . ' - ' . $value->deliveryTimeInDays->max
                : $value->deliveryTimeInDays->max;

            # calcular Custo de Entrega
            $price = ($value->price > 0.00)
                ? $value->price
                : 0;

            if ($price < $test_price) {

                ## company name
                switch ($value->company->name) {
                    case 'AliExpress Standard Shipping':
                        $name = 'Frete Internacional';
                        break;
                    case 'Aliexpress Direct':
                        $name = 'Frete Internacional';
                        break;
                    default:
                        $name = 'Frete Internacional';
                }

                // Cria uma instância do Carbon com a data do banco
                $funcao =  Carbon::now(new DateTimeZone('America/Sao_Paulo'))->settings(['locale' => 'pt_BR', 'timezone' => 'America/Sao_Paulo',])->addDays($deadline);
                $deadline_value = $funcao->format('d M');

                ## Carregar Frete Array
                array_push($result_table, [
                    'id' => $name . ':' .  $price . ':' . $deadline,
                    'name' => $name,
                    'value' => $price,
                    'deadline' => ' até ' . $deadline_value,
                ]);

                $test_price = $price;
            }
        }

        return  $result_table;
    }


    /** CALCULO FRETE ALIEXPRES */
    public function aliexpressFreightCheckout($data, $result_product_digital)
    {
        ## Carregar Produtos do Array
        $result_array = $data['value'];
        $idAli = null;
        $deadline = null;
        $price = null;

        foreach ($result_array as $value) {

            if (!is_null($value['sku_aliexpress']) && $value['condition'] != 3) {

                ## Localizar Produto Sku ALiexpress
                $result = ProductsShippingAliexpress::where('product_id', $value['id'])->first();
                $array = json_decode($result['shipping']);

                $price_item = 999999;
                foreach ($array as $value) {

                    ## Criar ID Frete
                    $idAli =  $idAli . '_' . $value->company->id;

                    ## Calcular Dias de Entrega
                    $result_deadline = ($value->deliveryTimeInDays->min < $value->deliveryTimeInDays->max)
                        ? $value->deliveryTimeInDays->min . ' - ' . $value->deliveryTimeInDays->max
                        : $value->deliveryTimeInDays->max;

                    $deadline = ($result_deadline >  $deadline)
                        ? $result_deadline
                        : $deadline;

                    ## calcular Custo de Entrega
                    $result_price = ($value->price > 0.00)
                        ? $value->price
                        : 0;

                    ## Resultado Custo do Frete
                    $price_item = ($price_item <  $result_price) ?  $price_item : $result_price;
                }

                ## Soma Multiplos Resultados
                $price =  $price + $price_item;
            }
        }

        if (!is_null($price)) {

            // Cria uma instância do Carbon com a data do banco
            $funcao =  Carbon::now(new DateTimeZone('America/Sao_Paulo'))->settings(['locale' => 'pt_BR', 'timezone' => 'America/Sao_Paulo',])->addDays($deadline);
            $deadline_value = $funcao->format('d M');

            $result_table = [[
                'id' => 'FreteAliexpress' . ':' .  $price . ':' . $deadline,
                'name' => 'Frete Internacional',
                'value' => $price,
                'deadline' => 'até ' . $deadline_value,
            ]];

            $result_frete = [
                'seller_id' => $data['shop_id'],
                'seller_name' => $data['shop_name'],
                'value' => $result_table
            ];

            ## Verificar Se Array tem conteudo
            if (empty($result_frete)) {
                $result_frete = null;
            }
        } else {

            $result_frete = null;
        }

        ## Incrementar Frete Digital
        if (!is_null($result_frete) && !is_null($result_product_digital)) {

            $price = ($result_product_digital['value'][0]['value'] > 0.00)
                ? ' - R$ ' . number_format($result_product_digital['value'][0]['value'], 2, ',', '.')
                : ' - Frete Gráis';
            $result_frete += ['digital_deadline' => ' Com ' . $result_product_digital['value'][0]['name'] . $price . ' - ' . $result_product_digital['value'][0]['deadline']];
        }


        return  $result_frete;
    }


    /** LIMPAR URL ID ALIEXPRESS */
    public function clearSkuUrl($string)
    {
        ## Exploder por Atributos
        $exploderAttribute = explode(";", $string);

        $arrayId = '';
        $i = 0;
        foreach ($exploderAttribute as $value) {
            $line = ($i == 0) ? '' : ';';
            $exploder = explode("_", $value);

            $arrayId = $arrayId . $line . $exploder[0];
            $i++;
        }

        return $arrayId;
    }

    /** Verificar SKU Final */
    public function checkFinalSku($sku)
    {
        $exploderAttribute = explode(";", $sku);

        $arrayId = '';
        $i = 0;
        foreach ($exploderAttribute as $value) {
            $line = ($i == 0) ? '' : ';';
            $exploder = explode("#", $value);

            $arrayId = $arrayId . $line . $exploder[0];
            $i++;
        }

        return $arrayId;
    }

    /** CALCULAR PREÇO ALIEXPRESS */
    public function priceAliexpress($price, $shopID, $product_id)
    {
        if ($price > 0) {
            ## Configurações Aliexpress
            $config = ShopsAliexpressConfig::where('shop_id', $shopID)->first();

            ## Incremento Personalizado
            $increment_porc = ProductsAliexpressMm::where('product_id', $product_id)->first();

            if (!is_null($increment_porc) && $increment_porc['increment_value'] > 0) {
                $increment = $increment_porc['increment_value'];
            } else {
                $increment = $config['increment_value'];
            }

            ## Calculo de Valores Price e Special Price
            $price_final =  $price + (($price *  $increment) / 100);

            return $price_final;
        } else {

            return 0;
        }
    }

    /** LOCALIZAR ID CORRETO ALIEXPRESS */
    public function searchAliexpressID($idAli)
    {
        ## exploder sku
        $exploderID =  explode(";", $idAli);

        $array = [];
        $result = '';
        $i = 0;
        foreach ($exploderID as $list) {

            $exploder = explode("#", $list);
            $line = ($i == 0) ? '' : ';';

            ## ID Unitario
            if ($i != 0) {
                array_push($array, $exploder[0]);
            }

            ## ID Grupo
            $result = $result . $line .  $exploder[0];
            array_push($array, $result);

            $i++;
        }

        return $array;
    }

    /** RECUPERAR PROPERTIES ALIEXPRESS */
    public function propertiesAliexpress($properties, $exploder)
    {
        ## Exploder por Atributos
        $exploderAttribute = explode(";", $exploder);
        $arrayId = [];
        foreach ($exploderAttribute as $value) {
            $exploderAttributeId = explode(":", $value);
            array_push($arrayId, $exploderAttributeId[0]);
        }

        ## Localizar Atributos
        $array = [];
        foreach ($properties as $value) {
            foreach ($arrayId as $key) {
                ## Carregar Array
                $data = [
                    'id' => $value->id,
                    'id_value' => $value->id . ':' . $value->value->id . '#' . $value->value->name,
                    'name' => $value->name
                ];
                array_push($array, $data);
            }
        }

        ## Limpar Atributos
        foreach ($arrayId as $key) {
            $id = $key;
            $array = array_filter($array, function ($line) use ($id) {
                return $line['id'] != $id;
            });
        }

        return $array;
    }

    /** CALCULO VARIAÇÃO ALIEXPRESS */
    public function calculatedVariation($request)
    {
        $string = $request->get('idAli');

        ## Recuperar SKU Original
        $replaced = $this->clearSkuUrl($string);

        $product_id = $request->get('productID');

        ## Configurar data
        // $dayAfter = (new DateTime())->modify('-7 day')->format('Y-m-d');

        ## Localizar Sku nos Atributos Aliexpress
        $result = ProductsAttributesAliexpress::where('products_attributes_aliexpress.product_id', $product_id)
            ->leftJoin('products_aliexpress_mm', 'products_aliexpress_mm.product_id', '=', 'products_attributes_aliexpress.product_id')
            ->select(
                'products_attributes_aliexpress.shop_id',
                'products_attributes_aliexpress.product_id',
                'products_attributes_aliexpress.price_min',
                'products_attributes_aliexpress.price_max',
                'products_attributes_aliexpress.discountedPrice_min',
                'products_attributes_aliexpress.discountedPrice_max',
                'products_attributes_aliexpress.properties',
                'products_attributes_aliexpress.variations',
                'products_aliexpress_mm.price as personal_price',
                'products_aliexpress_mm.special_price as personal_special_price',
            )
            ->first();

        ## Liberar ou Atualizar Dados Aliexpress
        $result_array = json_decode($result['variations']);

        $array_result = [];
        if (!is_null($result_array)) {
            foreach ($result_array as $list) {

                ## Formatar SKU Final
                $sku_final = $this->checkFinalSku($list->sku);

                ## Localizar SKU Final
                if ($sku_final == $replaced) {

                    ## PREÇO VARIAVEL
                    if (is_null($result['personal_price'])) {
                        ## Verificar Preço Original
                        if (!empty($list->price->web->originalPrice->value)) {
                            $oPrice = $list->price->web->originalPrice->value;
                        } else {
                            $oPrice = $list->price->value;
                        }

                        $originalPrice = $this->priceAliexpress($oPrice, $result['shop_id'], $product_id);

                        $discPrice = (!empty($list->price->web->discountedPrice->value))
                            ? $list->price->web->discountedPrice->value
                            :  0;

                        $discountedPrice = $this->priceAliexpress($discPrice, $result['shop_id'],  $product_id);

                        ## PREÇO PERSONALIZADO
                    } else {

                        $originalPrice = $result['personal_price'];
                        $discountedPrice = $result['personal_special_price'] ? $result['personal_special_price'] : 0;
                    }

                    $data = [
                        'sku' => $list->sku,
                        'stock' => $list->stock,
                        'price' => $originalPrice,
                        'special_price' => $discountedPrice,
                        'properties' => []
                    ];

                    $final = true;
                    array_push($array_result, $data);

                    ## Localizar Variantes SKU
                } else {

                    ## Ver Resultado Array
                    $exploderID = $this->searchAliexpressID($list->sku);

                    foreach ($exploderID as $key => $value) {

                        if ($replaced == $value) {

                            ## Verificar Produto Com Estouqe
                            if ($list->stock > 0) {
                                ## Recuperar Properties
                                $properties =  $this->propertiesAliexpress($list->properties, $value);
                                $data = [
                                    'properties' => $properties,
                                ];

                                $final = false;
                                array_push($array_result, $data);
                            }
                        }
                    }
                }
            }
        }

        /** Resposta Atributos */
        $response['success'] = true;
        $response['final'] = $final ?? false;
        $response['message'] =   $array_result;

        return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }


    /** Carregar Imagem Aliexpress */
    public function imageAliexpress($request)
    {
        $string = $request->get('idAli');

        ## Recuperar SKU Original
        $replaced = (string)str_replace('_', '#', $string);

        ## Recuparar ID Atributo
        $exploderAttribute = explode(":", $replaced);

        ## ExploderID do Tipo de Atributo
        $exploderAttributeID = explode("#", $exploderAttribute[1]);

        $product_id = $request->get('productID');

        $result = ProductsAttributesAliexpress::where('product_id', $product_id)->first();

        if (!is_null($result)) {

            $result_array =  json_decode($result['properties']);

            $array = [];
            foreach ($result_array as $list) {
                if ($list->id  == $exploderAttribute[0]) {
                    foreach ($list->values as $value) {
                        if ($value->id == $exploderAttributeID[0]) {
                            array_push($array, $value->imageUrl);
                        }
                    }
                }
            }

            if (sizeof($array) == 0) {
                $array = null;
            }

            ## Resposta Imagem
            $response['success'] = (is_null($array)) ? false : true;
            $response['message'] =  $array;

            return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        ## Resposta Imagem
        $response['success'] =  false;
        $response['message'] =  'Erro a Carregar Imagem';

        return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
}
