<?php

namespace App\Classes;

use App\Models\Product;
use App\Models\ProductsAffiliatesMm;
use App\Models\ProductsAgeGroup;
use App\Models\ProductsAliexpressMm;
use App\Models\ProductsAttribute;
use App\Models\ProductsBrand;
use App\Models\ProductsCategoriesMm;
use App\Models\ProductsCategory;
use App\Models\ProductsCondition;
use App\Models\ProductsDropifyMm;
use App\Models\ProductsDsliteMm;
use App\Models\ProductsBlingMm;
use App\Models\ProductsGender;
use App\Models\ProductsImagesGallery;
use App\Models\ProductsImagesGalleryRemove;
use App\Models\ProductsMarketplaceAchaddoMm;
use App\Models\ProductsMarketplaceMlbMm;
use App\Models\ProductsMarketplaceRedirectMm;
use App\Models\ProductsProvider;
use App\Models\ProductsRelationMkp;
use App\Models\ProductsVideosYoutube;
use App\Models\Site;
use App\Models\StoresDepartament;
use App\Models\StoresDepartamentsProductsMm;
use Illuminate\Support\Facades\Storage;

class EditProductsClass
{

    private $clearHtmlClass;

    public function __construct()
    {
        $this->clearHtmlClass = new ClearHtmlClass();
    }

    /** UPDATA DA IMAGEM PRINCIPAL */
    public function uploadImagePrincipal($request,  $shopID)
    {
        ## Remover Imagem Principal
        if (!is_null($request['remove_image'])) {
            /** VERIFICAR CONTA PRODUTO */
            $v = Product::where('shop_id', '=', $shopID)
                ->where('id', '=', $request['id'])
                ->where('image_type', '=', null)
                ->first();

            ## Deletar Imagem
            if (!is_null($v)) {

                Storage::delete($v['image']);

                $remove_img = new ProductsImagesGalleryRemove();
                $remove_img->product_id = $v['id'];
                $remove_img->images = $v['image'];
                $remove_img->type = $v['image_type'];
                $remove_img->save();
            }
        }

        /*** UPOLOUD DE 1 IMAGEM */
        if ($request->hasFile('imagem')) {

            $imagem_principal = null;
            // Verifica se informou o arquivo e se é válido
            if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {

                // Define um aleatório para o arquivo baseado no timestamps atual
                $name = uniqid(date('HisYmd'));
                // Recupera a extensão do arquivo
                $extension = $request->imagem->extension();
                // Define finalmente o nome
                $imagem_principal = "{$name}.{$extension}";
                // Se informou o arquivo, retorna um boolean

                // Salvar Imagem Banco de Dados
                $upload_image = $request->file("imagem")->storeAs('upload/images_products/store_' . $shopID . '/main/product/' . $request['id'], $imagem_principal);
            }
        }

        return $upload_image ?? null;
    }


    /** UPDATE PRODUTOS */
    public function saveUpdateProduct($_dataProduct, $request)
    {

        /** VERIFICAR CONTA PRODUTO */
        $verific = Product::where('shop_id', '=', $_dataProduct['shopID'])->where('id', '=', $request->id)->first();

        /* Converter Moeda */
        $_source = array('.', ',');
        $_replace = array('', '.');

        /* Converter Estue */
        $_source2 = array(',', '.');
        $_replace2 = array('', '.');

        # Atualizar Dados do Produto
        $producty = Product::where('products.id', '=', $verific->id)
            ->leftJoin('products_attributes_aliexpress', 'products_attributes_aliexpress.product_id', '=', 'products.id')
            ->select('products.*', 'products_attributes_aliexpress.product_id as aliexpress_id')
            ->first();

        if ($request->hasFile('imagem') || !is_null($request->remove_image)) {
            $producty->image =  $_dataProduct['upload_image'];
            $producty->image_type = null;
        }

        $producty->title = $request->titulo;
        $producty->description = $this->clearHtmlClass->clear2Html($request->descriptionEdit);
        $producty->note = $request->nota;
        $producty->short_description = $this->clearHtmlClass->clear2Html($request->breveDescricao);

        //// Bloquear Atualização Produtos do Aliexpress
        if (is_null($producty['aliexpress_id'])) {
            $producty->sku = $request->sku;
            $producty->gtin = $request->gtin;
            $producty->ncm = $request->ncm;
            $producty->condition_id = $request->tipo;
            $producty->provider_id =   $request->fornecedor;
            $producty->brand_id =   $request->marca;
            $producty->color = $request->cor;
            $producty->size = $request->size;
            $producty->gender_id = $request->gender;
            $producty->age_group_id = $request->age_group;
            $producty->price =  ($request->preco) ? str_replace($_source, $_replace, $request->preco) : 0.00;
            $producty->special_price = ($request->preco_especial) ? str_replace($_source, $_replace,  $request->preco_especial) : 0.00;
            $producty->cost_price = ($request->preco_custo) ? str_replace($_source, $_replace, $request->preco_custo) : 0.00;
            $producty->quantity = ($request->estoque) ? str_replace($_source2, $_replace2, $request->estoque) + 0 : 0;
            $producty->cross_docking = ($request->cross_docking) ? str_replace($_source, $_replace, $request->cross_docking) : 0;
            $producty->warranty = ($request->garantia) ? str_replace($_source, $_replace, $request->garantia) : 0;
            $producty->weight = ($request->peso) ? str_replace($_source, $_replace, $request->peso) : 0.000;
            $producty->height =  ($request->altura) ? str_replace($_source, $_replace, $request->altura) : 0.00;
            $producty->width = ($request->largura) ? str_replace($_source, $_replace, $request->largura) : 0.00;
            $producty->length = ($request->comprimento) ?  str_replace($_source, $_replace, $request->comprimento) : 0.00;
        }

        $producty->index = NULL;
        $producty->index_ali = NULL;
        $producty->index_drop = NULL;
        $producty->index_dslite = NULL;
        $producty->index_bling = NULL;
        $producty->condition_id = $request->tipo;

        /// Salvar Produto
        $producty->save();

        return  $producty;
    }

    /** UPDATE GALERIA DE IMAGENS */
    public function updateGalleryImages($request, $producty)
    {
        /*** REMOVER GALERIA */
        $remove_gallery = json_decode($request['remove_gallery']);

        for ($i = 0; $i < count($remove_gallery); $i++) {

            # Recuperar Galeria
            $v = ProductsImagesGallery::where('id', $remove_gallery[$i])->first();

            if (!is_null($v)) {

                # excluir Arquivo
                Storage::delete($v['images']);

                $remove_img = new ProductsImagesGalleryRemove();
                $remove_img->product_id = $v['product_id'];
                $remove_img->images = $v['images'];
                $remove_img->type = $v['type'];
                $remove_img->save();

                # Limpar Tabela
                ProductsImagesGallery::where('id', $v['id'])->delete();
            }
        }


        /** UPLOAD DA GALERIA  */
        if ($request->galeria) {
            if ($request->allFiles()['galeria']) {
                for ($i = 0; $i < count($request->allFiles()['galeria']); $i++) {

                    // Define o valor default para a variável que contém o nome da imagem
                    $imagem_galeria = null;

                    // Verifica se informou o arquivo e se é válido
                    if ($request->allFiles()['galeria'][$i]->isValid()) {

                        // Define um aleatório para o arquivo baseado no timestamps atual
                        $name = uniqid(date('HisYmd'));

                        // Recupera a extensão do arquivo
                        $extension = $request->allFiles()['galeria'][$i]->extension();

                        // Define finalmente o nome
                        $imagem_galeria = "{$name}.{$extension}";
                        // Se informou o arquivo, retorna um boolean

                        // Salvar Imagem Banco de Dados
                        $upload_galeria = $request->allFiles()['galeria'][$i]->storeAs('upload/images_products/store_' . session('shops') . '/' . $producty->id, $imagem_galeria);

                        $product_gallery = new ProductsImagesGallery();
                        $product_gallery->product_id = $producty['id'];
                        $product_gallery->images =  $upload_galeria;
                        $product_gallery->type =  '';
                        $product_gallery->save();

                        unset($product_gallery);
                    }
                }
            }
        }
    }

    /** SALVAR VIDEO */
    public function ProductsVideosYoutube($request, $producty)
    {
        if (!is_null($request->video)) {
            /** SALVAR VIDEO */
            ProductsVideosYoutube::updateOrCreate(
                ['product_id' => $producty->id],
                [
                    'link' => $request->video,
                    'type' => $request->type_video
                ]
            );
        }
    }

    /** UPDATE CATEGORIAS */
    public function updateProductsCategoriesMm($producty, $request)
    {
        /** LIMPAR CATEGORIAS */
        ProductsCategoriesMm::where('product_id', $producty->id)->delete();

        /** SALVAR CATEGORIAS  */
        $categorias = ($request->categorias) ? json_decode($request->categorias) : null;

        if (!is_null($categorias)) {
            for ($i = 0; $i < count($categorias); $i++) {
                $number = explode("|", $categorias[$i]);

                $cat = new  ProductsCategoriesMm();
                $cat->product_id =  $producty->id;
                $cat->category_id = $number[0];
                $cat->subcategory_id = $number[1] ?? null;
                $cat->save();

                unset($cat);
            }
        }
    }

    /** UPDATA SUBCATEGORIAS */
    public function updateProductsSubCategoriesMm($producty, $request)
    {

        $subcategorias = ($request->subcategorias) ? json_decode($request->subcategorias) : null;

        if (!is_null($subcategorias)) {
            for ($i = 0; $i < count($subcategorias); $i++) {
                $number = explode("|", $subcategorias[$i]);

                $subc = new ProductsCategoriesMm();
                $subc->product_id =  $producty->id;
                $subc->category_id = $number[0];
                $subc->subcategory_id = $number[1];
                $subc->save();

                unset($subc);
            }
        }
    }

    /** UPDATE CATEGORIA JUNTYME */
    public function updateCategoriesJuntyme($producty, $request)
    {

        $dataArray = $request->all();

        $departament_id = array_key_exists("departament_juntyme", $dataArray) && $request->departament_juntyme > 0
            ? $request->departament_juntyme
            : null;
        $category_id =  array_key_exists("category_juntyme", $dataArray)
            ? $request->category_juntyme
            : null;
        $subcategory_id = array_key_exists("subcategory_juntyme", $dataArray)
            ? $request->subcategory_juntyme
            : null;

        if (!is_null($departament_id)) {

            $new = StoresDepartamentsProductsMm::where('product_id', $producty['id'])
                ->first();

            if (is_null($new)) {
                $new = new  StoresDepartamentsProductsMm();
                $new->store_id = 1;
                $new->shop_id = 1;
                $new->product_id =  $producty['id'];
                $new->departament_id = $departament_id;
                $new->category_id = $category_id;
                $new->subcategory_id = $subcategory_id;
                $new->save();
            } else {

                StoresDepartamentsProductsMm::where('product_id', $producty['id'])
                    ->update([
                        'departament_id' => $departament_id,
                        'category_id' => $category_id,
                        'subcategory_id' => $subcategory_id,
                    ]);
            }
        }
    }

    /** UPDATE ATRIBUTOS */
    public function updateProductsAttribute($producty, $request)
    {

        if (!empty($request['attributes']) && !is_null($request['attributes'])) {

            # Limpar Tabela de Atributos
            ProductsAttribute::where('product_id', $producty->id)
                ->forceDelete();

            foreach ($request['attributes'] as $item) {

                $attribute = new ProductsAttribute();
                $attribute->product_id = $producty->id;
                $attribute->attribute_type_id = $item['type'];
                $attribute->title = $item['name'];
                $attribute->order = $item['ordem'];
                $attribute->requere = $item['requered'];
                $attribute->variations = $item['values'];
                $attribute->save();

                unset($attribute);
            }
        }
    }

    /** ATUALIZAR PRECO ACHADDO */
    public function updatePriceAffiliateMM($request, $producty, $shop_id)
    {
        ## converter moeda
        $_source = array('.', ',');
        $_replace = array('', '.');

        $dataArray = $request->all();

        ## Verificar se existe Campo para atualização
        if (array_key_exists("price_afiliado", $dataArray) && array_key_exists("special_price_afiliado", $dataArray)) {


            $increment_value = ($request->increment_affiliate)
                ?  floatval(str_replace($_source, $_replace, $request->increment_affiliate))
                : null;

            if ($request->commission_affiliate > 0.00) {
                $commission_value = ($request->commission_affiliate) && ($request->commission_affiliate > 10)
                    ?  floatval(str_replace($_source, $_replace, $request->commission_affiliate))
                    : 10;
            } else {
                $commission_value = null;
            }


            if ($request->commission_affiliate > 0 &&  $request->commission_affiliate < 10) {
                flash('Valor de Comissão Afiliado mínimo é de 10%')->error()->important();
                $commission_aliexpress = 10;
            }

            $price = ($request->price_afiliado)
                ? floatval(str_replace($_source, $_replace, $request->price_afiliado))
                : null;
            $special_price = ($request->special_price_afiliado)
                ? floatval(str_replace($_source, $_replace, $request->special_price_afiliado))
                : null;

            ## Corrigir erro 0 preço original
            if ($special_price > 0.00) {
                $price = ($price > 0.00) ? $price : floatval(str_replace($_source, $_replace, $request->preco));
            }

            ## Corrigir Preço Especial
            $special_price = ($special_price < $price) ? $special_price : null;

            ## Verificar se existe
            $existAffilate = ProductsAffiliatesMm::where('product_id', $producty['id'])
                ->where('shop_id', $shop_id)->first();

            ## Fixar Preço
            $fixed = is_null($price) ? null : 1;

            if (!is_null($existAffilate)) {

                ## Atualizar
                ProductsAffiliatesMm::where('product_id', $producty['id'])
                    ->where('shop_id', $shop_id)
                    ->update([
                        'price' => $price,
                        'special_price' =>  $special_price,
                        'increment_value' => $increment_value,
                        'commission_value' => $commission_value,
                        'fixed' => $fixed,
                    ]);
            } else {

                ## Criar novo
                $newRelacion = new  ProductsAffiliatesMm();
                $newRelacion->product_id = $producty['id'];
                $newRelacion->shop_id = $shop_id;
                $newRelacion->status_id = 1;
                $newRelacion->price = $price;
                $newRelacion->special_price =  $special_price;
                $newRelacion->increment_value = $increment_value;
                $newRelacion->commission_value = $commission_value;
                $newRelacion->fixed = $fixed;
                $newRelacion->save();
            }
        }
    }

    /** ATUALIZAR PREÇOS ACHADDO MM */
    public function updatePriceAchaddoMM($request, $producty, $shop_id)
    {
        ## converter moeda
        $_source = array('.', ',');
        $_replace = array('', '.');

        $dataArray = $request->all();

        ## Verificar se existe Campo para atualização
        if (array_key_exists("price_achaddo", $dataArray) && array_key_exists("special_price_achaddo", $dataArray)) {

            $price = ($request->price_achaddo)
                ? floatval(str_replace($_source, $_replace, $request->price_achaddo))
                : null;
            $special_price = ($request->special_price_achaddo)
                ? floatval(str_replace($_source, $_replace, $request->special_price_achaddo))
                : null;

            ## Corrigir erro 0 preço original
            if ($special_price > 0.00) {
                $price = ($price > 0.00) ? $price : floatval(str_replace($_source, $_replace, $request->preco));
            }

            ## Corrigir Preço Especial
            $special_price = ($special_price < $price) ? $special_price : null;

            ## Verificar se existe
            $existAchaddo = ProductsMarketplaceAchaddoMm::where('product_id', $producty['id'])
                ->where('shop_id', $shop_id)->first();

            ## Fixar Preço
            $fixed = is_null($price) ? null : 1;

            if (!is_null($existAchaddo)) {

                ## Atualizar
                ProductsMarketplaceAchaddoMm::where('product_id', $producty['id'])
                    ->where('shop_id', $shop_id)
                    ->update([
                        'price' => $price,
                        'special_price' =>  $special_price,
                        'fixed' =>  $fixed
                    ]);
            } else {

                ## Criar novo
                $newRelacion = new  ProductsMarketplaceAchaddoMm();
                $newRelacion->product_id = $producty['id'];
                $newRelacion->shop_id = $shop_id;
                $newRelacion->status_id = 1;
                $newRelacion->price = $price;
                $newRelacion->special_price =  $special_price;
                $newRelacion->fixed =  $fixed;
                $newRelacion->save();
            }
        }
    }



    /** ATUALIZAR PREÇOS REDIRECTS MM */
    public function updatePriceRedirectMM($request, $producty, $shop_id)
    {
        # converter moeda
        $_source = array('.', ',');
        $_replace = array('', '.');

        $dataArray = $request->all();

        ## Verificar se existe Campo para atualização
        if (array_key_exists("price_redirect", $dataArray) && array_key_exists("special_price_redirect", $dataArray)) {

            $price = ($request->price_redirect)
                ? floatval(str_replace($_source, $_replace, $request->price_redirect))
                : null;
            $special_price = ($request->special_price_redirect)
                ? floatval(str_replace($_source, $_replace, $request->special_price_redirect))
                : null;

            ## Corrigir erro 0 preço original
            if ($special_price > 0.00) {
                $price = ($price > 0.00) ? $price : floatval(str_replace($_source, $_replace, $request->preco));
            }

            ## Corrigir Preço Especial
            $special_price = ($special_price < $price) ? $special_price : null;

            ## Verificar se existe
            $existRedirect = ProductsMarketplaceRedirectMm::where('product_id', $producty['id'])
                ->where('shop_id', $shop_id)->first();

            ## Fixar Preço
            $fixed = is_null($price) ? null : 1;

            if (!is_null($existRedirect)) {

                ## Atualizar
                ProductsMarketplaceRedirectMm::where('product_id', $producty['id'])
                    ->where('shop_id', $shop_id)
                    ->update([
                        'price' => $price,
                        'special_price' =>  $special_price,
                        'fixed' => $fixed
                    ]);
            } else {

                ## Criar novo
                $newRelacion = new  ProductsMarketplaceRedirectMm();
                $newRelacion->product_id = $producty['id'];
                $newRelacion->shop_id = $shop_id;
                $newRelacion->status_id = 1;
                $newRelacion->price = $price;
                $newRelacion->special_price =  $special_price;
                $newRelacion->fixed = $fixed;
                $newRelacion->save();
            }
        }
    }

    /** ATUALIZAR PREÇOS DROPIFY MM */
    public function updatePriceDropifyMM($request, $producty, $shop_id)
    {

        ## converter moeda
        $_source = array('.', ',');
        $_replace = array('', '.');

        $dataArray = $request->all();

        ## Verificar se existe Campo para atualização
        if (array_key_exists("price_dropify", $dataArray) && array_key_exists("special_price_dropify", $dataArray)) {

            $price = ($request->price_dropify)
                ? floatval(str_replace($_source, $_replace, $request->price_dropify))
                : null;
            $special_price = ($request->special_price_dropify)
                ? floatval(str_replace($_source, $_replace, $request->special_price_dropify))
                : null;

            ## Corrigir erro 0 preço original
            if ($special_price > 0.00) {
                $price = ($price > 0.00) ? $price : floatval(str_replace($_source, $_replace, $request->preco));
            }

            ## Corrigir Preço Especial
            $special_price = ($special_price < $price) ? $special_price : null;

            ## Verificar se existe
            $existDropify = ProductsDropifyMm::where('product_id', $producty['id'])
                ->where('shop_id', $shop_id)->first();

            ## Fixar Preço
            $fixed = is_null($price) ? null : 1;

            if (!is_null($existDropify)) {

                ## Atualizar
                ProductsDropifyMm::where('product_id', $producty['id'])
                    ->where('shop_id', $shop_id)
                    ->update([
                        'price' => $price,
                        'special_price' =>  $special_price,
                        'fixed' => $fixed
                    ]);
            } else {

                ## Criar novo
                $newRelacion = new  ProductsDropifyMm();
                $newRelacion->product_id = $producty['id'];
                $newRelacion->shop_id = $shop_id;
                $newRelacion->status_id = 1;
                $newRelacion->price = $price;
                $newRelacion->special_price =  $special_price;
                $newRelacion->fixed =  $fixed;
                $newRelacion->save();

                unset($newRelacion);
            }
        }
    }

    /** ATUALIZAR PREÇOS DSLITE MM */
    public function updatePriceDsliteMM($request, $producty, $shop_id)
    {

        ## converter moeda
        $_source = array('.', ',');
        $_replace = array('', '.');

        $dataArray = $request->all();

        ## Verificar se existe Campo para atualização
        if (array_key_exists("price_dslite", $dataArray) && array_key_exists("special_price_dslite", $dataArray)) {

            $price = ($request->price_dslite)
                ? floatval(str_replace($_source, $_replace, $request->price_dslite))
                : null;
            $special_price = ($request->special_price_dslite)
                ? floatval(str_replace($_source, $_replace, $request->special_price_dslite))
                : null;

            ## Corrigir erro 0 preço original
            if ($special_price > 0.00) {
                $price = ($price > 0.00) ? $price : floatval(str_replace($_source, $_replace, $request->preco));
            }

            ## Corrigir Preço Especial
            $special_price = ($special_price < $price) ? $special_price : null;

            ## Verificar se existe
            $existDslite = ProductsDsliteMm::where('product_id', $producty['id'])
                ->where('shop_id', $shop_id)->first();

            ## Fixar Preço
            $fixed = is_null($price) ? null : 1;

            if (!is_null($existDslite)) {

                ## Atualizar
                ProductsDsliteMm::where('product_id', $producty['id'])
                    ->where('shop_id', $shop_id)
                    ->update([
                        'price' => $price,
                        'special_price' =>  $special_price,
                        'fixed' =>   $fixed
                    ]);
            } else {

                ## Criar novo
                $newRelacion = new  ProductsDsliteMm();
                $newRelacion->product_id = $producty['id'];
                $newRelacion->shop_id = $shop_id;
                $newRelacion->status_id = 1;
                $newRelacion->price = $price;
                $newRelacion->special_price =  $special_price;
                $newRelacion->fixed =  $fixed;
                $newRelacion->save();

                unset($newRelacion);
            }
        }
    }

    /** ATUALIZAR PREÇOS BLING MM */
    public function updatePriceBlingMM($request, $producty, $shop_id)
    {

        ## converter moeda
        $_source = array('.', ',');
        $_replace = array('', '.');

        $dataArray = $request->all();

        ## Verificar se existe Campo para atualização
        if (array_key_exists("price_bling", $dataArray) && array_key_exists("special_price_bling", $dataArray)) {

            $price = ($request->price_bling)
                ? floatval(str_replace($_source, $_replace, $request->price_bling))
                : null;
            $special_price = ($request->special_price_bling)
                ? floatval(str_replace($_source, $_replace, $request->special_price_bling))
                : null;

            ## Corrigir Preço Especial
            $special_price = ($special_price < $price) ? $special_price : null;

            ## Verificar se existe
            $existBling = ProductsBlingMm::where('product_id', $producty['id'])
                ->where('shop_id', $shop_id)->first();

            ## Fixar Preços
            $fixed = is_null($price) ? null : 1;

            if (!is_null($existBling)) {

                ## Atualizar
                ProductsBlingMm::where('product_id', $producty['id'])
                    ->where('shop_id', $shop_id)
                    ->update([
                        'price' => $price,
                        'special_price' =>  $special_price,
                        'fixed' => $fixed
                    ]);
            } else {

                ## Criar novo
                $newRelacion = new  ProductsBlingMm();
                $newRelacion->product_id = $producty['id'];
                $newRelacion->shop_id = $shop_id;
                $newRelacion->status_id = 1;
                $newRelacion->price = $price;
                $newRelacion->special_price =  $special_price;
                $newRelacion->fixed =  $fixed;
                $newRelacion->save();
            }
        }
    }


    /** ATUALIZAR POCENTAGEM INDIVIDUAL ALIEXPRESS */
    public function updataPercentageAliexpress($request, $producty, $shop_id)
    {

        ## converter moeda
        $_source = array('.', ',');
        $_replace = array('', '.');

        $dataArray = $request->all();

        ## Verificar se existe Campo para atualização Porcentagem
        if (
            array_key_exists("commission_aliexpress", $dataArray)
            && array_key_exists("increment_aliexpress", $dataArray)
        ) {

            if (!is_null($request->commission_aliexpress)) {
                $commission_aliexpress = ($request->commission_aliexpress)
                    ? str_replace(
                        $_source,
                        $_replace,
                        $request->commission_aliexpress
                    )
                    : 0.00;

                if ($commission_aliexpress < 10) {
                    flash('Valor de Comissão mínimo é de 10%')->error()->important();
                    $commission_aliexpress = 10;
                } else if ($commission_aliexpress > 60) {

                    flash('Valor de Comissão máximo é de 60%')->error()->important();
                    $commission_aliexpress = 60;
                } else {
                    $commission_aliexpress =  $commission_aliexpress;
                }
            } else {
                $commission_aliexpress = null;
            }


            if (!is_null($request->increment_aliexpress)) {
                $increment_aliexpress = ($request->increment_aliexpress)
                    ? str_replace(
                        $_source,
                        $_replace,
                        $request->increment_aliexpress
                    )
                    : null;

                if ($increment_aliexpress < 40) {
                    flash('Valor de Incremento mínimo é de 40%')->error()->important();
                    $increment_aliexpress = 40;
                } else {
                    $increment_aliexpress = $increment_aliexpress;
                }
            } else {
                $increment_aliexpress = null;
            }
        }

        ## Verificar se existe Valor Fixo
        if (array_key_exists("price_fixed_aliexpress", $dataArray)) {

            if (!is_null($request->price_fixed_aliexpress) && $request->price_fixed_aliexpress > 0.00) {
                $price = $request->price_fixed_aliexpress;
            } else {
                $price = null;
            }

            if (!is_null($request->special_price_fixed_aliexpress && $request->special_price_fixed_aliexpress > 0.00)) {
                $special_price =  $request->special_price_fixed_aliexpress;
            } else {
                $special_price = null;
            }
        }

        ## Verificar se existe
        $existAliexpres =  ProductsAliexpressMm::where('product_id', $producty['id'])
            ->where('shop_id', $shop_id)->first();

        if (!is_null($existAliexpres)) {

            ## Atualizar
            ProductsAliexpressMm::where('product_id', $producty['id'])
                ->where('shop_id', $shop_id)
                ->update([
                    'price' => $price ? floatval(str_replace($_source, $_replace, $price)) : null,
                    'special_price' => $special_price ? floatval(str_replace($_source, $_replace, $special_price)) : null,
                    'increment_value' =>   $increment_aliexpress,
                    'commission_value' =>  $commission_aliexpress,
                    'fixed' =>  1
                ]);
        } else {

            ## Criar novo
            $newRelacion = new  ProductsAliexpressMm();
            $newRelacion->shop_id = $shop_id;
            $newRelacion->product_id = $producty['id'];
            $newRelacion->status_id = 1;
            $newRelacion->price = $price ? str_replace($_source, $_replace, $price) : null;
            $newRelacion->special_price = $special_price ? str_replace($_source, $_replace, $special_price) : null;
            $newRelacion->increment_value = $increment_aliexpress;
            $newRelacion->commission_value =  $commission_aliexpress;
            $newRelacion->fixed =  1;
            $newRelacion->save();
        }

        ## Reindexar Valores Aliexpress
        Product::where('id', $producty['id'])->update(['index_ali' => null]);
    }


    /** UPDATE PREÇOS PERSONALIZADOS */
    public function customPricing($request, $producty, $shop_id)
    {
        ## Atualizar Preços individual Afiliados MM
        $this->updatePriceAffiliateMM($request, $producty, $shop_id);

        ## Atualizar Preços Individual Achaddo MM
        $this->updatePriceAchaddoMM($request, $producty, $shop_id);

        ## Atualizar Preços Individual Redirect Marketplace MM
        $this->updatePriceRedirectMM($request, $producty, $shop_id);

        ## Atualizar Preços Individual Dropify
        $this->updatePriceDropifyMM($request, $producty, $shop_id);

        ## Atualizar Preços Individual Dslite
        $this->updatePriceDsliteMM($request, $producty, $shop_id);

        ## Atualizar Preços Individual Bling
        $this->updatePriceBlingMM($request, $producty, $shop_id);

        if (!is_null($producty['aliexpress_id'])) {
            ## Atualizar Porcentagem Aliexpress Individual MM
            $this->updataPercentageAliexpress($request, $producty, $shop_id);
        }
    }

    public function saveProductNew($request, $shop_id)
    {

        /** UPDATA DA IMAGEM PRINCIPAL */
        if (!empty($request->sku)) {
            $upload_image = $this->uploadImagePrincipal($request, $shop_id);
        }

        /** SALVAR PRODUTO */
        $dataProduct = [
            'upload_image' =>  $upload_image ?? null,
            'shopID' => $shop_id
        ];
        $producty = $this->saveUpdateProduct($dataProduct, $request);

        /** UPDATE GALERIA DE IMAGENS */
        if ($request->sku) {
            $this->updateGalleryImages($request, $producty);
        }

        /** UPDATE VIDEO */
        $this->ProductsVideosYoutube($request, $producty);

        /** UPDATE CATEGORIAS */
        $this->updateProductsCategoriesMm($producty, $request);

        /** UPDATE SUBCATEGORIAS */
        $this->updateProductsSubCategoriesMm($producty, $request);

        ## UPDATE CATEGORIA JUNTYME
        $this->updateCategoriesJuntyme($producty, $request);

        /** UPDATE ATRIBUTOS */
        if (!empty($request->sku)) {
            $this->updateProductsAttribute($producty, $request);
        }

        /** UPDATE PREÇOS PERSONALIZADOS */
        if (!empty($request->sku)) {
            $this->customPricing($request, $producty, $shop_id);
        }
    }


    /** RELACIONAR CATEGORIAS */
    public function  categoryRelaction($producty)
    {
        $category_relaction = ProductsCategoriesMm::where('product_id', '=', $producty->id)
            ->select('category_id', 'subcategory_id')
            ->get();
        $relation = [];
        foreach ($category_relaction as $item) {
            array_push($relation, [
                $item['category_id'],
                $item['subcategory_id']
            ]);
        }

        return   $relation;
    }

    /** CARREGAR ARRAY ATRIBUTOS VARIAÇÃO */
    public function attributesVariations($variation)
    {
        if (!is_null($variation)) {

            ## Formatar Array Variações
            $data = [];
            foreach ($variation as $item) {
                if (!is_null($item->attributes)) {
                    $attr = [];
                    foreach (json_decode($item->attributes) as $item) {
                        $att = [
                            'title' => $item->title,
                            'code' => $item->code,
                            'option' => $item->option,
                        ];
                        array_push($attr, $att);
                    }

                    $itens = [
                        'sku' =>    $item->sku,
                        'attributes' =>   $attr,
                        'image' => $item->image,
                        'image_thumbnail' =>  $item->image_thumbnail,
                        'gtin' =>  $item->gtin,
                        'stock' =>  $item->stock,
                        'price' =>  $item->price,
                    ];

                    array_push($data, $itens);
                }
            }

            return count($data) > 0 ? $data : null;
        }

        return null;
    }

    /** CARREGAR URL LOJA OU PRODUTO */
    public function productUrl($shop_id, $producty)
    {
        ## recuperar URL
        $site = Site::join('store', 'store.id', '=', 'sites.store_id')
            ->where('store.shop_id', $shop_id)
            ->where('sites.principal', 1)
            ->first();

        if (!is_null($site)) {

            $link = $site['url_secure']
                ? $site['url_secure']
                : $site['url_subdomain'];

            $product_id = $producty->id;
            $slug = $producty->slug;

            $url = $link . '/produto/' . $product_id . '/' . $slug;
        } else {

            $slug =  $producty->slug;
            $store = $producty->shop_slug;
            $url = config('multisite.site') . '/produto/' . $store . '/' . $slug;
        }
        return $url;
    }

    /** CARREGAR CONTEUDO PARA EDIÇÃO */
    public function editProduct($request, $product_id, $shop_id)
    {

        $producty = Product::where('products.shop_id', '=', $shop_id)
            ->where('products.id', '=', $product_id)
            ->leftJoin('products_condition', 'products_condition.id', '=', 'products.condition_id')
            ->leftJoin('products_provider', 'products_provider.id', '=', 'products.provider_id')
            ->leftJoin('products_brand', 'products_brand.id', '=', 'products.brand_id')
            ->leftJoin('products_videos_youtube', 'products_videos_youtube.product_id', '=', 'products.id')
            ->leftJoin('products_attributes_aliexpress', 'products_attributes_aliexpress.product_id', '=', 'products.id')
            ->leftJoin('products_attributes_variations', 'products_attributes_variations.product_id', '=', 'products.id')
            ->join('shops', 'shops.id', '=', 'products.shop_id')
            ->with('products_images_galleries', 'products_attribute', 'products_attribute_variation', 'category', 'subcategory')
            ->select(
                'products.*',
                'shops.slug as shop_slug',
                'products_condition.title as condition_title',
                'products_provider.title as provider_title',
                'products_brand.title as brand_title',
                'products_videos_youtube.link as video_link',
                'products_videos_youtube.type as video_type',
                'products_attributes_aliexpress.product_id as aliexpress_id',
                'products_attributes_variations.product_id as dropify_id',
                'products_attributes_aliexpress.price_min',
                'products_attributes_aliexpress.price_max',
                'products_attributes_aliexpress.discountedPrice_min',
                'products_attributes_aliexpress.discountedPrice_max',
                'products_attributes_aliexpress.properties',
                'products_attributes_aliexpress.variations',
            )
            ->first();

        /** Produto Relacionado a Loja */
        if (!is_null($producty)) {

            # Lista de Condições
            $data_array['condition'] = ProductsCondition::orderBy('title', 'asc')->get();

            # lista de Marcas
            $data_array['brandy'] = ProductsBrand::where('shop_id', '=', $shop_id)->orderBy('title', 'asc')->get();

            # Lista de Fabricante
            $data_array['providery'] = ProductsProvider::where('shop_id', '=', $shop_id)->orderBy('title', 'asc')->get();

            #Categorias
            $data_array['category'] = ProductsCategory::where('products_categories.shop_id', '=', $shop_id)
                ->with('products_subcategories')
                ->orderby('products_categories.id', 'asc', 'products_categories.title', 'asc')
                ->get();

            #subcategoria Principal
            $catrelation = $this->categoryRelaction($producty);
            $data_array['category_relaction'] = json_encode($catrelation, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            $data_array['catrelation'] = $catrelation;

            # Relação com os APP
            $data_array['relation'] = ProductsRelationMkp::where('product_id', '=', $producty->id)->first();

            $data_array['affiliate'] = ProductsAffiliatesMm::where('product_id', '=', $producty->id)->first();
            $data_array['achaddo'] = ProductsMarketplaceAchaddoMm::where('product_id', '=', $producty->id)->first();
            $data_array['redirect'] = ProductsMarketplaceRedirectMm::where('product_id', '=', $producty->id)->first();
            $data_array['dropify'] = ProductsDropifyMm::where('product_id', '=', $producty->id)->first();
            $data_array['dslite'] = ProductsDsliteMm::where('product_id', '=', $producty->id)->first();
            $data_array['bling'] = ProductsBlingMm::where('product_id', '=', $producty->id)->first();

            $data_array['aliexpress'] = ProductsAliexpressMm::where('product_id', '=', $producty->id)->first();
            $data_array['mlb'] = ProductsMarketplaceMlbMm::where('product_id', '=', $producty->id)->first();

            $data_array['gender'] = ProductsGender::all();
            $data_array['age_group'] = ProductsAgeGroup::orderby('title', 'asc')->get();

            $data_array['properties'] = (!empty($producty->properties)) ? collect(json_decode($producty->properties)) : null;
            $data_array['attributes_variations'] = $this->attributesVariations($producty->products_attribute_variation);

            ## Url do Site
            $data_array['url'] = $this->productUrl($shop_id, $producty);

            ## Carregar Relação Categoria Juntyme
            $data_array['product_cat_juntyme'] = StoresDepartamentsProductsMm::where('product_id', '=', $producty->id)
                ->leftJoin('stores_departaments', 'stores_departaments.id', '=', 'stores_departaments_products_mm.departament_id')
                ->leftJoin('stores_categories', 'stores_categories.id', '=', 'stores_departaments_products_mm.category_id')
                ->leftJoin('stores_subcategories', 'stores_subcategories.id', '=', 'stores_departaments_products_mm.subcategory_id')
                ->select(
                    'stores_departaments.title as departament_title',
                    'stores_departaments.id as departament_id',
                    'stores_categories.id as category_id',
                    'stores_categories.title as category_title',
                    'stores_subcategories.id as subcategory_id',
                    'stores_subcategories.title as subcategory_title'
                )
                ->first();

            ## Carregar Categorias Juntyme
            $data_array['categories_juntyme'] = StoresDepartament::select('id', 'title', 'slug')
                ->orderBy('title', 'asc')
                ->get();

            $data_array['producty'] = $producty;

            return $data_array;
        }

        return null;
    }
}
