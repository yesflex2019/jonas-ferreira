<?php

namespace App\Classes;

use App\Models\Product;
use App\Models\ProductsAttribute;
use App\Models\ProductsBrand;
use App\Models\ProductsCategoriesMm;
use App\Models\ProductsImagesGallery;
use App\Models\ProductsImportsMl;
use App\Models\ProductsImportsMlHold;
use App\Models\ProductsMarketplaceMlbMm;
use App\Models\ProductsRelationMkp;
use App\Models\ProductsSubcategory;
use App\Models\ProductsVariationsMlb;
use App\Models\Shop;
use App\Notifications\ShopImportMercadoLivre;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ImportMLClass
{

    /** CARREGAR PRODUTOS MERCADO LIVRE */
    public function productsMl($dataAcess, $meli)
    {
        $url = '/users/' . $dataAcess['id_loja'] . '/items/search';

        try {
            $response = $meli->get($url, array(
                'search_type' => 'scan',
                'access_token' => $dataAcess['access_token'],
                'scroll_id' => $dataAcess['scroll_id']
            ));
        } catch (Exception $e) {
            Log::info('"Exceção capturada:"' . $e->getMessage() . '\n');
        }

        ## Erro importação
        if ($response['httpCode'] != 200) {

            ## Deletar Atualização
            ProductsImportsMl::where('id_loja', '=', $dataAcess['id_loja'])
                ->where('type', '=', 'import')
                ->delete();
        }

        return $response;
    }

    /** FINALIZAÇÃO DA IMPORTAÇÃO */
    public function finalizacaoImportacao($shop_id, $dataAcess)
    {
        ## Notificação de Importação
        $shop =  Shop::find($shop_id);
        $shop->notify(new ShopImportMercadoLivre());

        ## Limpar Campo de Pesquisa
        ProductsImportsMl::where('id_loja', '=', $dataAcess['id_loja'])
            ->where('type', '=', 'import')
            ->delete();

        ## Liberar Lista de Espera
        ProductsImportsMlHold::where('shop_id', '=', $shop_id)
            ->orderBy('created_at', 'asc')
            ->limit(1)
            ->update([
                'status' => 1
            ]);
    }

    /** ATIVAR IMPORTAÇÃO SHOP */
    public function ativarImportacao($dataAcess, $dados, $l)
    {
        ## Contar Paginas
        $page = ($dados['page'])
            ? $dados['page'] + 1 + $l
            : 2 + $l;

        ProductsImportsMl::where('id_loja', '=', $dataAcess['id_loja'])
            ->where('type', '=', 'import')
            ->update([
                'status' => '2',
                'page' => $page
            ]);
    }

    /** VERIFICAR SE O PRODUTO JÁ ESTA CADASTRADO */
    public function produtoCadastrado($anuncio, $dataAcess)
    {
        $result = ProductsMarketplaceMlbMm::where('shop_id', '=',  $dataAcess['verific_shop_id'])
            ->where('mlb_id', '=', $anuncio)
            ->first();

        return  !is_null($result) ? $result : null;
    }


    /** CARREGAR ITENS */
    public function carregarItens($meli, $anuncio, $dataAcess)
    {
        $url = '/items/' . $anuncio;

        try {
            $response = $meli->get($url, array(
                'access_token' => $dataAcess['access_token']
            ));
        } catch (Exception $e) {
            Log::info('"Exceção capturada:"' . $e->getMessage() . '\n');
        }

        return $response;
    }

    /** DADOS DO ANUNCIO */
    public function dataBody($response)
    {
        $data = [
            'id' => $response->id, // "<br /> ID: "
            'title' => $response->title, /// "<br /> Titulo: "
            'category' => $response->category_id,
            'thumbnail' => $response->thumbnail, /// "<br /> Thumbnail:
            'price' => $response->price, // Preço
            'permalink' => $response->permalink, /// MLB_
            'listing_type_id' =>  $response->listing_type_id, //// listing_type
            'condition' =>  $response->condition,
            'pictures' => $response->pictures,
            'descriptions' => $response->descriptions,
            'warranty' => $response->warranty, /// Garantia
            'initial_quantity' => $response->initial_quantity, // quantidade
            'category_id' => $response->category_id, /// MLB_categories
            'official_store_id' => $response->official_store_id, // loja oficial
            'status' => $response->status, // Status
            'buying_mode' => $response->buying_mode  // produto a venda
        ];

        return $data;
    }

    /** VERIFICAR DIMENSÕES */
    public function produtosDimensoes($meli, $dataAcess, $dataProduct)
    {
        $url = '/categories/' . $dataProduct['category'] . '/shipping_preferences';

        try {
            $response1 = $meli->get($url, array('access_token' => $dataAcess['access_token']));
        } catch (Exception $e) {
            Log::info('"Exceção capturada:"' . $e->getMessage() . '\n');
        }

        $height = (!empty($response1['body']->dimensions->height)) ? ($response1['body']->dimensions->height / 100) : 0;
        $width = (!empty($response1['body']->dimensions->width)) ? ($response1['body']->dimensions->width / 100) : 0;
        $length = (!empty($response1['body']->dimensions->length)) ? ($response1['body']->dimensions->length / 100) : 0;
        $weight = (!empty($response1['body']->dimensions->weight)) ? ($response1['body']->dimensions->weight / 1000) : 0;

        $dimension = [
            'height' => $height,
            'width' => $width,
            'length' => $length,
            'weight' => $weight,
        ];

        return $dimension;
    }


    /** PRODUTOS DESCRIÇÃO */
    public function produtoDescricao($meli, $anuncio, $dataAcess)
    {
        ## Carregar Descrição Anuncio ML
        $url = '/items/' . $anuncio . '/description';

        try {
            $response = $meli->get($url, array('access_token' => $dataAcess['access_token']));
        } catch (Exception $e) {
            Log::info('"Exceção capturada:"' . $e->getMessage() . '\n');
        }

        ## Criar Array Descrição
        $produto = [
            "text_1" => $response['body']->error ?? '',
            "text_2" => $response['body']->plain_text ?? '',
        ];
        $list[] =  $produto;

        for ($i = 0; $i < count($list); $i++) {
            $description = $list[$i]['text_1'] . $list[$i]['text_2'];
        }

        return $description;
    }

    /** PRODUTOS ATRIBUTOS */
    public function produtosAtributos($attributes)
    {
        $attribute = '';
        for ($a = 0; $a < count($attributes); $a++) {
            if ($attributes[$a]->value_name) {
                $attribute .= $attributes[$a]->name . ' = ' . $attributes[$a]->value_name . '<br/> ';

                ## Recuperar GTIN
                if ($attributes[$a]->id == 'GTIN') {
                    $gtin = $attributes[$a]->value_name;
                }

                ## Recuperar Marca
                if ($attributes[$a]->id == 'BRAND') {
                    $brand = $attributes[$a]->value_name;
                }
            }
        }

        $dataAtribute = [
            'attribute' =>  $attribute ?? null,
            'gtin' => $gtin ?? null,
            'brand' => $brand ?? null,
        ];

        return $dataAtribute;
    }


    /** GARANTIA DO PRODUTO */
    public function garantiaProduto($warranty)
    {
        if ($warranty) {

            $result =  preg_replace("/[^0-9]/", "", $warranty);

            ## Garantia Meses
            if ($result > 12) {
                $result = round($result / 30, 0);
            }

            ## Garantia Minima
            $result = 0;
        } else {
            $result = 0;
        }

        return $result;
    }

    /** MARCA PRODUTO */
    public function produtoFabricate($shop_id, $attribute)
    {
        if ($attribute) {
            $brand = ProductsBrand::updateOrCreate([
                'shop_id' => $shop_id,
                'title' => $attribute
            ]);
        }

        return  $brand->id ?? null;
    }

    /** SALVAR PRODUTOS */
    public function salvarProduto($shop_id, $dataProduct, $condition, $sku, $dimension, $description, $attribute, $brandID = null, $warranty)
    {
        $product = new Product();
        $product->shop_id =  $shop_id;
        $product->status_id = 2;
        $product->title =  $dataProduct['title'];
        $product->condition_id = $condition;
        $product->sku = $sku;
        $product->price = $dataProduct['price'];
        $product->special_price = 0.00;
        $product->cost_price = 0.00;
        $product->quantity =  $dataProduct['initial_quantity'];
        $product->cross_docking = 0;
        $product->weight = $dimension['weight'];
        $product->height =  $dimension['height'];
        $product->width =  $dimension['width'];
        $product->length = $dimension['length'];
        $product->description = $description;
        $product->short_description = '';
        $product->note = $attribute['attribute'];
        $product->gtin = $attribute['gtin'] ?? null;
        $product->brand_id = $brandID ?? null;
        $product->provider_id = 0;
        $product->warranty = $warranty;
        $product->save();

        return $product->id;
    }

    /** RELACIONAR PRODUTOS */
    public function relacionarProdutos($productID)
    {
        $relation = new ProductsRelationMkp();
        $relation->product_id = $productID;
        $relation->mlb = 1;
        $relation->juntyme = 1;
        $relation->save();

        unset($relation);
    }

    /** CARREGAR IMAGENS */
    public function carregarImages($pictures, $productID)
    {
        for ($i = 0; $i < count($pictures); $i++) {

            ## Recuperar Imagem Principal
            if ($i == 0) {
                if (!is_null($pictures[$i]->secure_url)) {

                    Product::where('id', $productID)
                        ->update([
                            'image' => $pictures[$i]->secure_url,
                            'image_type' => 'link'
                        ]);
                }
            }

            if (!is_null($pictures[$i]->secure_url)) {
                $images = new ProductsImagesGallery();
                $images->product_id = $productID;
                $images->images = $pictures[$i]->secure_url;
                $images->type = 'link';
                $images->save();
                unset($images);
            }
        }
    }

    /** VARIAÇÕES MERCADO LIVRE */
    public function variacoesMercadoLivre($variations, $shop_id, $productID)
    {

        for ($v = 0; $v < count($variations); $v++) {
            for ($c = 0; $c < count($variations[$v]->attribute_combinations); $c++) {

                $variaty = new ProductsVariationsMlb();
                $variaty->shop_id = $shop_id;
                $variaty->product_id = $productID;
                $variaty->variation_id = $variations[$v]->attribute_combinations[$c]->id;
                $variaty->variation_name = $variations[$v]->attribute_combinations[$c]->name;
                $variaty->variation_value = $variations[$v]->attribute_combinations[$c]->value_name;
                $variaty->save();

                unset($variaty);
            }
        }
    }


    /** CRIAR VARIAÇÃO JUNTYME */
    public function criarVariacaoJuntyme($productID)
    {
        $variacao = ProductsVariationsMlb::select('variation_name', 'variation_value')
            ->where('product_id', '=', $productID)
            ->groupBy('variation_name', 'variation_value')
            ->get();

        $variacao_list =  $variacao;

        $limit = '';
        foreach ($variacao as $list4) {

            if ($list4->variation_name <> $limit) {
                $variation_values = '';
                foreach ($variacao_list as $list5) {
                    if ($list5->variation_name  == $list4->variation_name) {
                        $variation_values .= $list5->variation_value . ", ";
                        $limit = $list4->variation_name;
                    }
                }
                $attributes = new ProductsAttribute();
                $attributes->product_id =  $productID;
                $attributes->attribute_type_id = 2;
                $attributes->title = $list4->variation_name;
                $attributes->order = 2;
                $attributes->requere = 2;
                $attributes->variations = substr($variation_values, 0, -2);
                $attributes->save();

                unset($attributes);
            }
        }

        unset($variacao);
    }

    /** SALVAR ATRIBUTOS MERCADO LIVRE */
    public function salvarAtributosML($shop_id, $productID, $sku, $dataProduct)
    {
        $attribute = new ProductsMarketplaceMlbMm();
        $attribute->shop_id = $shop_id;
        $attribute->product_id = $productID;
        $attribute->status_update = 'new';
        $attribute->product_sku =  $sku;
        $attribute->mlb_status = $dataProduct['status'];
        $attribute->mlb_id = $dataProduct['id'];
        $attribute->mlb_listing_type = $dataProduct['listing_type_id'];
        $attribute->mlb_thumbnail = Str::of($dataProduct['thumbnail'])->replace('http://', 'https://');
        $attribute->mlb_categories = $dataProduct['category_id'];
        $attribute->mlb_buy_it_now = $dataProduct['buying_mode'];
        $attribute->mlb_id_site = $dataProduct['official_store_id'];
        $attribute->mlb_permalink = Str::of($dataProduct['permalink'])->replace('http://', 'https://');
        $attribute->mlb_price = $dataProduct['price'];
        $attribute->mlb_log_error = '';
        $attribute->save();

        unset($attribute);
    }

    /** RELACIONAR CATEGORIA JUNTYME */
    public function relacionarCategoriaJuntyme($shop_id, $productID)
    {
        $categoria = ProductsSubcategory::where([
            ['shop_id', '=',  $shop_id],
            ['title', '=', 'Default']
        ])->first();

        $catagory_mm = new ProductsCategoriesMm();
        $catagory_mm->product_id = $productID;
        $catagory_mm->category_id = $categoria->category_id;
        $catagory_mm->subcategory_id = $categoria->id;
        $catagory_mm->save();

        unset($catagory_mm);
    }

    /** RECOMECAR IMPORTAÇÃO */
    public function recomecarImportacao($dataAcess)
    {
        ProductsImportsMl::where('id_loja', '=', $dataAcess['id_loja'])
            ->where('type', '=', 'import')
            ->update(['status' => '1']);
    }


    /** CLASS IMPORTAÇÃO MERCADO LIVRE */
    public function importNew($dataAcess, $meli, $shop_id, $dados, $i, $l)
    {

        ## Carregar Anúncios Mercado Livre
        $response = $this->productsMl($dataAcess, $meli);

        ## Finallização Importação
        if (!isset($response['body']->scroll_id)) {

            $this->finalizacaoImportacao($shop_id, $dataAcess);
        } else {

            ## Ativar Importação
            $this->ativarImportacao($dataAcess, $dados[$i], $l);

            ## Abaixo pegamos a lista de IDs dos anúncios da conta
            $anuncios = $response['body']->results;

            ## Aqui verificamos se a lista de anúncios não veio vazia
            if (!empty($anuncios) && is_array($anuncios)) {

                ## Vai pegar informações de cada anúncio separadamente no ML
                foreach ($anuncios as $anuncio) {

                    ## VERIFICAR SE EXISTE
                    $exit = $this->produtoCadastrado($anuncio, $dataAcess);

                    if (is_null($exit)) {

                        ## Carregar os Itens
                        $response = $this->carregarItens($meli, $anuncio, $dataAcess);

                        ## Aqui pegamos as informações que queremos de cada anúncio
                        if (!empty($response['httpCode']) && $response['httpCode'] == 200) {

                            ## dados dos Anuncios
                            $dataProduct = $this->dataBody($response['body']);

                            ## Verificar Condição do Produto
                            $condition = ($dataProduct['condition'] == 'new')
                                ? 1
                                : 2;

                            ## Verificar Peso e Dimensões
                            $dimension = $this->produtosDimensoes($meli, $dataAcess, $dataProduct);

                            ## Localizar Descrição
                            $description = ($dataProduct['descriptions'])
                                ? $this->produtoDescricao($meli, $anuncio, $dataAcess)
                                : Null;

                            ## Carregar Atributos
                            $attribute = $this->produtosAtributos($response['body']->attributes);

                            ## Recuperar ou Criar SKU
                            $sku = preg_replace("/[^0-9]/", "", $dataProduct['id']);

                            ## Verificar se Existe Garantia
                            $warranty = $this->garantiaProduto($dataProduct['warranty']);

                            ## Criar Marca Fabricante
                            $brandID = $this->produtoFabricate($shop_id, $attribute['brand']);

                            ## Criar Produto
                            $productID = $this->salvarProduto(
                                $shop_id,
                                $dataProduct,
                                $condition,
                                $sku,
                                $dimension,
                                $description,
                                $attribute,
                                $brandID,
                                $warranty
                            );

                            ## Relacionar Produtos
                            $this->relacionarProdutos($productID);

                            ## Carregar Links da Imagens
                            $this->carregarImages($dataProduct['pictures'], $productID);

                            ## carregar Variaçoes Mercado Livre
                            $this->variacoesMercadoLivre($response['body']->variations, $shop_id, $productID);

                            ## Criar Variações Juntyme
                            $this->criarVariacaoJuntyme($productID);

                            ## Salvar Atributos Mercado Livre
                            $this->salvarAtributosML($shop_id, $productID, $sku, $dataProduct);

                            ## Realcionar Categorias JuntyMe
                            $this->relacionarCategoriaJuntyme($shop_id, $productID);
                        }
                    }
                }
            }
            ## Recomeçar Importação
            $this->recomecarImportacao($dataAcess);
        }
    }
}
