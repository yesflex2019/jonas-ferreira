<?php

namespace App\Classes;

use App\Models\MultinivelTransferPagarme;
use App\Models\ShopsPagarmeRecebedor;
use App\Models\ShopsTransferPagarme;
use Exception;

class PagarmeClass
{

    public function checkAmount($shop_id, $type)
    {

        ## Recuperar Tipo
        switch ($type) {
            case 'seller':
                $type = 'seller';
                break;
            case 'affiliate':
                $type = 'affiliate';

                break;
            case 'partner':
                $type = 'partner';
                break;
            default:
                $type = null;
        }

        if (!is_null($type)) {

            $pm_taxa_saque = config('juntyme.pm_taxa_saque');

            ## Recuperar ID Recebedor
            $recebedor = ShopsPagarmeRecebedor::where('shops_id', $shop_id)
                ->where('type', $type)->first();

            $pagarme = new \PagarMe\Client(config('juntyme.pm_chave_de_api'));

            if (!is_null($recebedor)) {
                try {
                    $recipientBalance = $pagarme->recipients()->getBalance([
                        'recipient_id' => $recebedor['recebedor_id'],
                    ]);
                } catch (Exception $e) {

                    flash('<i data-feather="meh"></i>Erro ao Consultar Saldo Disponível, tente mais tarde ou entre em contato com Nosso Suporte.')->warning();
                }

                ## Valor Disponivel para Saque
                $amount = $recipientBalance->available->amount ? $recipientBalance->available->amount : 0;
                $amount = $amount / 100;
                $amount = $amount - $pm_taxa_saque;
            } else {
                return null;
            }

            $amount = $amount > 0 ? $amount : 0.00;

            $data = [
                'amount' => $amount,
                'type' => $type
            ];

            return $data;
        }

        return null;
    }

    /** SALVAR SOLICITAÇÃO DE SAQUE PAGARME SHOPS */
    public function savePlunderShop($transfer, $request)
    {
        ## SHOP ID
        $shop_id = session('shops');

        ## Valor Solicitado
        $amount = $transfer->amount / 100;

        ## Salvar Solicitação
        $new_transfer = new ShopsTransferPagarme();
        $new_transfer->shop_id =  $shop_id;
        $new_transfer->id_recebedor = $transfer->source_id;
        $new_transfer->type_recebedor = $request->type;
        $new_transfer->type_transfer =  $transfer->type;
        $new_transfer->id_pagarme =  $transfer->id;
        $new_transfer->status = $transfer->status;
        $new_transfer->amount = $amount;
        $new_transfer->data =  json_encode($transfer, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $new_transfer->save();
    }



    /** SALVAR SOLICITAÇÃO DE SAQUE PAGARME MULTINIVEL */
    public function savePlunderMultinivel($transfer, $request)
    {
        ## SHOP ID
        $shop_id = session('shops');

        ## Valor Solicitado
        $amount = $transfer->amount / 100;

        ## Salvar Solicitação
        $new_transfer = new MultinivelTransferPagarme();
        $new_transfer->shop_id =  $shop_id;
        $new_transfer->id_recebedor = $transfer->source_id;
        $new_transfer->type_recebedor = $request->type;
        $new_transfer->id_pagarme =  $transfer->id;
        $new_transfer->status = $transfer->status;
        $new_transfer->amount = $amount;
        $new_transfer->data =  json_encode($transfer, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $new_transfer->save();
    }

    /** CONFIRMAR SOLICITAÇÃIO DE SAQUE */
    public function confirmCashWithdrawal($request, $shop_id)
    {
        ## Recuperar ID Recebedor
        $recebedor = ShopsPagarmeRecebedor::where('shops_id', $shop_id)
            ->where('type', $request->type)->first();

        $pagarme = new \PagarMe\Client(config('juntyme.pm_chave_de_api'));

        ## Recuperar Valores Pagarme
        if (!is_null($recebedor)) {

            try {
                $recipientBalance = $pagarme->recipients()->getBalance([
                    'recipient_id' => $recebedor['recebedor_id'],
                ]);
            } catch (Exception $e) {

                flash('<i data-feather="meh"></i>Erro ao Consultar Saldo Disponível, tente mais tarde ou entre em contato com Nosso Suporte.')->warning();
            }

            ## Valor Disponivel para Saque
            $amount = $recipientBalance->available->amount ? $recipientBalance->available->amount : 0;
            $amount = $amount;
        }

        /* converter moeda */
        $source = array('.', ',');
        $replace = array('', '.');

        $saque = ($request->valor_saque) ? intval(str_replace($source, $replace, $request->valor_saque) * 100) : null;

        ## Solicitar Saque
        if (!is_null($saque) && $saque > 1999 && $saque <= $amount) {

            ## Criar Transferência
            try {
                $transfer = $pagarme->transfers()->create([
                    'amount' => $saque,
                    'recipient_id' => $recebedor['recebedor_id']
                ]);
            } catch (Exception $e) {

                flash('<i data-feather="meh"></i> Houve um erro ao Solicitar o Saque tente mais tarde.')->warning();
            }

            ## Solictar Saque Vendedor
            if ($request->type == 'seller') {
                $this->savePlunderShop($transfer, $request,);
            }

            ## Solicitar Saque Afiliado
            if ($request->type == 'affiliate') {
                $this->savePlunderShop($transfer, $request);
            }

            ## Solicitar Saque Parceiro
            if ($request->type == 'partner') {
                $this->savePlunderMultinivel($transfer, $request);
            }


            return true;
        } else {

            return null;
        }
    }
}
