<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Vcoud\Mercadolibre\Meli;
use App\Classes\ImportMLClass;
use Illuminate\Support\Facades\DB;

class ApiMercadoLivre extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aml:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importação de Produtos Mercado Livre';

    private $ImportMLClass;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->ImportMLClass = new ImportMLClass();
    }

    /** DADOS CONEXÃO ML */
    private function accessMl($shop_id, $dados)
    {
        $verific = DB::table('shops_access_mercado_livre')
            ->where('shop_id', '=', $shop_id)
            ->first();

        ##  Dados para Carregame
        $data = [
            'id_loja' => $dados->id_loja,
            'access_token' => $verific->access_token,
            'refresh_token' => $verific->refresh_token,
            'scroll_id' => $dados->scroll_id,
            'access_token' => $dados->access_token,
            'verific_shop_id' => $verific->shop_id,
        ];

        return $data;
    }

    /** API MERCADO LIVRE */
    private function apiMl($dataAcess)
    {
        try {
            $meli = new Meli(
                getenv('ML_App_ID'),
                getenv('ML_Secret_Key'),
                $dataAcess['access_token'],
                $dataAcess['refresh_token']
            );
        } catch (Exception $e) {
            Log::info('"Exceção capturada:"' . $e->getMessage() . '\n');
        }

        return $meli;
    }

    /** Execute the console command. */
    public function handle()
    {

        ## Limite Processar Lista */
        $l_count = DB::table('products_imports_ml')
            ->where('type', '=', 'import')
            ->where('status', '=', 2)
            ->select(DB::raw('COUNT(products_imports_ml.status) as total'))
            ->first(['total']);

        $limitar = $l_count->total;

        if ($limitar <= 10) {

            ## 1 IMPORTAÇÃO PRODUTOS 30 Segundos */
            $dados = DB::table('products_imports_ml')
                ->where('status', '=', 1)
                ->where('type', '=', 'import')
                ->limit(10)
                ->get();

            if ($dados->count() > 0) {

                for ($i = 0; $i < $dados->count(); $i++) {

                    ## Conectar Mercado Livre
                    $shop_id = $dados[$i]->shop_id ?? '';

                    ## Conexão Acesso Mercado Livre
                    $dataAcess = $this->accessMl($shop_id, $dados[$i]);

                    ## API Mercado Livre
                    $meli = $this->apiMl($dataAcess);

                    for ($l = 0; $l < 5; $l++) {

                        ## Carregar Class Mercado Livre
                        $this->ImportMLClass->importNew($dataAcess, $meli, $shop_id, $dados, $i, $l);
                    }
                }
            }
        }
    }
}
