<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\ProductsImagesGalleryRemove;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UpLoadImagePrimary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:upLoadImagePrimary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'UpLoad Imagem Primaria do Produto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // $data = Carbon::now()->subDays(1);
        // $dia = $data->startOfDay()->toDateString();
        // ->where('products.updated_at', '<', $dia)

        $v = ProductsImagesGalleryRemove::count();

        if ($v == 0) {

            Product::where('products.image_type', '=', 'link')
                ->where('products.image', '!=', null)
                ->select(
                    'products.image',
                    'products.image_type',
                    'products.id',
                    'products.shop_id',
                    'products.slug'
                )
                ->orderBy('products.id', 'asc')
                ->chunk(500, function ($product) {
                    foreach ($product as $list) {

                        #Verificar Arquivo
                        if (!is_null($list->image)) {

                            $response = Http::get($list->image);
                            $response = $response->headers();
                            $type_image = $response['Content-Type'][0];

                            switch ($type_image) {
                                case 'image/png':
                                    $type = "png";
                                    break;
                                case 'image/jpg':
                                    $type = "jpg";
                                    break;
                                case 'image/jpeg':
                                    $type = "jpeg";
                                    break;
                                case 'image/gif':
                                    $type = "gif";
                                    break;
                                case 'image/webp':
                                    $type = "webp";
                                    break;
                                default:
                                    $type = null;
                                    break;
                            }

                            if (!is_null($type)) {
                                # Criar Diretorio
                                $dirtory = '/upload/images_products/store_' . $list->shop_id . '/' . $list->id . '/';
                                Storage::makeDirectory($dirtory);

                                # copiar Imagens
                                $path = $dirtory . $list->slug . '-' . $list->id . '.' . $type;

                                copy(str_replace(' ', '%20', $list->image), storage_path('app/public') . $path); //code...

                                #Atualizar Imagens
                                Product::updateOrCreate(
                                    ['id' => $list->id],
                                    [
                                        'image_type' => NULL,
                                        'image' => $path
                                    ]
                                );
                            } else {
                                ## Informar Erro
                                Log::debug('Erro Type de Image: ' . $type_image);
                            }
                        }
                    }
                });
        }
    }
}
