<?php

namespace App\Exports;

use App\Models\Product;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class ProductsExport implements FromQuery, Responsable, WithMapping, WithHeadings
{

    use Exportable;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /*** @return \Illuminate\Support\Collection  */
    public function query()
    {
        /// Recuperar ID Shop
        $shop_id = Session::get('shops');

        //     //// Tipo de Estoque
        switch ($this->data['catalog_stock']) {
            case 1:
                $stock = '>'; /// Somente produtos com Estoque
                break;
            case 2:
                $stock =  '=';  ///Somente produtos sem Estoque
                break;
            case 3:
                $stock = '>='; //// Todos os Produtos
                break;
        }

        switch ($this->data['catalog_camp']) {
            case 1:
                $data =   [
                    'products.id',
                    'products.sku',
                    'products.price',
                    'products.special_price',
                    'products.cost_price',
                    'products.quantity',
                    'products.cross_docking',
                ];
                break;
            case 2:
                $data = [
                    'products.id',
                    'products_status.title as status_title',
                    'products_condition.title as condition_title',
                    'products_provider.title as provider_title',
                    'products_brand.title as brand_title',
                    'products.title',
                    'products.sku',
                    'products.image',
                    'products.image_type',
                    'products.color',
                    'products.gtin',
                    'products.ncm',
                    'products.note',
                    'products.description',
                    'products.short_description',
                    'products.price',
                    'products.special_price',
                    'products.cost_price',
                    'products.quantity',
                    'products.cross_docking',
                    'products.warranty',
                    'products.weight',
                    'products.height',
                    'products.width',
                    'products.length',
                    'products_videos_youtube.link as video_link',
                ];
                break;
        }

        $ship =   Product::query()->where('products.quantity', $stock, 0)
            ->where('products_status.id', '<', '4')
            ->join('products_relation_mkp', 'products_relation_mkp.product_id', '=', 'products.id')
            ->where('products_relation_mkp.aliexpress', '=', null)
            ->join('products_status', 'products_status.id', '=', 'products.status_id')
            ->leftJoin('products_condition', 'products_condition.id', '=', 'products.condition_id')
            ->leftJoin('products_provider', 'products_provider.id', '=', 'products.provider_id')
            ->leftJoin('products_brand', 'products_brand.id', '=', 'products.brand_id')
            ->leftJoin('products_videos_youtube', 'products_videos_youtube.product_id', '=', 'products.id')
            ->with('products_images_galleries')
            ->select($data);

        return  $ship;
    }


    /** @var Invoice $invoice */
    public function map($products): array
    {
        /// Formartar Colulas
        $cost_price = ($products->cost_price == 0) ? 0.00 :  $products->cost_price;
        $image =  ($products->image_type) ? $products->image : env('APP_URL') . '/storage/' . $products->image;
        $warranty = ($products->warranty == 0) ? '' : $products->warranty;

        if ($this->data['catalog_camp'] == 1) {

            // Contudo array
            $data_maps = [
                (string) $products->id,
                $products->sku,
                number_format($products->price, 2, ',', ''),
                number_format($products->cost_price, 2, ',', ''),
                number_format($cost_price, 2, ',', ''),
                (string) $products->quantity,
                (string) $products->cross_docking

            ];
        } else {
            // Contudo array
            $data_maps = [
                (string) $products->id,
                $products->status_title,
                $products->condition_title,
                $products->provider_title,
                $products->brand_title,
                $products->title,
                $products->sku,
                $image,
                $products->color ?? '',
                $products->gtin,
                $products->ncm,
                $products->note,
                $products->description,
                $products->short_description,
                number_format($products->price, 2, ',', ''),
                number_format($products->cost_price, 2, ',', ''),
                number_format($cost_price, 2, ',', ''),
                (string) $products->quantity,
                (string) $products->cross_docking,
                $warranty,
                number_format($products->weight, 3, ',', ''),
                number_format($products->height, 2, ',', ''),
                number_format($products->width, 2, ',', ''),
                number_format($products->length, 2, ',', ''),
                $products->video_link,
            ];

            /// Carregar Gallery Image
            foreach ($products->products_images_galleries as $value) {
                $image_gallery =  ($value->type) ? $value->images : env('APP_URL') . '/storage/' . $value->images;
                array_push(
                    $data_maps,
                    $image_gallery
                );
            }
        }





        return $data_maps;
    }


    public function headings(): array
    {

        $dataTitulo = [];
        /// Colunas Principais
        if ($this->data['catalog_camp'] == 1) {

            array_push(
                $dataTitulo,
                'id',
                'sku',
                'preco_original',
                'preco_promocional',
                'preco_custo',
                'quantidade',
                'prazo_cross_docking'
            );
        } else {

            array_push(
                $dataTitulo,
                'id',
                'status',
                'condicao',
                'fornecedor',
                'marca',
                'titulo',
                'sku',
                'imagem_principal',
                'cor',
                'gtin',
                'ncm',
                'anotacoes',
                'descricao',
                'breve_descricao',
                'preco',
                'preco_promocional',
                'preco_custo',
                'quantidade',
                'prazo_cross_docking',
                'garantia',
                'peso',
                'altura',
                'largura',
                'comprimento',
                'link_video',
                'galeria_imgem_1',
                'galeria_imgem_2',
                'galeria_imgem_3',
                'galeria_imgem_4',
                'galeria_imgem_5',
                'galeria_imgem_6',
                'galeria_imgem_7',
                'galeria_imgem_8',
                'galeria_imgem_9',
                'galeria_imgem_10',
            );
        }

        $dataTitulo = $dataTitulo;

        return $dataTitulo;
    }
}
