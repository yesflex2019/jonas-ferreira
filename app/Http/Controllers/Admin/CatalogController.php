<?php

namespace App\Http\Controllers\Admin;

use App\Classes\ClearHtmlClass;
use Cache;
use App\Http\Controllers\Controller;
use App\Models\OrderShop;
use App\Models\OrdersShopsCommissionsSeller;
use App\Models\Product;
use App\Models\ProductsAttribute;
use App\Models\ProductsBrand;
use App\Models\ProductsCategoriesMm;
use App\Models\ProductsCategory;
use App\Models\ProductsCondition;
use App\Models\ProductsImagesGallery;
use App\Models\ProductsProvider;
use App\Models\ProductsRelationMkp;
use App\Models\ProductsSubcategory;
use App\Models\Shop;
use App\Models\ShopsAccessMercadoLivre;
use App\Models\ShopsAchaddoConfig;
use App\Models\ShopsAffiliatesConfig;
use App\Models\ShopsMagentoConfig;
use App\Models\ShopsPagarmeRecebedor;
use App\Models\ShopsWoocommerceConfig;
use App\Models\ShopsDsliteConfig;
use App\Models\ShopsBlingConfig;
use App\Models\User;
use App\Models\ProductsImportUpdate;
use App\Models\StoresCategory;
use App\Models\StoresSubcategory;
use App\Models\ProductsGender;
use App\Models\ProductsAgeGroup;
use Carbon\Carbon;
use DateTime;
use DateInterval;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use function GuzzleHttp\json_decode;
use App\Exports\ProductsExport;
use App\Http\Requests\Admin\NewProductRequest;
use App\Http\Requests\Admin\UpdateProductsRequest;
use App\Http\Requests\Admin\ProviderAddressRequest;
use App\Imports\NewProductImport;
use App\Imports\UpdateProductPriceStockImport;
use App\Models\ProductsImportNew;
use App\Models\StoresDepartament;
use App\Models\ShopsDropifyConfig;
use Exception;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;
use App\Classes\ErrorClass;
use App\Classes\EditProductsClass;
use App\Classes\DsliteClass;
use App\Classes\DropifyClass;
use App\Classes\BlingClass;
use App\Classes\InvoiceJuntymeClass;
use Encript;

class CatalogController extends Controller
{

    ## Verificação de Erros
    // if (\Request::ip() == '177.68.60.245') {
    //     dd($user);
    // }

    private $totalPage = 20;
    ## Periodo é minutos
    private   $period = 30;
    private $editProductClass;
    private $clearHtmlClass;
    private $dsliteClass;
    private $dropifyClass;
    private $blingClass;
    private $invoice;


    public function __construct()
    {
        /*** Autendicação Sitema */
        $this->middleware('auth:web');

        $this->editProductClass = new EditProductsClass();
        $this->clearHtmlClass = new ClearHtmlClass();
        $this->dsliteClass = new DsliteClass();
        $this->dropifyClass = new DropifyClass();
        $this->blingClass = new BlingClass();
        $this->invoice = new InvoiceJuntymeClass();
    }

    /** FILTRO BUSCA */
    public function filtroSearch($dateForm)
    {

        ## Filtro por Palavras
        if (!empty($dateForm['search'])) {
            $frase = explode(' ', $dateForm['search']);
            $retirar = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "de", "do", "Ante", "Após", "Até", "Com", "Contra", "De", "Desde", "Em", "Entre", "Para", "Per", "Perante", "Por", "Sem", "Sob", "Sobre", "Trás", "ante", "após", "até", "com", "contra", "de", "desde", "em", "entre", "para", "por", "perante", "sem", "sob", "sobre", "trás");

            $novovalor = '';
            for ($x = 0; $x < count($frase); $x++) {
                if (!in_array($frase[$x], $retirar)) {
                    $novovalor = $novovalor . $frase[$x] . '%';
                }
            }

            $formSearch = $novovalor;
        } else {
            $formSearch = NULL;
        }

        return $formSearch;
    }


    /**  PAGINA HOME PAINEL VENDEDOR */
    public function index(Request $request)
    {
        ## SHOP ID
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $dateForm = $request->except('_token');

        $formSearch  = $this->filtroSearch($dateForm);

        $products = Product::where('products.shop_id', $shop_id)
            ->where('products.status_id', '<', '4')
            ->where(function ($query) use ($formSearch, $dateForm) {
                if (isset($formSearch) && !is_null($formSearch))
                    $query->where('products.title', 'ILIKE', '%' . $formSearch . '%')
                        ->orWhere('products.slug', 'ILIKE', '%' . $formSearch . '%')
                        ->orWhere('products.sku', 'ILIKE', '%' .  $formSearch . '%')
                        ->orWhere('products.id', 'ILIKE', '%' .  $formSearch . '%');
                if (isset($dateForm['type']))
                    $query->where('products.status_id', '=', $dateForm['type']);
                if (isset($dateForm['channel']) && $dateForm['channel'] == 'woocommerce')
                    $query->where('products_relation_mkp.woocommerce', '=', 1);
                if (isset($dateForm['channel']) &&  $dateForm['channel'] == 'magento')
                    $query->where('products_relation_mkp.magento', '=', 1);
                if (isset($dateForm['channel']) && $dateForm['channel'] == 'mlb')
                    $query->where('products_relation_mkp.mlb', '=', 1);
                if (isset($dateForm['channel']) && $dateForm['channel'] == 'aliexpress')
                    $query->where('products_relation_mkp.aliexpress', '=', 1);
                if (isset($dateForm['channel']) && $dateForm['channel'] == 'dropify')
                    $query->where('products_relation_mkp.dropify', '=', 1);
                if (isset($dateForm['channel']) && $dateForm['channel'] == 'dslite')
                    $query->where('products_relation_mkp.dslite', '=', 1);
                if (isset($dateForm['channel']) && $dateForm['channel'] == 'direct')
                    $query->where('products_relation_mkp.woocommerce', '=', null)
                        ->where('products_relation_mkp.magento', '=',  null)
                        ->where('products_relation_mkp.mlb', '=',  null)
                        ->where('products_relation_mkp.aliexpress', '=', null)
                        ->where('products_relation_mkp.dropify', '=', null)
                        ->where('products_relation_mkp.dslite', '=',  null);
            })
            ->join('products_relation_mkp', 'products.id', '=', 'products_relation_mkp.product_id')
            ->leftJoin('stores_departaments_products_mm', 'stores_departaments_products_mm.product_id', '=', 'products.id')
            ->select(
                'products.id',
                'products.shop_id',
                'products.title',
                'products.sku',
                'products.price',
                'products.special_price',
                'products.quantity',
                'products.image',
                'products.image_type',
                'products.status_id',
                'products.approved',
                'products_relation_mkp.juntyme',
                'products_relation_mkp.afiliados',
                'products_relation_mkp.achaddo',
                'products_relation_mkp.redirect',
                'products_relation_mkp.woocommerce',
                'products_relation_mkp.magento',
                'products_relation_mkp.mlb',
                'products_relation_mkp.aliexpress',
                'products_relation_mkp.dropify',
                'products_relation_mkp.dslite',
                'products_relation_mkp.bling',
                'stores_departaments_products_mm.product_id as category_juntyme',

            )
            ->orderBy('category_juntyme', 'desc')
            ->orderBy('quantity', 'desc')
            ->orderBy('id', 'desc')
            ->paginate($this->totalPage)
            ->fragment('id');

        $lib_produts = ($dateForm) ? 1 : $products->all();

        $verific_ml = ShopsAccessMercadoLivre::where('shop_id', '=', $shop_id)->first();
        $verific_achaddo = ShopsAchaddoConfig::where('shop_id', '=', $shop_id)->first();
        $verific_afiliados = ShopsAffiliatesConfig::where('shop_id', '=', $shop_id)->first();
        $verific_woocommerce = ShopsWoocommerceConfig::where('shop_id', '=', $shop_id)->first();
        $verific_magento = ShopsMagentoConfig::where('shop_id', '=', $shop_id)->first();
        $verific_dropify = ShopsDropifyConfig::where('shop_id', '=', $shop_id)->first();
        $verific_dslite = ShopsDsliteConfig::where('shop_id', '=', $shop_id)->first();
        $verific_bling = ShopsBlingConfig::where('shop_id', '=', $shop_id)->first();


        return view('backend.pages.catalogs.listCatalog', compact(
            'verific_ml',
            'products',
            'lib_produts',
            'verific_achaddo',
            'verific_afiliados',
            'verific_woocommerce',
            'verific_magento',
            'verific_dropify',
            'verific_dslite',
            'verific_bling',
            'dateForm'
        ));
    }

    /** SALDO DISPONIVEL */
    public function balanceAvailableBackend()
    {
        /** ID Shop */
        $shop_id = session('shops');

        /** Valor Disponível */
        $sales_available = OrdersShopsCommissionsSeller::where('seller_shop_id', '=', $shop_id)
            ->where('commission_type_id', '=', 4)
            ->where('orders_status_id', '=', 1)
            ->where('payment', '!=', 'pagarme')
            ->sum('vlr_total');

        // Formatar Número
        $data = number_format($sales_available, 2, '.', '');

        $response['success'] = true;
        $response['message'] = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /** VALOR A RECEBER */
    public function amountReceivedBackend()
    {
        /** ID Shop */
        $shop_id = session('shops');

        // Localizar ID Recebedor

        $recebedor = ShopsPagarmeRecebedor::where('shops_id', '=', $shop_id)
            ->where('type', '=', 'seller')
            ->first();

        if (!is_null($recebedor)) {
            $recebedor = $recebedor['recebedor_id'];

            /// API Pagarme
            $pagarme = new \PagarMe\Client(config('juntyme.pm_chave_de_api'));

            ////Carregar Valores Pagarme
            try {
                $recipientBalance = $pagarme->recipients()->getBalance([
                    'recipient_id' => $recebedor,
                ]);

                //// Resposta
                $balance = $recipientBalance->available->amount / 100;
            } catch (Exception $e) {

                //// Erro no Calculo
                $balance = 0;
            }
        } else {
            //// Erro no Calculo
            $balance = 0;
        }


        /** Valor a Receber*/
        $sale_receivable = OrdersShopsCommissionsSeller::where('seller_shop_id', '=', $shop_id)
            ->where('commission_type_id', '=', 4)
            ->where('orders_status_id', '=', 1)
            ->where('payment', '!=', 'pagarme')
            ->sum('vlr_total');

        // Formatar Número
        $data = number_format($balance + $sale_receivable, 2, '.', '');

        $response['success'] = true;
        $response['message'] = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /** REEMBOLSO PENDENTE */
    public function pendingRefundBackend()
    {
        /** ID Shop */
        $shop_id = session('shops');

        /** Valor Reembolso*/
        $sales_refund = OrdersShopsCommissionsSeller::where('seller_shop_id', '=', $shop_id)
            ->where('commission_type_id', '=', 7)
            ->where('orders_status_id', '=', 1)
            ->sum('vlr_total');

        // Formatar Número
        $data = number_format($sales_refund, 2, '.', '');

        $response['success'] = true;
        $response['message'] = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /** Verificar se é Dia Util */
    public function workingDays()
    {
        $date = new DateTime();
        $date->add(new DateInterval('P1D'));
        $date = $date->getTimestamp();

        $dia_semana = date("w",  $date);

        // domingo = 0;
        // sábado = 6;
        // verifica sábado e domingo
        if ($dia_semana == 0) {
            $date = new DateTime();
            $date->add(new DateInterval('P2D'));
            $date_final = $date->getTimestamp() * 1000;
        } elseif ($dia_semana == 6) {

            //// Sabado
            $date = new DateTime();
            $date->add(new DateInterval('P3D'));
            $date_final = $date->getTimestamp() * 1000;
        } else {

            //// Dias Uteis
            $date_final =   $date * 1000;
        }

        return $date_final;
    }

    /** VALOR DE ANTECIPAÇÃO */
    public function advanceValueBackend()
    {

        /** ID Shop */
        $shop_id = session('shops');

        // Localizar ID Recebedor
        $recebedor = ShopsPagarmeRecebedor::where('shops_id', '=', $shop_id)
            ->where('type', '=', 'seller')
            ->first();


        if (!is_null($recebedor)) {
            $recebedor = $recebedor['recebedor_id'];

            /// Carregar API Pagarme
            $pagarme = new \PagarMe\Client(config('juntyme.pm_chave_de_api'));

            // Carregar Valores Pagarme
            try {
                $anticipationLimits = $pagarme->bulkAnticipations()->getLimits([
                    'recipient_id' => $recebedor['recebedor_id'],
                    'payment_date' => $this->workingDays(),
                    'timeframe' => 'end'
                ]);

                // Construir Array
                $data =  $anticipationLimits->maximum->amount / 100;
            } catch (Exception $e) {

                //// Erro no Calculo
                $data =  0;
            }
        } else {
            //// Erro no Calculo
            $data =  0;
        }



        // Formatar Numero
        $data = number_format($data, 2, '.', '');

        $response['success'] = true;
        $response['message'] = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /*** GRAFICO PEDIDOS GERADOS */
    public function reportRequestGenerated($shop_id)
    {
        $ctxLabel = [];
        $ctxData1 = [];
        // Dias no mês
        $funcao = new DateTime(date('Y-m'));
        $numDias = $funcao->format('t');


        $max = 0;
        // Com essa expressão no for é possível gerar datas do mês atual até o inicio do ano
        for ($i = 1; $i <= $numDias; $i++) {

            $ctxLabel[] .= $i;

            $data = Carbon::today()->days($i);
            $inicio = $data->startOfDay()->toDateString();

            $name_cache = "reportRequestGenerated::OrdersShopsCommissionsSeller_" . $shop_id . '_' . $inicio . '_' . config('app.env');
            $vlor  = Cache::remember(
                $name_cache,
                $this->period,
                function () use ($shop_id, $inicio) {
                    return  OrdersShopsCommissionsSeller::where('seller_shop_id', '=', $shop_id)
                        ->where('created_at', 'LIKE', $inicio . '%')
                        ->sum('vlr_total');
                }
            );
            unset($name_cache);


            $max = ($max > $vlor) ? $max : $vlor;
            $ctxData1[] .= number_format($vlor, 2, '.', '');
        }
        $max = $max * 2;
        $max = ($max > 1000) ? $max : 1000;

        $data = [
            'ctxLabel' => $ctxLabel,
            'ctxData1' => $ctxData1,
            'max' => $max
        ];

        return $data;
    }

    /** GRAFICO PEDIDOS GERADOS PAGOS */
    public function reportRequestGeneratedPaid($shop_id)
    {
        $ctxLabel = [];
        $ctxData1 = [];
        // Dias no mês
        $funcao = new DateTime(date('Y-m'));
        $numDias = $funcao->format('t');

        $data = Carbon::today()->subMonths(1);

        // Com essa expressão no for é possível gerar datas do mês atual até o inicio do ano
        for ($i = 1; $i <= $numDias; $i++) {

            $ctxLabel[] .= $i;

            $data = Carbon::today()->days($i);
            $inicio = $data->startOfDay()->toDateString();

            $name_cache = "reportRequestGeneratedPaid::OrdersShopsCommissionsSeller_" . $shop_id . '_' . $inicio . '_' . config('app.env');
            $vlor  = Cache::remember(
                $name_cache,
                $this->period,
                function () use ($shop_id, $inicio) {
                    return  OrdersShopsCommissionsSeller::where('orders_shops_commissions_seller.seller_shop_id', '=', $shop_id)
                        ->where('orders_shops_commissions_seller.created_at', 'LIKE', $inicio . '%')
                        ->join('order_shop', 'order_shop.id', '=', 'orders_shops_commissions_seller.orders_id')
                        ->where('order_shop.orders_status_id', '=', 3)
                        ->orwhere('order_shop.orders_status_id', '=', 4)
                        ->sum('orders_shops_commissions_seller.vlr_total');
                }
            );
            unset($name_cache);


            $ctxData1[] .= number_format($vlor, 2, '.', '');
        }

        $data = [
            'ctxLabel' => $ctxLabel,
            'ctxData1' => $ctxData1
        ];

        return $data;
    }

    /** VERIFICAR CONTA BANCÁRIA */
    public function verificBank()
    {
        /** ID Shop */
        $shop_id = session('shops');

        $bank = ShopsPagarmeRecebedor::where('shops_id', $shop_id)
            ->where('type', 'seller')->first();

        return $bank;
    }

    /*** PAGINA PRINCIPAL */
    public function admin()
    {
        if (session('user_status') > 3) {
            /// Conta Pausada
            return redirect()->route('shop.accountPaused');
        }

        /** ID Shop */
        $shop_id = session('shops');

        ## Verificar Faturas
        $this->invoice->index($shop_id);

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $funcao = new DateTime(date('Y-m-d'));

        $numDias = $funcao->format('Y-m-d');
        $numMes = $funcao->format('Y-m');

        /// Carregar Ultimos 30  Dias
        $sub30data =  $funcao;
        $sub30data->sub(new DateInterval('P30D'));

        // dd($numDias);
        $name_cache = "backendAdmin::OrdersShopsCommissionsSeller_" . $shop_id . '_' . config('app.env');
        $ordes_list  = Cache::remember(
            $name_cache,
            $this->period,
            function () use ($shop_id) {
                return  OrdersShopsCommissionsSeller::where('orders_shops_commissions_seller.seller_shop_id', '=', $shop_id)
                    ->join('order_shop', 'order_shop.id', '=', 'orders_shops_commissions_seller.orders_id')
                    ->where('order_shop.orders_status_id', '>=', '3')
                    ->where('order_shop.orders_status_id', '<=', '4')
                    ->get();
            }
        );

        /// Vendas do Mes
        $sales_month = $ordes_list->where('orders_shops_commissions_seller.created_at', 'LIKE', $numMes . '%')
            ->sum('orders_shops_commissions_seller.vlr_total');

        /// Vendas do Dia
        $sales_day = $ordes_list->where('orders_shops_commissions_seller.created_at', 'LIKE', $numDias . '%')
            ->sum('orders_shops_commissions_seller.vlr_total');


        // Pedidos Recemtes
        $recent_orders =  OrderShop::where('order_shop.origin_shop_id', '=', $shop_id)
            ->join('orders_status', 'order_shop.orders_status_id', '=', 'orders_status.id')
            ->join('order_shop_client', 'order_shop.orders_master_id', '=', 'order_shop_client.orders_master_id')
            ->where('order_shop.created_at', '>', $sub30data)
            ->select('order_shop.*', 'orders_status.title  as order_status', 'order_shop_client.name as order_cliente')
            ->orderBy('created_at', 'desc')
            ->limit(5)
            ->get();

        $selling_products = OrderShop::where('order_shop.origin_shop_id', '=', $shop_id)
            ->join('order_shop_itens', 'order_shop_itens.order_shop_id', '=', 'order_shop.id')
            ->join('products', 'products.id', '=', 'order_shop_itens.product_id')
            ->where('order_shop.created_at', '>', $sub30data)
            ->select('products.*', DB::raw('SUM(order_shop_itens.qtd) as total_qtd'))
            ->orderBy('total_qtd', 'desc')
            ->groupBy('products.id', 'order_shop_itens.product_id')
            ->limit(5)
            ->get();


        /// Relatório de Pedidos Gerados
        $chart1 = $this->reportRequestGenerated($shop_id);

        ## Relatórios de Pedidos Gerados Pagos
        $chart2 = $this->reportRequestGeneratedPaid($shop_id);

        ## Verificar Conta Bancaria
        $back = $this->verificBank();
        if (is_null($back)) {
            flash('<i data-feather="alert-octagon"></i> Sua Conta Digital não esta Configurada Corretamente, e Necessário que esteja configurada corretamente para seus recebimento. <a href=' . route('financial.bankAccount') . '
                                        class="nav-sub-link mg-t-10"> Dados Bancários >></a> ')->error()->important();
        }

        return view('backend.home', compact(
            'sales_month',
            'sales_day',
            'chart1',
            'chart2',
            'recent_orders',
            'selling_products',
            'shop_id'
        ));
    }




    /** PAGINA LIXEIRA PRODUTOS */
    public function productTrash()
    {
        $shop_id = session('shops');

        # Verificar Cadastro Completo
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        //// Carregar Produtos Lixeira
        $products = Product::where('shop_id', $shop_id)
            ->where('status_id', '=', '4')
            ->paginate($this->totalPage);

        return view('backend.pages.catalogs.trashProduct', compact('products'));
    }


    /** PÁGINA NOVO PRODUTO  */
    public function productCreate(Request $request)
    {

        ## Shoop ID
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        ## Condições do Produtos
        $name_cache = "productCreate::OrdersShopsCommissionsSeller_" . $shop_id . '_' . config('app.env');
        $condition  = Cache::remember(
            $name_cache,
            $this->period,
            function () use ($shop_id) {
                return  ProductsCondition::get();
            }
        );


        $providery = ProductsProvider::where('shop_id', '=', $shop_id)->where('title', '!=', '')->orderby('title', 'asc')->get();
        $brandy = ProductsBrand::where('shop_id', '=', $shop_id)->where('title', '!=', '')->orderby('title', 'asc')->get();

        ## Carregar Atributos
        $name_cache = "productCreate::ProductsCategory_" . $shop_id . '_' . config('app.env');
        $category  = Cache::remember(
            $name_cache,
            $this->period,
            function () use ($shop_id) {
                return  ProductsCategory::where('shop_id', '=', $shop_id)
                    ->with('products_subcategories')
                    ->orderby('id', 'asc')
                    ->get();
            }
        );

        ## Carregar Categorias Juntyme
        $categories_juntyme = StoresDepartament::select('id', 'title', 'slug')
            ->orderBy('title', 'asc')
            ->get();

        $gender = ProductsGender::all();
        $age_group = ProductsAgeGroup::orderby('title', 'asc')->get();

        return view('backend.pages.catalogs.createProduct', compact(
            'condition',
            'category',
            'providery',
            'brandy',
            'categories_juntyme',
            'gender',
            'age_group'

        ));
    }

    /*** UPOLOUD DE 1 IMAGEM */
    public function uploadImage($request)
    {
        /// Shop ID
        $shop_id =  session('shops');

        // Define o valor default para a variável que contém o nome da imagem
        $imagem_principal = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $request->imagem->extension();

            // Define finalmente o nome
            $imagem_principal = "{$name}.{$extension}";
            // Se informou o arquivo, retorna um boolean
            $path = 'app/public/upload/images_products/store_' . $shop_id . '/main';

            if (!file_exists(storage_path($path))) { // Cria pasta para o projeto, caso não já exista uma
                Storage::makeDirectory('upload/images_products/store_' . $shop_id . '/main');
            }

            // Salvar Imagem Banco de Dados
            $upload_image = $request->file("imagem")->storeAs('upload/images_products/store_' . $shop_id . '/main', $imagem_principal);
        }

        return $upload_image ?? null;
    }

    /** Função Salvar Produtos */
    public function newProduct($request, $dataProduct)
    {

        /** Verificar SKU */
        $verify_sku = Product::where('sku', $request->sku)->first();

        /* converter moeda */
        $source = array('.', ',');
        $replace = array('', '.');

        $producty = new Product();
        $producty->shop_id =  $dataProduct['shopID'];
        $producty->status_id = 2;
        $producty->condition_id = $request->tipo ?? null;
        $producty->title = $request->titulo ?? null;
        $producty->sku = (!is_null($verify_sku)) ? ($request->sku . ':' . date('s')) : $request->sku;
        $producty->image = $dataProduct['upload_image'];
        $producty->image_type = empty($request->image_duplicate) ? null : 'link';
        $producty->gtin = $request->gtin ?? null;
        $producty->ncm = $request->ncm ?? null;
        $producty->note = $request->note ?? null;
        $producty->description = $this->clearHtmlClass->clear2Html($request->descriptionEdit) ?? null;
        $producty->short_description = $this->clearHtmlClass->clear2Html($request->breveDescricao) ?? null;
        $producty->provider_id =   $request->fornecedor  ?? null;
        $producty->brand_id =   $request->marca ?? null;
        $producty->color = $request->cor  ?? null;
        $producty->price = ($request->preco) ? str_replace($source, $replace, $request->preco) : 0.00;
        $producty->special_price = ($request->preco_especial) ? str_replace($source, $replace, $request->preco_especial) : 0.00;
        $producty->cost_price = ($request->preco_custo) ?  str_replace($source, $replace, $request->preco_custo) : 0.00;
        $producty->quantity = ($request->estoque) ? str_replace($source, $replace, $request->estoque) : 0;
        $producty->cross_docking = ($request->cross_docking) ?  str_replace($source, $replace,  $request->cross_docking) : 0;
        $producty->warranty =  $request->garantia  ?? null;
        $producty->weight = ($request->peso) ? str_replace($source, $replace, $request->peso) : 0.000;
        $producty->height = ($request->altura) ? str_replace($source, $replace, $request->altura) : 0.00;
        $producty->width = ($request->largura) ? str_replace($source, $replace,  $request->largura) : 0.00;
        $producty->length = ($request->comprimento) ? str_replace($source, $replace,  $request->comprimento) : 0.00;
        $producty->size = ($request->size) ? $request->size : null;
        $producty->gender_id = ($request->gender) ? $request->gender : null;
        $producty->age_group_id = ($request->age_group) ? $request->age_group : null;
        $producty->save();

        return  $producty;
    }

    /** CJRIAR RELAÇÃP MKP */
    public function ProductsRelationMkp($producty)
    {
        $relation = new ProductsRelationMkp();
        $relation->product_id =  $producty->id;
        $relation->juntyme = 1;
        $relation->save();

        unset($relation);
    }

    /** UPLOAD DA GALERIA */
    public function ProductsImagesGallery($request, $producty)
    {
        /// ID SHOP
        $shop_id =  session('shops');

        if ($request->hasFile('galeria')) {
            for ($i = 0; $i < count($request->allFiles()['galeria']); $i++) {

                // Define o valor default para a variável que contém o nome da imagem
                $imagem_galeria = null;
                // Verifica se informou o arquivo e se é válido
                if ($request->allFiles()['galeria'][$i]->isValid()) {
                    // Define um aleatório para o arquivo baseado no timestamps atual
                    $name = uniqid(date('HisYmd'));
                    // Recupera a extensão do arquivo
                    $extension = $request->allFiles()['galeria'][$i]->extension();
                    // Define finalmente o nome
                    $imagem_galeria = "{$name}.{$extension}";
                    // Se informou o arquivo, retorna um boolean
                    $path = 'app/public/upload/images_products/store_' . $shop_id . '/';

                    if (!file_exists(storage_path($path))) { // Cria pasta para o projeto, caso não já exista uma
                        Storage::makeDirectory('upload/images_products/store_' . $shop_id);
                    }

                    // Salvar Imagem Banco de Dados
                    $upload_galeria = $request->allFiles()['galeria'][$i]->storeAs('upload/images_products/store_' . $shop_id . '/' . $producty->id, $imagem_galeria);

                    $product_gallery = new ProductsImagesGallery();
                    $product_gallery->product_id = $producty['id'];
                    $product_gallery->images =  $upload_galeria;
                    $product_gallery->save();

                    unset($product_gallery);
                }
            }
        }
    }

    /** SALVAR CATEGORIAS */
    public function ProductsCategoriesMm($request, $producty)
    {
        $categorias = json_decode($request['categorias']);

        for ($i = 0; $i < count($categorias); $i++) {

            $number = explode("|", $categorias[$i]);

            $category = new ProductsCategoriesMm();
            $category->product_id = $producty['id'];
            $category->category_id = $number[0];
            $category->subcategory_id = $number[1] ?? null;
            $category->save();

            unset($category);
        }
    }

    /** SALVAR SUBCAtEGORIAS */
    public function ProductsSubCategoriesMm($request, $producty)
    {
        $subcategorias = json_decode($request->subcategorias);

        for ($i = 0; $i < count($subcategorias); $i++) {

            $number = explode("|", $subcategorias[$i]);

            $subCategory = new ProductsCategoriesMm();
            $subCategory->product_id = $producty['id'];
            $subCategory->category_id = $number[0];
            $subCategory->subcategory_id = $number[1];
            $subCategory->save();

            unset($subCategory);
        }
    }

    /** SALVAR ATRIBUTOS */
    public function atributos($request, $producty)
    {
        if (!empty($request->attributes)) {

            foreach ($request['attributes'] as $item) {

                $attribute = new ProductsAttribute();
                $attribute->product_id = $producty->id;
                $attribute->attribute_type_id = $item['type'];
                $attribute->title = $item['name'];
                $attribute->order = $item['ordem'];
                $attribute->requere = $item['requered'];
                $attribute->variations = $item['values'];
                $attribute->save();

                unset($attribute);
            }
        }
    }
    /** SALVAR GALERIA DUPLICAÇÃO */
    public function ProductsImagesGalleryDuplicate($request, $producty)
    {

        foreach (json_decode($request->gallery_duplicate) as $item) {
            $product_gallery = new ProductsImagesGallery();
            $product_gallery->product_id = $producty['id'];
            $product_gallery->images = $item;
            $product_gallery->type = 'link';
            $product_gallery->save();
        }
    }


    /** SALVAR PRODUTO NOVO  */
    public function storeProduct(NewProductRequest $request)
    {
        ## Resposta Validate
        $shop_id =  session('shops');

        ## Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        ## UPOLOUD DE 1 IMAGEM
        if (empty($request->image_duplicate)) {
            $upload_image = $this->uploadImage($request);
        } else {
            ## Salvar duplicação
            $upload_image = $request->image_duplicate;
        }

        ## SALVAR PRODUTO
        $dataProduct = [
            'shopID' => $shop_id,
            'upload_image' => $upload_image
        ];
        $producty = $this->newProduct($request, $dataProduct);

        ## CRIAR RELAÇÃP MKP
        $this->ProductsRelationMkp($producty);

        ## UPLOAD DA GALERIA
        if (empty($request->image_duplicate)) {
            $this->ProductsImagesGallery($request, $producty);
        } else {
            ## Salvar Imagem Duplicação
            $this->ProductsImagesGalleryDuplicate($request, $producty);
        }
        ## IMAGENS DUPLICAÇÃO

        ## SALVAR CATEGORIAS
        $this->ProductsCategoriesMm($request, $producty);

        ## SALVAR SUBCAtEGORIAS
        $this->ProductsSubCategoriesMm($request, $producty);

        ## UPDATE CATEGORIA JUNTYME
        $this->editProductClass->updateCategoriesJuntyme($producty, $request);

        ## SALVAR ATRIBUTOS
        $this->atributos($request, $producty);

        ## SALVAR VIDEO
        $this->editProductClass->ProductsVideosYoutube($request, $producty);

        if (empty($request->image_duplicate)) {
            return redirect()->route('catalog.index');
        } else {

            flash('<i data-feather="thumbs-up"></i> Produto Duplicado com Sucesso.')->success();
            return redirect()->route('catalog.productEdit', ['id' => \Encript::encriptar($producty['id'])]);
        }
    }

    /** PAGINA CRIAR CATEGORIA */
    public function   storeCategory(Request $request)
    {
        /** Salvar Categoria */
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $category = new ProductsCategory();
        $category->shop_id = $shop_id;
        $category->title = $request->categoria;
        $category->save();

        unset($category);

        flash('<i data-feather="thumbs-up"></i> Categoria Incluida com Sucesso.')->success();
        return back();
    }

    /** LIXEIRA PRODUTOS */
    public function  trashProduct(Request $request)
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        Product::where('id', $request->id)
            ->where('shop_id', $shop_id)
            ->update(['status_id' => 4]);

        flash('<i data-feather="thumbs-up"></i> Produto Adicionado na Lixeira será excluido em 7 dias.')->success();
        return redirect()->route('catalog.index');
    }


    /** LIXEIRA PRODUTOS */
    public function  trashListProduct(Request $request)
    {
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $products = json_decode($request->list_array);

        for ($i = 0; $i < count($products); $i++) {
            Product::where('id', $products[$i])
                ->where('shop_id', $shop_id)
                ->update(['status_id' => 4]);
        }

        flash('<i data-feather="alert-octagon"></i> Produtos Adicionados na Lixeira serão excluidos 02:00hs. ')->error()->important();
        return back();
    }

    /** ALTERAR STATUS PRODUTOS */
    public function statusProduct(Request $request)
    {

        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $products = json_decode($request->list_array_status);

        for ($i = 0; $i < count($products); $i++) {
            Product::where('id', $products[$i])
                ->where('shop_id', $shop_id)
                ->where('price', '>', 0.00)
                ->where('quantity', '>', 0)
                ->update(['status_id' => $request->status_type]);
        }

        flash('<i data-feather="alert-octagon"></i> Status dos produtos atualizados com sucesso. Apenas produtos com Valor e Estoque podem ser alterados. ')->success();
        return back();
    }


    /** RESTAURAR PRODUTO */
    public function  restoreProduct(Request $request)
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $products = json_decode($request->list_array);

        for ($i = 0; $i < count($products); $i++) {
            Product::where('id', $products[$i])
                ->update(['status_id' => 2]);
        }

        flash('<i data-feather="alert-octagon"></i> Produtos Adicionados na Lixeira serão excluidos em 7 dias.')->error()->important();
        return back();
    }


    /** PAUSAR PRODUTO */
    public function  pauseProduct(Request $request)
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        Product::where('id', '=', $request->id)->update(['status_id' => 3]);

        flash('<i data-feather="meh"></i> Produto Pausado com Sucesso.')->warning();
        return redirect()->route('catalog.index');
    }


    /** Delete Categoria */
    public function  deleteCategory(Request $request)
    {
        $categoryID = Encript::desencriptar($request->id);

        if (is_null($categoryID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        ## Deletar Categoria
        ProductsCategory::where('id', '=', $categoryID)
            ->where('shop_id', $shop_id)
            ->delete();

        ## Deletar Subcategoria
        ProductsSubcategory::where('category_id', '=', $categoryID)
            ->where('shop_id', $shop_id)
            ->delete();

        ## Redirar Relação Produtos
        ProductsCategoriesMm::where('category_id', '=', $categoryID)
            ->delete();

        flash('<i data-feather="thumbs-up"></i> Categoria / Subcategorias Excluida com Sucesso.')->success();
        return back();
    }

    /** Delete SubCategoria */
    public function deleteSubcategory(Request $request)
    {
        $subcategoryID = Encript::desencriptar($request->id);

        if (is_null($subcategoryID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## ID Shop
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        ProductsSubcategory::where('id', '=', $subcategoryID)
            ->where('shop_id', $shop_id)
            ->delete();

        ## Redirar Relação Produtos
        ProductsCategoriesMm::where('subcategory_id', '=', $subcategoryID)
            ->delete();

        flash('<i data-feather="thumbs-up"></i> Subcategoria Excluida com Sucesso.')->success();
        return back();
    }


    /** DELETAR FORNECEDOR */
    public function deleteProvider(Request $request)
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $provider = ProductsProvider::find($request->id);
        $provider->delete();

        flash('<i data-feather="thumbs-up"></i> Subcategoria Excluida com Sucesso.')->success();
        return back();
    }

    /** DELETAR MARCA */
    public function deleteBrand(Request $request)
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $brand = ProductsBrand::find($request->id);
        $brand->delete();

        flash('<i data-feather="thumbs-up"></i> Marca Excluida com Sucesso.')->success();
        return back();
    }


    /** SALVAR SUBCATEGORIA */
    public function   storeSubcategory(Request $request)
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $subcategory = new ProductsSubcategory();
        $subcategory->shop_id = $shop_id;
        $subcategory->category_id = $request->cat_name;
        $subcategory->title = $request->subcat_name;
        $subcategory->save();

        unset($subcategory);

        flash('<i data-feather="thumbs-up"></i> Subcategoria Incluida com Sucesso.')->success();
        return back();
    }

    /** Salvar Fornecedor */
    public function   storeProvider(Request $request)
    {

        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }


        $provider = new ProductsProvider();
        $provider->shop_id = $shop_id;
        $provider->title = $request->fornecedor;
        $provider->save();

        unset($provider);

        flash('<i data-feather="thumbs-up"></i> Fornecedor Incluido com Sucesso.')->success();
        return back();
    }

    /*** EDITAR ENDEREÇO FORNECEDOR */
    public function storeProviderAddress(Request $request, $id)
    {

        $provider = ProductsProvider::where('id', $id)->first();

        return view(
            'backend.pages.catalogs.storeProviderAddress',
            compact('provider')
        );
    }

    /** SALVAR ENDEREÇO FORNECEDOR */
    public function storeProviderUpdate(ProviderAddressRequest $request)
    {

        ## Atualizar Fornecedor
        ProductsProvider::where('id', $request->provider_id)->update([
            'cross_docking' => $request->cross_docking,
            'street'  => $request->rua,
            'number'  => $request->numero,
            'complement'  => $request->complemento,
            'district'  => $request->bairro,
            'zip_code'  => $request->cep,
            'city'  => $request->cidade,
            'state'  => $request->uf,
            'country'  => 'br',
            'telephone'  => $request->telefone,
            'cell_phone'  => $request->celular,
        ]);

        flash('<i data-feather="thumbs-up"></i> Fornecedor Atualizado com Sucesso.')->success();
        return back();
    }

    /** Salvar Marca */
    public function   storeBrand(Request $request)
    {
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $brand = new ProductsBrand();
        $brand->shop_id = $shop_id;
        $brand->title = $request->marca;
        $brand->save();

        unset($brand);

        flash('<i data-feather="thumbs-up"></i> Marca Incluida com Sucesso.')->success();
        return back();
    }


    /** PAGINA EDITAR PRODUTOS */
    public function productEdit(Request $request)
    {
        $product_id = Encript::desencriptar($request['id']);

        if (is_null($product_id)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## ID Shop
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        ## Carrega Conteudo Class
        $data_array =  $this->editProductClass->editProduct($request, $product_id, $shop_id);

        if (!is_null($data_array)) {

            $producty = $data_array['producty'];
            $condition = $data_array['condition'];
            $category = $data_array['category'];
            $category_relaction = $data_array['category_relaction'];
            $providery = $data_array['providery'];
            $brandy = $data_array['brandy'];
            $mlb = $data_array['mlb'];
            $relation = $data_array['relation'];
            $achaddo = $data_array['achaddo'];
            $affiliate = $data_array['affiliate'];
            $redirect = $data_array['redirect'];
            $properties = $data_array['properties'];
            $aliexpress = $data_array['aliexpress'];
            $dropify = $data_array['dropify'];
            $dslite = $data_array['dslite'];
            $bling = $data_array['bling'];
            $categories_juntyme = $data_array['categories_juntyme'];
            $product_cat_juntyme = $data_array['product_cat_juntyme'];
            $attributes_variations = $data_array['attributes_variations'];
            $url = $data_array['url'];
            $gender = $data_array['gender'];
            $age_group = $data_array['age_group'];

            return view(
                'backend.pages.catalogs.editProduct',
                compact(
                    'producty',
                    'condition',
                    'category',
                    'category_relaction',
                    'providery',
                    'brandy',
                    'mlb',
                    'relation',
                    'achaddo',
                    'affiliate',
                    'redirect',
                    'properties',
                    'aliexpress',
                    'categories_juntyme',
                    'product_cat_juntyme',
                    'dropify',
                    'dslite',
                    'bling',
                    'attributes_variations',
                    'url',
                    'gender',
                    'age_group'
                )
            );
        }

        /** Produto não localizado */
        return redirect()->route('catalog.index');
    }

    /** CARREGAR ARRAY GALERIA DE IMAGENS */
    public function arrayImageDuplicate($galleries)
    {
        $data = [];
        foreach ($galleries as $item) {

            $image_gallery = $item->type ? $item->images : config('app.url') . '/storage/' . $item->images;

            array_push($data, $image_gallery);
        }

        $data = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $data;
    }

    /** PAGINA EDITAR PRODUTOS */
    public function productDuplicate(Request $request)
    {
        ## Dados Cripto
        $product_id = Encript::desencriptar($request['id']);
        if (is_null($product_id)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## ID Shop
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        ## Carrega Conteudo Class
        $data_array =  $this->editProductClass->editProduct($request, $product_id, $shop_id);

        if (!is_null($data_array)) {

            $producty = $data_array['producty'];
            $condition = $data_array['condition'];
            $category = $data_array['category'];
            $category_relaction = $data_array['category_relaction'];
            $providery = $data_array['providery'];
            $brandy = $data_array['brandy'];
            $mlb = $data_array['mlb'];
            $relation = $data_array['relation'];
            $achaddo = $data_array['achaddo'];
            $affiliate = $data_array['affiliate'];
            $redirect = $data_array['redirect'];
            $properties = $data_array['properties'];
            $aliexpress = $data_array['aliexpress'];
            $dropify = $data_array['dropify'];
            $dslite = $data_array['dslite'];
            $bling = $data_array['bling'];
            $categories_juntyme = $data_array['categories_juntyme'];
            $product_cat_juntyme = $data_array['product_cat_juntyme'];
            $attributes_variations = $data_array['attributes_variations'];
            $url = $data_array['url'];
            $gender = $data_array['gender'];
            $age_group = $data_array['age_group'];

            $duplicate = true;

            $galleryJson = $this->arrayImageDuplicate($producty->products_images_galleries);

            return view(
                'backend.pages.catalogs.productDuplicate',
                compact(
                    'producty',
                    'condition',
                    'category',
                    'category_relaction',
                    'providery',
                    'brandy',
                    'mlb',
                    'relation',
                    'achaddo',
                    'affiliate',
                    'redirect',
                    'properties',
                    'aliexpress',
                    'categories_juntyme',
                    'product_cat_juntyme',
                    'dropify',
                    'dslite',
                    'bling',
                    'attributes_variations',
                    'url',
                    'gender',
                    'age_group',
                    'duplicate',
                    'galleryJson'
                )
            );
        }

        /** Produto não localizado */
        return redirect()->route('catalog.index');
    }

    /**  ATUALIZAR PRODUTO */
    public function updateProduct(UpdateProductsRequest $request)
    {
        $shop_id = (Session::has('shops')) ? session('shops') : null;

        # Verificar Cadastro SHOP
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        ## Atualziar Produto Class
        $this->editProductClass->saveProductNew($request, $shop_id);

        flash('<i data-feather="thumbs-up"></i> Produto Atualizado com Sucesso.
        Agora estamos Indexando seu Catalogo este processo pode demora até <b>5 minutos</b>')->success();
        return back();
    }


    /** Cadastrar marca no Cataloo. */
    public function productProvider(Request $request)
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $provider =  ProductsProvider::where('shop_id', '=',  $shop_id)
            ->paginate($this->totalPage);

        return view(
            'backend.pages.catalogs.providerProduct',
            compact(
                'provider',

            )
        );
    }


    /*** Cadastrar marca no Catalogo. */
    public function productBrand(Request $request)
    {
        ## Shop ID
        $shop_id = session('shops');

        ## Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $dateForm = $request->except('_token');

        $formSearch  = $this->filtroSearch($dateForm);

        $list_brand = ProductsBrand::where('shop_id', '=', $shop_id)
            ->where(function ($query) use ($formSearch) {
                if (isset($formSearch) && !is_null($formSearch))
                    $query->where('products_brand.title', 'ILIKE', '%' . $formSearch . '%');
            })
            ->orderBy('title', 'asc')
            ->paginate($this->totalPage);


        return view(
            'backend.pages.catalogs.brandProduct',
            compact(
                'list_brand',
                'dateForm'
            )
        );
    }

    /*** RELATORIO PRODUTOS PARADO NO ESTOQUE */
    public function productEntry()
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        return view('backend.pages.catalogs.stockStopped');
    }

    /**  PRODUTO CATEGORIA  */
    public function productCategory(Request $request)
    {
        ## ID Shop
        $shop_id = session('shops');

        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $dateForm = $request->except('_token');

        $formSearch  = $this->filtroSearch($dateForm);

        $list_category = ProductsCategory::where('shop_id', '=', $shop_id)
            ->with('products_subcategories')
            ->where(function ($query) use ($formSearch) {
                if (isset($formSearch) && !is_null($formSearch))
                    $query->where('products_categories.title', 'ILIKE', '%' . $formSearch . '%');
            })
            ->orderBy('title', 'asc')
            ->paginate($this->totalPage);


        $categories = ProductsCategory::where('shop_id', '=', $shop_id)
            ->orderBy('title', 'asc')
            ->get();


        return view(
            'backend.pages.catalogs.categoryProduct',
            compact(
                'categories',
                'list_category',
                'dateForm'
            )
        );
    }

    /** NOTIFICAÇÕES */
    public function notification(Request $request)
    {

        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $shop = Shop::find(session('shops'));
        if (!empty($shop->notifications)) {
            $shop->notifications->markAsRead();
        }
        $notifi = $shop->notifications()->orderBy('created_at', 'DESC')->paginate($this->totalPage);

        return view(
            'backend.pages.notification',
            compact('notifi')
        );
    }

    /** MENSAGENS  */
    public function message()
    {
        $shop_id = session('shops');
        # Verificar Cadastro Comploeto
        if (is_null($shop_id)) {
            return  redirect()->route('shop.create');
        }

        $user = User::find(Auth::user()->id);
        if (!empty($user->notifications)) {
            $user->notifications->markAsRead();
        }
        $messag = $user->notifications()->orderBy('created_at', 'DESC')->paginate($this->totalPage);

        return view(
            'backend.pages.message',
            compact('messag')
        );
    }

    /** Pagina Importar Produtos por Planilha */
    public function importSpreadsheetProducts()
    {
        /// Shop ID
        $shop_id = session('shops');

        $products = ProductsImportNew::where('shop_id', '=', $shop_id)
            ->where('approved', '=', null)
            ->select('key',  DB::raw('count(*) as count'))
            ->groupBy('key')
            ->get();

        return view('backend.pages.catalogs.importSpreadsheetProducts', compact('products'));
    }


    /** DOWNLOAD PLANILHA PADRÃO */
    public function defaultTemplateDownload()
    {
        return view('backend.pages.catalogs.defaultTemplateDownload');
    }

    /** UPLOAD PLANILHA PRODUTOS */
    public function downloadProducts()
    {

        return view('backend.pages.catalogs.downloadProducts');
    }


    /** ATUALIZAR ESTOQUE POR PLANILHA */
    public function updateStockProducts()
    {
        /// Localizar Loja
        $shop_id =  session('shops');

        /// Carregar Tabela Uptade Valor Estoque
        $products = ProductsImportUpdate::where('shop_id', '=', $shop_id)
            ->where('approved', '=', null)
            ->select('key',  DB::raw('count(*) as count'))
            ->groupBy('key')
            ->get();

        return view('backend.pages.catalogs.updateStockProducts', compact('products'));
    }

    /** CONFIRMAR ATUALIZAÇÃO DO ESTOQUE */
    public function updateStockProductsDo(Request $request, $key)
    {
        //// Confirmar Atualização
        ProductsImportUpdate::where('key', '=', $key)->update(['approved' => 1]);

        flash('<i data-feather="thumbs-up"></i> Sucesso! Estamos atualizando os dados informados.')->success();
        return redirect()->route('catalog.updateStockProducts');
    }

    /** CONFIRMAR ATUALIZAÇÃO DO CANCELAR */
    public function updateStockProductsCancel(Request $request, $key)
    {

        /// Remover Atualização
        ProductsImportUpdate::where('key', '=', $key)->delete();

        flash('<i data-feather="thumbs-up"></i> Cancelado! Não realizamos a atualização.')->success();
        return redirect()->route('catalog.updateStockProducts');
    }



    /** CONFIRMAR ATUALIZAÇÃO DO IMPORTAÇÃO */
    public function updateImportProductsDo(Request $request, $key)
    {

        //// Confirmar Atualização
        ProductsImportNew::where('key', '=', $key)->update(['approved' => 1]);

        flash('<i data-feather="thumbs-up"></i> Sucesso! Estamos atualizando os dados informados.')->success();
        return redirect()->route('catalog.importSpreadsheetProducts');
    }

    /** CONFIRMAR CANCELAR IMPORTAÇAO */
    public function updateImportProductsCancel(Request $request, $key)
    {

        /// Remover Atualização
        ProductsImportNew::where('key', '=', $key)->delete();

        flash('<i data-feather="thumbs-up"></i> Cancelado! Não realizamos a atualização.')->success();
        return redirect()->route('catalog.importSpreadsheetProducts');
    }


    /** CONFIRMAR ATUALIZAÇÃO ESTOQUE */
    public function confirmImportProducts(Request $request, $key)
    {

        /// Localizar Loja
        $shop_id =  session('shops');

        /// Carregar Tabela Uptade Valor Estoque
        $products = ProductsImportNew::where('shop_id', '=', $shop_id)
            ->where('key', '=', $key)
            ->paginate($this->totalPage);

        return view(
            'backend.pages.catalogs.confirmImportProducts',
            compact('products', 'key')
        );
    }

    public function confirmImportProductsStock(Request $request, $key)
    {

        /// Localizar Loja
        $shop_id =  session('shops');

        /// Carregar Tabela Uptade Valor Estoque
        $products = ProductsImportUpdate::where('shop_id', '=', $shop_id)
            ->where('key', '=', $key)
            ->paginate($this->totalPage);

        return view(
            'backend.pages.catalogs.confirmImportProductsStock',
            compact('products', 'key')
        );
    }

    /** IMPORTAÇÃO ARQUIVO EXCEL */
    public function importExcelFile($request, $key)
    {

        /* O campo do form com o arquivo tinha o atributo name="file".  */
        $file = $request->file('import');

        /// Arquivo não existe
        if (empty($file)) {
            $name = 'erro_archive';
        }

        /// Verificar Tamanho do Arquivo
        /// Arquivo não existe
        if ($file->getSize() > 52428800) {
            $name = 'erro_size';
        }

        /// Arquivo existe
        if (!empty($file)) {

            $extension = $file->getClientOriginalExtension();

            if ($extension == 'csv' || $extension == 'xls' || $extension == 'xlsx') {

                // nome do Arquivo
                $name = $key . '.' . $file->getClientOriginalExtension();
                /// Caminho do Arquivo
                $path = storage_path('table_csv');

                /// Salvar arquivo nominal
                $file->move($path, $name);

                // Recuperar Caminho do Arquivo
                $name = $path . '/' . $name;
            } else {

                $name = 'erro_extension';
            }
        }

        return $name;
    }

    /** RELACIONAR TITULOS TABELA */
    public function listTitleTableProducts(Request $request)
    {
        // Recuperar Titulos
        $titles = session('titleTable');
        $titles = $titles['titleTable'];

        return view(
            'backend.pages.catalogs.listTitleTableProducts',
            compact('titles')
        );
    }

    /** Verificar Titulos Tabela Importação */
    public function checkTableTitles($headings)
    {

        /// Titulo Vázio
        foreach ($headings[0][0] as $value) {
            if ($value == '') {

                $result = 'empty';
                return $result;
            }
        }

        //// Titulos Tabela Padrão
        foreach ($headings[0] as $value) {

            /// Verificar Titulos
            if (
                $value[0] != 'id'
                || $value[1] != 'status'
                || $value[2] != 'condicao'
                || $value[3] != 'fornecedor'
                || $value[4] != 'marca'
                || $value[5] != 'titulo'
                || $value[6] != 'sku'
                || $value[7] != 'imagem_principal'
                || $value[8] != 'cor'
                || $value[9] != 'gtin'
                || $value[10] != 'ncm'
                || $value[11] != 'anotacoes'
                || $value[12] != 'descricao'
                || $value[13] != 'breve_descricao'
                || $value[14] != 'preco'
                || $value[15] != 'preco_promocional'
                || $value[16] != 'preco_custo'
                || $value[17] != 'quantidade'
                || $value[18] != 'prazo_cross_docking'
                || $value[19] != 'garantia'
                || $value[20] != 'peso'
                || $value[21] != 'altura'
                || $value[22] != 'largura'
                || $value[23] != 'comprimento'
                || $value[24] != 'link_video'
                || $value[25] != 'galeria_imgem_1'
            ) {


                $result = 'createHead';
                return $result;
            }
        }
        $result = 'import';
        return $result;
    }

    /** CADASTRAR PRODUTOS POR PLANILHA */
    public function completSpreadsheetUpload(Request $request)
    {

        /// Localizar Loja
        $shop_id =  session('shops');

        $data = $request->all();

        $key = $shop_id . '-' . Str::random(10);

        /// Importação Arquivo Excel CSV();
        $name = $this->importExcelFile($request, $key);

        //// Erro Extension
        if ($name == 'erro_size') {

            flash('<i data-feather="alert-octagon"></i> Erro! O tamanho permitio é 50M, utilizar o formado .csv ou divida o arquivo.')->error()->important();
            return back();
        }



        //// Erro Extension
        if ($name == 'erro_archive') {

            //// Excluir Tabela Excel
            unlink($name);

            flash('<i data-feather="alert-octagon"></i> Erro! Nenhum arquivo foi enviado no formulário.')->error()->important();
            return back();
        }


        //// Erro Extension
        if ($name == 'erro_extension') {

            //// Excluir Tabela Excel
            unlink($name);

            flash('<i data-feather="alert-octagon"></i> Erro! Parece que você esta tentando enviar um arquivo com extensão diferente da permitida.')->error()->important();
            return back();
        }

        // Recuperar o Titulo
        try {

            $headings = (new HeadingRowImport())->toArray($name);
        } catch (\Exception $exception) {

            //// Excluir Tabela Excel
            unlink($name);

            flash('<i data-feather="alert-octagon"></i> Erro! Não foi Possivel Recuperar o Tíluto, verifique se a primeira linha esta com os títulos correto.')->error()->important();
            return back();
        }

        /// Verificar se Existe o Titulo
        foreach ($headings[0] as $value) {
            if (empty($value[0])) {

                //// Excluir Tabela Excel
                unlink($name);

                flash('<i data-feather="alert-octagon"></i> Erro! Não foi Possivel Recuperar o Tíluto, verifique se a primeira linha esta com os títulos correto.')->error()->important();
                return back();
            }
        }


        /// Veriifcar Se é Tabela Padrão
        $checkHead = $this->checkTableTitles($headings);

        //// Relacionar Titulo
        if ($checkHead == 'createHead') {
            $dataTable = [
                'titleTable' => $value,
                'name' => $name,
                'key' => $key,
                'dataTable' => [
                    'catalog_status' => $data['catalog_status'],
                    'catalog_stock' => $data['catalog_stock'],
                    'catalog_gallery' => $data['catalog_gallery']
                ]
            ];

            /// Criar Session Titulo Tabela
            Session::put('titleTable', $dataTable);

            return redirect()->route('catalog.listTitleTableProducts', ['key' => $key]);
        }

        if ($checkHead == 'empty') {
            //// Excluir Tabela Excel
            unlink($name);

            flash('<i data-feather="alert-octagon"></i> Erro! Título com caracteres especiais, corrija o título retirando os caracteres especiais como "ç, ã, &, etc..." e importe novamente.')->error()->important();
            return back();
        }

        if ($checkHead == 'import') {
            //// Importar Produtos Para Confirmação
            try {

                ///Importar Planilha para Atualizar
                Excel::import(new NewProductImport($data, $key, $shop_id), $name);
            } catch (\Exception $exception) {

                unlink($name);

                flash('<i data-feather="alert-octagon"></i> Erro! Parece que tivemos um erro no processamento de sua tabela, tente mais tarde ou entre em contato com nosso suporte.<br />[' . $exception->getMessage() . ']')->error()->important();
                return back();
            }
        }

        //// Excluir Tabela Excel
        unlink($name);

        // Redirecionar Tabela visualização
        return redirect()->route('catalog.confirmImportProducts', ['key' => $key]);
    }

    /** PREPARAR TITULOS PARA SALVAR TABELAS */
    public function prepareTableTitles($request)
    {
        /// Preparar Capeçalho titulo
        $array = [];
        foreach ($request as $key => $value) {
            if ($value != 'no') {
                $array += [$value => $key];
            }
        }

        return $array;
    }

    /** SALVAR TABELA COM TITULOS SELECIONADOS */
    public function completSpreadsheetUploadDo(Request $request)
    {

        //// Recuperar Nomes da Tabela
        $titles = $this->prepareTableTitles($request->except(['_token']));

        /// Localizar Loja
        $shop_id =  session('shops');

        // Recuperar Data
        $titleTable = session('titleTable');
        $data = $titleTable['dataTable'];
        $name = $titleTable['name'];
        $key = $titleTable['key'];


        try {

            ///Importar Planilha para Atualizar
            Excel::import(new NewProductImport($data, $key, $shop_id, $titles), $name);
        } catch (\Exception $exception) {

            flash('<i data-feather="alert-octagon"></i> Erro! Parece que tivemos um erro no processamento de sua tabela, tente mais tarde ou entre em contato com nosso suporte.<br />[' . $exception->getMessage() . ']')->error()->important();
            return back();
        }


        //// Excluir Tabela Excel
        unlink($name);

        return redirect()->route('catalog.confirmImportProducts', ['key' => $key]);
    }



    /** ATUALIZAR VALOR ESTOQUE PRODUTOS */
    public function priceStockSpreadsheetUpload(Request $request)
    {

        ## Localizar Loja
        $shop_id =  session('shops');

        $data = $request->except(['_token']);

        $key = $shop_id . '-' . Str::random(10);

        ## Importação Arquivo Excel CSV();
        $name = $this->importExcelFile($request, $key);

        ## Erro Extension
        if ($name == 'erro_archive') {

            ## Excluir Tabela Excel
            unlink($name);

            flash('<i data-feather="alert-octagon"></i> Erro! Nenhum arquivo foi enviado no formulário.')->error()->important();
            return back();
        }


        //// Erro Extension
        if ($name == 'erro_extension') {

            //// Excluir Tabela Excel
            unlink($name);

            flash('<i data-feather="alert-octagon"></i> Erro! Parece que você esta tentando enviar um arquivo com extensão diferente da permitida.')->error()->important();
            return back();
        }

        // Recuperar o Titulo
        try {

            $headings = (new HeadingRowImport())->toArray($name);
        } catch (\Exception $exception) {

            //// Excluir Tabela Excel
            unlink($name);

            flash('<i data-feather="alert-octagon"></i> Erro! Não foi Possivel Recuperar o Tíluto, verifique se a primeira linha esta com os títulos correto.')->error()->important();
            return back();
        }

        /// Verificar se Existe o Titulo
        foreach ($headings[0] as $value) {
            if (empty($value[0])) {

                //// Excluir Tabela Excel
                unlink($name);

                flash('<i data-feather="alert-octagon"></i> Erro! Não foi Possivel Recuperar o Tíluto, verifique se a primeira linha esta com os títulos correto.')->error()->important();
                return back();
            }
        }

        /// Veriifcar se os titulos estão Corretos
        foreach ($headings[0] as $value) {
            if (
                $value[0] != 'id'
                || $value[1] != 'sku'
                || $value[2] != 'preco_original'
                || $value[3] != 'preco_promocional'
                || $value[4] != 'preco_custo'
                || $value[5] != 'quantidade'
                || $value[6] != 'prazo_cross_docking'
            ) {

                //// Excluir Tabela Excel
                unlink($name);

                // Alert
                flash('<i data-feather="alert-octagon"></i> Erro! Titulo não esta conforme tabela padrão verifique e realize novamente a importação.')->error()->important();
                return back();
            }
        }

        try {

            ///Importar Planilha para Atualizar
            Excel::import(new UpdateProductPriceStockImport($data, $key, $shop_id), $name);
        } catch (\Exception $exception) {

            //// Excluir Tabela Excel
            unlink($name);

            flash('<i data-feather="alert-octagon"></i> Erro! Parece que tivemos um erro no processamento de sua tabela, tente mais tarde ou entre em contato com nosso suporte.<br />[' . $exception->getMessage() . ']')->error()->important();
            return back();
        }

        //// Excluir Tabela Excel
        unlink($name);

        return redirect()->route('catalog.confirmImportProductsStock', ['key' => $key]);
    }

    /** DOWNLOAD CATALO */
    public function exportCatalogProducts(Request $request)
    {
        /// Recuperar Resquest POST
        $data = $request->all();
        try {

            /// Retornar Resultado da Pesquisa
            return (new ProductsExport($data))->download('Catalogo.csv');
        } catch (\Exception $exception) {

            flash('<i data-feather="alert-octagon"></i> Erro! Parece que tivemos um erro no processamento de sua tabela, tente mais tarde ou entre em contato com nosso suporte.<br />[' . $exception->getMessage() . ']')->error()->important();
            return back();
        }
    }

    /** CARREGAR CATEGORIAS ARRAY */
    public function productCategoryArray(Request $request)
    {

        ## Carregar Categorias
        $categories = StoresCategory::where('departament_id', $request->get('dep'))
            ->select(
                'id',
                'title',
                'slug'
            )
            ->orderBy('title', 'asc')
            ->get();

        $response['success'] = true;
        $response['message'] = json_encode($categories, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /** CARREGAR SUBCATEGORIAS ARRAY */
    public function productSubcategoryArray(Request $request)
    {

        ## Carregar Categorias
        $subcategories = StoresSubcategory::where('category_id', $request->get('cat'))
            ->select(
                'id',
                'title',
                'slug'
            )
            ->orderBy('title', 'asc')
            ->get();

        $response['success'] = true;
        $response['message'] = json_encode($subcategories, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /** ATUALIZAR PRODUTO DSLITE */
    public function productUpdateDslite(Request $request, $id)
    {
        $productID = Encript::desencriptar($request->id);

        if (is_null($productID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## Localizar Loja
        $shop_id =  session('shops');

        ## Localizar Token de acesso
        $access = ShopsDsliteConfig::where('shop_id', $shop_id)->first();

        ## Atualizar Produto
        $result = $this->dsliteClass->updateNewUnique($productID, $shop_id, $access->access_token);

        ## Erro
        if (is_null($result)) {
            flash('<i data-feather="alert-octagon"></i> Ocorreu um erro na atualização do Produto pela Dslite.')->error()->important();
            return back();
        }

        flash('<i data-feather="alert-octagon"></i> Produto Atualizado pela Dslite com successo ')->success();
        return back();
    }

    /** ATUALIZAR PRODUTO DROPIFY */
    public function productUpdateDropify(Request $request, $id)
    {
        $productID = Encript::desencriptar($request->id);

        if (is_null($productID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## Localizar Loja
        $shop_id =  session('shops');

        ## Carregar Produto
        $prod = Product::where('products.id', $productID)
            ->join('products_marketplace_dropify', 'products_marketplace_dropify.product_id', '=', 'products.id')
            ->with('products_images_galleries', 'products_attributes_variation')
            ->select(
                'products_marketplace_dropify.product_sku',
                'products.id',
                'products.sku',
                'products.image',
                'products.image_type'
            )
            ->first();

        ## Recuperar Token
        $accessDropify = ShopsDropifyConfig::where('shop_id', $shop_id)->first();
        $token = $accessDropify->access_token;

        ## Conectar Dropify
        $product = $this->dropifyClass->loadNewUnique($token, $prod['sku']);

        ## Atualizar token
        if (is_null($product)) {

            ## Atualizar Banco de Dados
            $accessDropify = $this->dropifyClass->updateToken($accessDropify, $shop_id, $product);

            ## Conectar Dropify
            $product = $this->dropifyClass->loadNewUnique($token, $prod['sku']);
        }

        ## Importar via Class
        $result = $this->dropifyClass->updateNewUnique($prod, $product, $accessDropify, $shop_id);

        ## Erro
        if (is_null($result)) {
            flash('<i data-feather="alert-octagon"></i> Ocorreu um erro na atualização do Produto pela Dropify.')->error()->important();
            return back();
        }

        flash('<i data-feather="alert-octagon"></i> Produto Atualizado pelo Dropify com successo ')->success();
        return back();
    }

    /** ATUALIZAR PRODUTO MERCADO LIVRE */
    public function productUpdateMl(Request $request, $id)
    {
        $productID = Encript::desencriptar($request->id);

        if (is_null($productID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## Localizar Loja
        $shop_id =  session('shops');
        dd($productID);
        ## Localizar Token de acesso
        // $access = ShopsDsliteConfig::where('shop_id', $shop_id)->first();

        ## Atualizar Produto
        // $result = $this->dsliteClass->updateNewUnique($productID, $shop_id, $access->access_token);

        ## Erro
        // if (is_null($result)) {
        //     flash('<i data-feather="alert-octagon"></i> Ocorreu um erro na atualização do Produto pela Dslite.')->error()->important();
        //     return back();
        // }

        flash('<i data-feather="alert-octagon"></i> Produto Atualizado pela Dslite com successo ')->success();
        return back();
    }

    /** ATUALIZAR PRODUTO BLING */
    public function productUpdateBling(Request $request, $id)
    {
        $productID = Encript::desencriptar($id);

        if (is_null($productID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## Localizar Loja
        $shop_id =  session('shops');

        ## Carregar Produto
        $prod = Product::where('products.id', $productID)
            ->join('products_marketplace_bling', 'products_marketplace_bling.product_id', '=', 'products.id')
            ->with('products_images_galleries', 'products_attributes_variation')
            ->select(
                'products_marketplace_bling.product_sku',
                'products.id',
                'products.sku',
                'products.image',
                'products.image_type'
            )
            ->first();

        ## Recuperar Token
        $accessBling = ShopsBlingConfig::where('shop_id', $shop_id)->first();
        $token = $accessBling->access_token;

        ## Conectar Bling
        $product = $this->blingClass->loadNewUnique($token, $prod['sku']);

        ## Importar via Class
        $result = $this->blingClass->updateNewUnique($prod, $product->retorno->produtos, $accessBling, $shop_id, $token);

        ## Erro
        if (is_null($result)) {
            flash('<i data-feather="alert-octagon"></i> Ocorreu um erro na atualização do Produto pelo Bling.')->error()->important();
            return back();
        }

        flash('<i data-feather="alert-octagon"></i> Produto Atualizado pelo Bling com successo ')->success();
        return back();
    }
}
