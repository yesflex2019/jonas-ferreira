<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginUserRequest;
use App\Http\Requests\Admin\newUserRequest;
use App\Mail\newJuntyme;
use App\Models\AccessDirectShop;
use App\Models\MultinivelShopRelation;
use App\Models\MultinivelShopRelationUserMm;
use App\Models\PasswordReset;
use App\Models\Shop;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    ## Verificação de Erros
    // if (\Request::ip() == '177.68.60.245') {
    //     dd($user);
    // }

    /**  FUNÇÃO ACESSAR PAINEL */
    public function index()
    {
        return (Auth::check() === true)
            ? view('backend.home')
            : redirect()->route('panel.admin');
    }


    /** SALVAR TOKEN REMEMBER */
    public function rememberToken($remember_token, $email)
    {
        $created = Carbon::now();

        $new = new PasswordReset();
        $new->email = $email;
        $new->token = $remember_token;
        $new->created_at = $created;
        $new->save();
    }

    public function page(Request $request)
    {
        ## Session Parceiro
        if (!empty($request->get('parceiro'))) {
            Session::put('parceiroToken', $request->get('parceiro'));
        }

        ## Verificar Usuario Logado
        return (Auth::user())
            ? redirect()->route('panel.admin')
            : view('backend.login');
    }

    /**  FUNÇÃO REALIZAR LOGIN */
    public function login(LoginUserRequest $request)
    {
        ## Resultado Validade
        $validated = $request->validated();

        /** Verificar Email  Valido*/
        $userMail = User::where('email', $validated['email'])->first();


        if (!isset($userMail['email_verified_at'])) {
            // E-mail não Verificado
            flash('<i data-feather="alert-octagon"></i> Confirme seu email para acessar o Sistema.')->error();
            return redirect()->route('login.confirmMailWaiting')->withInput();
        }

        if (!isset($userMail)) {
            // E-mail não cadastrado
            flash('<i data-feather="alert-octagon"></i> E-mail informado não esta cadastrado.')->error();
            return back()->withInput();
        }

        $credentials = [
            'email' => $validated['email'],
            'password' => $validated['password']
        ];

        if (Auth::guard('web')->attempt($credentials)) {

            // Get the currently authenticated user...
            $user_local = User::where('users.id', Auth::user()->id) //
                ->leftJoin('users_admin_mm', 'users_admin_mm.user_id', '=', 'users.id')
                ->leftJoin('shops_users', 'shops_users.user_id', '=', 'users.id')
                ->select(
                    'users.id',
                    'users.name',
                    'users.status_id',
                    'shops_users.shop_id',
                    'users_admin_mm.type',
                    'users_admin_mm.admin_id'
                )
                ->first();

            switch ($user_local['status_id']) {
                case 1:
                    return  redirect()->route('shop.create');
                    break;
                case 2:
                    ## Loja Ativa
                    Session::put([
                        'user_status' => $user_local['status_id'],
                        'user_name' => $user_local['name'],
                        'shops' => $user_local['shop_id'],
                        'type' => $user_local['type']
                    ]);

                    ## Painel Seller
                    if ($user_local['admin_id'] == 1) {
                        return  redirect()->route('panel.admin');
                    }
                    ## Painel Affiliate
                    if ($user_local['admin_id'] == 2) {

                        return  redirect()->route('affiliate.affiliateIndex');
                    }
                    ## Painel MarketPlace
                    if ($user_local['admin_id'] == 3) {
                        return  redirect()->route('marketplace.marketplaceIndex');
                    }

                    return  redirect()->route('panel.admin');
                    break;
                case 3:
                    ## Shop Pausada
                    return  redirect()->route('shop.accountPaused');
                    break;
                case 4:
                    ## Shop Excluido
                    return  redirect()->route('shop.accountPaused');
                    break;
            }
        }

        /** Voltar a pagina pela rota */
        flash('<i data-feather="alert-octagon"></i> Os dados Informados não conferem')->error();
        return back()->withInput();
    }


    /** Shop Conta Paulsada  */
    public function  accountPaused(Shop $shop)
    {

        return view('backend.pages.users.accountPaused');
    }

    /** FUNÇÃO LOGOUT */
    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        Session::forget('user_status');
        Session::forget('user_name');
        Session::forget('panel');
        Session::forget('shops');
        return redirect()->route('login.page');
    }


    /**  Confirmação de Email */
    public function verify(Request $request, User $user)
    {
        $user = User::where('remember_token', $request->token)->first();

        if (!is_null($user)) {
            $user->markEmailAsVerified();
            $user->remember_token =  Str::random(80);
            $user->save();

            return view('backend.pages.login.emailConfirmed');
        }

        /** Solicitar Novo Acesso */
        return view('backend.pages.login.recoverTokenUser');
    }

    /** Recuperar Token User */
    public function recoverTokenUser()
    {
        return view('backend.pages.login.recoverTokenUser');
    }

    /** Gerar novo Token */
    public function newTokenUser(Request $request)
    {

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            /** Redirecionar Aguardando Confirmação  Erro email não cadastrado*/
            flash('<i data-feather="alert-octagon"></i> E-mail informado não é valido.')->error();
            return back()->withInput();
        }

        $user = User::where('email', $request->email)->first();

        if (!is_null($user)) {

            $remember_token = Str::random(80);
            $email = $request->email;

            $user->remember_token = $remember_token;
            $user->save($request->except(['_token']));

            ## Salvar Tokem Resets
            $this->rememberToken($remember_token, $email);


            /** Envio direto  */
            try {
                Mail::send(new newJuntyme($user));
            } catch (Exception $e) {
                flash('<i data-feather="alert-octagon"></i> Email não pode ser enviado entre em contato com Nosso Suporte suporte@juntyme.com.br.')->error();
            }

            /** Redirecionar Aguardando Confirmação */
            return redirect()->route('login.confirmMailWaiting');
        }

        /** Redirecionar Aguardando Confirmação  Erro email não cadastrado*/
        flash('<i data-feather="alert-octagon"></i> E-mail informado não esta cadastrado em Nosso Sistema.')->error();
        return back()->withInput();
    }


    /**  Recuperar Senha */
    public function resetMail()
    {
        return view('backend.pages.login.loginRecover');
    }


    /**  Cadastrar Usuário */
    public function register(Request $request)
    {
        # Session Parceiro
        if (!empty($request->get('parceiro'))) {

            Session::put('parceiroToken', $request->get('parceiro'));
        }

        return view('backend.pages.login.loginRegister');
    }

    /*** VERIFICAR HISTORICO DE TOKENS GERADO */
    public function verificRememberToken($token)
    {

        $result_token = PasswordReset::join('users', 'users.email', '=', 'password_resets.email')
            ->where('password_resets.token', $token)
            ->first();

        if (!is_null($result_token)) {

            return  $result_token;
        } else {

            return null;
        }
    }

    /**  Cadastrar Usuário */
    public function confirmMail(Request $request, $token)
    {
        $user = User::where('remember_token', '=', $token)->first();

        if (is_null($user)) {

            $user = $this->verificRememberToken($token);
        }

        if (!is_null($user)) {
            #Atualizar Senha
            User::where('id', '=', $user->id)
                ->update(['email_verified_at' => now()]);
            return view('backend.pages.login.emailConfirmed');
        }
        flash('<i data-feather="alert-octagon"></i> Token inválido solicite novamente.')->error();
        return view('backend.pages.login.recoverTokenUser');
    }

    /** INDICAR PARCEIRO UNILEVEL */
    public function partnerUnilevel($user_id)
    {
        $parceiroID = (Session::has('parceiroToken'))
            ? session('parceiroToken')
            : NULL;

        if (!is_null($parceiroID)) {

            # Localizar Parceiro
            $partner = MultinivelShopRelation::where('link_onepage', '=', $parceiroID)
                ->orWhere('link_register', '=', $parceiroID)
                ->first();

            $multinivel_shop_id = $partner['id'];
            $shop_id = $partner['shop_id'];

            # Relacionar Parceior com CLiente
            $partner = new MultinivelShopRelationUserMm();
            $partner->multinivel_shop_id = $multinivel_shop_id;
            $partner->shop_id = $shop_id;
            $partner->client_user_id = $user_id;
            $partner->save();

            unset($partner);
        }
    }

    /** Novo Usuario */
    public function userCriate(newUserRequest $request)
    {

        ## Resultado Validade
        $validated = $request->validated();

        ## Verificar se exite email cadastrado
        $user = User::where('email', $validated['email'])->first();

        ## Usuario Já Cadastrado
        if (!is_null($user)) {
            flash('<i data-feather="alert-octagon"></i> E-mail já Cadastrado na Juntyme')->error();
            return back()->withInput();
        }
        $remember_token = Str::random(80);
        $email = $validated['email'];
        $cell = preg_replace("/[^0-9]/", "", $validated['cell']);

        ## Salvar Usuario
        $user = new User();
        $user->status_id = '1';
        $user->type = '1';
        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->cell =  $cell;
        $user->password = Hash::make($validated['password']);
        $user->remember_token =  $remember_token;
        $user->save();

        ## Salvar Tokem Resets
        $this->rememberToken($remember_token, $email);

        ## Localizar Parceiro Indicador
        $this->partnerUnilevel($user->id);

        /** Envio Emediato  */
        try {
            Mail::send(new newJuntyme($user));
        } catch (Exception $e) {
            flash('<i data-feather="alert-octagon"></i> Erro ao enviar o email para validação entre em contato com Nosso Suporte suporte@juntyme.com.br.')->error()->important();
        }

        # Redirecinar Para Confirmar Email
        return redirect()->route('login.confirmMailWaiting');
    }


    /**  Cadastrar Usuário */
    public function confirmMailWaiting(Request $request)
    {

        ## Aguardando Confirmação de E-mail
        return view('backend.pages.login.confirmMail');
    }

    /**  Direcionar Painel Usuário */
    public function  redirectUser(Request $request)
    {

        if ($request->id == 1) {

            /** Direcionar Painel Vendedor */
            Session::put(['panel' => $request->id]);
            return redirect()->route('login.page');
        }
        if ($request->id == 3) {

            /** Direcionar Painel MarketPlace */
            Session::put(['panel' => $request->id]);
            return redirect()->route('marketplace.marketplaceIndex');
        }
        if ($request->id == 4) {

            /** Direcionar Painel Afiliado */
            Session::put(['panel' => $request->id]);
            return redirect()->route('affiliate.affiliateIndex');
        }
        if ($request->id == 5) {

            /** Direcionar Painel Afiliado */
            Session::put(['panel' => $request->id]);
            return redirect()->route('touch.home');
        }
    }


    /**  Direcionar Painel Usuário */
    public function  redirectLogin(Request $request)
    {
        dd(session('shops'), session('user_status'));
    }

    /** PAGINA ERROR  */
    public function errorPage(Request $request)
    {

        $error = $request->erro;
        return view(
            'backend.pages.login.errorPage',
            compact('error')
        );
    }


    /** DIRECT LOGIN */
    public function directLogin(Request $request)
    {

        ## Verificar token de acesso
        $user = AccessDirectShop::where('token', $request->token)->first();

        if (!is_null($user)) {

            if (Auth::guard('web')->loginUsingId($user['user_id'])) {

                // Get the currently authenticated user...
                $user_local = User::where('users.id', Auth::user()->id) //
                    ->leftJoin('users_admin_mm', 'users_admin_mm.user_id', '=', 'users.id')
                    ->leftJoin('shops_users', 'shops_users.user_id', '=', 'users.id')
                    ->select(
                        'users.id',
                        'users.name',
                        'users.status_id',
                        'shops_users.shop_id',
                        'users_admin_mm.type',
                        'users_admin_mm.admin_id'
                    )
                    ->first();

                AccessDirectShop::where('token', $request->token)->forceDelete();

                switch ($user_local['status_id']) {
                    case 1:
                        return  redirect()->route('shop.create');
                        break;
                    case 2:
                        ## Loja Ativa
                        Session::put([
                            'user_status' => $user_local['status_id'],
                            'user_name' => $user_local['name'],
                            'shops' => $user_local['shop_id'],
                            'type' => $user_local['type']
                        ]);

                        ## Painel Seller
                        if ($user_local['admin_id'] == 1) {
                            return  redirect()->route('panel.admin');
                        }
                        ## Painel Affiliate
                        if ($user_local['admin_id'] == 2) {

                            return  redirect()->route('affiliate.affiliateIndex');
                        }
                        ## Painel MarketPlace
                        if ($user_local['admin_id'] == 3) {
                            return  redirect()->route('marketplace.marketplaceIndex');
                        }

                        return  redirect()->route('panel.admin');
                        break;
                    case 3:
                        ## Shop Pausada
                        return  redirect()->route('shop.accountPaused');
                        break;
                    case 4:
                        ## Shop Excluido
                        return  redirect()->route('shop.accountPaused');
                        break;
                }
            }
        }

        /** Voltar a pagina pela rota */
        return '<center>Token inválido ou vencido.. Login bloqueado </center>';
    }
}
