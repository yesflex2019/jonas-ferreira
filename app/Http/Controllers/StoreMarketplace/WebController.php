<?php

namespace App\Http\Controllers\StoreMarketplace;

use Cache;
use App\Models\Client as AppClient;
use App\Http\Controllers\Controller;
use App\Models\ClientsStoresMM;
use App\Models\Country;
use App\Models\OrderShop;
use App\Models\OrderShopIten;
use App\Models\OrdersMaster;
use App\Models\Product;
use App\Models\Shop;
use App\Models\ShopsAddress;
use App\Models\Store;
use App\Models\StoresHelpCenter;
use App\Models\StoresPagesConfig;
use App\Models\OrderShopCommission;
use App\Models\OrderShopInvoice;
use App\Models\OrderShopShipping;
use App\Models\OrdersMasterBoleto;
use App\Models\OrdersMasterPix;
use App\Models\OrdersShopsCommissionsSeller;
use App\Models\ShopsPagarmeRecebedor;
use App\Models\ShopsNewsletter;
use App\Models\OrderShopAliexpress;
use App\Models\ClientCartSession;
use App\Models\ClientsWishlist;
use App\Models\ProductsAttributesVariation;
use Illuminate\Support\Facades\File;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use LukeSnowden\GoogleShoppingFeed\Containers\GoogleShopping;
use Mpdf\QrCode\QrCode;
use Mpdf\QrCode\Output;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Http\Requests\StoreMarketPlace\CheckoutRequest;
use App\Classes\AliexpressClass;
use App\Classes\ShipClass;
use App\Classes\ClearHtmlClass;
use App\Classes\CheckoutClass;
use App\Classes\DropifyClass;
use App\Classes\MarketplaceClass;
use App\Classes\StoreAffiliateClass;
use App\Mail\Achaddo\ClientAchaddoNew;
use App\Mail\Achaddo\ClientAchaddoNewPassword;
use App\Mail\Achaddo\orderAchaddoAffiliate;
use App\Mail\Achaddo\orderAchaddoClient;
use App\Mail\Achaddo\orderAchaddoSeller;
use Illuminate\Support\Facades\Http;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\JsonLd;
use Illuminate\Support\Facades\Mail;
use  Encript;

class WebController extends Controller
{

    private $Aliexpress;
    private $Shipping;
    private $clearhtml;
    private $checkout;
    private $dropify;
    private $marketplace;
    private $storeAffiliate;


    private $totalPage = 20;

    ## Periodo é minutos
    private   $period = 30;

    public function __construct()
    {
        // Carregar Classes
        $this->Aliexpress = new AliexpressClass();
        $this->Shipping = new ShipClass();
        $this->clearhtml = new ClearHtmlClass();
        $this->checkout = new CheckoutClass();
        $this->dropify = new DropifyClass();
        $this->storeAffiliate = new StoreAffiliateClass();
        $this->marketplace = new MarketplaceClass();
    }


    /** PÁGINA PRINCIPAL HOME */
    public function index(Request $request)
    {

        ## Recupera a URL do SITE
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Departamentos
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']) ?? '';

        ## Carregar Share
        $share = $this->marketplace->myShare($store['id']) ?? '';

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        ## Conteúdo Banner
        if (!empty($store['banner_type'])) {
            $banners = $this->marketplace->storeBannersImage($store['id']);
            $banner_home  = null;
        } else {
            $banner_home = $this->marketplace->productBannerHome($store['shop_id']);
            $banners = null;
        }

        ## Conteudo Produtos em Destaque Get
        $products_detach = $this->marketplace->productsDetach($store['shop_id']);

        ## Conteúdo O que há de Novidade
        $limitNews = $store['products_qty_news'];
        $products_news = $this->marketplace->productsNews($store['shop_id'], $limitNews);

        ## Carregar Conteudo produtos mais vendidos
        $limitSelles = $store['products_qty_sales'];
        $products_sales = $this->marketplace->productsSales($store['shop_id'], $limitSelles);

        ## Popular Produtos
        $popular_products = true;

        ## Sem produtos Cadastrados
        $release =  (count($products_detach) > 0) ? true : null;

        ## Produtos Promocionais Rodapé
        $footer_products = $this->marketplace->rotatingBaseboardProduct($store['shop_id']);

        ## Taxa Parcelamento
        $fees =  $this->marketplace->taxSistemConfig(true);

        $explode = explode(" ", $store['title']);
        $metaKeyword = '';
        foreach ($explode as $item) {
            $metaKeyword .=     $item . ',';
        }

        SEOMeta::setTitle($store['title']);
        SEOMeta::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        SEOMeta::setCanonical(URL::current());
        SEOMeta::addKeyword(['MarketPlace', 'Loja Virtual', $metaKeyword]);

        OpenGraph::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        OpenGraph::setTitle($store['title']);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);

        // TwitterCard::setTitle('JuntyMe');
        // TwitterCard::setSite('@LuizVinicius73');

        JsonLd::setTitle($store['title']);
        JsonLd::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        JsonLd::addImage(asset($store['logo_main']));

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.home', compact(
            'release',
            'banner_home',
            'products_detach',
            'products_news',
            'products_sales',
            'store',
            'share',
            'wishlist',
            'popular_products',
            'departaments',
            'banners',
            'fees',
            'footer_products',
            'wishlist'
        ));
    }

    /** PAGINA DOS LOGISTAS */
    public function stores(Request $request, $slug)
    {
        ## Recupera a URL do SITE;
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $store_shop_id = $store['shop_id'];
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Share
        $share = $this->marketplace->myShare($store['id']) ?? null;

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        $dateForm = $request->except('_token');

        ## LOCALIZAR LOJA VENDEDORA
        $loja = Shop::where('slug', $slug)->first();

        ## Taxa Parcelamento
        $fees =  $this->marketplace->taxSistemConfig(true);

        $storeID = $store['shop_id'];
        $lojaIDSHOP = $loja['id'];

        ## CARREGAR DEPARTAMENTOS
        $menu_departament = $this->marketplace->departamentSeach($dateForm, $store_shop_id, $lojaIDSHOP);

        ## Consulta Produtos Logistas
        $products = $this->marketplace->productStoresMarketPlace($dateForm, $storeID, $lojaIDSHOP, $slug, $request);

        ## Produtos Promocionais Rodapé
        $footer_products = $this->marketplace->rotatingBaseboardProduct($store['shop_id']);

        if ($products->all()) {

            $themes = $this->marketplace->originThemes($store);

            return view($store['route'] .  $themes . '.site.web.stores', compact(
                'menu_departament',
                'products',
                'store',
                'loja',
                'share',
                'wishlist',
                'dateForm',
                'departaments',
                'fees',
                'footer_products'
            ));
        }

        return redirect()->route('mkp.noResults');
    }

    /** CONTROLE PÁGINA SHOP */
    public function shop(Request $request)
    {
        ## Recupera a URL do SITE
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $store_shop_id = $store['shop_id'];

        ## Carregar Departamentos
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Share
        $share = $this->marketplace->myShare($store['id']) ??  null;

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        $dateForm = $request->except('_token');

        ## CARREGAR DEPARTAMENTOS
        $menu_departament = $this->marketplace->departamentSeach($dateForm, $store_shop_id);

        ## FILTRO SEARCH
        $result_filter = $this->marketplace->filtroSearch($dateForm, $store_shop_id);

        ## CARREGAR PRODUTOS
        $products = $this->marketplace->chargeProducts($result_filter, $store, $dateForm);

        $filtro_price = $this->marketplace->filtroPrice($result_filter, $store, $dateForm);

        ## Resultado Price Achaddo e Afiliados
        if ($store['shop_id'] == 1) {

            $maior_preco =  $products->max('achaddo_price');
            $menor_preco =   $products->min('achaddo_price');
        } else {

            $maior_preco =  $products->max('affiliate_price');
            $menor_preco =   $products->min('affiliate_price');
        }

        ## Popular Produtos
        $popular_products = true;

        ## Taxa Parcelamento
        $fees =  $this->marketplace->taxSistemConfig(true);

        ## Produtos Promocionais Rodapé
        $footer_products = $this->marketplace->rotatingBaseboardProduct($store['shop_id']);

        $explode = explode(" ", $store['title']);
        $metaKeyword = '';
        foreach ($explode as $item) {
            $metaKeyword .=     $item . ',';
        }

        SEOMeta::setTitle($store['title']);
        SEOMeta::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        SEOMeta::setCanonical(URL::current());
        SEOMeta::addKeyword(['MarketPlace', 'Loja Virtual', $metaKeyword]);

        OpenGraph::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        OpenGraph::setTitle($store['title']);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);

        // TwitterCard::setTitle('JuntyMe');
        // TwitterCard::setSite('@LuizVinicius73');

        JsonLd::setTitle($store['title']);
        JsonLd::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        JsonLd::addImage(asset($store['logo_main']));

        if ($products->all()) {

            $themes = $this->marketplace->originThemes($store);

            return view($store['route'] . $themes . '.site.web.shop', compact(
                'menu_departament',
                'products',
                'filtro_price',
                'store',
                'share',
                'wishlist',
                'dateForm',
                'popular_products',
                'departaments',
                'fees',
                'footer_products'
            ));
        }

        return redirect()->route('mkp.noResults');
    }

    /** PAGINA LISTA DE DESEJO */
    public function wishlist(Request $request)
    {
        ## Localizar Loja
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Departamentos
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Usuario
        $user_id = session('user_id');

        ## Recuperar IP do Cliente
        $client_ip = $request->ip();

        ## carregar Lista
        $wishlist = $this->marketplace->clientsWishlist($user_id, $client_ip, $store);

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.wishlist', compact(
            'wishlist',
            'store',
            'departaments'
        ));
    }

    /** PAGINA DE PESQUISA SEM RESULTADO */
    public function noResults(Request $request)
    {
        ## Localizar Loja
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $content = StoresPagesConfig::where('slug', $request->slug)->first();

        ## Carregar Menu Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Menu Share
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        ## Produtos Promocionais Rodapé
        $footer_products = $this->marketplace->rotatingBaseboardProduct($store['shop_id']);

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.noResults', compact(
            'store',
            'share',
            'wishlist',
            'content',
            'departaments',
            'footer_products'
        ));
    }


    /** Carregar Imagem Aliexpress */
    public function imageAliexpress(Request $request)
    {
        ## CarregarImages Aliexpress
        return $this->Aliexpress->imageAliexpress($request);
    }

    /** CALCULO VARIAÇÃO ALIEXPRESS */
    public function calculatedVariation(Request $request)
    {
        ## Retornar Variação
        return $this->Aliexpress->calculatedVariation($request);
    }


    /** PAGINA DO PRODUTO */
    public function product(Request $request)
    {
        ## Configuração Loja
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Shere
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        ## Url Comment Fecebook
        $comment_fb = url()->full();

        ## Carregar Produtos
        $product = $this->marketplace->productDetails($request, $store['shop_id']);

        ## CARREGAR ARRAY VARIAÇÕES
        $variations = $this->marketplace->productsAttributesVariation($product);

        ## Verificar Se Existe
        if (is_null($product)) {
            return redirect()->route('mkp.shop')->with('warning', 'Produto não localizado.');
        }

        ## ADD Atualização Produto Aliexpress
        if (!is_null($product['aliexpress_id'])) {
            $this->marketplace->productAliUpdate($product);
        }

        ## Conteudo Perguntas
        $faq = $this->marketplace->contentFaq();

        ## Taxa Parcelamento
        $fees =  $this->marketplace->taxSistemConfig(true);

        ## Popular Produtos
        $popular_products = true;

        ## Produtos Promocionais Rodapé
        $footer_products = $this->marketplace->rotatingBaseboardProduct($store['shop_id']);

        ## Verificar Lista de Desejos
        $product_wishlist = $this->marketplace->productWishlist($store['id'], $product->id, $client_ip);

        $metaKeyword = $this->marketplace->seoMetaKeyword($product['title']);
        $description = $this->storeAffiliate->short_description($product);

        SEOMeta::setTitle($product['title']);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical(URL::current());
        SEOMeta::addMeta('article:published_time', $product['updated_at']->toW3CString(), 'property');
        SEOMeta::addMeta('article:section', $product['depart_title'], 'property');
        SEOMeta::addKeyword([$metaKeyword]);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($product['title']);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);

        OpenGraph::addImage($product['image']);
        OpenGraph::addImage(['url' => $product['image'], 'size' => 300]);
        OpenGraph::addImage($product['image'], ['height' => 300, 'width' => 300]);

        // article
        OpenGraph::setTitle($product['title'])
            ->setDescription($description)
            ->setType('article')
            ->setArticle([
                'published_time' => $product['created_at'],
                'modified_time' => $product['updated_at'],
                'expiration_time' => 'datetime',
                'author' => 'Juntyme MarketPlace',
                'section' => 'produtos',
                'tag' => $metaKeyword
            ]);

        // TwitterCard::setTitle('JuntyMe');
        // TwitterCard::setSite('@LuizVinicius73');

        JsonLd::setTitle($product['title']);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage($product['image']);

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.product', compact(
            'product',
            'faq',
            'store',
            'share',
            'wishlist',
            'popular_products',
            'departaments',
            'comment_fb',
            'fees',
            'footer_products',
            'product_wishlist',
            'variations'
        ));
    }

    /** PAGINA robots */
    public function miniLink(Request $request)
    {

        ## Configuração Loja
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Shere
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        ## Url Comment Fecebook
        $comment_fb = url()->full();

        ## Carregar Produtos
        $product = $this->marketplace->productDetails($request, $store['shop_id']);

        ## CARREGAR ARRAY VARIAÇÕES
        $variations = $this->marketplace->productsAttributesVariation($product);

        ## Verificar Se Existe
        if (is_null($product)) {
            return redirect()->route('mkp.shop')->with('warning', 'Produto não localizado.');
        }


        ## ADD Atualização Produto Aliexpress
        if (!is_null($product['aliexpress_id'])) {
            $this->marketplace->productAliUpdate($product);
        }

        ## Conteudo Perguntas
        $faq = $this->contentFaq();

        ## Taxa Parcelamento
        $fees =  $this->marketplace->taxSistemConfig(true);

        ## Popular Produtos
        $popular_products = true;

        ## Produtos Promocionais Rodapé
        $footer_products = $this->marketplace->rotatingBaseboardProduct($store['shop_id']);

        ## Verificar Lista de Desejos
        $product_wishlist = $this->marketplace->productWishlist($store['id'], $product->id, $client_ip);

        $metaKeyword = $this->marketplace->seoMetaKeyword($product['title']);
        $description = $this->storeAffiliate->short_description($product);

        SEOMeta::setTitle($product['title']);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical(URL::current());
        SEOMeta::addMeta('article:published_time', $product['updated_at']->toW3CString(), 'property');
        SEOMeta::addMeta('article:section', $product['depart_title'], 'property');
        SEOMeta::addKeyword([$metaKeyword]);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($product['title']);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);

        OpenGraph::addImage($product['image']);
        OpenGraph::addImage(['url' => $product['image'], 'size' => 300]);
        OpenGraph::addImage($product['image'], ['height' => 300, 'width' => 300]);

        // article
        OpenGraph::setTitle($product['title'])
            ->setDescription($description)
            ->setType('article')
            ->setArticle([
                'published_time' => $product['created_at'],
                'modified_time' => $product['updated_at'],
                'expiration_time' => 'datetime',
                'author' => 'Juntyme MarketPlace',
                'section' => 'produtos',
                'tag' => $metaKeyword
            ]);

        // TwitterCard::setTitle('JuntyMe');
        // TwitterCard::setSite('@LuizVinicius73');

        JsonLd::setTitle($product['title']);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage($product['image']);

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.product', compact(
            'product',
            'faq',
            'store',
            'share',
            'wishlist',
            'popular_products',
            'departaments',
            'comment_fb',
            'fees',
            'footer_products',
            'product_wishlist',
            'variations'
        ));
    }

    /** ADICIONAR LISTA DE DESEJOS */
    public function addWishlist(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Recuperar IP do Cliente
        $client_ip = $request->ip();

        ## Salvar LIsta de Desejo
        $client_id = Session::has('user_id') ? session('user_id') : null;

        ## Salvar no Banco de Dados
        $newCart = new ClientsWishlist();
        $newCart->store_id = $store['id'];
        $newCart->client_id = $client_id;
        $newCart->product_id = $request->id_wishlist;
        $newCart->client_ip = $client_ip;
        $newCart->save();

        ## Email com formato inválido
        return back()->with('success', 'Produto Adiconar na Lista de Desejos.');
    }

    /** REMOVER PRODUTO DA LISTA DE DESEJOS */
    public function removeWishlist(Request $request, $id)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Carregar Usuario
        $user_id = session('user_id');

        ## Recuperar IP do Cliente
        $client_ip = $request->ip();

        ## Deletar da lista
        ClientsWishlist::where('clients_wishlist.store_id', $store['store_id'])
            ->where(function ($query) use ($user_id,  $client_ip) {
                if (!is_null($user_id))
                    $query->where('clients_wishlist.client_id', '=', $user_id);
                else
                    $query->where('clients_wishlist.client_ip', '=', $client_ip);
            })
            ->where('product_id', $id)
            ->delete();

        return back()->with('success', 'Produto retirado da Lista de Desejos.');
    }

    /** Adicionar ao Carrinho */
    public function addCart(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Varificar se Precisa de Atributo Result True
        $v_product = $this->marketplace->needsAttributes($request);

        ## True precisa de atributos
        if ($v_product == 'required') {
            return  redirect()->route('mkp.product', ['product_id' => $v_product->id, 'slug' => $v_product->slug]);
        }

        ## Recuperar Produto
        $prods = $this->marketplace->addCartProduct($v_product);

        if (!is_null($prods)) {

            ## Recuperar Atributo Variação
            $variation = ProductsAttributesVariation::where('sku', $request->sku_attr)
                ->first();

            ## Recuperar Valor AtuaL
            $result_price = $this->marketplace->addPriceCart($prods, $request, $store['shop_id'], $variation);

            ## Prepar Array Carrinho
            $result_array = $this->marketplace->prepareCartArray($prods, $request, $result_price, $variation);

            ## Verificar Sessão
            if (Session::has('cart')) {

                $products = session('cart');

                $productsId = array_column($products, 'id_cart');

                ## Verificar se existe atributo array
                if (in_array($result_array['id_cart'], $productsId)) {

                    $products = $this->marketplace->incrementCart($result_array['id_cart'], $result_array['qtd'], $result_array['attributes'], $products);

                    ## Adicionar produto na Session
                    Session::put('cart', $products);

                    ## Update Carrinho no Banco de Dados
                    ## Recuperar Key Cart
                    foreach ($products as $value) {
                        $key_cart = $value['key_cart'];
                    }

                    $product_array[] =  $key_cart;

                    $this->marketplace->updateCartBancoDados($product_array, $key_cart);

                    return redirect()->route('mkp.cart');
                }

                ## Adicionar produto na Session
                Session::push('cart', $result_array);
                $key_cart = $result_array['key_cart'];

                ## Preparar Array
                $product[] = $result_array;

                $this->marketplace->updateCartBancoDados($product, $key_cart);

                return redirect()->route('mkp.cart');
            }

            ## Recuperar IP do Cliente
            $client_ip = $request->ip();

            ## Preparar Array
            $product[] = $result_array;

            ## Cria uma nova Session
            Session::put('cart', $product);

            ## Salvar Carrinho no Banco de Dados
            $this->marketplace->saveCartBancoDados($product, $client_ip, $store);
        }

        return redirect()->route('mkp.cart');
    }

    /** Remover Produto do Carrinho  */
    public function removeCart(Request $resquest)
    {
        ## ID cart
        $id = $resquest->id;

        ## Verificar se existe Carrinho
        if (!Session::has('cart')) {
            return redirect()->route('mkp.cart');
        } else {

            $products = session('cart');

            ## Recuperar Key_cart
            foreach ($products as $value) {
                $key_cart = $value['key_cart'];
            }

            $products = array_filter($products, function ($line) use ($id) {
                return $line['id_cart'] != $id;
            });

            ## Verificar Se Carrinho esta Vazio Destroi Session
            if (count($products) > 0) {

                ## Atualizar Session
                Session::put('cart', $products);

                ## Atualizar Banco de Dados
                $this->marketplace->updateCartBancoDados($products, $key_cart);
            } else {

                ## Remover Cart DB
                ClientCartSession::where('key_cart', $key_cart)->delete();

                ## Remover Session
                Session::forget('cart');
            }

            return redirect()->route('mkp.cart');
        }
    }


    /** Carrinho */
    public function cart()
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Salvar Session Cart
        $cart = Session::has('cart') ? session('cart') : [];

        ## Logo
        $logo_main = $store['logo_main'];

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.cart', compact(
            'cart',
            'store',
            'logo_main'
        ));
    }

    /** Carrinho Update */
    public function updateCart(Request $request)
    {

        $product  = $request->qtd;
        $products = session('cart');

        foreach ($product as $key => $value) {

            ## Localizar Produto Ver estoque Disponivel
            $prod = Product::where('products.id', $key)
                ->leftJoin('products_attributes_aliexpress', 'products_attributes_aliexpress.product_id', '=', 'products.id')
                ->select(
                    'products.id',
                    'products.sku',
                    'products.quantity',
                    'products_attributes_aliexpress.product_id as aliexpress_id',
                    'products_attributes_aliexpress.variations'
                )
                ->first();

            ## Verificar Quantidade Juntyme
            if (is_null($prod['aliexpress_id'])) {

                $qtd = ($prod['quantity'] <= $value)
                    ? $prod['quantity']
                    : $value;
                $qtd = ($qtd <= 0)
                    ? 1
                    :  $qtd;
            } else {
                ## Verificar Quantidade Aliexpress
                $qtd = $this->marketplace->aliexpressQuantity($request, $prod, $value);
            }

            ## Atualizar Carrinho
            $products = array_map(function ($line) use ($key, $qtd) {
                if ($key == $line['id']) {
                    $line['qtd'] = (int) $qtd;
                }
                return $line;
            }, $products);

            ## Adicionar produto na Session
            Session::put('cart', $products);
        }


        ## Email com formato inválido
        return back()->with('success', 'Carrinho Atualizado com Sucesso.');
    }

    /** Checkout */
    public function checkout(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }
        ## Verificar Carrinho
        if (empty(Session::has('cart'))) {
            return redirect()->route('mkp.cart');
        }

        ## Salvar Session Cart
        $cart = Session::has('cart') ? session('cart') : [];

        ## Carregar CEP pelo Session Cart
        $zip = $this->marketplace->loadZipSession($cart);

        ## Carregar Pais
        $country = Country::orderBy('country_name_pt', 'asc')->get();
        $callback = $request->callback;

        $locale = $this->marketplace->locale($request->country);

        ## Carregar Usuario Logado
        $user_id = session('user_id');

        $dates = $this->marketplace->AppClientProduct($user_id);

        $logo_main = $store['logo_main'];

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] .  $themes . '.site.web.checkout', compact(
            'cart',
            'country',
            'locale',
            'callback',
            'dates',
            'store',
            'logo_main',
            'zip'

        ));
    }

    /** Página de Login */
    public function login(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Menu Departamentos
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Menuy Share
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        if (session('shops_id')) {
            return redirect()->route('mkp.myAccount');
        }

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.login', compact(
            'store',
            'share',
            'wishlist',
            'departaments'
        ));
    }

    /** Fazer Login Cliente */
    public function loginDo(Request $request)
    {

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {

            ## Email com formato inválido
            return back()->with('error', 'E-mail informado não é válido.');
        }

        ## Configuração Store;
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $mail = $request->get('email');

        $userLogin = AppClient::where('clients.email', $mail)
            ->where('clients_stores_mm.store_id', $store['shop_id'])
            ->join('clients_stores_mm', 'clients.id', '=', 'clients_stores_mm.client_id')
            ->select(
                'clients.*',
                'clients_stores_mm.*'
            )
            ->first();

        if ($userLogin && Hash::check($request->get('password'), $userLogin->password)) {

            if (Auth::guard('client')->loginUsingId($userLogin->id)) {

                Session::put(['user_id' => $userLogin->id, 'user_name' => $userLogin->name, 'shops_id' => $store['shop_id']]);
                return redirect()->route('mkp.myAccount');
            }

            return back()->with('error', 'Houve um erro ao acessar tente novamente.');
        }

        return back()->with('error', 'E-mail não cadastrado ou inválido.');
    }

    /** Criar Conta */
    public function   registerNew(Request $request)
    {

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            ## Email com formato inválido
            return back()->with('error', 'Email informado não é valido');
        }

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $mail = $request->get('email');
        $id = $store['id'];

        $userLogin = AppClient::where('clients.email', $mail)
            ->where('clients_stores_mm.store_id', $store['shop_id'])
            ->join('clients_stores_mm', 'clients.id', '=', 'clients_stores_mm.client_id')
            ->select(
                'clients.*',
                'clients_stores_mm.*'
            )
            ->first();

        if (isset($userLogin->id)) {
            ## Cliente já exite no Banco de Dados
            return back()->with('warning', 'Cliente já cadastrado, fazer login.');
        }

        ## Cliente não exite no Banco de Dados
        $verific_mail = AppClient::where('clients.email', $mail)->first();

        if (isset($verific_mail)) {

            ## Relacionar com a Loja
            $stores_mm = new ClientsStoresMM();
            $stores_mm->client_id = $verific_mail->id;
            $stores_mm->store_id = $id;
            $stores_mm->status_id = 1;
            $stores_mm->password = Hash::make($request->password);
            $stores_mm->save();

            ## Recuperar id cliente para login auth
            $user_id = $verific_mail->id;
            $user_name =  $verific_mail->name;
        } else {

            ## Cliar Conta de Cliente
            $client = new AppClient();
            $client->status_id = 1;
            $client->name = $request->name;
            $client->email = $request->email;
            $client->remember_token =  Str::random(80);
            $client->save($request->except(['_token']));

            ## Relacionar com a Loja
            $stores_mm = new ClientsStoresMM();
            $stores_mm->client_id = $client->id;
            $stores_mm->store_id = $store['shop_id'];
            $stores_mm->status_id = 1;
            $stores_mm->password = Hash::make($request->password);
            $stores_mm->save();

            ## Recuperar id cliente para login auth
            $user_id = $client->id;
            $user_name =  $client->name;
        }

        if (Auth::guard('client')->loginUsingId($user_id)) {

            ## Envio por Jobs
            try {
                // ClientAchaddoNew::dispatch($client)->delay(now()->addSeconds('1'));
                Mail::send(new ClientAchaddoNew($client));
            } catch (Exception $e) {

                // return redirect()->route('mkp.myAccount')->with('error', 'Email não pode ser enviado para confirmação entre em contato com Nosso Suporte.');
            }


            ## Acesso com sucesso
            Session::put(['user_id' => $user_id, 'user_name' => $user_name, 'shops_id' => $store['shop_id']]);
            return redirect()->route('mkp.myAccount');
        }

        ## Erro ao acessar
        return back()->with('error', 'Houve um erro ao acessar tente novamente.');
    }


    /** Recuperar Senha Cliente */
    public function  confirmEmail(Request $request, $token)
    {

        $verific = AppClient::where('remember_token', $token)
            ->first(['id']);

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Menu Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Menu Share
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        $themes = $this->marketplace->originThemes($store);

        return view(
            $store['route'] .  $themes . '.site.web.confirmEmail',
            compact('store', 'departaments', 'verific')
        );
    }

    /** Recuperar Senha Cliente */
    public function tokenNew(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $client = AppClient::where('clients.email', $request->email)
            ->join('clients_stores_mm', 'clients_stores_mm.client_id', '=', 'clients.id')
            ->where('clients_stores_mm.store_id', $store['id'])
            ->first();

        if (!empty($client)) {

            ## gerar novo token
            $client->remember_token =  Str::random(80);
            $client->save();

            return back()->with('success', ' Token enviado com sucesso para o email cadastrado.');
        }

        return back()->with('error', 'Erro! E-mail informado não esta cadastrado na Loja.');
    }

    /** Recuperar Senha Cliente */
    public function recoverPassword()
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        $themes = $this->marketplace->originThemes($store);

        return view(
            $store['route'] . $themes . '.site.web.recoverPassword',
            compact(
                'store',
                'departaments'
            )
        );
    }

    /** Página Registro */
    public function passwordNew(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $client = AppClient::where('clients.email', $request->email)
            ->join('clients_stores_mm', 'clients_stores_mm.client_id', '=', 'clients.id')
            ->where('clients_stores_mm.store_id', $store['id'])
            ->first();

        if (!empty($client)) {

            ## gerar novo token
            $client->remember_token =  Str::random(80);
            $client->save();

            ## Enviar Email
            try {
                // ClientAchaddoNewPassword::dispatch($client)->delay(now()->addSeconds('1'));
                Mail::send(new ClientAchaddoNewPassword($client));
            } catch (Exception $e) {
                // return back()->with('error', 'Email não pode ser enviado para confirmação entre em contato com Nosso Suporte.');
            }

            return back()->with('success', 'E-mail enviado para alteração da Senha.');
        }

        return back()->with('error', 'Erro! E-mail informado não esta cadastrado na Loja.');
    }

    /** Alterar Senha */
    public function  changePassword(Request $request, $token)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        $client = AppClient::where('remember_token', $token)->first();

        if (!empty($client)) {

            ## $client->markEmailAsVerified();
            $client->remember_token =  Str::random(80);
            $client->save();

            $themes = $this->marketplace->originThemes($store);

            return view(
                $store['route'] . $themes . '.site.web.changePassword',
                compact(
                    'store',
                    'departaments',
                    'client',
                    'token'
                )
            );
        }

        return redirect()->route('mkp.recoverPassword')->with('error', 'Erro! Token inválido solicite um novo.');
    }

    /** Alterar Senha */
    public function  passwordNewDo(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ClientsStoresMm::where('client_id', $request->clientID)
            ->where('store_id', $store['id'])
            ->update(['password' => Hash::make($request->password)]);

        return redirect()->route('mkp.login')->with('success', '<i data-feather="thumbs-up"></i> Senha Atualizada com Sucesso.');
    }

    /** Página Registro */
    public function register(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Menu Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Menu Share
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        $themes = $this->marketplace->originThemes($store);

        $imBlade = session('shops_id') ? $themes . '.site.web.myAccount' : $themes . '.site.web.register';

        return view($store['route'] . $imBlade, compact(
            'store',
            'share',
            'wishlist',
            'departaments'
        ));
    }


    /** CONSULTAR RASTREIO PAGINA DO PRODUTO */
    public function calculateShipping(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();
        $product_id = (int)$request->id;

        ## Carregar Produtos
        $product  = $this->marketplace->calculateShipping($product_id);

        if (is_null($product)) {
            ## Resposta Frete
            $response['success'] = false;
            $response['message'] =  'Consulta sem resultado';

            return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        ## CALCULO DE FRETE DROPIFY
        if (!is_null($product['dropify_sku'])) {

            // $result_final = $this->dropify->freightDropify($product, $request, $store);

            // ## Resposta Frete
            // if ($result_final) {
            //     $response['success'] = true;
            //     $response['message'] =  $result_final;
            // } else {
            //     $response['success'] = false;
            //     $response['message'] =  'Consulta sem resultado';
            // }

            // return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }


        ## CALCULO FRETE ALIEXPRESS
        if (!is_null($product['aliexpress_id'])) {

            ## Carregar Resultado ALiexpress Classes
            $result_final = $this->Aliexpress->freeAliexpress($product['aliexpress_id']);

            ## Resposta Frete
            $response['success'] = true;
            $response['message'] =  $result_final;

            return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        if ($product['shops_correios_status_id'] == 2) {

            ## CALCULO FRETE CORREIOS SEM CONTRATO
            $result_correios = $this->Shipping->correiosSemContrato($product, $request, $store);
        } else {

            ## CALUCLO FRETE FACIL COM CONTRATO
            $result_correios = $this->Shipping->correiosSigepContrato($product, $request, $store);
        }

        ## CALCULO FRETE TRANSPORTADORA
        $result_table = $this->Shipping->tabelaTransportadora($product, $request, $store) ?? null;

        if (is_null($result_table) && is_null($result_correios)) {
            ## Resposta Frete
            $response['success'] = false;
            $response['message'] =  'Servidor indisponível no momento tente mais tarde';

            return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }


        ## Agrupar Arrays
        $result_final = array_merge($result_table ?? [], $result_correios ?? []);

        ## Resposta Frete
        $response['success'] = true;
        $response['message'] =  $result_final;

        return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /** Consultar Rastreio */
    public function calculateShippingCheckout(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        if ($request->get('cep')) {

            ## Carrrinho Existe
            if (!empty(Session::has('cart'))) {

                $cart = session('cart');

                ## GroupBy Array
                $group = array();
                foreach ($cart as $value) {
                    $group[$value['shop_id']][] = $value;
                }

                ## Calcula Frete
                $result_final = [];

                foreach ($group as $list => $value) {

                    ## Recuperar ID SHOP
                    $shops = $this->marketplace->shopsAddress($list);

                    ## Verificar CEP Cross Docking
                    foreach ($value as $item) {
                        $origin_zip_code = $item['origin_zip_code'];
                    }


                    $origin_zip_code = $item['origin_zip_code'] ? $item['origin_zip_code'] : $shops['zip_code'];

                    $data = [
                        'store_id' => $store['shop_id'],
                        'shop_id' =>  $list,
                        'shop_name' => $shops['title'],
                        'cep_client' => str_replace('-', '', $request->get('cep')),
                        'cep_shops' => $origin_zip_code,
                        'value' => $value,
                        'shops_correios_status_id' => $shops['shops_correios_status_id'],
                        'shops_correios_type_id' => $shops['shops_correios_type_id'],
                        'shops_correios_format_id' => $shops['shops_correios_format_id'],
                        'usuario' => $shops['usuario'],
                        'password' => $shops['password'],
                        'cod_administrativo' => $shops['cod_administrativo'],
                        'numero_contrato' => $shops['numero_contrato'],
                        'cartao_postagem' => $shops['cartao_postagem'],
                        'cnpj_empresa' => $shops['cnpj_empresa'],
                        'ano_contrato' => $shops['ano_contrato'],
                        'diretoria' => $shops['diretoria'],
                        'extra_term' => $shops['extra_term'],
                    ];

                    ## CALCULO FRETE PRODUTO DIGITAL
                    $result_product_digital = $this->Shipping->productDigital($data);

                    ## CALCULO FRETE ALIEXPRESS
                    $result_aliexpress = $this->Aliexpress->aliexpressFreightCheckout($data, $result_product_digital);



                    if ($shops['shops_correios_status_id'] == 2) {

                        ## CALCULO FRETE CORREIOS SEM CONTRATO
                        $result_correios = $this->Shipping->correiosSemContratoCheckout($data, $result_aliexpress);
                    } else {

                        ## CALCULO FRETE CORREIOS SIGEP COM CONTRATO
                        $result_correios = $this->Shipping->correiosSigepContratoCheckout($data, $result_aliexpress, $result_product_digital);
                    }


                    ## CALCULO FRETE TRANSPORTADORA
                    $result_table = $this->Shipping->checkoutCarrierTable($data, $result_aliexpress, $result_product_digital);

                    ## RESULTADO PRODUTOS INTENACIONAIS
                    if ($result_correios == null && $result_table == null && $result_product_digital == null) {
                        $result_final += [
                            $shops['title'] => [
                                $result_aliexpress
                            ]
                        ];
                    } else if ($result_correios == null && $result_table == null && $result_aliexpress == null) {
                        $result_final += [
                            $shops['title'] => [
                                $result_product_digital
                            ]
                        ];
                    } else if ($result_correios == null && $result_table == null) {
                        $result_final += [
                            $shops['title'] => [
                                $result_aliexpress
                            ]
                        ];
                    } else {
                        $result_final += [
                            $shops['title'] => [
                                $result_correios,
                                $result_table
                            ]
                        ];
                    }
                }

                if (!empty($result_final)) {
                    Session::put('shipping', $result_final);
                }
            }
        }
        ## Resposta Frete
        $response['success'] = true;
        $response['message'] =  (!empty($result_final))
            ? $result_final
            : 'Servidor indisponível no momento tente mais tarde';

        return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /** Paginas De Informações */
    public function pages(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Menu Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Menu Share
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        $content = StoresPagesConfig::where('slug', $request->slug)->first();

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] .      $themes . '.site.web.pages', compact(
            'store',
            'share',
            'wishlist',
            'content',
            'departaments'
        ));
    }


    /** Consultar Rastreio */
    public function helpCenter(Request $request)
    {

        $dateForm = $request->except('_token');

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Menju Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Menu Share
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        ## Fitlro
        $filter = $this->marketplace->filterDateForm($dateForm);

        ## Conteudo Perguntas e Dúvidas
        $help_center = StoresHelpCenter::where(function ($help) use ($filter) {
            $help->where('title', 'ILIKE', '%' . $filter . '%')
                ->orWhere('description', 'ILIKE', '%' . $filter . '%');
        })->paginate(10);

        $explode = explode(" ", $store['title']);
        $metaKeyword = '';
        foreach ($explode as $item) {
            $metaKeyword .=     $item . ',';
        }

        SEOMeta::setTitle($store['title']);
        SEOMeta::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        SEOMeta::setCanonical(URL::current());
        SEOMeta::addKeyword(['MarketPlace', 'Loja Virtual', $metaKeyword]);

        OpenGraph::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        OpenGraph::setTitle($store['title']);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);

        // TwitterCard::setTitle('JuntyMe');
        // TwitterCard::setSite('@LuizVinicius73');

        JsonLd::setTitle($store['title']);
        JsonLd::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        JsonLd::addImage(asset($store['logo_main']));

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.helpCenter', compact(
            'store',
            'departaments',
            'help_center',
            'dateForm'
        ));
    }

    /** Consultar Rastreio */
    public function error(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.error');
    }

    /** Consultar Rastreio */
    public function feedGoogle(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## SHOP ID
        $shop_id = $store['shop_id'];

        ## Recuperar URL
        $url = URL::to('/');

        ## Verificar Dominio
        if (is_null($store)) {
            return 'URL Não esta cadastrada em nosso Sistema.';
        }

        $lang = 'br';
        GoogleShopping::categories($lang);
        GoogleShopping::title($store['title']);
        GoogleShopping::link($url);
        GoogleShopping::description('Nosso feed do Google Shopping');

        ## Shere da Loja
        $products = $this->marketplace->feedGoogleProduct($store, $shop_id);

        foreach ($products as $product) {

            $item = GoogleShopping::createItem();
            $item->id($product->id);

            $title = strtolower(Str::limit($product->title, 146, $end = '...'));
            $item->title($title);

            $description = strip_tags(trim(html_entity_decode($product->description,   ENT_QUOTES, 'UTF-8'), "\xc2\xa0"));

            // ----- remove control characters -----
            $description  = str_replace("\r", '', $description);    // --- replace with empty space
            $description = str_replace("\n", ' ', $description);   // --- replace with space
            $description = str_replace("\t", ' ', $description);   // --- replace with space
            $description =  Str::limit($description, 300, $end = '...');

            if (str_Word_count($description) < 5) {
                $description = $title;
            }

            $item->description($description);

            $item->mpn($product->sku);
            $item->color($product->color);
            $item->price($product->price);

            if ($product->special_price > 0 && $product->special_price <= $product->price)
                $item->sale_price($product->special_price);
            $item->link($url . '/produto/' . $product->id . '/' . $product->slug);

            $ship_cal = $product->price / 100;
            if (
                $ship_cal > 12 && $ship_cal < 150
            ) {
                $ship = (float)number_format($ship_cal, 2);
            } elseif (($ship_cal > 150)) {
                $ship = 120.00;
            } else {
                $ship = 12.00;
            }

            $item->shipping('BRA', 'Correios',  $ship, $region = null);

            if ($product->image_type) {
                $item->image_link($product->image);
            } else {
                $item->image_link(config('app.url') . '/storage/' . $product->image);
            }

            switch ($product->condition_id) {
                case 1:
                    $condition = "new"; // Novo
                    break;
                case 2:
                    $condition = "used"; // Usado
                    break;
                case 3:
                    $condition = "new"; // Digital
                    break;
                case 4:
                    $condition = "refurbished"; // Recondicionado
                    break;
                case 5:
                    $condition = "refurbished"; // Remodelado
                    break;
            }

            $item->condition($condition);
            $item->availability('in_stock');
            if (!empty($product->gtin)) {
                $item->gtin($product->gtin);
            }
            if (!empty($product->brand_title)) {
                $item->brand($product->brand_title);
            }

            if (!is_null($product->category_google)) {
                $item->google_product_category($product->category_google);
            }
        }

        ## boolean value indicates output to browser
        GoogleShopping::asRss(true);
    }

    /** Consultar Rastreio */
    public function feedFacebook(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## SHOP ID
        $shop_id = $store['shop_id'];

        ## Recuperar URL
        $url = URL::to('/');

        /// Verificar Dominio
        if (is_null($store)) {
            return 'URL não esta cadastrada em nosso sistema.';
        }

        GoogleShopping::title($store['title']);
        GoogleShopping::link($url);
        GoogleShopping::description('Nosso feed do Facebook / Instagram');

        ## Shere da Loja
        $products = $this->marketplace->feedFacebookProduct($store, $shop_id);

        foreach ($products as $product) {
            $item = GoogleShopping::createItem();
            $item->id($product->id);

            $title = strtolower(Str::limit($product->title, 146, $end = '...'));
            $item->title($title);

            $description = strip_tags(trim(html_entity_decode($product->description,   ENT_QUOTES, 'UTF-8'), "\xc2\xa0"));

            // ----- remove control characters -----
            $description  = str_replace("\r", '', $description);    // --- replace with empty space
            $description = str_replace("\n", ' ', $description);   // --- replace with space
            $description = str_replace("\t", ' ', $description);   // --- replace with space
            $description =  Str::limit($description, 250, $end = '...');

            if (str_Word_count($description) < 5) {
                $description = $title;
            }

            $item->description(strtolower($description));

            $item->mpn($product->sku);
            $item->color($product->color);
            $item->price($product->price);

            if ($product->special_price > 0 && $product->special_price <= $product->price)
                $item->sale_price($product->special_price);
            $item->link($url . '/produto/' . $product->id . '/' . $product->slug);
            if ($product->image_type) {
                $item->image_link($product->image);
            } else {
                $item->image_link(config('app.url') . '/storage/' . $product->image);
            }


            switch ($product->condition_id) {
                case 1:
                    $condition = "new"; // Novo
                    break;
                case 2:
                    $condition = "used"; // Usado
                    break;
                case 3:
                    $condition = "new"; // Digital
                    break;
                case 4:
                    $condition = "refurbished"; // Recondicionado
                    break;
                case 5:
                    $condition = "refurbished"; // Remodelado
                    break;
            }

            $item->condition($condition);
            $item->availability('in stock');
            if (!empty($product->gtin)) {
                $item->gtin($product->gtin);
            }
            if (!empty($product->brand_title)) {
                $item->brand($product->brand_title);
            }

            if (!is_null($product->category_google)) {
                $item->google_product_category($product->category_google);
            }
        }

        ## boolean value indicates output to browser
        GoogleShopping::asRss(true);
    }

    /** Consultar Rastreio */
    public function pageDepartaments(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Menu Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Menu Share
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        $shopID = $store['shop_id'];

        $page_departaments = $departaments;


        $explode = explode(" ", $store['title']);
        $metaKeyword = '';
        foreach ($explode as $item) {
            $metaKeyword .=     $item . ',';
        }

        SEOMeta::setTitle($store['title']);
        SEOMeta::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        SEOMeta::setCanonical(URL::current());
        SEOMeta::addKeyword(['MarketPlace', 'Loja Virtual', $metaKeyword]);

        OpenGraph::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        OpenGraph::setTitle($store['title']);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);

        // TwitterCard::setTitle('JuntyMe');
        // TwitterCard::setSite('@LuizVinicius73');

        JsonLd::setTitle($store['title']);
        JsonLd::setDescription('MarketPlace Loja Virtual ' . $store['title']);
        JsonLd::addImage(asset($store['logo_main']));

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.departaments', compact(
            'store',
            'share',
            'wishlist',
            'departaments',
            'page_departaments'
        ));
    }

    /** Consultar Rastreio CheckoutRequest */
    public function newOrder(CheckoutRequest $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        if (empty($request->frete)) {
            return back()->with('warning', 'Selecione o Tipo de Envio.')->withInput();
        }

        ## Salvar Frete
        Session::put('frete', $request->frete);

        ## Recuperar Usuario
        $user_id = session('user_id');

        $verific_document = AppClient::where('clients_stores_mm.store_id', $store['id'])
            ->where('clients.email', $request->email)
            ->join('clients_stores_mm', 'clients.id', '=', 'clients_stores_mm.client_id')
            ->first(['id']);


        ## Verificar Cliente no Sistema
        if (!empty($verific_document->id) && is_null($user_id)) {

            return back()->with('error', 'Cliente Já Cadastrado, Verifique seu e-mail. Se já tem cadastro faça Login no Sistema.')->withInput();
        }

        ## Cadastrar Cliente
        if (is_null($user_id)) {

            $email_verific = AppClient::where('email', $request->email)
                ->first(['id', 'name']);

            if (empty($email_verific)) {

                ## Criar Novo Cliente
                $client = $this->marketplace->newClient($request);
            }

            $clientID = (!empty($client->id)) ? $client->id : $email_verific['id'];
            $clientName = (!empty($client->name)) ? $client->name : $email_verific['name'];

            ## Relacionar Cliente com a Loja
            $this->marketplace->ClientsStoresMM($clientID, $store['id']);

            ## Criar Endereço CLiente
            $this->marketplace->ClientsAddress($request, $clientID);

            ## Acessar Sistema
            if (Auth::guard('client')->loginUsingId($clientID)) {
                Session::put(['user_id' => $clientID, 'user_name' =>  $clientName, 'shops_id' => $store['id']]);
            }
        }


        $address = [
            'name' => $request->name,
            'email' => $request->email,
            'celular' => $request->celular,
            'data_nascimento' => $request->data_nascimento,
            'cpfcnpj' => $request->cpfcnpj,
            'cep' => $request->cep,
            'rua' => $request->rua,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'bairro' => $request->bairro,
            'cidade' => $request->cidade,
            'uf' => $request->uf,
            'country' =>  $request->pais,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'pais' => $request->pais,
        ];

        Session::put('address', $address);

        ## Pedido Criado com sucesso
        return redirect()->route('mkp.payment');
    }


    /** CHECKOUT PAGAMENTO */
    public function payment(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Pagarme
        $pagarme = new \PagarMe\Client(config('juntyme.pm_chave_de_api'));

        ## Criar Order Shop
        $cart = Session::has('cart') ? session('cart') : [];
        $frete = Session::has('frete') ? session('frete') : [];


        if (!empty($cart)) {

            ## Taxa Parcelamento
            $fees =  $this->marketplace->taxSistemConfig(true);

            $soma = 0;
            foreach ($cart as $key => $value) {
                $soma = $soma + ($value['price'] * $value['qtd']);
            }

            ## Calcular Frete
            $frete = $this->marketplace->calcularFrete();

            $total_price = $frete + $soma;
            $amount = (float)number_format(($total_price * 100), 0, '', '');

            $parcelas = $pagarme->transactions()->calculateInstallments([
                'amount' => $amount,
                'free_installments' => 0,
                'max_installments' => 12,
                'interest_rate' => $fees
            ]);

            $logo_main = $store['logo_main'];

            $themes = $this->marketplace->originThemes($store);

            return view($store['route'] . $themes . '.site.web.payment', compact(
                'store',
                'cart',
                'frete',
                'parcelas',
                'total_price',
                'logo_main'
            ));
        }

        return redirect()->route('mkp.checkout');
    }


    /** CHECKOUT PAGAMENTO */
    public function paymentReprocess(Request $request, $orderID)
    {

        $orderID = Encript::desencriptar($orderID);

        if (is_null($orderID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## Carregar Pagarme
        $pagarme = new \PagarMe\Client(config('juntyme.pm_chave_de_api'));

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        $order = OrdersMaster::where('orders_master.id', $orderID)
            ->where('orders_master.origin_shop_id', $store['shop_id'])
            ->where('orders_master.order_status_id', '<', '3')
            ->join('order_shop', 'order_shop.orders_master_id', '=', 'orders_master.id')
            ->join('order_shop_shipping', 'order_shop_shipping.order_shop_id', '=', 'order_shop.id')
            ->select('orders_master.id', DB::raw('SUM(order_shop.vlr_total) AS soma_total'), DB::raw('SUM(order_shop_shipping.vlr_frete) AS soma_frete'))
            ->groupBy('orders_master.id')
            ->first();


        if (!empty($order)) {

            ## Taxa Parcelamento
            $fees =  $this->marketplace->taxSistemConfig(true);

            $total_price = $order['soma_total'] + $order['soma_frete'];
            $amount = (float)number_format(($total_price * 100), 0, '', '');

            $parcelas = $pagarme->transactions()->calculateInstallments([
                'amount' => $amount,
                'free_installments' => 0,
                'max_installments' => 12,
                'interest_rate' => $fees
            ]);

            $logo_main = $store['logo_main'];

            $themes = $this->marketplace->originThemes($store);

            return view($store['route'] . $themes . '.site.web.paymentReprocess', compact(
                'store',
                'order',
                'parcelas',
                'total_price',
                'logo_main'
            ));
        }

        return redirect()->route('mkp.checkout');
    }

    /** SALVAR ITENS NAS ORDEM */
    public function newItens($dataIten, $shopID)
    {
        $taxs = $this->marketplace->taxSistemConfig();
        $tax_commission = $taxs['tax_commission_juntyme'];
        $tax_order = $taxs['tax_order_juntyme'];

        $qtd_total = 0;
        $peso_bruto = 0;

        $vlr_subtotal_affiliate = 0;
        $vlr_total_affiliate = 0;

        $vlr_subtotal_juntyme = 0;
        $vlr_total_juntyme = 0;

        $vlr_subtotal = 0;
        $amount_rules_total = $dataIten['amount_rules_total'];

        $payment_products = [];

        foreach ($dataIten['value'] as $list) {

            ## Calculo Price Produtos
            $price =  $list['price'];
            $qtd_total =   $qtd_total + $list['qtd'];
            $vlr_subtotal = $vlr_subtotal + $price * $list['qtd'];

            if ($list['shop_id'] <> $shopID) {

                ## Calculo Afiliados Produtos
                $vlr_affiliate = (($list['commission_value'] * ($price * $list['qtd'])) / 100);
                $vlr_subtotal_affiliate = $vlr_subtotal_affiliate + $vlr_affiliate;
                $vlr_total_affiliate =  $vlr_total_affiliate + $vlr_affiliate;

                ## Calculo Juntyme Produtos
                $vlr_juntyme = ((($price * $list['qtd']) * $tax_commission) / 100) + $tax_order;
                $vlr_subtotal_juntyme =   $vlr_subtotal_juntyme + $vlr_juntyme;
                $vlr_total_juntyme = $vlr_total_juntyme +  $vlr_juntyme;
            } else {

                ## Calculo Juntyme Produtos
                $vlr_juntyme = ((($price * $list['qtd']) * ($tax_commission + $list['commission_value'])) / 100) + $tax_order;
                $vlr_subtotal_juntyme =   $vlr_subtotal_juntyme + $vlr_juntyme;
                $vlr_total_juntyme = $vlr_total_juntyme +  $vlr_juntyme;
            }

            ## valor total pagamer
            $amount_rules_total = $amount_rules_total + ($price * $list['qtd']);
            $attributes = (!empty($list['attributes'])) ? json_encode($list['attributes'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : null;
            $peso_bruto = $peso_bruto +  ($list['qtd'] * $list['weight']);


            ## Criar Shop Itens
            $orderItens = new OrderShopIten();
            $orderItens->order_shop_id = $dataIten['orderShopId'];
            $orderItens->product_id = $list['id'];
            $orderItens->sku = $list['sku'];
            $orderItens->sku_variations = $list['sku_variation'] ?? null;
            $orderItens->sku_variations_aliexpress = $list['sku_aliexpress'] ?? null;
            $orderItens->un = 1;
            $orderItens->qtd = $list['qtd'];
            $orderItens->vlr_unit = $price;
            $orderItens->vlr_desconto = 0.00;
            $orderItens->attributes = $attributes;
            $orderItens->save();


            if (!is_null($list['sku_aliexpress'])) {

                ## Relacionar Produtos com Aliexpress
                $iten_ali = new OrderShopAliexpress();
                $iten_ali->order_shop_id = $dataIten['orderShopId'];
                $iten_ali->product_id = $list['id'];
                $iten_ali->sku_aliexpress = $list['sku_aliexpress'];
                $iten_ali->save();
            }


            ## Payment Produtos Pagarme
            array_push($payment_products, [
                'id' => strval($list['id']),
                'title' => strval($list['title']),
                'unit_price' => (float)number_format(($price * 100), 0, '', '') + 0,
                'quantity' => $list['qtd'],
                'tangible' => true
            ]);
        }

        $dadasItens = [
            'qtd_total' => $qtd_total,
            'peso_bruto' => $peso_bruto,
            'vlr_subtotal_affiliate' => $vlr_subtotal_affiliate,
            'vlr_total_affiliate' => $vlr_total_affiliate,
            'vlr_subtotal_juntyme' => $vlr_subtotal_juntyme,
            'vlr_total_juntyme' => $vlr_total_juntyme,
            'amount_rules_total' => $amount_rules_total,
            'payment_products' => $payment_products,
            'vlr_subtotal' => $vlr_subtotal
        ];


        return $dadasItens;
    }

    /** UPDATE VALORES ORDER */
    public function updateOrderShop($orderShopId, $price_subtotal, $price_total)
    {
        ## Atualizar Order
        OrderShop::where('id', $orderShopId)
            ->update([
                'vlr_subtotal' => $price_subtotal,
                'vlr_total' => $price_total
            ]);
    }

    /** SALVAR FRETE ORDERS */
    public function orderShipping($dataShip)
    {
        ## Recuperar ID Cliente
        $clientID =  session('user_id');

        ## Session Frete
        $frete = session('frete');

        $vlr_frete_total = 0;
        $vlr_frete_total_juntyme = 0;
        foreach ($frete as $key => $value) {
            if ($key == $dataShip['key']) {

                ## Verificar Frete Pagamento
                $explode = explode(':', $value);

                if ($explode[0] == 'PAC' || $explode[0] == 'Sedex') {

                    if ($explode[3] == 'frete_facil') {
                        ## Recebimento Juntyme
                        $vlr_frete_total_juntyme = $vlr_frete_total_juntyme + $explode[1];
                        $amount_rules_frete_juntyme =  $dataShip['amount_rules_frete_juntyme'] + $explode[1];
                    } else {
                        ## Recebimento Cliente
                        $vlr_frete_total = $vlr_frete_total + $explode[1];
                        $amount_rules_frete =  $dataShip['amount_rules_frete'] + $explode[1];
                    }

                    ## Valor Total para Pagamer

                    $company = ($explode[0] == 'PAC' || $explode[0] == 'Sedex')
                        ? 'Correios'
                        : $explode[0];
                    $tipo_frete = (!empty($explode[3])) ? $explode[3] : $explode[0];
                    $servico_correios = $explode[0];
                } else {

                    $vlr_frete_total = $vlr_frete_total + $explode[1];

                    ## company name
                    switch ($explode[0]) {
                        case 'Frete Internacional':
                            $name = 'FreteAliexpress';
                            break;
                        default:
                            $name = $explode[0];
                    }

                    ## Valor Total para Pagamer
                    $amount_rules_frete =  $dataShip['amount_rules_frete'] + $explode[1];
                    $company =  $name;
                    $tipo_frete = $name;
                    $servico_correios = $name;
                }


                ## Salvar Frete
                $orderShipping = new OrderShopShipping();
                $orderShipping->client_id = $clientID;
                $orderShipping->order_shop_id = $dataShip['orderShopId'];
                $orderShipping->company = $company;
                $orderShipping->tipo_frete = $tipo_frete;
                $orderShipping->servico_correios = $servico_correios;
                $orderShipping->peso_bruto = $dataShip['peso_bruto'];
                $orderShipping->qtde_volumes = $dataShip['qtde_volumes'];
                $orderShipping->vlr_frete = $explode[1];
                $orderShipping->vlr_desconto = 0.0;
                $orderShipping->obs = null;
                $orderShipping->deadline =   $explode[2];
                $orderShipping->save();
            }
        }


        $dataShipping = [
            'vlr_frete_total' => $vlr_frete_total ?? 0,
            'amount_rules_frete' =>  $amount_rules_frete ?? 0,
            'vlr_frete_total_juntyme' => $vlr_frete_total_juntyme ?? 0,
            'amount_rules_frete_juntyme' => $amount_rules_frete_juntyme ?? 0,
        ];

        return $dataShipping;
    }

    /** FUNÇÃO SALVAR COMISSÃO AFILIADOS */
    public function OrderShopCommission($dataAffiliate)
    {

        $commission = new OrderShopCommission();
        $commission->orders_id = $dataAffiliate['orderShopId'];
        $commission->orders_status_id = 1; /// Pagamento Pendente
        $commission->affiliate_shop_id = $dataAffiliate['shopID'];
        $commission->commission_type_id = $dataAffiliate['commission_type_id']; /// 3 Comisssão Afiliados
        $commission->vlr_subtotal = $dataAffiliate['vlr_subtotal'];
        $commission->vlr_total = $dataAffiliate['vlr_total'];
        $commission->vlr_desconto = 0.00;
        $commission->payment = 'pagarme';
        $commission->obs     = null;
        $commission->obs_internas = null;
        $commission->save();
    }



    /** PREPARAR DADOS PAGARME */
    public function dataPagarme($shopID, $request)
    {

        $orderID = !empty($request->orderID) ? Encript::desencriptar($request->orderID) : null;

        $clientID =  session('user_id');

        if (session('address')) {
            $address = session('address');
        } else {
            $address  = OrderShop::where('order_shop.orders_master_id', $orderID)
                ->where('order_shop.affiliate_shop_id', $shopID)
                ->where('orders_status_id', '<', '3')
                ->join('order_shop_client', 'order_shop_client.orders_master_id', '=', 'order_shop.orders_master_id')
                ->join('order_shop_client_address', 'order_shop_client_address.order_master_id', '=', 'order_shop.orders_master_id')
                ->select(
                    'order_shop_client.client_id',
                    'order_shop_client.orders_master_id',
                    'order_shop_client.name',
                    'order_shop_client.tipoPessoa',
                    'order_shop_client.cpf_cnpj',
                    'order_shop_client.ie',
                    'order_shop_client.rg',
                    'order_shop_client_address.address',
                    'order_shop_client_address.complement',
                    'order_shop_client_address.district',
                    'order_shop_client_address.zip_code',
                    'order_shop_client_address.city',
                    'order_shop_client_address.uf',
                    'order_shop_client_address.cell_phone',
                    'order_shop_client_address.email',
                    'order_shop_client_address.number',
                    'order_shop_client_address.country',
                )
                ->first();
        }


        ## Tratamento de Dados Pagarme
        $cpfcnpj = $address['cpfcnpj'] ?? $address['cpf_cnpj'];
        $type = (strlen($cpfcnpj) <= 15) ? 'individual' : 'corporation';
        $documents_type = (strlen($cpfcnpj) <= 15) ? 'cpf' : 'cnpj';
        $documents_number = preg_replace("/[^0-9]/", "", $cpfcnpj);

        $phone_numbers = '+55' . preg_replace("/[^0-9]/", "", $address['celular'] ?? $address['cell_phone']);
        $country = 'br';
        $zipcode = preg_replace("/[^0-9]/", "", $address['cep'] ?? $address['zip_code']);
        $complementary = (!empty($address['complemento'])) ? $address['complemento'] : '-';
        $pix_expiration_date = date('Y/m/d', strtotime('+3 days'));

        ## Endereço de Entrega
        $dataPagarme = [
            'orders_master_id' => $orderID ?? null,
            'pix_expiration_date' => $pix_expiration_date,
            'billing' => [
                'name' => $address['name'],
                'address' => [
                    'country' => 'br',
                    'street' => $address['rua'] ??  $address['address'],
                    'complementary' =>  $complementary ??  $address['complement'],
                    'street_number' => $address['numero'] ??  $address['number'],
                    'state' => $address['uf'],
                    'city' => $address['cidade'] ?? $address['city'],
                    'neighborhood' => $address['bairro'] ??  $address['district'],
                    'zipcode' => $zipcode
                ]
            ],

            'customer' => [
                'external_id' => strval($clientID),
                'name' => $address['name'],
                'email' => $address['email'],
                'type' =>  $type,
                'country' => $country,
                'documents' => [
                    [
                        'type' => $documents_type,
                        'number' => $documents_number
                    ]
                ],
                'phone_numbers' => [strval($phone_numbers)]
            ],
        ];

        return $dataPagarme;
    }

    /** PREPARAR AMOUNT PAGARME */
    public function amountPagarme($dataAmount)
    {
        ## Taxa Parcelamento
        $fees =  $this->marketplace->taxSistemConfig(true);

        $amount_total =  $dataAmount['amount_rules_total'] + $dataAmount['amount_rules_frete'];

        ## Calculo Cartão de Crédito
        if ($dataAmount['cartao'] == 'yes') {
            if ($dataAmount['installments'] == 1) {
                $amount = (float)number_format(($amount_total * 100), 0, '', '') + 0;
            } else {
                ## Calculo Parcelamento
                $amountParcelas = $amount_total * (1 +  $fees * $dataAmount['installments'] / 100);
                $amount = (float)number_format(($amountParcelas * 100), 0, '', '') + 0;
            }

            ## Calculo Boleto Bancário
        } else {

            $amount = (float)number_format(($amount_total * 100), 0, '', '') + 0;
        }

        return $amount;
    }

    /** ATUALIZAR ESTOQUE PRODUTO */
    public function updateProductStock($id)
    {
        ## Atualizar Estoque Fornecedor
        $itens = OrderShopIten::where('order_shop_itens.order_shop_id', $id)
            ->join('products', 'products.id', '=', 'order_shop_itens.product_id')
            ->select(
                'products.id',
                'products.quantity as product_qtd',
                'order_shop_itens.qtd as itens_qtd'
            )
            ->get();

        foreach ($itens as $value) {

            ## Calcular Estoque
            $quanty = $value['product_qtd'] - $value['itens_qtd'];
            $quanty = ($quanty >= 0)
                ? $quanty
                : 0;

            ## Atualizar Estoque
            Product::where('id', $value['id'])
                ->update(['quantity' => $quanty]);
        }
    }


    /** SALVAR DADOS PAGAMENTO CARTÃO DE CREDITO */
    public function paidCard($transaction, $request, $dataPaid)
    {

        ## Confirmar Pagamento e Criar Invoice
        $transaction_code = $transaction->authorization_code;
        $transaction_id = $transaction->id;
        $payment_installment = $request->installments ?? 1;
        $payment_total = ($dataPaid['amount_final'] / 100);
        $payment_type = $request->modo;

        ## Atualizar Order Master Pagamento Confirmado
        OrdersMaster::where('id', $dataPaid['order_masterId'])
            ->update(
                [
                    'order_status_id' => 3,
                    'payment_installment' => $payment_installment,
                    'payment_total' => $payment_total,
                    'payment_type' => $payment_type

                ]
            );

        ## Atualizar Order Shop
        OrderShop::where('orders_master_id', $dataPaid['order_masterId'])
            ->chunk(10, function ($orders) use ($transaction_code, $transaction_id) {
                foreach ($orders as $list) {

                    ## Atualizar Orders Pagamento Confirmado
                    OrderShop::where('id', $list->id)->update(['orders_status_id' => 3]);

                    ## Atualizar Estoque Produto
                    $this->updateProductStock($list->id);

                    ## Criar Invoice Pagamento
                    $newInvoice = new OrderShopInvoice();
                    $newInvoice->order_shop_id = $list->id;
                    $newInvoice->order_shop_invoice_status_id = 2; /// Pagamento Confirmado
                    $newInvoice->order_shop_form_payment_id = 3; // Cartão de Crédito
                    $newInvoice->transaction_code = $transaction_code;
                    $newInvoice->transaction_id = $transaction_id;
                    $newInvoice->save();
                }
            });
    }

    /** SALVAR BOLETO PAGARME */
    public function boletoPagarme($transaction, $request, $dataBoleto)
    {

        ## Confirmar Pagamento e Criar Invoice
        $transaction_code = $transaction->authorization_code;
        $transaction_id = $transaction->id;
        $payment_installment = $request->installments ?? 1;
        $payment_total = ($dataBoleto['amount_final'] / 100);
        $payment_type = $request->modo;

        ## Atualizar Order Master Pagamento Confirmado
        OrdersMaster::where('id', $dataBoleto['orderMasterId'])
            ->update(
                [
                    'order_status_id' => 1,
                    'payment_installment' => $payment_installment,
                    'payment_total' => $payment_total,
                    'payment_type' => $payment_type

                ]
            );

        OrderShop::where('orders_master_id', $dataBoleto['orderMasterId'])
            ->chunk(10, function ($orders) use ($transaction_code, $transaction_id) {
                foreach ($orders as $list) {

                    /// Criar Invoice Pagamento
                    $newInvoice = new OrderShopInvoice();
                    $newInvoice->order_shop_id = $list->id;
                    $newInvoice->order_shop_invoice_status_id = 1; /// Pagamento Pendente
                    $newInvoice->order_shop_form_payment_id = 1; // Boleto Bancario
                    $newInvoice->transaction_code = $transaction_code;
                    $newInvoice->transaction_id = $transaction_id;
                    $newInvoice->save();
                }
            });


        ## Salvar Boleto
        $newBoleto = new OrdersMasterBoleto();
        $newBoleto->orders_master_id = $dataBoleto['orderMasterId'];
        $newBoleto->boleto_url = $transaction->boleto_url;
        $newBoleto->boleto_barcode = $transaction->boleto_barcode;
        $newBoleto->boleto_expiration_date = $transaction->boleto_expiration_date;
        $newBoleto->save();
    }


    /** SALVAR DADOS PIX PAGARME */
    public function pixPagarme($transaction, $request, $dataPix)
    {
        ## Confirmar Pagamento e Criar Invoice
        $transaction_code = $transaction->authorization_code;
        $transaction_id = $transaction->id;
        $payment_installment = $request->installments ?? 1;
        $payment_total = ($dataPix['amount_final'] / 100);
        $payment_type = $request->modo;

        ## Atualizar Order Master Pagamento Confirmado
        OrdersMaster::where('id', $dataPix['orderMasterId'])
            ->update(
                [
                    'order_status_id' => 1,
                    'payment_installment' => $payment_installment,
                    'payment_total' => $payment_total,
                    'payment_type' => $payment_type

                ]
            );

        OrderShop::where('orders_master_id', $dataPix['orderMasterId'])
            ->chunk(10, function ($orders) use ($transaction_code, $transaction_id) {
                foreach ($orders as $list) {

                    ## Criar Invoice Pagamento
                    $newInvoice = new OrderShopInvoice();
                    $newInvoice->order_shop_id = $list->id;
                    $newInvoice->order_shop_invoice_status_id = 1; /// Pagamento Pendente
                    $newInvoice->order_shop_form_payment_id = 6; // Pagamento Pix
                    $newInvoice->transaction_code = $transaction_code;
                    $newInvoice->transaction_id = $transaction_id;
                    $newInvoice->save();
                }
            });


        ## Salvar Pix Code
        $newPix = new OrdersMasterPix();
        $newPix->orders_master_id = $dataPix['orderMasterId'];
        $newPix->pix_qr_code = $transaction->pix_qr_code;
        $newPix->pix_expiration_date = $transaction->pix_expiration_date;
        $newPix->save();
    }

    /** UPDATE ORDER MASTER */
    public function updateOrderMasterCard($orderMasterId, $request, $amount_final)
    {
        OrdersMaster::where('id', $orderMasterId)
            ->update(
                [
                    'order_status_id' => 2, // Pagamento Recusado
                    'payment_installment' => 1,
                    'payment_total' => ($amount_final / 100),
                    'payment_type' => $request->modo

                ]
            );
    }

    /*** paymentProducts($payment_products) */
    public function paymentProducts($payment_products)
    {

        $products = [];
        foreach ($payment_products as $value) {

            foreach ($value as $list) {
                array_push($products, [
                    "id" => $list['id'],
                    "title" => $list['title'],
                    "unit_price" => $list['unit_price'],
                    "quantity" => $list['quantity'],
                    "tangible" => $list['tangible'],
                ]);
            }
        }

        return $products;
    }

    /** FECHAR PEDIDO CLENTE*/
    public function finishOrder(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();
        $shopID = $store['shop_id'];

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        if (is_null(session('address'))) {
            return redirect()->route('mkp.cart')->with('error', 'Houve um erro no processamento do Pedido, tente novamente.');
        }

        ## Carrrinho Existe
        if (!empty(Session::has('cart'))) {

            ## Criar Order Shop
            $cart = Session::has('cart') ? session('cart') : [];

            ## CRIAR ORDER MASTER
            $order_master = $this->marketplace->orderMaster($shopID);

            ## CRIAR ORDER SHOP CLIENT
            $this->marketplace->orderShopClient($order_master->id);

            ## CRIAR PEDIDO ENDEREÇO CLIENTE
            $this->marketplace->OrderShopClientAddress($order_master->id);

            ## GroupBy Array Carrrinho
            $group = array();
            foreach ($cart as $value) {
                $group[$value['shop_id']][] = $value;
            }

            ## Variaveis a serem carregadas
            $payment_products = array();
            $payment_rules = array();
            $amount_rules_affiliate = 0;
            $amount_rules_total = 0;
            $amount_rules_frete = 0;
            $amount_rules_frete_juntyme = 0;
            $amount_rules_seller = 0;
            $soma_rules = 0;

            foreach ($group as $key => $value) {

                ## CRIAR ORDER SHOP
                $orderShop = $this->marketplace->orderShop($order_master->id, $key, $shopID);

                $dataIten = [
                    'value' => $value,
                    'orderShopId' => $orderShop->id,
                    'amount_rules_total' => $amount_rules_total,
                ];

                ## CRIAR ORDENS ITENS
                $dadasItens = $this->newItens($dataIten, $shopID);

                ## Carregar $amount_rules_total
                $amount_rules_total = $dadasItens['amount_rules_total'];

                ## Payment Produtos Pagarme
                array_push(
                    $payment_products,
                    $dadasItens['payment_products']
                );


                ## SALVAR FRETE ORDERS
                $dataShip = [
                    'key' => $key,
                    'orderShopId' => $orderShop->id,
                    'amount_rules_frete'   =>  $amount_rules_frete,
                    'amount_rules_frete_juntyme'   =>  $amount_rules_frete_juntyme,
                    'qtde_volumes'   =>  $dadasItens['qtd_total'],
                    'peso_bruto' =>  $dadasItens['peso_bruto'],
                ];

                $dataShipping = $this->orderShipping($dataShip);

                ## Somar Valores Fretes
                $amount_rules_frete = $dataShipping['amount_rules_frete'];
                $amount_rules_frete_juntyme = $dataShipping['amount_rules_frete_juntyme'];


                ## UPDATE VALORES ORDER
                $vlr_total = $dadasItens['vlr_subtotal'] + $amount_rules_frete + $amount_rules_frete_juntyme;
                $this->updateOrderShop($orderShop->id, $dadasItens['vlr_subtotal'], $vlr_total);

                ## SALVAR COMISSÃO AFILIADOS
                if ($dadasItens['vlr_total_affiliate'] > 0.00) {

                    $dataAffiliate = [
                        'orderShopId' =>  $orderShop->id,
                        'shopID' =>  $shopID,
                        'vlr_subtotal' => $dadasItens['vlr_subtotal_affiliate'],
                        'vlr_total' => $dadasItens['vlr_total_affiliate'],
                        'commission_type_id' => 3
                    ];
                    $this->OrderShopCommission($dataAffiliate);

                    ## Valor Afiliado Pagarme
                    $amount_rules_affiliate = $amount_rules_affiliate + $dadasItens['vlr_total_affiliate'];
                }

                ## SALVAR COMISSÃO JUNTYME
                $dataAffiliate = [
                    'orderShopId' =>  $orderShop->id,
                    'shopID' =>  $shopID,
                    'affiliate_shop_id' => 1,
                    'vlr_subtotal' => $dadasItens['vlr_subtotal_juntyme'],
                    'vlr_total' => $dadasItens['vlr_total_juntyme'],
                    'commission_type_id' => 5,
                ];
                $this->OrderShopCommission($dataAffiliate);

                ## SALVAR COMISSÃO SELLER
                $dataSeller = [
                    'shopSellerId' => $key,
                    'vlr_subtotal' => $dadasItens['vlr_subtotal'],
                    'vlr_frete_total' => $dataShipping['vlr_frete_total'],
                    'vlr_total_affiliate' => $dadasItens['vlr_total_affiliate'],
                    'vlr_total_juntyme' => $dadasItens['vlr_total_juntyme'],
                    'orderShopId' => $orderShop->id,
                    'commission_type_id' => 4,
                ];

                $vlr_total_seller = $this->marketplace->OrdersShopsCommissionsSeller($dataSeller);

                ## Dados do Recebedor Pagarme Affiliade
                $recebedor_seller = ShopsPagarmeRecebedor::where('shops_id', $key)
                    ->where('type', 'seller')
                    ->first();

                if (!is_null($recebedor_seller)) {
                    $amount_price_seller = $vlr_total_seller;
                    $amount_rules_seller =  $amount_rules_seller + $amount_price_seller;
                    $soma_rules = $soma_rules + (float)number_format(($amount_price_seller * 100), 0, '', '') + 0;
                    array_push(
                        $payment_rules,
                        [
                            'amount' => (float)number_format(($amount_price_seller * 100), 0, '', '') + 0,
                            'recipient_id' => $recebedor_seller->recebedor_id,
                            'charge_processing_fee' => false,
                            'liable' => true,
                            'charge_remainder' => false
                        ]
                    );
                }

                ## Envios dos EmailS
                $order = $orderShop;

                ## Enviar Email Para o Afiliado
                try {
                    // orderAchaddoAffiliate::dispatch($order)->delay(now()->addSeconds('2'));
                    Mail::send(new orderAchaddoAffiliate($order));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                }


                ## Envio por Jobs Seller
                try {
                    // orderAchaddoSeller::dispatch($order)->delay(now()->addSeconds('2'));
                    Mail::send(new orderAchaddoSeller($order));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                }
            }


            $orderMaster = $order_master;
            ## Enviar Email Para o Cliente Imediato
            try {
                // orderAchaddoClient::dispatch($orderMaster)->delay(now()->addSeconds('1'));
                Mail::send(new orderAchaddoClient($orderMaster));
            } catch (Exception $e) {

                // $errors = 'Email não pode ser enviado entre em contato com Nosso Suporte.';
            }


            //// Criar Session Paginas
            Session::put('orderPage', $order_master->id);


            ////////////////////////////////
            ///// FORMAS DE PAGAMENTOS /////
            ////////////////////////////////

            ## Carregar API Pagarme
            $pagarme = new \PagarMe\Client(config('juntyme.pm_chave_de_api'));

            ## Prepara Billing e Customer Pagarme
            $dataPagarme = $this->dataPagarme(null, null);


            ## Preparar Valor amount Pagarme
            $dataAmount = [
                'amount_rules_total' => $dadasItens['amount_rules_total'],
                'amount_rules_frete' => $amount_rules_frete + $amount_rules_frete_juntyme,
                'installments' => $request->installments,
                'cartao' => $request->cartao,
            ];
            $amount = $this->amountPagarme($dataAmount);

            if ($amount_rules_affiliate > 0.00) {

                ## Dados do Recebedor Pagarme Affiliade
                $recebedor_affiliate = ShopsPagarmeRecebedor::where('shops_id', $shopID)
                    ->where('type', 'affiliate')
                    ->first();

                if (!is_null($recebedor_affiliate)) {
                    ## Array Rules Affiliate
                    $soma_rules = $soma_rules +  ((float)number_format(($amount_rules_affiliate * 100), 0, '', '') + 0);
                    array_push(
                        $payment_rules,
                        [
                            'amount' =>  (float)number_format(($amount_rules_affiliate * 100), 0, '', '') + 0,
                            'recipient_id' => $recebedor_affiliate->recebedor_id,
                            'charge_processing_fee' => false,
                            'liable' => true,
                            'charge_remainder' => false
                        ]
                    );
                }
            }

            ## RULES AFILIADO

            $amount_rules_mkp = $amount_rules_seller + $amount_rules_affiliate;

            ## RULES JUNTYME MARKETLACE
            $amount_rules_juntyme = $amount - ((float)number_format(($amount_rules_mkp * 100), 0, '', '') + 0);
            $soma_rules = $soma_rules + $amount_rules_juntyme;
            array_push(
                $payment_rules,
                [
                    'amount' => $amount_rules_juntyme,
                    'recipient_id' => config('juntyme.pm_id_recebedor'),
                    'charge_processing_fee' => true,
                    'liable' => true,
                    'charge_remainder' => true
                ]
            );

            ## CORRIGIR ERROS CENTAVOS
            $amount_final = ($amount == $soma_rules) ? $amount : $soma_rules;

            $payment_products = $this->paymentProducts($payment_products);


            ////////////////////////////////////
            //// PAGAMENTO UNICO CARTÃO ////////
            ////////////////////////////////////

            if ($request->cartao == 'yes') {

                $data = [
                    'amount' =>  $amount_final,
                    'card_hash' => $request->card_hash,
                    'payment_method' => $request->modo,
                    'async' => false,
                    'installments' => $request->installments,
                    'capture' => true,
                    'soft_descriptor' => 'P#' . $order_master->id,
                    'X-PagarMe-User-Agent' => 'Checout MarketPlace Seller',
                    'X-PagarMe-Version' => '001',
                    'postback_url' => config('juntyme.pm_link_postback') . '/pagarme/postback/client/payment',
                    'customer' => $dataPagarme['customer'],
                    'billing' => $dataPagarme['billing'],
                    'items' => $payment_products,
                    'split_rules' => $payment_rules,
                    'metadata' => [
                        'order_master' => $order_master->id

                    ]
                ];

                try {
                    $transaction = $pagarme->transactions()->create($data);

                    if ($transaction->status == 'paid') {

                        ## SALVAR TRANSAÇÃO PAGA CARTÃO
                        $dataPaid = [
                            'amount_final' => $amount_final,
                            'order_masterId' => $order_master->id
                        ];
                        $this->paidCard($transaction, $request, $dataPaid);
                    } else {

                        ## Update Order Master Cartão de Credito
                        $this->updateOrderMasterCard($order_master->id, $request, $amount_final);
                    }
                } catch (Exception $e) {

                    $errors =  $e->getMessage();

                    ## Update Order Master Cartão de Credito
                    $this->updateOrderMasterCard($order_master->id, $request, $amount_final);
                }
            }

            /////////////////////////////////////////
            //// PAGAMENTO UNICO BOLETO BANCÁRIO ////////
            ///////////////////////////////////////////

            if ($request->boleto == 'yes') {

                $data = [
                    'amount' =>  $amount_final,
                    'payment_method' => $request->modo,
                    'async' => false,
                    'installments' => 1,
                    'capture' => true,
                    'soft_descriptor' => 'P#' .  $order_master->id,
                    'X-PagarMe-User-Agent' => 'Checout MarketPlace Seller',
                    'X-PagarMe-Version' => '001',
                    'postback_url' => config('juntyme.pm_link_postback') . '/pagarme/postback/client/payment',
                    'customer' => $dataPagarme['customer'],
                    'billing' => $dataPagarme['billing'],
                    'items' => $payment_products, /// Array produtos
                    'split_rules' => $payment_rules,
                    'metadata' => [
                        'order_master' => $order_master->id

                    ]
                ];



                try {
                    ## Transação Pagarme
                    $transaction = $pagarme->transactions()->create($data);

                    ## SALVAR DADOS DO BOLETO
                    $dataBoleto = [
                        'amount_final' => $amount_final,
                        'orderMasterId' => $order_master->id
                    ];
                    $this->boletoPagarme($transaction, $request, $dataBoleto);
                } catch (Exception $e) {

                    $errors =  $e->getMessage();
                }
            }

            ///////////////////////////////////////////
            //// PAGAMENTO UNICO PIX //////////////////
            ///////////////////////////////////////////
            if ($request->pix == 'yes') {

                $data = [
                    'amount' =>  $amount_final,
                    'payment_method' => $request->modo,
                    'pix_expiration_date' => $dataPagarme['pix_expiration_date'],
                    'async' => false,
                    'installments' => 1,
                    'capture' => true,
                    'soft_descriptor' => 'P#' . $order_master->id,
                    'X-PagarMe-User-Agent' => 'Checout MarketPlace Seller',
                    'X-PagarMe-Version' => '001',
                    'postback_url' => config('juntyme.pm_link_postback') . '/pagarme/postback/client/payment',
                    'customer' => $dataPagarme['customer'],
                    'billing' => $dataPagarme['billing'],
                    'items' => $payment_products, /// Array produtos
                    'split_rules' => $payment_rules,
                    'metadata' => [
                        'order_master' => $order_master->id

                    ]
                ];


                try {

                    $transaction = $pagarme->transactions()->create($data);

                    ## SALVAR DADOS PIX PAGARME
                    $dataPix = [
                        'amount_final' => $amount_final,
                        'orderMasterId' => $order_master->id
                    ];
                    $this->pixPagarme($transaction, $request, $dataPix);
                } catch (Exception $e) {

                    $errors =  $e->getMessage();
                }
            }

            Session::forget('cart');
            Session::forget('address');
            Session::forget('shipping');
            Session::forget('frete');

            ## Resposta com errors
            if (!empty($errors)) {
                return redirect()->route('mkp.concluded', ['ordeMaster' => Encript::encriptar($order_master->id)])->with('error', $errors);
            }

            return redirect()->route('mkp.concluded', ['ordeMaster' => Encript::encriptar($order_master->id)]);
        }

        ## Carrinho não Existe
        return redirect()->route('mkp.cart');
    }

    /** REPROCESSAR PAGAMENTO*/
    public function orderReprocess(Request $request)
    {

        $orderID = Encript::desencriptar($request->orderID);

        if (is_null($orderID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }


        ## Configuração Store
        $store = $this->marketplace->storeUrl();
        $shopID = $store['shop_id'];

        $verific = OrderShop::where('order_shop.orders_master_id', $orderID)
            ->where('order_shop.affiliate_shop_id', $shopID)
            ->where('order_shop.orders_status_id', '<', '3')
            ->first();

        ## Verificar se Existe o Pedido
        if (!empty($verific)) {

            $orders = OrderShop::where('order_shop.orders_master_id', $orderID)
                ->where('order_shop.affiliate_shop_id', $shopID)
                ->where('order_shop.orders_status_id', '<', '3')
                ->join('order_shop_shipping', 'order_shop_shipping.order_shop_id', '=', 'order_shop.id')
                ->select(
                    'order_shop_shipping.vlr_frete',
                    'order_shop.id',
                    'order_shop.orders_master_id',
                    'order_shop.vlr_total'
                )
                ->get();

            $payment_products = array();
            $payment_rules = array();
            $soma_affiliate = 0;
            $amount_rules_seller = 0;
            $soma_frete = 0;
            $soma_total = 0;

            foreach ($orders as $list) {

                ## Soma total dos Valores dos Fretes
                $soma_frete = $soma_frete + $list->vlr_frete;
                ## Soma Total dos Valores dos Pedidos
                $soma_total =   $soma_total + $list->vlr_total;

                $itens = OrderShopIten::where('order_shop_itens.order_shop_id', $list->id)
                    ->join('products', 'products.id', '=', 'order_shop_itens.product_id')
                    ->select(
                        'products.title',
                        'order_shop_itens.*'
                    )
                    ->get();

                foreach ($itens as $products) {
                    $price = $products->qtd * $products->vlr_unit;
                    ## Payment Produtos Pagarme
                    array_push(
                        $payment_products,
                        [
                            'id' => strval($products->product_id),
                            'title' => strval($products->title),
                            'unit_price' => (float)number_format(($price * 100), 0, '', '') + 0,
                            'quantity' => $products->qtd,
                            'tangible' => true
                        ]
                    );
                }


                ## Rules Affiliate
                $commissions_affiliate = OrderShopCommission::where('orders_id', $list->id)
                    ->where('commission_type_id', 3)
                    ->join('shops_pagarme_recebedor', 'shops_pagarme_recebedor.shops_id', '=', 'orders_shops_commissions.affiliate_shop_id')
                    ->first();
                if (!is_null($commissions_affiliate)) {
                    ## Rules Afiliados
                    $soma_affiliate = $soma_affiliate + $commissions_affiliate['vlr_total'];
                }

                ## Rules Sellers
                $commissions_seller = OrdersShopsCommissionsSeller::where('orders_id', $list->id)
                    ->where('commission_type_id', 4)
                    ->join('shops_pagarme_recebedor', 'shops_pagarme_recebedor.shops_id', '=', 'orders_shops_commissions_seller.seller_shop_id')
                    ->first();

                ## Rules Seller
                array_push(
                    $payment_rules,
                    [
                        'amount' => (float)number_format(($commissions_seller['vlr_total'] * 100), 0, '', '') + 0,
                        'recipient_id' => $commissions_seller['recebedor_id'],
                        'charge_processing_fee' => false,
                        'liable' => true,
                        'charge_remainder' => false
                    ]
                );

                ## Soma Valores Seller
                $amount_rules_seller = $amount_rules_seller + $commissions_seller['vlr_total'];
            }

            ## Apenas 1 Afiliado
            if (!is_null($commissions_affiliate)) {
                ## Rules Afiliados
                array_push(
                    $payment_rules,
                    [
                        'amount' => (float) number_format(($soma_affiliate * 100), 0, '', '') + 0,
                        'recipient_id' =>  $commissions_affiliate['recebedor_id'],
                        'charge_processing_fee' => false,
                        'liable' => true,
                        'charge_remainder' => false
                    ]
                );
            }

            ////////////////////////////////
            ///// FORMAS DE PAGAMENTOS /////
            ////////////////////////////////
            $pagarme = new \PagarMe\Client(config('juntyme.pm_chave_de_api'));

            ## Prepara Billing e Customer Pagarme
            $dataPagarme = $this->dataPagarme($shopID, $request);

            ## Criar Session Paginas
            Session::put('orderPage', $dataPagarme['orders_master_id']);

            ## Preparar Valor amount Pagarme
            $dataAmount = [
                'amount_rules_total' => $soma_total,
                'amount_rules_frete' => $soma_frete,
                'installments' => $request->installments,
                'cartao' => $request->cartao,
            ];
            $amount = $this->amountPagarme($dataAmount);


            ## RULES MARKETPLACE
            $amount_rules_mkp =  ((float)number_format((($amount_rules_seller + $soma_affiliate) * 100), 0, '', '') + 0);

            ## RULES JUNTYME MARKETLACE
            $amount_rules_juntyme = $amount - $amount_rules_mkp;
            $soma_rules =  $amount_rules_mkp  + $amount_rules_juntyme;
            array_push(
                $payment_rules,
                [
                    'amount' => $amount_rules_juntyme,
                    'recipient_id' => config('juntyme.pm_id_recebedor'),
                    'charge_processing_fee' => true,
                    'liable' => true,
                    'charge_remainder' => true
                ]
            );


            ## CORRIGIR ERROS CENTAVOS
            $amount_final = ($amount == $soma_rules) ? $amount : $soma_rules;

            $data = [
                'amount' =>  $amount_final,
                'payment_method' => $request->modo,
                'async' => false,
                'capture' => true,
                'soft_descriptor' => 'P#' . $dataPagarme['orders_master_id'],
                'X-PagarMe-User-Agent' => 'Checout MarketPlace Seller',
                'X-PagarMe-Version' => '001',
                'postback_url' => config('juntyme.pm_link_postback') . '/pagarme/postback/client/payment',
                'customer' => $dataPagarme['customer'],
                'billing' => $dataPagarme['billing'],
                'items' => $payment_products, /// Array produtos
                'split_rules' => $payment_rules,
                'metadata' => [
                    'order_master' => $dataPagarme['orders_master_id']
                ]
            ];

            ////////////////////////////////////
            //// PAGAMENTO UNICO CARTÃO ////////
            ////////////////////////////////////

            if ($request->cartao == 'yes') {

                $data += [
                    'card_hash' => $request->card_hash,
                    'installments' => $request->installments
                ];

                try {
                    $transaction = $pagarme->transactions()->create($data);
                    if ($transaction->status == 'paid') {

                        ## SALVAR TRANSAÇÃO PAGA CARTÃO
                        $dataPaid = [
                            'amount_final' => $amount_final,
                            'order_masterId' => $dataPagarme['orders_master_id']
                        ];
                        $this->paidCard($transaction, $request, $dataPaid);
                    } else {

                        ## Update Order Master Cartão de Credito
                        $this->updateOrderMasterCard($dataPagarme['orders_master_id'], $request, $amount_final);
                    }
                } catch (Exception $e) {

                    $errors =  $e->getMessage();

                    ## Update Order Master Cartão de Credito
                    $this->updateOrderMasterCard($dataPagarme['orders_master_id'], $request, $amount_final);
                }
            }

            /////////////////////////////////////////
            //// PAGAMENTO UNICO BOLETO BANCÁRIO ////////
            ///////////////////////////////////////////
            if ($request->boleto == 'yes') {

                $data += [
                    'installments' => 1
                ];


                try {

                    ## Processar Pagamento Pagarme
                    $transaction = $pagarme->transactions()->create($data);

                    ## SALVAR DADOS DO BOLETO
                    $dataBoleto = [
                        'amount_final' => $amount_final,
                        'orderMasterId' => $dataPagarme['orders_master_id']
                    ];
                    $this->boletoPagarme($transaction, $request, $dataBoleto);
                } catch (Exception $e) {

                    $errors =  $e->getMessage();
                }
            }
            /////////////////////////////////////////////
            //// PAGAMENTO UNICO PIX ////////////////////
            /////////////////////////////////////////////

            if ($request->pix == 'yes') {

                $data += [
                    'pix_expiration_date' => $dataPagarme['pix_expiration_date'],
                    'installments' => 1
                ];

                ## Processar Pagamento Pagarme
                try {

                    ## Processar Pagamento Pagarme
                    $transaction = $pagarme->transactions()->create($data);

                    ## SALVAR DADOS PIX PAGARME
                    $dataPix = [
                        'amount_final' => $amount_final,
                        'orderMasterId' => $dataPagarme['orders_master_id']
                    ];
                    $this->pixPagarme($transaction, $request, $dataPix);
                } catch (Exception $e) {
                    $errors =  $e->getMessage();
                }
            }

            ## Resposta com errors
            if (!empty($errors)) {
                return redirect()->route('mkp.concluded', ['ordeMaster' => Encript::encriptar($dataPagarme['orders_master_id'])])->with('error', $errors);
            }

            return redirect()->route('mkp.concluded', ['ordeMaster' =>  Encript::encriptar($dataPagarme['orders_master_id'])]);
        }

        ## Carrinho não Existe
        return redirect()->route('mkp.cart');
    }

    /** CHECKOUT PAGAMENTO */
    public function concluded(Request $request)
    {
        $orderMasterID = Encript::desencriptar($request->get('ordeMaster'));

        if (is_null($orderMasterID)) {
            return back()->with('error', '<i data-feather="alert-octagon"></i> Erro ao Abrir o Pedido.');
        }

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        $orderMaster = $this->marketplace->ordersMaster($orderMasterID, $store);

        if (!empty($orderMaster)) {

            $orders = $this->marketplace->orderShopConcluded($orderMasterID, $store);

            $fatura = $this->marketplace->orderShopFatura($orderMasterID, $store);

            ## Codigo QRCODE
            if (isset($orderMaster['pix_qr_code'])) {
                $qrCode = new QrCode($orderMaster['pix_qr_code']);
                $output = new Output\Png();
                $qrcode = $output->output($qrCode, 400);
            } else {

                $qrcode = '';
            }

            $logo_main = $store['logo_main'];

            $themes = $this->marketplace->originThemes($store);

            return view($store['route'] .   $themes . '.site.web.concluded', compact(
                'store',
                'orderMaster',
                'orders',
                'fatura',
                'qrcode',
                'logo_main'
            ));
        }

        ## Pedido não Existe
        return redirect()->route('mkp.cart');
    }

    /** ASSINAR NEWSLETTER*/
    public function signNewsletter(Request $request)
    {

        ## Verificar Validade de e-mail
        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {

            ## Resposta Frete
            $response['success'] = false;
            $response['message'] =  'E-mail inválido.';
            return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ShopsNewsletter::updateOrCreate(
            ['shop_id' => $store['shop_id'], 'email' => $request->email],
            ['name' => $request->name]
        );

        ## Abrir Session Temporária
        Session::put('newsletter', $request->email);

        ## Resposta Frete
        $response['success'] = true;
        $response['message'] =  'Cadastro realizado com Sucesso.';
        return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /** PAGINA QUEM SOMOS */
    public function aboutUs(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Verificar Dominio
        if (is_null($store)) {
            return redirect(config('multisite.achaddo'));
        }

        ## Carregar Menu Departamento
        $departaments = $this->marketplace->departaments($store['shop_id'], $store['store_id']);

        ## Carregar Menu Share
        $share = $this->marketplace->myShare($store['id']);

        ## Carregar Wishlist
        $client_ip = $request->ip();
        $wishlist = $this->marketplace->menuWishlist($store['id'], $client_ip);

        $content = Store::where('shop_id', $store['shop_id'])
            ->select('page_about')
            ->first();

        $themes = $this->marketplace->originThemes($store);

        return view($store['route'] . $themes . '.site.web.aboutUs', compact(
            'store',
            'share',
            'wishlist',
            'content',
            'departaments'
        ));
    }


    /** GERADOR DE SITE MAPS */
    public function sitemaps(Request $request)
    {

        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        $storeUrlfeed = Store::where('shop_id', '=', $store['shop_id'])
            ->leftJoin('sites', 'store.id', '=', 'sites.store_id')
            ->where('sites.principal', '=', 1)
            ->first();

        $storeUrl = ($storeUrlfeed['url_secure']) ? $storeUrlfeed['url_secure'] : $storeUrlfeed['url_subdomain'];

        // criar um novo objeto de mapa de site
        $sitemap = App::make('sitemap');

        // obtenha todos os produtos da db (ou onde quer que você os armazene)
        $products = $this->marketplace->sitemapsProduct($store);

        // counters
        $counter = 0;
        $sitemapCounter = 0;

        // Gerar diretorio
        $path = public_path() . '/sitemaps/' . $store['shop_id'] . '/';

        File::makeDirectory($path,  0777, true, true);

        // adicione cada produto a vários sitemaps com um índice de sitemap
        foreach ($products as $p) {
            if ($counter == 50000) {

                // gerar novo arquivo de mapa do site
                $sitemap->store('xml', 'sitemaps/' . $store['shop_id'] . '/sitemap-' . $sitemapCounter);

                // adicione o arquivo à matriz de mapas de sites
                $sitemap->addSitemap(secure_url('sitemap-' . $sitemapCounter . '.xml'));

                // redefinir a matriz de itens (limpar a memória)
                $sitemap->model->resetItems();

                // zere o contador
                $counter = 0;

                // contagem do mapa do site gerado
                $sitemapCounter++;
            }

            // adicionar produto ao array de itens
            $link = $storeUrl . '/produto/' . $p->product_id . '/' . $p->slug;
            $sitemap->add($link, $p->updated_at, '0.9', 'weekly',);

            // conte o número de elementos
            $counter++;
        }

        // você precisa verificar se há itens não usados
        if (!empty($sitemap->model->getItems())) {

            // gerar mapa do site com os últimos itens
            $link = 'sitemaps/' . $store['shop_id'] . '/sitemap-' . $sitemapCounter;

            $sitemap->store('xml', $link);

            // adicionar sitemap à matriz de sitemaps
            $sitemap->addSitemap(secure_url($link . '.xml'));

            // redefinir matriz de itens
            $sitemap->model->resetItems();
        }

        // generate new sitemapindex that will contain all generated sitemaps above
        $sitemap->store('xml', 'sitemaps/' . $store['shop_id'] . '/sitemap');

        if (config('app.env') == 'production') {
            // Envia para o Google o novo sitemap gerado
            $urlSitemap = "http://www.google.com/webmasters/sitemaps/ping?sitemap=";

            // Envia os dois arquivos sitemap gerados para a URL do Google
            $url = $urlSitemap . route('mkp.sitemaps');
            $response = Http::get($url);
        }

        // show sitemap
        return $sitemap->render('sitemapindex');
    }


    /*** TRATAR VARIAÇÕES */
    public function arrayVariation($variation)
    {
        $class = '';
        $item = '';
        $i = 0;
        foreach (json_decode($variation->attributes) as $itens) {

            $space_code = ($i > 0) ? '_' : '';

            $class = $class .  $space_code . $this->clearhtml->sanitizeString($itens->code);
            $item = $item .  $space_code . $itens->option;
            $i++;
        }

        $array_attr = [
            'inactive' => strtolower($class),
            'active' => strtolower($class .  '_item_' . $item),
        ];

        return $array_attr;
    }

    /** CALCULAR VALOR ATRIBUTO VARIAÇÃO */
    public function calculatedAttributeVariation(Request $request)
    {
        ## Configuração Store
        $store = $this->marketplace->storeUrl();

        ## Recuperar Atributo Variação
        $variation = ProductsAttributesVariation::where('sku', $request->productSku)
            ->first();

        ## Recuperar Produto
        $product = $this->marketplace->calculatedAttributeVariationProduct($variation);

        ## Calcular Valores do Produto
        $price = $this->checkout->calculatePriceVariation($product, $variation, $store['shop_id']);

        ## Array Variações
        $variations_array = $this->arrayVariation($variation);

        $data = [
            'price' => $price,
            'variations' =>  $variations_array
        ];

        ## Resposta Frete
        $response['success'] = true;
        $response['message'] = $data;
        return json_encode($response, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
}
