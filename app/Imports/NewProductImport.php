<?php

namespace App\Imports;

use App\Models\ProductsImportNew;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;



class NewProductImport implements ToModel, WithHeadingRow, WithBatchInserts, WithChunkReading
{

    private $data;
    private $key;
    private $shop_id;
    private $titles;

    public function __construct($data, $key, $shop_id, $titles = null)
    {
        $this->data = $data;
        $this->key = $key;
        $this->shop_id = $shop_id;
        $this->titles =  $titles;
    }


    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        ## IMPORTAÇÃO COM TITULO RELACIONADO
        if (!is_null($this->titles)) {

            //// Modificar Status
            if ($this->data['catalog_status'] == 1) {
                $status = 'ativo';
            } elseif ($this->data['catalog_status'] == 2) {
                $status = 'pausado';
            } else {
                $status = !empty($this->titles['status']) ? $this->titles['status'] : null;
            }

            $condition = !empty($this->titles['condicao'])
                ? $this->titles['condicao']
                : null;
            $provider = !empty($this->titles['fornecedor'])
                ? $this->titles['fornecedor']
                : null;
            $brand = !empty($this->titles['marca'])
                ? $this->titles['marca']
                : null;
            $title = !empty($this->titles['titulo'])
                ? $this->titles['titulo']
                : null;
            $sku = !empty($this->titles['sku'])
                ? $this->titles['sku']
                : null;
            $image = !empty($this->titles['imagem_principal'])
                ? $this->titles['imagem_principal']
                : null;
            $color = !empty($this->titles['cor'])
                ? $this->titles['cor']
                : null;
            $gtin = !empty($this->titles['gtin'])
                ? $this->titles['gtin']
                : null;
            $ncm = !empty($this->titles['ncm'])
                ? $this->titles['ncm']
                : null;
            $note = !empty($this->titles['anotacoes'])
                ? $this->titles['anotacoes']
                : null;
            $description = !empty($this->titles['descricao'])
                ? $this->titles['descricao']
                : null;
            $short_description = !empty($this->titles['breve_descricao'])
                ? $this->titles['breve_descricao']
                : null;
            $price = !empty($this->titles['preco'])
                ? $this->titles['preco']
                : null;
            $special_price = !empty($this->titles['preco_promocional'])
                ? $this->titles['preco_promocional']
                : null;
            $cost_price = !empty($this->titles['preco_custo'])
                ? $this->titles['preco_custo']
                : null;

            //// Alterar Quantidade na Tabela
            if ($this->data['catalog_stock'] == 1) {
                $quantity = 0;
            } elseif ($this->data['catalog_stock'] == 2) {
                $quantity = 10;
            } else {
                $quantity = !empty($this->titles['quantidade']) ?  (int)$this->titles['quantidade'] : null;
            }

            $cross_docking = !empty($this->titles['prazo_cross_docking'])
                ?  (int)$this->titles['prazo_cross_docking']
                : null;
            $warranty = !empty($this->titles['garantia'])
                ? $this->titles['garantia']
                : null;
            $weight = !empty($this->titles['peso'])
                ? $this->titles['peso']
                : null;
            $height = !empty($this->titles['altura'])
                ? $this->titles['altura']
                : null;
            $width = !empty($this->titles['largura'])
                ? $this->titles['largura']
                : null;
            $length = !empty($this->titles['comprimento'])
                ? $this->titles['comprimento']
                : null;
            $link_video = !empty($this->titles['link_video'])
                ? $this->titles['link_video']
                : null;
            $images_gallery_1 = !empty($this->titles['galeria_imagem_1'])
                ? $this->titles['galeria_imagem_1']
                : null;
            $images_gallery_2 = !empty($this->titles['galeria_imagem_2'])
                ? $this->titles['galeria_imagem_2']
                : null;
            $images_gallery_3 = !empty($this->titles['galeria_imagem_3'])
                ? $this->titles['galeria_imagem_3']
                : null;
            $images_gallery_4 = !empty($this->titles['galeria_imagem_4'])
                ? $this->titles['galeria_imagem_4']
                : null;
            $images_gallery_5 = !empty($this->titles['galeria_imagem_5'])
                ? $this->titles['galeria_imagem_5']
                : null;
            $images_gallery_6 = !empty($this->titles['galeria_imagem_6'])
                ? $this->titles['galeria_imagem_6']
                : null;
            $images_gallery_7 = !empty($this->titles['galeria_imagem_7'])
                ? $this->titles['galeria_imagem_7']
                : null;
            $images_gallery_8 = !empty($this->titles['galeria_imagem_8'])
                ? $this->titles['galeria_imagem_8']
                : null;
            $images_gallery_9 = !empty($this->titles['galeria_imagem_9'])
                ? $this->titles['galeria_imagem_9']
                : null;
            $images_gallery_10 = !empty($this->titles['galeria_imagem_10'])
                ? $this->titles['galeria_imagem_10']
                : null;

            $catalog_status = $this->data['catalog_status'] ?? null;
            $catalog_stock = $this->data['catalog_stock'] ?? null;
            $catalog_gallery = $this->data['catalog_gallery'] ?? null;

            //// Importe Diretamente tabela Padrão
            return new ProductsImportNew([
                'key' => $this->key,
                'shop_id' => $this->shop_id,
                'status' => !is_null($status) ? $row[$status] : null,
                'condition' => !is_null($condition) ? $row[$condition] : null,
                'provider' => !is_null($provider) ? $row[$provider] : null,
                'brand' => !is_null($brand) ? $row[$brand] : null,
                'title' => !is_null($title) ? $row[$title] : null,
                'sku' => !is_null($sku) ? $row[$sku] : null,
                'image' => !is_null($image) ? $row[$image] : null,
                'color' => !is_null($color) ? $row[$color] : null,
                'gtin' => !is_null($gtin) ?  str_replace(" ", "", $row[$gtin])  : null,
                'ncm' => !is_null($ncm) ?  str_replace(" ", "", $row[$ncm]) : null,
                'note' => !is_null($note) ? $row[$note] : null,
                'description' => !is_null($description) ? $row[$description] : null,
                'short_description' => !is_null($short_description) ? $row[$short_description] : null,
                'price' => !is_null($price) ?  number_format(floatval($row[$price]), 2, '.', '') : null,
                'special_price' => !is_null($special_price) ? number_format(floatval($row[$special_price]), 2, '.', '') : null,
                'cost_price' => !is_null($cost_price) ? number_format(floatval($row[$cost_price]), 2, '.', '') : null,
                'quantity' => !is_null($quantity) ? (int)$row[$quantity] : null,
                'cross_docking' => !is_null($cross_docking) ? (int)$row[$cross_docking] : null,
                'warranty' => !is_null($warranty) ? (int)$row[$warranty] : null,
                'weight' => !is_null($weight) ? number_format(floatval($row[$weight]), 3, '.', '') : null,
                'height' => !is_null($height) ? number_format(floatval($row[$height]), 2, '.', '') : null,
                'width' => !is_null($width) ? number_format(floatval($row[$width]), 2, '.', '') : null,
                'length' => !is_null($length) ? number_format(floatval($row[$length]), 2, '.', '') : null,
                'link_video' => !is_null($link_video) ? $row[$link_video] : null,
                'images_gallery_1' => !is_null($images_gallery_1) ? $row[$images_gallery_1] : null,
                'images_gallery_2' => !is_null($images_gallery_2) ? $row[$images_gallery_2] : null,
                'images_gallery_3' => !is_null($images_gallery_3) ? $row[$images_gallery_3] : null,
                'images_gallery_4' => !is_null($images_gallery_4) ? $row[$images_gallery_4] : null,
                'images_gallery_5' => !is_null($images_gallery_5) ? $row[$images_gallery_5] : null,
                'images_gallery_6' => !is_null($images_gallery_6) ? $row[$images_gallery_6] : null,
                'images_gallery_7' => !is_null($images_gallery_7) ? $row[$images_gallery_7] : null,
                'images_gallery_8' => !is_null($images_gallery_8) ? $row[$images_gallery_8] : null,
                'images_gallery_9' => !is_null($images_gallery_9) ? $row[$images_gallery_9] : null,
                'images_gallery_10' => !is_null($images_gallery_10) ? $row[$images_gallery_10] : null,
            ]);

            ## IMPOTAÇÃO DIRETA TABELA PADRÃO
        } else {

            //// Alterar Quantidade na Tabela
            if ($this->data['catalog_stock'] == 1) {
                $quantity = 0;
            } elseif ($this->data['catalog_stock'] == 2) {
                $quantity = 10;
            } else {
                $quantity = null;
            }

            //// Modificar Status
            if ($this->data['catalog_status'] == 1) {
                $status = 'ativo';
            } elseif ($this->data['catalog_status'] == 2) {
                $status = 'pausado';
            } else {
                $status =  null;
            }

            $catalog_status = $this->data['catalog_status'] ?? null;
            $catalog_stock = $this->data['catalog_stock'] ?? null;
            $catalog_gallery = $this->data['catalog_gallery'] ?? null;

            //// Importe Diretamente tabela Padrão
            return new ProductsImportNew([
                'catalog_status' => $catalog_status,
                'catalog_stock' => $catalog_stock,
                'catalog_gallery' => $catalog_gallery,
                'key' => $this->key,
                'shop_id' => $this->shop_id,
                'status' => !is_null($status) ? $status : $row['status'],
                'condition' => $row['condicao'],
                'provider' => $row['fornecedor'],
                'brand' => $row['marca'],
                'title' => $row['titulo'],
                'sku' => $row['sku'],
                'image' => $row['imagem_principal'],
                'color' => $row['cor'],
                'gtin' =>  str_replace(" ", "", $row['gtin']),
                'ncm' =>  str_replace(" ", "", $row['ncm']),
                'note' => $row['anotacoes'],
                'description' => $row['descricao'],
                'short_description' => $row['breve_descricao'],
                'price' => !is_null($row['preco']) ? number_format(floatval($row['preco']), 2, '.', '') : null,
                'special_price' => !is_null($row['preco_promocional']) ? number_format(floatval($row['preco_promocional']), 2, '.', '') : null,
                'cost_price' => !is_null($row['preco_custo']) ? number_format(floatval($row['preco_custo']), 2, '.', '') : null,
                'quantity' =>  !is_null($quantity) ? $quantity : $row['quantidade'],
                'cross_docking' => (int)$row['prazo_cross_docking'],
                'warranty' => (int)$row['garantia'],
                'weight' => !is_null($row['peso']) ? number_format(floatval($row['peso']), 3, '.', '') : null,
                'height' => !is_null($row['altura']) ? number_format(floatval($row['altura']), 2, '.', '') : null,
                'width' => !is_null($row['largura']) ? number_format(floatval($row['largura']), 2, '.', '') : null,
                'length' => !is_null($row['comprimento']) ? number_format(floatval($row['comprimento']), 2, '.', '') : null,
                'link_video' => $row['link_video'],
                'images_gallery_1' => $row['galeria_imgem_1'],
                'images_gallery_2' => $row['galeria_imgem_3'],
                'images_gallery_4' => $row['galeria_imgem_4'],
                'images_gallery_5' => $row['galeria_imgem_5'],
                'images_gallery_6' => $row['galeria_imgem_6'],
                'images_gallery_7' => $row['galeria_imgem_7'],
                'images_gallery_8' => $row['galeria_imgem_8'],
                'images_gallery_9' => $row['galeria_imgem_9'],
                'images_gallery_10' => $row['galeria_imgem_10'],
            ]);
        }
    }



    public function batchSize(): int
    {
        return 1000;
    }


    public function chunkSize(): int
    {
        return 1000;
    }
}
