<?php

namespace App\Jobs\Backend;

use App\Mail\Backend\sendTracking as BackendSendTracking;
use App\Models\OrderShopShippingLabel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class sendTracking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** Número de tentativas */
    public $tries = 5;

    private $rastreio;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrderShopShippingLabel $rastreio)
    {
        $this->rastreio = $rastreio;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send(new BackendSendTracking($this->rastreio));
    }
}
