<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class newJuntyme extends Mailable
{
    use Queueable, SerializesModels;

    private $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        $this->subject('Confirme seu e-mail.');
        $this->to($this->user->email, $this->user->name);

        $link = config('app.url');
        /** Visualizar email Diretmente */
        // return new newJuntyme($user);

        /** Disparo direto do E-mail */
        /// Mail::send(new \App\Mail\newJuntyme($this->user));

        /** Disparo com tema markdown */

        return  $this->markdown('mail.juntyme.newMailConfirm')
            ->with([
                'user' => $this->user,
                'link' => $link
            ]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
}
