<?php

namespace App\Models;

use App\Models\ClientsAddress;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Authenticatable implements JWTSubject
{


    use Notifiable;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /** Especifica a Tabela */
    protected $table = 'clients';
    //

    /**
     * Os atributos que são atribuíveis em massa.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'status_id',
        'email', 'password',
        'type',
        'id_facebook', 'id_google', 'remember_tokem'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function address()
    {
        /** Relacionar 1/N => o Usuario com o Endereço */
        return $this->hasOne(ClientsAddress::class, 'client_id');
    }

    public function addressList()
    {
        /** Relacionar 1/N => o Usuario com o Endereço */
        return $this->hasMany(ClientsAddress::class);
    }

    

    public function  clientCalled()
    {
        /** Relacionar 1/N => o Usuario com o Endereço */
        return $this->hasMany(ClientCalled::class, 'client_id');
    }


    public function shops_bling_relation_client()
    {
        return $this->hasOne(ShopsBlingRelationClient::class, 'client_id');
    }


    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
