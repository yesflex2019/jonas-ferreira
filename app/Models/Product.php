<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 *
 * @property int $id
 * @property int $shop_id
 * @property int $status_id
 * @property int $condition_id
 * @property int|null $provider_id
 * @property int|null $brand_id
 * @property string $title
 * @property string $slug
 * @property string $sku
 * @property string|null $image
 * @property string|null $image_type
 * @property string|null $color
 * @property string|null $gtin
 * @property string|null $note
 * @property string|null $description
 * @property string|null $short_description
 * @property float $price
 * @property float $special_price
 * @property float $cost_price
 * @property int $quantity
 * @property int $cross_docking
 * @property string|null $warranty
 * @property float $weight
 * @property float $height
 * @property float $width
 * @property float $length
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property ProductsCondition $products_condition
 * @property Shop $shop
 * @property ProductsStatus $products_status
 * @property AffiliateProductMm $affiliate_product_mm
 * @property OrderShopIten $order_shop_iten
 * @property ProductsAffiliatesMm $products_affiliates_mm
 * @property ProductsAttribute $products_attribute
 * @property ProductsCategoriesMm $products_categories_mm
 * @property Collection|ProductsImagesGallery[] $products_images_galleries
 * @property ProductsMarketplaceAchaddoMm $products_marketplace_achaddo_mm
 * @property ProductsMarketplaceMlbMm $products_marketplace_mlb_mm
 * @property ProductsMarketplaceRedirectMm $products_marketplace_redirect_mm
 * @property ProductsRelationMkp $products_relation_mkp
 * @property Collection|Store[] $stores
 *
 * @package App\Models
 */
class Product extends Model
{
	// use Searchable;

	use SoftDeletes;

	use Sluggable;

	protected $dates = ['deleted_at'];

	protected $table = 'products';

	protected $casts = [
		'shop_id' => 'int',
		'status_id' => 'int',
		'condition_id' => 'int',
		'provider_id' => 'int',
		'brand_id' => 'int',
		'title' => 'string',
		'slug' => 'string',
		'sku' => 'string',
		'image' => 'string',
		'image_type' => 'string',
		'color' => 'string',
		'gtin' => 'string',
		'ncm' => 'string',
		'price' => 'float',
		'special_price' => 'float',
		'cost_price' => 'float',
		'quantity' => 'int',
		'cross_docking' => 'int',
		'warranty' => 'string',
		'weight' => 'float',
		'height' => 'float',
		'width' => 'float',
		'length' => 'float',
		'approved' => 'int',
		'index' => 'int',
		'index_ali' => 'int',
		'store_status' => 'int',
		'index_drop' => 'int',
		'feed_google' => 'int',
		'feed_facebook' => 'int',
		'created_at' => 'datetime',
		'updated_at' => 'datetime'
	];

	protected $fillable = [
		'shop_id',
		'status_id',
		'condition_id',
		'provider_id',
		'brand_id',
		'title',
		'slug',
		'sku',
		'image',
		'image_type',
		'color',
		'gtin',
		'ncm',
		'note',
		'description',
		'short_description',
		'price',
		'special_price',
		'cost_price',
		'quantity',
		'cross_docking',
		'warranty',
		'weight',
		'height',
		'width',
		'length',
		'approved',
		'index',
		'index_ali',
		'store_status',
		'index_drop',
		'feed_google',
		'feed_facebook'
	];


	// Slug Unico ///
	public function sluggable(): array
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}

	/** Criar Slug da Loja */
	// public function setTitleAttribute($value)
	// {
	// 	$this->attributes['title'] = $value;
	// 	$this->attributes['slug'] = Str::slug($value);
	// }


	/** Filtro Catalogo Admin Backend */
	public function searchBackendStore(array $date, $totalPage, $id)
	{
		/** Verificar loja */

		return $this->where(function ($query) use ($date) {
			if (isset($date['search']))
				$query->where('products.title', 'ILIKE', '%' . $date["search"] . '%')
					->orWhere('products.slug', 'ILIKE', '%' . $date["search"] . '%')
					->orWhere('products.sku', 'ILIKE', '%' . $date["search"] . '%')
					->orWhere('shops.title', 'ILIKE', '%' . $date["search"] . '%');
			if (isset($date['type']))
				$query->where('products.status_id', '=', $date['type']);
		})
			->where('products.approved', '=', 1)
			->where('products.shop_id', '=', $id)
			->join('products_status', 'products.status_id', '=', 'products_status.id')
			->join('shops', 'products.shop_id', '=', 'shops.id')
			->select('products.*', 'products_status.title as title_status', 'shops.title as title_shops')
			->paginate($totalPage);
	}




	/** Filtro Catalogo Affiliate */
	public function searchAffiliate(array $date, $totalPage, $shop_id)
	{
		/** Filtro catalogo Affiliados */
		return $this->where(function ($query) use ($date) {
			if (isset($date['search']))
				$query->where('products.title', 'ILIKE', '%' . $date["search"] . '%')
					->orWhere('products.slug', 'ILIKE', '%' . $date["search"] . '%')
					->orWhere('products.sku', 'ILIKE', '%' . $date["search"] . '%');
			if (isset($date['type']))
				$query->where('products.condition_id', '=', $date['type']);
		})->join('products_affiliates_mm', 'products.id', '=', 'products_affiliates_mm.product_id')
			->where('products.status_id', '=', 2)
			->where('products.approved', '=', 1)
			->where('products.quantity', '>', 0)
			->where('products_affiliates_mm.status_id', '=', 1)
			->whereNotIn(
				'id',
				DB::table('affiliate_product_mm')
					->where('affiliate_product_mm.shop_id', '=', $shop_id)
					->select('affiliate_product_mm.product_id')
			)
			->select('products.*')
			->paginate($totalPage);
	}



	/** Relação da Galeria de Imagens */
	public function pig_shop()
	{
		return $this->hasMany(ProductsImagesGallery::class, 'id', 'product_id');
	}

	public function products_condition()
	{
		return $this->belongsTo(ProductsCondition::class, 'condition_id',);
	}

	public function products_brand()
	{
		return $this->belongsTo(ProductsBrand::class, 'brand_id');
	}

	public function products_provider()
	{
		return $this->belongsTo(ProductsProvider::class, 'provider_id');
	}

	public function shop()
	{
		return $this->belongsTo(Shop::class);
	}

	public function products_status()
	{
		return $this->belongsTo(ProductsStatus::class, 'status_id');
	}

	public function affiliate_product_mm()
	{
		return $this->hasOne(AffiliateProductMm::class);
	}

	public function order_shop_iten()
	{
		return $this->hasOne(OrderShopIten::class);
	}

	public function products_affiliates_mm()
	{
		return $this->hasOne(ProductsAffiliatesMm::class);
	}

	public function products_attribute()
	{
		return $this->hasMany(ProductsAttribute::class);
	}

	public function products_attribute_variation()
	{
		return $this->hasMany(ProductsAttributesVariation::class);
	}

	public function products_categories_mm()
	{
		return $this->hasMany(ProductsCategoriesMm::class);
	}

	public function products_images_galleries()
	{
		return $this->hasMany(ProductsImagesGallery::class);
	}

	public function products_marketplace_achaddo_mm()
	{
		return $this->hasOne(ProductsMarketplaceAchaddoMm::class);
	}

	public function products_marketplace_mlb_mm()
	{
		return $this->hasOne(ProductsMarketplaceMlbMm::class);
	}

	public function products_marketplace_redirect_mm()
	{
		return $this->hasOne(ProductsMarketplaceRedirectMm::class);
	}

	public function products_relation_mkp()
	{
		return $this->hasOne(ProductsRelationMkp::class);
	}

	public function stores()
	{
		return $this->belongsToMany(Store::class, 'store_product_mm')
			->withPivot('status', 'sales');
	}

	public function category()
	{
		/** Relacionamento para muitos */
		return $this->belongsToMany(ProductsCategory::class, 'products_categories_mm', 'product_id', 'category_id');
	}

	public function subcategory()
	{
		/** Relacionamento para muitos */
		return $this->belongsToMany(ProductsSubcategory::class, 'products_categories_mm', 'product_id', 'subcategory_id');
	}

	public function stores_categories()
	{
		/** Relacionamento para muitos */
		return $this->belongsToMany(StoresCategory::class, 'stores_departaments_products_mm', 'product_id', 'category_id');
	}

	public function stores_subcategories()
	{
		/** Relacionamento para muitos */
		return $this->belongsToMany(StoresSubcategory::class, 'stores_departaments_products_mm', 'product_id', 'subcategory_id');
	}

	public function shops_bling_relation_product()
	{
		return $this->hasOne(ShopsBlingRelationProduct::class, 'product_id');
	}

	public function products_attributes_variation()
	{
		return $this->hasMany(ProductsAttributesVariation::class);
	}

	public function products_aliexpress_mm()
	{
		return $this->hasOne(ProductsAliexpressMm::class, 'product_id');
	}

	public function products_attributes_aliexpress()
	{
		return $this->hasOne(ProductsAttributesAliexpress::class, 'product_id');
	}

	public function products_bling_export_redirect_mm()
	{
		return $this->hasOne(ProductsBlingExportRedirectMm::class, 'product_id');
	}

	public function products_mercadolivre_export_redirect_mm()
	{
		return $this->hasOne(ProductsMercadolivreExportRedirectMm::class, 'product_id');
	}

	public function products_bling_export_redirect_attribute()
	{
		return $this->hasOne(ProductsBlingExportRedirectAttribute::class, 'product_id');
	}

	public function products_braip_export_redirect()
	{
		return $this->hasOne(ProductsBraipExportRedirect::class, 'product_id');
	}

	public function products_eduzz_export_redirect()
	{
		return $this->hasOne(ProductsEduzzExportRedirect::class, 'product_id');
	}

	public function products_monetizze_export_redirect()
	{
		return $this->hasOne(ProductsMonetizzeExportRedirect::class, 'product_id');
	}

	public function products_monetizze_export_redirect_plans()
	{
		return $this->hasMany(ProductsMonetizzeExportRedirectPlan::class, 'product_id');
	}

	public function products_braip_export_redirect_plans()
	{
		return $this->hasMany(ProductsBraipExportRedirectPlan::class, 'product_id');
	}
}
