<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shop_id')->comment('Shop Loja');
            $table->unsignedBigInteger('status_id')->comment('Status Ativo ou Inativo');
            $table->unsignedBigInteger('condition_id')->comment('Condição do Produto exemplo Novo ou Usado');
            $table->integer('provider_id')->nullable()->comment('Fornecedor');
            $table->integer('brand_id')->nullable()->comment('Marca');
            $table->string('title')->index()->comment('Este campo espera um texto');
            $table->string('slug')->index()->comment('Url amigavel');
            $table->string('sku')->comment('Numero Referencial');
            $table->string('image')->nullable()->comment('Imagem Principal do Produto');
            $table->string('image_type')->nullable()->comment('Tipo do link da Imagem');
            $table->string('color')->nullable()->comment('Codigo de Barra');
            $table->string('gtin')->nullable()->comment('Codigo de Barra');
            $table->string('ncm')->nullable()->comment('Codigo NCM');
            $table->longText('note')->nullable()->comment('Anotações Importantes');
            $table->longText('description')->nullable()->comment('Descrição do Produto');
            $table->longText('short_description')->nullable()->comment('Preve Descrição');
            $table->decimal('price', 11, 2)->comment('Preço');
            $table->decimal('special_price', 11, 2)->comment('Preço Especial');
            $table->decimal('cost_price', 11, 2)->comment('Preço de Custo');
            $table->integer('quantity')->comment('Quantidade');
            $table->integer('cross_docking')->comment('Quantidade');
            $table->string('warranty')->nullable()->comment('Garantia');
            $table->decimal('weight', 11, 3)->comment('Peso do Produto em kg');
            $table->decimal('height', 11, 2)->comment(' Altura metros');
            $table->decimal('width', 11, 2)->comment('Largura metros');
            $table->decimal('length', 11, 2)->comment('Largura metros');
            $table->integer('approved')->nullable()->comment('Aprovação Administrador (1)approved / (2)disapproved');
            $table->integer('index')->nullable()->comment('1 indexado');
            $table->integer('store_status')->nullable()->comment('Status Store 1 Ativo, 2 Pausado');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('CASCADE');
            $table->foreign('status_id')->references('id')->on('products_status');
            $table->foreign('condition_id')->references('id')->on('products_condition');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
