<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateOrdersShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_shop', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('orders_master_id')->nullable()->comment('ID Relacão Order Master');
            $table->unsignedBigInteger('orders_status_id')->nullable()->comment('ID Relação Order Status');
            $table->unsignedBigInteger('origin_shop_id')->nullable()->comment('ID Relação Loja Vendedora Entrega');
            $table->unsignedBigInteger('affiliate_shop_id')->nullable()->comment('ID Relação Afiliado');
            $table->integer('affiliate_store_id')->nullable()->comment('Store Afiliado');
            $table->integer('order_shop_invoice_id')->nullable()->comment('ID Order Shop Invoice');
            $table->decimal('vlr_subtotal', 11, 2)->comment('Valor Subtotal do Pedido');
            $table->decimal('vlr_total', 11, 2)->comment('Valor Total com Frete');
            $table->decimal('vlr_desconto', 11, 2)->nullable()->comment('Valor do Desconto no pedido');
            $table->decimal('outrasDespesas', 11, 2)->nullable()->comment('Outras despesas da venda');
            $table->string('obs')->nullable()->comment('Observações do Pedido');
            $table->string('obs_internas')->nullable()->comment('Observações internas do Pedido');
            $table->integer('commission_id')->nullable()->comment('Relação Comissão  0');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('orders_master_id')->references('id')->on('orders_master');
            $table->foreign('orders_status_id')->references('id')->on('orders_status');
            $table->foreign('origin_shop_id')->references('id')->on('shops');
            $table->foreign('affiliate_shop_id')->references('id')->on('shops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_shop');
    }
}
