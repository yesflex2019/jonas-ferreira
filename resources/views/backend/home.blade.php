@extends('backend.master.layout')

@section('content')

    <div class="content content-fixed">
        <div class="pd-x-0 pd-lg-x-10 pd-xl-x-0">

            {{-- Notificação --}}
            @include('flash::message')

            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div class="input-group-prepend">
                    <a href="#painel" class="btn btn-lg btn-success" data-toggle="modal"><i data-feather="airplay"></i>
                        Trocar Painel</a>
                </div>
                <div class="d-none d-md-block mg-r-20">
                    {{-- Atendimetno Ao Cliente --}}
                    <a href="{{ route('sale.crm') }}" class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><i
                            data-feather="message-square" class="wd-10 mg-r-5"></i> Atendimento Ao Cliente</a>

                    {{-- Saque Conta Digital --}}
                    <a href="{{ route('financial.plunderRecebedor', ['type' => 'seller', 'id' => $shop_id]) }}"
                        class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><i data-feather="download"
                            class="wd-10 mg-r-5"></i> Saque Conta Digital</a>

                    {{-- Cadastrar Produtos --}}
                    <a href="{{ route('catalog.index') }}" class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><i
                            data-feather="upload" class="wd-10 mg-r-5"></i> Cadastrar Produtos</a>

                    {{-- Configurações --}}
                    <a href="{{ route('system.configMkp') }}"
                        class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="file"
                            class="wd-10 mg-r-5"></i> Configuração</a>

                    {{-- Tutorial --}}
                    <a href="#modalTutorial" class="btn btn-sm pd-x-15 btn-info btn-uppercase mg-l-5" data-toggle="modal"><i
                            data-feather="help-circle" class="wd-10 mg-r-5"></i> Tutorial</a>

                    {{-- Cursos Grátis --}}
                    <a href="{{ route('affiliate.courses') }}"
                        class="btn btn-sm pd-x-15 btn-warning btn-uppercase mg-l-5"><i data-feather="help-circle"
                            class="wd-10 mg-r-5"></i> Cursos Grátis</a>

                </div>
            </div>
        </div>
        <div class="row row-xs">
            <div class="col-6 col-sm-6 col-lg-2">
                <div class="card card-body">
                    <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Total deste mês
                        <span data-container="body" data-toggle="popover" data-placement="top"
                            data-content="Vendas realizadas no mês corrente."> <i class="fas fa-question-circle"></i></span>
                    </h6>
                    <div class="d-flex d-lg-block d-xl-flex align-items-end">
                        <h3 class="tx-normal tx-16 tx-sm-20 tx-rubik mg-b-0 mg-r-5 lh-1">R$
                            {{ number_format($sales_month, 2, ',', '.') }}</h3>
                    </div>
                    <div class="chart-three">
                        <div id="flotChart3" class="flot-chart ht-30"></div>
                    </div>{{-- chart-three --}}
                </div>
            </div>{{-- col --}}
            <div class="col-6 col-sm-6 col-lg-2 ">
                <div class="card card-body">
                    <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Vendas de hoje
                        <span data-container="body" data-toggle="popover" data-placement="top"
                            data-content="Vendas realizadas no dia corrente."> <i class="fas fa-question-circle"></i></span>
                    </h6>
                    <div class="d-flex d-lg-block d-xl-flex align-items-end">
                        <h3 class="tx-normal tx-16 tx-sm-20 tx-rubik mg-b-0 mg-r-5 lh-1">R$
                            {{ number_format($sales_day, 2, ',', '.') }}
                        </h3>
                    </div>
                    <div class="chart-three">
                        <div id="flotChart4" class="flot-chart ht-30"></div>
                    </div>{{-- chart-three --}}
                </div>
            </div>{{-- col --}}
            <div class="col-6 col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
                <div class="card card-body">
                    <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">A Receber
                        <span data-container="body" data-toggle="popover" data-placement="top"
                            data-content="O Valor integral a receber."> <i class="fas fa-question-circle"></i></span>
                    </h6>
                    <div class="d-flex d-lg-block d-xl-flex align-items-end">
                        <h3 id="aReceber" class="tx-normal tx-16 tx-sm-20 tx-rubik mg-b-0 mg-r-5 lh-1"></h3>
                        <br />
                    </div>
                    <div class="chart-three">
                        <div id="flotChart5" class="flot-chart ht-30"></div>
                    </div>{{-- chart-three --}}
                </div>
            </div>{{-- col --}}
            <div class="col-6 col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
                <div class="card card-body">
                    <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Saldo Disponível
                        <span data-container="body" data-toggle="popover" data-placement="top"
                            data-content="O Saldo disponível para saque."> <i class="fas fa-question-circle"></i></span>
                    </h6>
                    <div class="d-flex d-lg-block d-xl-flex align-items-end">
                        <h3 id="saldoDisponivel" class="tx-normal tx-16 tx-sm-20 tx-rubik mg-b-0 mg-r-5 lh-1"></h3>
                        <br />
                    </div>
                    <div class="chart-three">
                        <div id="flotChart6" class="flot-chart ht-30"></div>
                    </div>{{-- chart-three --}}
                </div>
            </div>{{-- col --}}
            <div class="col-6 col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
                <div class="card card-body">
                    <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8">Reembolso
                        <span data-container="body" data-toggle="popover" data-placement="top"
                            data-content="Valores reembolsados ou aguardando reembolso."> <i
                                class="fas fa-question-circle"></i></span>
                    </h6>
                    <div class="d-flex d-lg-block d-xl-flex align-items-end">
                        <h3 id="reembolso" class="tx-normal tx-16 tx-sm-20 tx-rubik mg-b-0 mg-r-5 lh-1"></h3>
                        <br />
                    </div>
                    <div class="chart-three">
                        <div id="flotChart7" class="flot-chart ht-30"></div>
                    </div>{{-- chart-three --}}
                </div>
            </div>{{-- col --}}
            <div class="col-6 col-sm-6 col-lg-2 mg-t-10 mg-lg-t-0">
                <div class="card card-body">
                    <h6 class="tx-uppercase tx-11 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 ">Antecipação
                        <span data-container="body" data-toggle="popover" data-placement="top"
                            data-content="Valores disponíveis para antecipação."> <i
                                class="fas fa-question-circle"></i></span>
                    </h6>
                    <div class="d-flex d-lg-block d-xl-flex align-items-end">
                        <h3 id="antecipacao" class="tx-normal tx-16 tx-sm-20 tx-rubik mg-b-0 mg-r-5 lh-1"></h3>
                        <br />
                    </div>
                    <div class="chart-three">
                        <div id="flotChart8" class="flot-chart ht-30"></div>
                    </div>{{-- chart-three --}}
                </div>
            </div>{{-- col --}}

            {{-- - Relatorios de Vendas --}}
            <div class="col-lg-10 col-xl-10 mg-t-10">
                <div class="card">
                    <div class="card-header pd-y-20 d-md-flex align-items-center justify-content-between">
                        <h5 class="mg-b-0">Relatorio de vendas</h5>
                        <ul class="list-inline d-flex mg-t-20 mg-sm-t-10 mg-md-t-0 mg-b-0">
                            <li class="list-inline-item d-flex align-items-center">
                                <span class="d-block wd-10 ht-10 bg-df-1 rounded mg-r-5"></span>
                                <span class="tx-sans tx-uppercase tx-10 tx-medium tx-color-03">Mês Atual</span>
                            </li>
                        </ul>
                    </div>{{-- card-header --}}
                    <div class="card-body pos-relative pd-0">
                        <div data-label="Relatório de venda mês" class="df-example">
                            <div class="ht-250 ht-lg-300"><canvas id="vendasMes"></canvas></div>
                        </div>
                        {{-- chart-one --}}
                    </div>{{-- card-body --}}
                </div>{{-- card --}}
            </div>

            <div class="col-lg-2 col-xl-2 mg-t-10">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ route('marketplace.marketplaceIndex') }}">
                            <img src="{{ asset('images/banner_mkp.png') }}" class="img-fluid" alt="udemy">
                        </a>
                    </div>{{-- card-body --}}
                </div>{{-- card --}}
            </div>

            {{-- Pedidos Recentes --}}
            <div class="col-md-6 col-xl-5 mg-t-10">
                <div class="card ht-100p">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h6 class="mg-b-0">Pedidos Recentes 30 Dias</h6>

                    </div>
                    <ul class="list-group list-group-flush tx-13">
                        @if (count($recent_orders) > 0)
                            @foreach ($recent_orders as $list)
                                <li class="list-group-item d-flex pd-sm-x-20">
                                    <div class="avatar d-none d-sm-block"><span
                                            class="avatar-initial rounded-circle bg-teal"><i
                                                class="icon ion-md-checkmark"></i></span></div>
                                    <div class="pd-sm-l-10">
                                        <p class="tx-medium mg-b-0">{{ $list->order_cliente }}</p>
                                        <small class="tx-12 tx-color-03 mg-b-0">Pedido: #{{ $list->id }} | Data:
                                            {{ date('d/m/Y h:m', strtotime($list->created_at)) }} </small>
                                    </div>
                                    <div class="mg-l-auto text-right">
                                        <p class="tx-medium mg-b-0">R$ {{ number_format($list->vlr_total, 2, ',', '.') }}
                                        </p>
                                        <small class="tx-12 tx-success mg-b-0">{{ $list->order_status }}</small>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li class="list-group-item d-flex pd-sm-x-20">
                                <div class="pd-sm-l-10">
                                    <p class="tx-medium mg-b-0 mg-t-20 tx-gray-500">Sem Pedidos</p>
                                </div>
                            </li>
                        @endif

                    </ul>
                    {{-- card-footer --}}
                </div>{{-- card --}}
            </div>

            {{-- Produtos Mais Vendidos --}}
            <div class="col-md-6 col-xl-5 mg-t-10">
                <div class="card ht-100p">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h6 class="mg-b-0">Produtos Mais Vendidos 30 Dias</h6>
                    </div>
                    <ul class="list-group list-group-flush tx-13">

                        @if (count($selling_products) > 0)
                            @foreach ($selling_products as $list)
                                <li class="list-group-item d-flex pd-sm-x-20">
                                    <div class="avatar d-none d-sm-block"><span
                                            class="avatar-initial rounded-circle bg-teal"><i
                                                class="icon ion-md-checkmark"></i></span></div>
                                    <div class="pd-sm-l-10">
                                        <p class="tx-medium mg-b-0">
                                            {{ \Illuminate\Support\Str::limit($list->title, 50, $end = '...') }}</p>

                                    </div>
                                    <div class="mg-l-auto text-right">
                                        <p class="tx-medium mg-b-0">{{ $list->total_qtd }} peças</p>
                                        <small class="tx-12 tx-success mg-b-0"></small>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li class="list-group-item d-flex pd-sm-x-20">
                                <div class="pd-sm-l-10 ">
                                    <p class="tx-medium mg-b-0 mg-t-20 tx-gray-500">Sem Pedidos</p>
                                </div>
                            </li>
                        @endif
                    </ul>
                    {{-- card-footer --}}
                </div>{{-- card --}}
            </div>
            {{-- Publicidade Udemy --}}
            <div class="col-12 col-sm-6 col-lg-2 mg-t-10  ">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ route('affiliate.courses') }}"><img src="{{ asset('images/udemy.jpg') }}"
                                class="img-fluid" alt="udemy"></a>
                    </div>
                </div>
            </div>{{-- col --}}
        </div>
        {{-- Modal Tutorial Seller --}}
        <div class="modal fade" id="modalTutorial" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
                        <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                        <div class="media align-items-center">
                            <span class="tx-color-03 d-none d-sm-block"><i data-feather="thumbs-up"
                                    class="wd-60 ht-60"></i></span>
                            <div class="media-body mg-sm-l-20">
                                <h4 class="tx-18 tx-sm-20 mg-b-2">Tutorial Painel Fornecedor</h4>
                                <p class="tx-13 tx-color-03 mg-b-0">Saiba como funciona o Painel do Fornecedor.</p>
                            </div>
                        </div><!-- media -->
                    </div><!-- modal-header -->
                    <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
                        <div class="embed-container">
                            <iframe width="728" height="409" src="https://www.youtube.com/embed/II-wkqwEQLM"
                                title="Tutorial JuntyMe" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>

                    </div><!-- modal-body -->
                    <div class="modal-footer pd-x-20 pd-y-15">
                        <button type="button" onclick="neverCookie()" class="btn btn-white" data-dismiss="modal"><i
                                data-feather="x"></i> Não ver
                            mais</button>
                        <button type="button" onclick="laterCookie()" class="btn btn-primary" data-dismiss="modal"><i
                                data-feather="clock"></i> Ver
                            depois</button>
                    </div>
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    </div>{{-- row --}}




    {{-- Carregar Javascript --}}
    <input type="hidden" id="ctxMax" value="{{ $chart1['max'] }}">
    <input type="hidden" id="balanceAvailableBackend" value="{{ route('panel.balanceAvailableBackend') }}">
    <input type="hidden" id="amountReceivedBackend" value="{{ route('panel.amountReceivedBackend') }}">
    <input type="hidden" id="pendingRefundBackend" value="{{ route('panel.pendingRefundBackend') }}">
    <input type="hidden" id="advanceValueBackend" value="{{ route('panel.advanceValueBackend') }}">

@endsection

@section('script')

    <script src="{{ asset('backend/lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('backend/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('backend/lib/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('backend/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('backend/lib/jquery.flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('backend/lib/jquery.flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('backend/lib/jquery.flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('backend/lib/chart.js/Chart.bundle.min.js') }}"></script>

    <link href="{{ asset('backend/lib/select2/css/select2.min.css') }}" rel="stylesheet">

    <script src="{{ asset('backend/assets/js/dashforge.js') }}"></script>
    <script src="{{ asset('backend/assets/js/dashforge.sampledata.js') }}"></script>

    {{-- append theme customizer --}}
    <script src="{{ asset('backend/lib/js-cookie/js.cookie.js') }}"></script>

    {{-- Chat de Atendimento --}}
    <script src="//code-sa1.jivosite.com/widget/oAozFBpRom" async></script>

    @if (!is_null(config('juntyme.popup_tutorial')))
        <script>
            // Abrir Modal
            if (!Cookies.get("tutorialSeller")) {
                $('#modalTutorial').modal('show')
            }

            // Ver depois o cookie tutorial
            function laterCookie() {
                Cookies.set("tutorialSeller", "true", {
                    expires: 1
                });
            }

            // Não ver mais o cookie tutorial
            function neverCookie() {
                Cookies.set("tutorialSeller", "true");
            }
        </script>
    @endif

    <script>
        /// Chart Principal
        $(function() {
            'use strict'

            var ctxLabel = @json($chart2['ctxLabel']);
            var ctxData1 = @json($chart2['ctxData1']);
            var ctxData2 = @json($chart1['ctxData1']);
            var ctxColor1 = '#0168fa';
            var ctxColor2 = '#0168fa';
            var ctxMax = parseInt($('#ctxMax').val());

            // Area chart
            var ctx5 = document.getElementById('vendasMes');
            new Chart(ctx5, {
                type: 'line',
                data: {
                    labels: ctxLabel,
                    datasets: [{
                        data: ctxData1,
                        borderColor: ctxColor1,
                        borderWidth: 1,
                        backgroundColor: 'rgba(1,104,250, .5)'
                    }, {
                        data: ctxData2,
                        borderColor: ctxColor2,
                        borderWidth: 1,
                        backgroundColor: 'rgba(1,104,250, .5)'
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    scales: {
                        yAxes: [{
                            stacked: true,
                            gridLines: {
                                color: '#e5e9f2'
                            },
                            ticks: {
                                beginAtZero: true,
                                fontSize: 10,
                                max: ctxMax
                            }
                        }],
                        xAxes: [{
                            stacked: true,
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                beginAtZero: true,
                                fontSize: 11
                            }
                        }]
                    }
                }
            });

        })

        // Chart Decorativo
        $(function() {
            'use strict'

            var plot = $.plot('#flotChart', [{
                data: df3,
                color: '#69b2f8'
            }, {
                data: df1,
                color: '#d1e6fa'
            }, {
                data: df2,
                color: '#d1e6fa',
                lines: {
                    fill: false,
                    lineWidth: 1.5
                }
            }], {
                series: {
                    stack: 0,
                    shadowSize: 0,
                    lines: {
                        show: true,
                        lineWidth: 0,
                        fill: 1
                    }
                },
                grid: {
                    borderWidth: 0,
                    aboveData: true
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 350
                },
                xaxis: {
                    show: true,
                    ticks: [
                        [0, ''],
                        [8, 'Jan'],
                        [20, 'Feb'],
                        [32, 'Mar'],
                        [44, 'Apr'],
                        [56, 'May'],
                        [68, 'Jun'],
                        [80, 'Jul'],
                        [92, 'Aug'],
                        [104, 'Sep'],
                        [116, 'Oct'],
                        [128, 'Nov'],
                        [140, 'Dec']
                    ],
                    color: 'rgba(255,255,255,.2)'
                }
            });


            $.plot('#flotChart2', [{
                data: [
                    [0, 55],
                    [1, 38],
                    [2, 20],
                    [3, 70],
                    [4, 50],
                    [5, 15],
                    [6, 30],
                    [7, 50],
                    [8, 40],
                    [9, 55],
                    [10, 60],
                    [11, 40],
                    [12, 32],
                    [13, 17],
                    [14, 28],
                    [15, 36],
                    [16, 53],
                    [17, 66],
                    [18, 58],
                    [19, 46]
                ],
                color: '#69b2f8'
            }, {
                data: [
                    [0, 80],
                    [1, 80],
                    [2, 80],
                    [3, 80],
                    [4, 80],
                    [5, 80],
                    [6, 80],
                    [7, 80],
                    [8, 80],
                    [9, 80],
                    [10, 80],
                    [11, 80],
                    [12, 80],
                    [13, 80],
                    [14, 80],
                    [15, 80],
                    [16, 80],
                    [17, 80],
                    [18, 80],
                    [19, 80]
                ],
                color: '#f0f1f5'
            }], {
                series: {
                    stack: 0,
                    bars: {
                        show: true,
                        lineWidth: 0,
                        barWidth: .5,
                        fill: 1
                    }
                },
                grid: {
                    borderWidth: 0,
                    borderColor: '#edeff6'
                },
                yaxis: {
                    show: false,
                    max: 80
                },
                xaxis: {
                    ticks: [
                        [0, 'Jan'],
                        [4, 'Feb'],
                        [8, 'Mar'],
                        [12, 'Apr'],
                        [16, 'May'],
                        [19, 'Jun']
                    ],
                    color: '#fff',
                }
            });

            $.plot('#flotChart3', [{
                data: df4,
                color: '#9db2c6'
            }], {
                series: {
                    shadowSize: 0,
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0
                            }, {
                                opacity: .5
                            }]
                        }
                    }
                },
                grid: {
                    borderWidth: 0,
                    labelMargin: 0
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 60
                },
                xaxis: {
                    show: false
                }
            });

            $.plot('#flotChart4', [{
                data: df5,
                color: '#9db2c6'
            }], {
                series: {
                    shadowSize: 0,
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0
                            }, {
                                opacity: .5
                            }]
                        }
                    }
                },
                grid: {
                    borderWidth: 0,
                    labelMargin: 0
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 80
                },
                xaxis: {
                    show: false
                }
            });

            $.plot('#flotChart5', [{
                data: df6,
                color: '#9db2c6'
            }], {
                series: {
                    shadowSize: 0,
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0
                            }, {
                                opacity: .5
                            }]
                        }
                    }
                },
                grid: {
                    borderWidth: 0,
                    labelMargin: 0
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 80
                },
                xaxis: {
                    show: false
                }
            });

            $.plot('#flotChart6', [{
                data: df4,
                color: '#9db2c6'
            }], {
                series: {
                    shadowSize: 0,
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0
                            }, {
                                opacity: .5
                            }]
                        }
                    }
                },
                grid: {
                    borderWidth: 0,
                    labelMargin: 0
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 60
                },
                xaxis: {
                    show: false
                }
            });

            $.plot('#flotChart7', [{
                data: df4,
                color: '#9db2c6'
            }], {
                series: {
                    shadowSize: 0,
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0
                            }, {
                                opacity: .5
                            }]
                        }
                    }
                },
                grid: {
                    borderWidth: 0,
                    labelMargin: 0
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 60
                },
                xaxis: {
                    show: false
                }
            });

            $.plot('#flotChart8', [{
                data: df4,
                color: '#9db2c6'
            }], {
                series: {
                    shadowSize: 0,
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0
                            }, {
                                opacity: .5
                            }]
                        }
                    }
                },
                grid: {
                    borderWidth: 0,
                    labelMargin: 0
                },
                yaxis: {
                    show: false,
                    min: 0,
                    max: 60
                },
                xaxis: {
                    show: false
                }
            });


        })

        /// Recuperar Saldo ou Disponivel
        $("#saldoDisponivel").html(
            '<div style="filter: none; width: 204px; height: 20px; background-color: rgb(72 94 144 / 16%);"></div>'
        );
        $.ajax({
            url: $('#balanceAvailableBackend').val(),
            type: "get",
            dataType: "json",
            success: function(response) {
                if (response.success == true) {
                    var data = response.message;
                    data = parseFloat(data.replace(/^"|"$/g, ''));
                    value = data.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                    $('#saldoDisponivel').html(value);
                } else {
                    data = 0;
                    value = data.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                    $('#saldoDisponivel').html(value);
                }
            },
        });

        /// Recuperar Saldo a Receber
        $("#aReceber").html(
            '<div style="filter: none; width: 204px; height: 20px; background-color: rgb(72 94 144 / 16%);"></div>'
        );
        $.ajax({
            url: $('#amountReceivedBackend').val(),
            type: "get",
            dataType: "json",
            success: function(response) {
                if (response.success == true) {
                    var data = response.message;
                    data = parseFloat(data.replace(/^"|"$/g, ''));
                    value = data.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                    $('#aReceber').html(value);
                } else {
                    data = 0;
                    value = data.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                    $('#aReceber').html(value);
                }
            },
        });

        /// Valor Reembolso
        $("#reembolso").html(
            '<div style="filter: none; width: 204px; height: 20px; background-color: rgb(72 94 144 / 16%);"></div>'
        );
        $.ajax({
            url: $('#pendingRefundBackend').val(),
            type: "get",
            dataType: "json",
            success: function(response) {
                if (response.success == true) {
                    var data = response.message;
                    data = parseFloat(data.replace(/^"|"$/g, ''));
                    value = data.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                    $('#reembolso').html(value);
                } else {
                    data = 0;
                    value = data.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                    $('#reembolso').html(value);
                }
            },
        });

        /// Valor Antecipação
        $("#antecipacao").html(
            '<div style="filter: none; width: 204px; height: 20px; background-color: rgb(72 94 144 / 16%);"></div>'
        );
        $.ajax({
            url: $('#advanceValueBackend').val(),
            type: "get",
            dataType: "json",
            success: function(response) {
                if (response.success == true) {
                    var data = response.message;
                    data = parseFloat(data.replace(/^"|"$/g, ''));
                    value = data.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                    $('#antecipacao').html(value);
                } else {
                    data = 0;
                    value = data.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                    $('#antecipacao').html(value);
                }
            },
        });

        // Informações ?
        $(function() {
            'use strict'

            $('[data-toggle="popover"]').popover();

        });


        /// Script Icon
        feather.replace();

        /// Script Alert
        $('div.alert').not('.alert-important').delay(5000).fadeOut(350);
    </script>

@endsection
