@extends('storeMarketplace.affiliates.master.layout')

@section('content')

    {{-- Content --}}
    {{-- Banner Topo --}}
    <div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload"
        data-bg="{{ asset('juntyme/site/img/1920x258_1.jpg') }}" data-brk-library="component__breadcrumbs_css">
        <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    </div>

    {{-- Conteudo result product --}}
    @if (!is_null($release))
        <div class="main-wrapper">
            <main class="main-container pb-50  ">
                {{-- Conteúdo da Página --}}
                {{-- Banner 1 --}}
                @include('storeMarketplace.affiliates.site.include.bannerHome')

                {{-- Produtos em Destaque --}}
                <section>
                    <div class="container mb-20 mt-20">
                        <div class="text-left">
                            <h3 class="font__family-montserrat font__size-28 font__weight-bold">
                                <span class="heading__number">Produtos Em Destaque:</span>
                            </h3>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row brk-gutters-10">
                            @foreach ($products_detach as $itens)

                                @php
                                    // Imagem
                                    $image = $itens->image_type ? $itens->image : asset('storage/' . $itens->image);

                                    // Descrição
                                    // $description = $itens->short_description ? $itens->short_description : $itens->description;

                                    // Price
                                    $price = $itens->special_price < $itens->price && $itens->special_price > 0 ? $itens->special_price : $itens->price;

                                    // Parcelamento
                                    $amountParcelas = $price * (1 + ($fees * 12) / 100);
                                    $amount = $amountParcelas / 12;
                                @endphp

                                @if ($loop->iteration == 1)

                                    {{-- Produto 1 --}}
                                    <div class="col-lg-6 pt-10 pb-10">

                                        <div class="brk-sc-masonry-two brk-sc-masonry_long brk-sc-masonry_main-590"
                                            data-brk-library="component__shop_masonry">
                                            {{-- Imagem --}}
                                            <div class="brk-sc-masonry-two__thumbnail">

                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                    data-src="{{ $image ?? '' }}" class="lazyload"
                                                    alt="{{ $itens->title }}" onerror="handleError(this)">

                                            </div>
                                            {{-- Titulo / Descrição --}}
                                            <div
                                                class="brk-sc-masonry-two__container d-flex align-content-between flex-wrap">
                                                <div class="brk-sc-masonry-two__top-content">
                                                    <a
                                                        href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}">
                                                        <h4
                                                            class="font__family-montserrat font__weight-semibold text-uppercase font__size-25 line__height-27 letter-spacing-40">
                                                            {!! Str::limit(strip_tags($itens->title), 70, '...') !!}
                                                        </h4>
                                                    </a>
                                                    {{-- <div
                                                        class="brk-sc-masonry-two__description brk-dark-font-color font__family-roboto font__weight-light">
                                                        {!! Str::limit(strip_tags($description), 150, '...') !!}

                                                    </div> --}}
                                                </div>
                                                <div class="brk-sc-masonry-two__bottom-content">

                                                    {{-- Preço --}}
                                                    <div class="brk-sc-masonry-two__price">
                                                        <div
                                                            class="price font__family-montserrat font__weight-bold font__size-30">
                                                            12 x R$ {{ number_format($amount, 2, ',', '.') }}
                                                        </div>
                                                    </div>

                                                    {{-- Detalhes --}}
                                                    <div
                                                        class="brk-sc-masonry-two__links brk-sc-masonry-two__links_colored">
                                                        <a href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}"
                                                            class="btn btn-inside-out btn-md btn-icon border-radius-5 font__family-montserrat font__weight-bold"
                                                            data-brk-library="component__button">
                                                            <i class="fa fa-shopping-bag icon-inside"></i>
                                                            <span class="before">Detalhes</span><span
                                                                class="text">Agora</span><span
                                                                class="after">Detalhes</span>
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @elseif($loop->iteration > 1)

                                    {{-- PRODUTO 2 --}}
                                    @if ($loop->iteration == 2)

                                        <div class="col-lg-6">
                                            <div class="row brk-gutters-10">


                                                <div class="col-12 pt-10 pb-10">
                                                    <div class="brk-sc-masonry-two brk-sc-masonry_long brk-sc-masonry_small-285"
                                                        data-brk-library="component__shop_masonry">
                                                        {{-- Imagem --}}
                                                        <div class="brk-sc-masonry-two__thumbnail">
                                                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                                data-src="{{ $image ?? '' }}" class="lazyload"
                                                                alt="{{ $itens->title }}" onerror="handleError(this)">
                                                        </div>
                                                        <div
                                                            class="brk-sc-masonry-two__container d-flex align-content-between flex-wrap">
                                                            <div class="brk-sc-masonry-two__top-content">
                                                                {{-- Titulo --}}
                                                                <div
                                                                    class="brk-sc-masonry-two__description brk-dark-font-color font__family-roboto font__weight-light">
                                                                    <a
                                                                        href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}">
                                                                        <h4
                                                                            class="font__family-montserrat font__weight-semibold text-uppercase font__size-16 line__height-27 letter-spacing-60">

                                                                            {{ Str::limit(strip_tags($itens->title), 70, '...') }}
                                                                        </h4>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="brk-sc-masonry-two__bottom-content">
                                                                <div
                                                                    class="brk-sc-masonry-two__price d-flex flex-wrap align-items-center">

                                                                    {{-- Price --}}
                                                                    <div class="brk-sc-masonry-two__price">
                                                                        <div
                                                                            class="price font__family-montserrat font__weight-bold font__size-20">
                                                                            12 x R$
                                                                            {{ number_format($amount, 2, ',', '.') }}

                                                                        </div>
                                                                    </div>

                                                                    {{-- Detalhes --}}
                                                                    <div
                                                                        class="brk-sc-masonry-two__links brk-sc-masonry-two__links_colored">
                                                                        <a href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}"
                                                                            class="btn btn-inside-out btn-md btn-icon border-radius-5 font__family-montserrat font__weight-bold"
                                                                            data-brk-library="component__button">
                                                                            <i class="fa fa-shopping-bag icon-inside"></i>
                                                                            <span
                                                                                class="before">Detalhes</span><span
                                                                                class="text">Agora</span><span
                                                                                class="after">Detalhes</span>
                                                                        </a>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- PRODUTO 3 --}}
                                            @elseif($loop->iteration == 3)
                                                <div class="col-lg-6 pt-10 pb-10">

                                                    <div class="brk-sc-masonry-two brk-sc-masonry_narrow brk-sc-masonry_small-285"
                                                        data-brk-library="component__shop_masonry">
                                                        <div class="brk-sc-masonry-two__container">

                                                            {{-- Imagem --}}
                                                            <div class="brk-sc-masonry-two__thumbnail">
                                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                                    data-src="{{ $image ?? '' }}" class="lazyload"
                                                                    alt="{{ $itens->title }}"
                                                                    onerror="handleError(this)">
                                                            </div>

                                                            {{-- Titulo --}}
                                                            <div class="brk-sc-masonry-two__layer">
                                                                <a href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}"
                                                                    class="brk-sc-masonry-two__title">
                                                                    <h4
                                                                        class="font__family-montserrat font__weight-semibold text-uppercase font__size-16 line__height-27 letter-spacing-60 brk-white-font-color">
                                                                        {{ Str::limit(strip_tags($itens->title), 70, '...') }}
                                                                    </h4>
                                                                </a>

                                                                {{-- Descrição --}}
                                                                {{-- <div
                                                                    class="brk-sc-masonry-two__description brk-dark-font-color-3 font__family-roboto font__weight-light">
                                                                    {!! Str::limit(strip_tags($description), 100, '...') !!}
                                                                </div> --}}
                                                                <div
                                                                    class="brk-sc-masonry-two__buy brk-sc-masonry-two__buy_colored d-flex flex-wrap align-items-center">

                                                                    {{-- Preço --}}
                                                                    <div
                                                                        class="price font__family-montserrat font__weight-bold brk-white-font-color">
                                                                        12 x R$
                                                                        {{ number_format($amount, 2, ',', '.') }}

                                                                    </div>

                                                                    {{-- Detalhes --}}
                                                                    <a href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}"
                                                                        class="btn btn-prime btn-md border-radius-5 font__family-montserrat font__weight-bold brk-white-font-color"
                                                                        data-brk-library="component__button">
                                                                        <i class="fa fa-shopping-bag"></i>
                                                                        <span class="before"></span><span
                                                                            class="after"></span><span
                                                                            class="border-btn"></span>Detalhes
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                {{-- PRODUTO 4 --}}
                                            @elseif($loop->last == 4)

                                                <div class="col-lg-6 pt-10 pb-10">

                                                    <div class="brk-sc-masonry-two brk-bg-secondary brk-sc-masonry_narrow brk-sc-masonry_small-285"
                                                        data-brk-library="component__shop_masonry">
                                                        <div class="brk-sc-masonry-two__container">

                                                            {{-- Imagem --}}
                                                            <div class="brk-sc-masonry-two__thumbnail">
                                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                                    data-src="{{ $image ?? '' }}" class="lazyload"
                                                                    alt="{{ $itens->title }}"
                                                                    onerror="handleError(this)">
                                                            </div>

                                                            {{-- Titulo --}}
                                                            <div class="brk-sc-masonry-two__layer">
                                                                <a href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}"
                                                                    class="brk-sc-masonry-two__title">
                                                                    <h4
                                                                        class="font__family-montserrat font__weight-semibold text-uppercase font__size-16 line__height-27 letter-spacing-60 brk-white-font-color">
                                                                        {{ Str::limit($itens->title, 40, '...') }}
                                                                    </h4>
                                                                </a>

                                                                {{-- Descrição --}}
                                                                {{-- <div
                                                                    class="brk-sc-masonry-two__description brk-dark-font-color-3 font__family-roboto font__weight-light">
                                                                    {!! Str::limit(strip_tags($description), 100, '...') !!}
                                                                </div> --}}
                                                                <div
                                                                    class="brk-sc-masonry-two__buy brk-sc-masonry-two__buy_colored d-flex flex-wrap align-items-center">

                                                                    {{-- Preços --}}
                                                                    <div
                                                                        class="price font__family-montserrat font__weight-bold brk-white-font-color">
                                                                        12 x R$ {{ number_format($amount, 2, ',', '.') }}
                                                                    </div>

                                                                    {{-- Detalhes --}}
                                                                    <a href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}"
                                                                        class="btn btn-prime btn-md border-radius-5 font__family-montserrat font__weight-bold brk-white-font-color"
                                                                        data-brk-library="component__button">
                                                                        <i class="fa fa-shopping-bag"></i>
                                                                        <span class="before"></span><span
                                                                            class="after"></span><span
                                                                            class="border-btn"></span>Detalhes
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="text-center mt-35">
                        <a href="{{ route('mkp.shop') }}"
                            class="btn btn-inside-out btn-inside-out-invert btn-lg btn-icon btn-violet border-radius-5 font__family-montserrat font__weight-bold"
                            data-brk-library="component__button">
                            <i class="fa fa-angle-right icon-inside icon-inline" aria-hidden="true"></i>
                            <span class="before">Todos os Produtos</span><span class="text">Clique
                                Aqui</span><span class="after">Todos os Produtos</span>
                        </a>
                    </div>
                </section>


                {{-- PRODUTOS MAIS VENDIDOS --}}
                <section>
                    <div class="container mb-20 mt-20">
                        <div class="text-left">
                            <h3 class="font__family-montserrat font__size-28 font__weight-bold">
                                <span class="heading__number">Mais Vendidos</span>
                            </h3>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            {{-- Produtos Mais Vendidos --}}
                            @foreach ($products_sales as $itens)
                                @php
                                    // Imagem
                                    $image = $itens->image_type ? $itens->image : asset('storage/' . $itens->image);

                                    $price = $itens->special_price < $itens->price && $itens->special_price > 0 ? $itens->special_price : $itens->price;

                                    // Parcelamento
                                    $amountParcelas = $price * (1 + ($fees * 12) / 100);
                                    $amount = $amountParcelas / 12;
                                @endphp

                                <div class="col-xl-3 col-lg-4 col-sm-6">
                                    <div class="flip-box brk-sc-flip-one text-center"
                                        data-brk-library="component__shop_flip,component__flip_box_css,slider__swiper">
                                        <div class="flip flip_horizontal">
                                            <div class="flip__front brk-sc-flip-one__front">

                                                {{-- Aviso desconto --}}
                                                @if ($itens->discount_percentage > 0)
                                                    <div
                                                        class="brk-sc-flip-one__stick brk-sc-flip-one__stick_style-1 font__family-montserrat font__weight-bold">
                                                        -{{ $itens->discount_percentage }}%</div>
                                                @endif

                                                {{-- Carregar Imagem --}}
                                                <div class="brk-sc-flip-one__thumb">
                                                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                        data-src="{{ $image ?? '' }}" alt="{{ $itens->title }}"
                                                        class="brk-abs-img lazyload" onerror="handleError(this)">
                                                </div>

                                                {{-- Detalhes --}}
                                                <div class="brk-sc-flip-one__description">
                                                    <div
                                                        class="brk-sc-flip-one__cat font__family-montserrat font__weight-bold text-uppercase brk-white-font-color brk-bg-primary brk-base-box-shadow-primary">
                                                        Detalhes</div>
                                                    <div class="brk-sc-flip-one__title ">
                                                        <h4 class="font__family-montserrat font__weight font__size-10">
                                                            {{ Str::limit($itens->title, 32, '...') }}</h4>
                                                    </div>

                                                    {{-- Valores e Parcelamento --}}
                                                    <div
                                                        class="brk-sc-flip-one__price font__family-montserrat line__height-10">
                                                        <div
                                                            class="col-12 price font__weight-bold brk-base-font-color font__size-18 ">
                                                            R$ {{ number_format($price, 2, ',', '.') }}</div>
                                                        <div
                                                            class="brk-base-font-color font__family-montserrat font__weight-bold font__size-16  mt-10">
                                                            ou até 12 x R$ {{ number_format($amount, 2, ',', '.') }}
                                                        </div>
                                                        <div class="col-12 d-flex flex-wrap mt-10">

                                                            @if (!is_null($itens->aliexpress_id))
                                                                <span class="brk-sc-flip-four__stick">Importado</span>
                                                            @endif
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flip__back brk-sc-flip-one__back ">

                                                {{-- Aviso desconto --}}
                                                @if ($itens->discount_percentage > 0)
                                                    <div
                                                        class="brk-sc-flip-one__stick brk-sc-flip-one__stick_style-1 font__family-montserrat font__weight-bold">
                                                        -{{ $itens->discount_percentage }}%</div>
                                                @endif

                                                {{-- Imagem --}}
                                                <div class="brk-swiper-default brk-sc-flip-one__slider">
                                                    <div class="swiper-container">
                                                        <div class="swiper-wrapper">
                                                            <div class="swiper-slide">
                                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                                    data-src="{{ $image ?? '' }}"
                                                                    alt="{{ $itens->title }}" class="lazyload"
                                                                    onerror="handleError(this)">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brk-swiper-default-nav-light brk-swiper-default-nav-prev">
                                                        <i class="fal fa-angle-left"></i>
                                                    </div>
                                                    <div class="brk-swiper-default-nav-light brk-swiper-default-nav-next">
                                                        <i class="fal fa-angle-right"></i>
                                                    </div>
                                                </div>

                                                {{-- Button Detalhes --}}
                                                <a href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}"
                                                    class="btn btn-inside-out btn-md btn-icon border-radius-25 font__family-open-sans font__weight-bold"
                                                    data-brk-library="component__button">
                                                    <i class="fa fa-shopping-basket icon-inside" aria-hidden="true"></i>
                                                    <span class="before">Detalhes</span><span
                                                        class="text">Clique Aqui</span><span
                                                        class="after">Detalhes</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </section>


                {{-- Produtos Novos Cadastros --}}
                <section>
                    <div class="container mb-20 mt-20">
                        <div class="text-left">
                            <h3 class="font__family-montserrat font__size-28 font__weight-bold">
                                <span class="heading__number">O que há de Novidade?</span>
                            </h3>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">

                            @foreach ($products_news as $itens)
                                @php
                                    // Carregar Imagem
                                    $image = $itens->image_type ? $itens->image : asset('storage/' . $itens->image);

                                    $price = $itens->special_price > 0 ? $itens->special_price : $itens->price;

                                    // Parcelamento
                                    $amountParcelas = $price * (1 + ($fees * 12) / 100);
                                    $amount = $amountParcelas / 12;
                                @endphp
                                <div class="col-xl-3 col-lg-4 col-sm-6">
                                    <div class="flip-box brk-sc-flip-one text-center"
                                        data-brk-library="component__shop_flip,component__flip_box_css,slider__swiper">
                                        <div class="flip flip_horizontal">
                                            <div class="flip__front brk-sc-flip-one__front">
                                                {{-- Aviso de Desconto --}}
                                                @if ($itens->discount_percentage > 0)
                                                    <div
                                                        class="brk-sc-flip-one__stick brk-sc-flip-one__stick_style-1 font__family-montserrat font__weight-bold">
                                                        -{{ $itens->discount_percentage }}%</div>
                                                @endif

                                                <div class="brk-sc-flip-one__thumb">
                                                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                        data-src="{{ $image ?? '' }}" alt="{{ $itens->title }}"
                                                        class="brk-abs-img lazyload" onerror="handleError(this)">
                                                </div>
                                                <div class="brk-sc-flip-one__description ">
                                                    <div
                                                        class="brk-sc-flip-one__cat font__family-montserrat font__weight-bold text-uppercase brk-white-font-color brk-bg-primary brk-base-box-shadow-primary">
                                                        Detalhes</div>
                                                    <div class="brk-sc-flip-one__title ">
                                                        <h4 class=" font__family-montserrat font__weight font__size-10 ">
                                                            {{ Str::limit($itens->title, 32, '...') }}</h4>
                                                    </div>
                                                    <div
                                                        class="brk-sc-flip-one__price font__family-montserrat line__height-10">

                                                        <div
                                                            class="col-12 price font__weight-bold brk-base-font-color font__size-18">
                                                            R$ {{ number_format($price, 2, ',', '.') }}</div>
                                                        <div
                                                            class="col-12 brk-base-font-color font__family-montserrat font__weight-bold font__size-15 mt-10">
                                                            ou até 12 x R$ {{ number_format($amount, 2, ',', '.') }}
                                                        </div>
                                                        <div class="col-12 d-flex flex-wrap mt-10">
                                                            @if (!is_null($itens->aliexpress_id))
                                                                <span
                                                                    class="brk-sc-flip-four__stick brk-bg-secondary">Importado</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="flip__back brk-sc-flip-one__back">
                                                {{-- Aviso Desconto --}}
                                                @if ($itens->discount_percentage > 0)
                                                    <div
                                                        class="brk-sc-flip-one__stick brk-sc-flip-one__stick_style-1 font__family-montserrat font__weight-bold">
                                                        -{{ $itens->discount_percentage }}%</div>
                                                @endif
                                                {{-- Carregar Imagem --}}
                                                <div class="brk-swiper-default brk-sc-flip-one__slider">
                                                    <div class="swiper-container">
                                                        <div class="swiper-wrapper">
                                                            <div class="swiper-slide">
                                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                                    data-src="{{ $image ?? '' }}"
                                                                    alt="{{ $itens->title }}" class="lazyload"
                                                                    onerror="handleError(this)">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="brk-swiper-default-nav-light brk-swiper-default-nav-prev">
                                                        <i class="fal fa-angle-left"></i>
                                                    </div>
                                                    <div class="brk-swiper-default-nav-light brk-swiper-default-nav-next">
                                                        <i class="fal fa-angle-right"></i>
                                                    </div>
                                                </div>

                                                {{-- Button Detalhes --}}
                                                <a href="{{ route('mkp.product', ['product_id' => $itens->id, 'slug' => $itens->slug]) }}"
                                                    class="btn btn-inside-out btn-md btn-icon border-radius-25 font__family-open-sans font__weight-bold"
                                                    data-brk-library="component__button">
                                                    <i class="fa fa-shopping-basket icon-inside" aria-hidden="true"></i>
                                                    <span class="before">Detalhes</span><span
                                                        class="text">Clique Aqui</span><span
                                                        class="after">Detalhes</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </section>
            </main>
        </div>
    @else

        <div class="main-wrapper">
            <main class="main-container">
                <div class="mt-50 mt-md-50">
                    <div class="container mt-20 mt-md-80">
                        <div class="cfa__wrapper cfa__trend indent__1  font__size-18 text-left"
                            data-brk-library="component__call_to_action">
                            <span class="grad-border"></span>
                            <div>
                                <h2 class=" font__weight-bold font__size-18 ">Olá, você ainda
                                    <br>
                                    <span class="text-blue">Não tem</span> produtos cadastrados
                                </h2>
                                <div class="divider-layers">
                                    <span class="before"></span>
                                </div>
                                <p class="font__size-16 font__family-oxygen font__weight-light line__height-26 text-dark">
                                    Acesse o painel de administrador e relacione. </p>
                                Relacione um número superior a 50 itens para o site ser preenchido.
                            </div>

                        </div>
                    </div>
                </div>
            </main>
        </div>
    @endif
    <input id="noImage" type="hidden" value="{{ asset('/images/noimage.jpg') }}">

    {{-- Content --}}

@endsection


@section('script')

    <script src="{{ asset('juntyme/site/js/scripts.min.js') }}"></script>

    <script>
        // erro Imagem
        function handleError(image) {
            image.onerror = "";
            image.src = $('#noImage').val();
            return true;
        }
    </script>

@endsection
