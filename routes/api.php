
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });apiJwt



//////////////////////////////////////////////////////
/** ROTAS PAGARME POSTBACK */
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
Route::group([
    'namespace' => 'Pagarme'
], function () {

    //// Importação de Produtos Mercado Livre
    Route::post('pagarme/postback/recebedor', 'PagarmeController@recebedor')->name('pagarme.recebedor');
    Route::post('pagarme/postback/user/assinatura', 'PagarmeController@assinatura')->name('pagarme.assinatura');
    Route::post('pagarme/postback/user/payment', 'PagarmeController@userPayment')->name('pagarme.userPayment');
    Route::post('pagarme/postback/user/payment/invoice', 'PagarmeController@clientPaymentInvoiceShop')->name('pagarme.clientPaymentInvoiceShop');
    Route::post('pagarme/postback/client/payment', 'PagarmeController@clientPayment')->name('pagarme.clientPayment');
});


Route::group(
    [
        'namespace' => 'Locaweb'
    ],
    function () {

        //// Importação de Produtos Mercado Livre
        Route::post('locaweb/newsletter/bounces', 'NewsletterController@bounces')->name('locaweb.bounces');
        Route::post('locaweb/newsletter/openings', 'NewsletterController@openings')->name('pagarme.openings');
    }
);

Route::group(
    [
        'namespace' => 'Dropify'
    ],
    function () {

        //// Importação de Produtos Mercado Livre
        Route::any('dropify/webhooks', 'ApiController@webhooks')->name('dropify.webhooks');
    }
);


//////////////////////////////////////////////////////
/** ROTAS API HTTP ENDPOINT */
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
Route::group([
    'namespace' => 'Api'
], function () {

    ## ROTA GET
    Route::get('v1/credencial', 'ApiHttpController@credencial')->name('http.credencial');
    Route::get('v1/catalog/all', 'ApiHttpController@catalogAll')->name('http.catalog');
    Route::get('v1/product/details', 'ApiHttpController@productDetails')->name('http.productDetails');
    Route::get('v1/category/all', 'ApiHttpController@categoryAll')->name('http.categoryAll');
    Route::get('v1/provider/all', 'ApiHttpController@providerAll')->name('http.providerAll');
    Route::get('v1/brand/all', 'ApiHttpController@brandAll')->name('http.brandAll');
    Route::get('v1/client/all', 'ApiHttpController@clientAll')->name('http.clientAll');
    Route::get('v1/client/details', 'ApiHttpController@clientDetails')->name('http.clientDetails');
    Route::get('v1/affiliate/all', 'ApiHttpController@affiliateAll')->name('http.affiliateAll');
    Route::get('v1/affiliate/details', 'ApiHttpController@affiliateDetails')->name('http.affiliateDetails');

    Route::get('v1/orders/all', 'ApiHttpController@ordersAll')->name('http.ordersAll');
    Route::get('v1/orders/details', 'ApiHttpController@ordersDetails')->name('http.ordersDetails');

    Route::get('v1/called/all', 'ApiHttpController@calledAll')->name('http.calledAll');
    Route::get('v1/called/details', 'ApiHttpController@calledDetails')->name('http.calledDetails');

    Route::get('v1/marketpleca/all', 'ApiHttpController@marketplace')->name('http.marketplace');
    Route::get('v1/marketpleca/product/details', 'ApiHttpController@marketplaceDetails')->name('http.marketplaceDetails');
    Route::get('v1/marketpleca/product/update', 'ApiHttpController@marketplaceUpdate')->name('http.marketplaceUpdate');


    ## ROTA POST
    Route::post('v1/product/new', 'ApiHttpController@productNew')->name('http.productNew');
    Route::post('v1/product/gallery', 'ApiHttpController@productGallery')->name('http.productGallery');
    Route::post('v1/product/attribute', 'ApiHttpController@productAttributy')->name('http.productAttributy');
    Route::post('v1/category/new', 'ApiHttpController@categoryNew')->name('http.categoryNew');
    Route::post('v1/provider/new', 'ApiHttpController@providerNew')->name('http.providerNew');
    Route::post('v1/brand/new', 'ApiHttpController@brandNew')->name('http.brandNew');

    Route::post('v1/orders/tracking', 'ApiHttpController@ordersTracking')->name('http.ordersTracking');
    Route::post('v1/orders/invoice', 'ApiHttpController@ordersInvoice')->name('http.ordersInvoice');

    Route::post('v1/called/response', 'ApiHttpController@calledResponse')->name('http.calledResponse');

    ## ROTA PUT
    Route::put('v1/product/update', 'ApiHttpController@productUpdate')->name('http.productUpdate');

    ## ROTA DELETE
    Route::delete('v1/product/delete', 'ApiHttpController@productDelete')->name('http.productDelete');
    Route::delete('v1/category/delete', 'ApiHttpController@categoryDelete')->name('http.categoryDelete');
    Route::delete('v1/provider/delete', 'ApiHttpController@providerDelete')->name('http.providerDelete');
    Route::delete('v1/brand/delete', 'ApiHttpController@brandDelete')->name('http.brandDelete');
});

/** Segurança */
Route::post('auth/login', 'Api\\AuthController@login');

/** Controle de Rotas API */
Route::group(['middleware' => ['apiJwt']], function () {

    Route::post('auth/refresh', 'Api\\AuthController@refresh');
    Route::post('auth/me', 'Api\\AuthController@me');
    Route::post('auth/logout', 'Api\\AuthController@logout');
    Route::get('users', 'Api\\UserController@index');
    Route::get('user/{id}', 'Api\\UserController@show');
});
