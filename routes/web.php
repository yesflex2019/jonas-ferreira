<?php
//// INSTALAÇÃO DO LARAVEL ////
/// https://dev.to/dendihandian/laradock-a-php-developer-s-best-friend-33ef
/// Clonar Projeto no Git
/// https://medium.com/@mateusgalasso/laravel-importando-projetos-dc31a3488d04
//// Erro Mysql e Postgres  https://academy.especializati.com.br/ticket/laradock-e-erro-mysql
//// https://academy.especializati.com.br/ticket/laradock-e-erro-mysql
/// Link para dominios no windows C:\Windows\System32\drivers\etc\hosts
/// https://packagist.org/
/// https://thephpleague.com/

//// Site de modulos importantes
//// https://spatie.be/open-source?search=&sort=-downloads


////// ATIVAR CRON LARAVEL
// Certifique-se de executar esses comandos após cada nova implantação

// php artisan horizon:purge
// php artisan horizon:terminate
// php artisan horizon:publish
// php artisan queue:restart
// Se você enfrentar qualquer problema, pode reiniciar o serviço de supervisor
// sudo service supervisor restart
// OU - Você pode reiniciar o programa específico
// sudo supervisorctl restart laravel_horizon


//// Proteção ssh
///// https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04

//  Session::put(['user_status' => $user_local->status, 'panel' => $user_painel->admin, 'shops' => $shop_user->shops ]);
//  echo "user_status - ".Session::get('user_status')." <br/>";
//  echo "panel - ".Session::get('panel')." <br/>";
//  echo "shops - ".Session::get('shops')." <br/>";
//  Instalação Docker mysql  docker-compose up -d nginx mysql phpmyadmin redis redis-webui workspace php-worker
//  Instalação Docker postgres  docker-compose up -d nginx postgres adminer redis redis-webui workspace php-worker

//// Comandos php artisan config:cache $$  php artisan route:cache
//// Acessar docker-compose exec workspace bash

/////  woocommerce https://codexshaper.github.io/docs/laravel-woocommerce/

//// https://woocommerce.github.io/woocommerce-rest-api-docs/#delete-a-refund
/////  Magento https://packagist.org/packages/grayloon/laravel-magento-api

//// sudo certbot --apache -d developers.juntyme.com.br  certificado digital
//////  docker exec -it myproject_workspace_1 /bin/sh  ## Entrar no Workpace

// Comandos do php artisan:
// php artisan clear-compiled
// php artisan auth:clear-resets
// php artisan cache:clear
// php artisan config:clear
// php artisan event:clear
// php artisan optimize:clear
// php artisan route:clear
// php artisan view:clear

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use UniSharp\LaravelFilemanager\Lfm;

// Get the current URL without the query string...

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//////////////////////////////////////////////////////
/** ROTAS FILEMANEGER */
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
Route::group(
    [
        'prefix' => 'filemanager-dashboard',
        'middleware' => ['web', 'auth:admin']
    ],
    function () {
        Lfm::routes();
    }
);


Route::group([
    'prefix' => 'filemanager-backend',
    'middleware' => ['web', 'auth:web']
], function () {
    Lfm::routes();
});


## Redirecionar para o Site Principal Area de Desenvolvimento
Route::get('/erro-page-development', function () {
    $url = 'https://juntyme.com.br';
    return Redirect::to($url);
});

if (config('multisite.host') === URL::to('/')) {

    Route::group([
        'domain' =>  config('multisite.host'),
        'namespace' => 'Home',
        'middleware' => 'demoMode'
    ], function () {

        //////////////////////////////////////////////////////
        /** CONTROLE DE ROTAS SITE JUNTYME */ ////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        // Acesso Página Principal

        Route::get('/', 'HomeController@index')->name('home.index');
        Route::get('/open-email', 'HomeController@openMail')->name('home.openMail');
        Route::get('/open-logo/{camp}/logo.png', 'HomeController@openMailLogo')->name('home.openMailLogo');
        Route::get('/cancelar-email', 'HomeController@cancelMail')->name('home.cancelMail');
        Route::get('/politica-de-privacidade', 'HomeController@privacyPolicy')->name('home.privacyPolicy');
        Route::get('/termos-de-servicos', 'HomeController@termsService')->name('home.termsService');
        Route::get('/resolucao-de-disputa', 'HomeController@disputeResolution')->name('home.disputeResolution');
        Route::get('/exclusao-de-dados', 'HomeController@deleteData')->name('home.deleteData');
        Route::get('/quanto-custo', 'HomeController@howMuch')->name('home.howMuch');

        /////////////////////////////////////////////////////
        /** CONTROLE DE ROTAS LANDINGPAGE E CHECKOUT  */ /////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////

        // Acesso Página Principal
        Route::get('/pd/{token}', 'HomeController@linkPage')->name('login.linkPage');
        Route::get('/onepage/{slug}', 'HomeController@onePageSales')->name('login.onePageSales');
        Route::get('/ck/{token}', 'HomeController@linkCheckout')->name('login.linkCheckout');
        Route::get('/checkout/{id}', 'HomeController@checkoutSales')->name('login.checkoutSales');

        //////////////////////////////////////////////////////
        /** CONTROLE DE FEED DE DADOS */ ////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        Route::get('/feeds/{slug}/google-feed.xml', 'FeedController@feedGoogle')->name('login.feedGoogle');
        Route::get('/feeds/{slug}/facebook-feed.xml', 'FeedController@feedFacebook')->name('login.feedFacebook');


        //////////////////////////////////////////////////////
        /** CONTROLE DE BANNERS */ ////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        Route::get('/banners/{codigo}/160x600.html', 'BannersController@bannerOne')->name('banner.bannerOne');
        Route::get('/banners/{codigo}/240x400.html', 'BannersController@bannerTwo')->name('banner.bannerTwo');
        Route::get('/banners/{codigo}/300x250.html', 'BannersController@bannerThree')->name('banner.bannerThree');
        Route::get('/banners/{codigo}/300x600.html', 'BannersController@bannerFour')->name('banner.bannerFour');
        Route::get('/banners/{codigo}/728x90.html', 'BannersController@bannerFive')->name('banner.bannerFive');
        Route::get('/banners/{codigo}/970x250.html', 'BannersController@bannerSix')->name('banner.bannerSix');
    });
} else if (config('multisite.backend') === URL::to('/')) {



    //////////////////////////////////////////////////////
    /** ROTAS DATATABLE */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    Route::group([
        'domain' =>  config('multisite.host'),
        'namespace' => 'DataTable',
        'middleware' => 'demoMode'
    ], function () {

        Route::get('/datatable-products', 'TableController@products')->name('datatable.products');
        Route::get('/datatable-lixeira', 'TableController@trashProducts')->name('datatable.trashProducts');
        Route::get('/datatable-meucatalogo', 'TableController@myCatalog')->name('datatable.myCatalog');
    });



    Route::group([
        'domain' =>  config('multisite.backend'),
        'middleware' => 'demoMode'
    ], function () {

        //////////////////////////////////////////////////////
        /** CONTROLE DE ROTAS LOGIN SOCIAL */
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////


        /** Acesso Facebook */
        Route::get('/login/facebook', 'SocialiteController@redirectToProvider')->name('login.facebook');
        Route::get('/login/facebook/callback', 'SocialiteController@handleProviderCallback')->name('login.facebookCallback');

        /** Acesso Google */
        Route::get('/login/google', 'SocialiteController@redirectToProviderGoogle')->name('login.google');
        Route::get('/login/google/callback', 'SocialiteController@handleProviderCallbackGoogle')->name('login.googleCallback');
    });

    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS ADMIN LOGISTA */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    Route::group(
        [
            'domain' =>  config('multisite.backend'),
            'namespace' => 'Admin',
            'middleware' => 'demoMode'
        ],
        function () {

            //////////////////////////////////////////////////////
            /** CONTROLE DE ROTAS CONTROLE DE ACESSO */ /////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            Route::get('/', 'LoginController@page')->name('login.pageIndex');
            Route::get('/error', 'LoginController@errorPage')->name('login.errorPage');
            Route::get('/acesso', 'LoginController@page')->name('login.page');
            Route::get('/acesso/logout', 'LoginController@logout')->name('login.logout');
            Route::get('/acesso/cadastro', 'LoginController@register')->name('login.register');
            Route::get('/acesso/recuperar-senha', 'LoginController@resetMail')->name('login.resetMail');
            Route::get('/acesso/aguardando-confirmacao', 'LoginController@confirmMailWaiting')->name('login.confirmMailWaiting');
            Route::get('/acesso/confirmar-email/{token}', 'LoginController@confirmMail')->name('login.confirmMail');
            Route::get('/acesso/recuperar-token', 'LoginController@recoverTokenUser')->name('login.recoverTokenUser');
            Route::get('/acesso/email-sucesso/{token}', 'LoginController@verify')->name('login.verify');
            Route::get('/acesso/shops/conta-pausada', 'LoginController@accountPaused')->name('shop.accountPaused');

            Route::any('/acesso/usuario/novo', 'LoginController@userCriate')->name('login.userCriate');

            /** ROTA POST  */
            Route::post('/acesso/login/do', 'LoginController@login')->name('login.login.do');
            Route::get('/acesso/direct-login/do', 'LoginController@directLogin')->name('login.directLogin.do');

            /** ROTA PUT  */
            Route::put('/acesso/novo-token', 'LoginController@newTokenUser')->name('login.newTokenUser');

            /** ROTA DELETE  */

            /** Envio Email */
            Route::post('/acesso/password/do', 'MailController@newUserMail')->name('mail.newUserMail.do');
            Route::put('/acesso/password/new', 'MailController@newPasswordUser')->name('mail.newPasswordUser.do');
            Route::any('/acesso/alterar-senha/{token}', 'MailController@editNewPassword')->name('mail.editNewPassword');

            //////////////////////////////////////////////////////
            /** CONTROLE DE ROTAS ALIEXPRESS */ ////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////

            ## Rota Any de Busca
            Route::any('/aliexpress/catalogo', 'AliexpressController@aliexpressCatalog')->name('catalog.aliexpressCatalog');
            Route::get('/aliexpress/catalogo/detalhes', 'AliexpressController@aliexpressCatalogDetails')->name('catalog.aliexpressCatalogDetails');

            ## Rota Any de busca
            Route::any('/aliexpress/produtos', 'AliexpressController@aliexpressProducts')->name('catalog.aliexpressProducts');

            ## Rota Any de Busca
            Route::any('/aliexpress/pedidos-clientes', 'AliexpressController@aliexpressCustomerOrders')->name('catalog.aliexpressCustomerOrders');
            Route::post('/aliexpress/salvar-produto', 'AliexpressController@aliexpressNewProduct')->name('catalog.aliexpressNewProduct');
            Route::get('/aliexpress/criar-ordem-compra/{id}', 'AliexpressController@aliexpressCreateOrder')->name('catalog.aliexpressCreateOrder');
            Route::get('/aliexpress/ordem-compra/detalhes/{id}', 'AliexpressController@aliexpressOrderDetails')->name('catalog.aliexpressOrderDetails');
            Route::get('/aliexpress/recuperar-lista-ordem-compra', 'AliexpressController@aliexpressRetrieveOrdersList')->name('catalog.aliexpressRetrieveOrdersList');
            Route::post('/aliexpress/ordem-compra/novo', 'AliexpressController@aliexpressCreateOrderNew')->name('catalog.aliexpressCreateOrderNew');
            Route::get('/aliexpress/ordem-compra/rastreio/{order}/{id}', 'AliexpressController@aliexpressOrderTracking')->name('catalog.aliexpressOrderTracking');
            Route::get('/aliexpress/ordem-compra/update-status/{id}', 'AliexpressController@aliexpressUpdateStatus')->name('catalog.aliexpressUpdateStatus');
            Route::post('/aliexpress/ordem-compra/delete', 'AliexpressController@aliexpressOrderDelete')->name('catalog.aliexpressOrderDelete');


            //////////////////////////////////////////////////////
            /** CONTROLE DE ROTAS CONTROLE VENDEDOR */ ////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////

            // Redirecinar Painel Usuário ///
            Route::get('/vendedor', 'CatalogController@admin')->name('panel.admin');
            Route::get('/vendedor/advanceValueBackend', 'CatalogController@advanceValueBackend')->name('panel.advanceValueBackend');
            Route::get('/vendedor/pendingRefundBackend', 'CatalogController@pendingRefundBackend')->name('panel.pendingRefundBackend');
            Route::get('/vendedor/amountReceivedBackend', 'CatalogController@amountReceivedBackend')->name('panel.amountReceivedBackend');
            Route::get('/vendedor/balanceAvailableBackend', 'CatalogController@balanceAvailableBackend')->name('panel.balanceAvailableBackend');

            // Redirecinar Painel Usuário ///
            Route::get('/vendedor/painel/{id}', 'LoginController@redirectUser')->name('panel.redirectUser');

            /** ROTA NOTIFICAÇÕES  */
            Route::get('/vendedor/notificacoes', 'CatalogController@notification')->name('catalog.notification');

            /** ROTA POST  */
            Route::get('/vendedor/mensagens', 'CatalogController@message')->name('catalog.message');
            Route::get('/vendedor/shops/nova-loja', 'ShopController@create')->name('shop.create');

            /** ROTA POST  */
            Route::post('/vendedor/shops/novo/do', 'ShopController@store')->name('shop.store.do');

            /** ROTA PUT  */

            /** ROTA DELETE  */

            Route::get('/vendedor/catalogo', 'CatalogController@index')->name('catalog.index');
            /**  Todas as Requisições ANY */
            Route::any('/vendedor/catalogo-search', 'CatalogController@index')->name('catalog.searchIndex');

            Route::get('/vendedor/catalogo/produto/', 'CatalogController@productCreate')->name('catalog.productCreate');
            Route::get('/vendedor/catalogo/produto/lixeira', 'CatalogController@productTrash')->name('catalog.productTrash');

            Route::get('/vendedor/catalogo/produto/update/dslite/{id}', 'CatalogController@productUpdateDslite')->name('catalog.productUpdateDslite');
            Route::get('/vendedor/catalogo/produto/update/bling/{id}', 'CatalogController@productUpdateBling')->name('catalog.productUpdateBling');
            Route::get('/vendedor/catalogo/produto/update/dropify/{id}', 'CatalogController@productUpdateDropify')->name('catalog.productUpdateDropify');
            Route::get('/vendedor/catalogo/produto/update/ml/{id}', 'CatalogController@productUpdateMl')->name('catalog.productUpdateMl');

            ## Link Produto Detalhes
            Route::get('/vendedor/catalogo/produto/duplicar/{id}', 'CatalogController@productDuplicate')->name('catalog.productDuplicate');
            Route::get('/vendedor/catalogo/produto/{id}', 'CatalogController@productEdit')->name('catalog.productEdit');

            Route::get('/vendedor/catalogo/busca/categoria', 'CatalogController@productCategoryArray')->name('catalog.productCategoryArray');
            Route::get('/vendedor/catalogo/busca/subcategoria', 'CatalogController@productSubcategoryArray')->name('catalog.productSubcategoryArray');

            Route::any('/vendedor/catalogo/categoria', 'CatalogController@productCategory')->name('catalog.productCategory');
            Route::get('/vendedor/catalogo/fornecedor', 'CatalogController@productProvider')->name('catalog.productProvider');
            Route::any('/vendedor/catalogo/marca', 'CatalogController@productBrand')->name('catalog.productBrand');

            Route::get('/vendedor/catalogo/importar-produtos-planilha', 'CatalogController@importSpreadsheetProducts')->name('catalog.importSpreadsheetProducts');
            Route::get('/vendedor/catalogo/confirmar-importacao-produtos/{key}', 'CatalogController@confirmImportProducts')->name('catalog.confirmImportProducts');
            Route::get('/vendedor/catalogo/confirmar-importacao-produtos-estoque/{key}', 'CatalogController@confirmImportProductsStock')->name('catalog.confirmImportProductsStock');
            Route::get('/vendedor/catalogo/download-produtos', 'CatalogController@downloadProducts')->name('catalog.downloadProducts');
            Route::get('/vendedor/catalogo/download-modelo-padrao', 'CatalogController@defaultTemplateDownload')->name('catalog.defaultTemplateDownload');
            Route::get('/vendedor/catalogo/atualizar-produtos-estoque', 'CatalogController@updateStockProducts')->name('catalog.updateStockProducts');

            Route::get('/vendedor/catalogo/atualizar-importar-produtos/aprovado/{key}', 'CatalogController@updateImportProductsDo')->name('catalog.updateImportProductsDo');
            Route::get('/vendedor/catalogo/atualizar-importar-produtos/cancelar/{key}', 'CatalogController@updateImportProductsCancel')->name('catalog.updateImportProductsCancel');

            Route::get('/vendedor/catalogo/atualizar-produtos-estoque/aprovado/{key}', 'CatalogController@updateStockProductsDo')->name('catalog.updateStockProductsDo');
            Route::get('/vendedor/catalogo/atualizar-produtos-estoque/cancelar/{key}', 'CatalogController@updateStockProductsCancel')->name('catalog.updateStockProductsCancel');

            Route::get('/vendedor/catalogo/relacionar-titulo-tabela/{key}', 'CatalogController@listTitleTableProducts')->name('catalog.listTitleTableProducts');

            Route::post('/vendedor/catalogo/upload-planilha-completa', 'CatalogController@completSpreadsheetUpload')->name('catalog.completSpreadsheetUpload');
            Route::post('/vendedor/catalogo/upload-planilha-completa/salvar', 'CatalogController@completSpreadsheetUploadDo')->name('catalog.completSpreadsheetUploadDo');
            Route::post('/vendedor/catalogo/upload-planilha-valor-estoque', 'CatalogController@priceStockSpreadsheetUpload')->name('catalog.priceStockSpreadsheetUpload');
            Route::post('/vendedor/catalogo/download-catalogo', 'CatalogController@exportCatalogProducts')->name('catalog.exportCatalogProducts');

            /** ROTA POST  */
            Route::post('/vendedor/catalogo/produto/novo', 'CatalogController@storeProduct')->name('catalog.productStore.to');
            Route::post('/vendedor/catalogo/categoria/novo', 'CatalogController@storeCategory')->name('catalog.storeCategory.to');
            Route::post('/vendedor/catalogo/subcategoria/novo', 'CatalogController@storeSubcategory')->name('catalog.storeSubcategor.to');
            Route::post('/vendedor/catalogo/fornecedor/novo', 'CatalogController@storeProvider')->name('catalog.storeProvider.to');
            Route::get('/vendedor/catalogo/fornecedor/address/{id}', 'CatalogController@storeProviderAddress')->name('catalog.storeProviderAddress');

            Route::post('/vendedor/catalogo/marca/novo', 'CatalogController@storeBrand')->name('catalog.storeBrand.to');
            Route::any('/vendedor/catalogo/mercadolivre/importar', 'MercadoLivreController@importMercadoLivre')->name('catalog.importMercadoLivre');

            /** ROTA PUT  */
            Route::put('/vendedor/catalogo/produto/edit', 'CatalogController@updateProduct')->name('catalog.updateProduct.to');
            Route::put('/vendedor/catalogo/produto/lixeira-to', 'CatalogController@trashProduct')->name('catalog.trashProduct.to');
            Route::put('/vendedor/catalogo/produto/lixeira-lista', 'CatalogController@trashListProduct')->name('catalog.trashListProduct.to');
            Route::put('/vendedor/catalogo/produto/status', 'CatalogController@statusProduct')->name('catalog.statusProduct');
            Route::put('/vendedor/catalogo/produto/restaurar', 'CatalogController@restoreProduct')->name('catalog.restoreProduct.to');
            Route::put('/vendedor/catalogo/produto/pausar', 'CatalogController@pauseProduct')->name('catalog.pauseProduct.to');
            Route::put('/vendedor/catalogo/fornecedor/address/update', 'CatalogController@storeProviderUpdate')->name('catalog.storeProviderUpdate');

            /** ROTA DELETE  */
            Route::delete('/vendedor/catalogo/fornecedor/excluir', 'CatalogController@deleteProvider')->name('catalog.deleteProvider.to');
            Route::delete('/vendedor/catalogo/marca/excluir', 'CatalogController@deleteBrand')->name('catalog.deleteBrand.to');
            Route::delete('/vendedor/catalogo/categoria/excluir', 'CatalogController@deleteCategory')->name('catalog.deleteCategory.to');
            Route::delete('/vendedor/catalogo/subcategoria/excluir', 'CatalogController@deleteSubcategory')->name('catalog.deleteSubcategory.to');

            /** CONTROLE CORREIOS SIGEP */
            ## Rota Any Busca
            Route::any('/vendedor/sigep/lista-etiquetas', 'SigepController@listaEtiquetasCorreios')->name('sigep.listaEtiquetasCorreios');

            Route::get('/vendedor/sigep/lista-plps', 'SigepController@listaPlpCorreios')->name('sigep.listaPlpCorreios');
            ## Rota Any Busca
            Route::any('/vendedor/sigep/lista-acompanhamento', 'SigepController@followMailingList')->name('sigep.followMailingList');

            ## Rota Any Busca
            Route::any('/vendedor/sigep/lista-postagem-reversa', 'SigepController@reversePostList')->name('sigep.reversePostList');

            Route::get('/vendedor/sigep/visualizarEtiquetaCorreios', 'SigepController@visualizarEtiquetaCorreios')->name('sigep.visualizarEtiquetaCorreios');
            Route::get('/vendedor/sigep/gerar-etiquetas', 'SigepController@gerarEtiquetasCorreios')->name('sigep.gerarEtiquetasCorreios');
            Route::get('/vendedor/sigep/declaracao-de-conteudo', 'SigepController@contentStatement')->name('sigep.contentStatement');
            Route::get('/vendedor/sigep/rastreio-correios', 'SigepController@rastrerObjetoCorreios')->name('sigep.rastrerObjetoCorreios');
            Route::get('/vendedor/sigep/rastreio-aliexpress', 'SigepController@rastrerObjetoAliexpress')->name('sigep.rastrerObjetoAliexpress');
            Route::post('/vendedor/sigep/adicionar-credito', 'SigepController@easyShippingaddCredit')->name('sigep.easyShippingaddCredit');
            Route::get('/vendedor/sigep/detalhes-etiqueta', 'SigepController@customerLabelDetails')->name('sigep.customerLabelDetails');

            Route::any('/vendedor/sigep/frete-facil/nova-etiqueta-envio', 'SigepController@newShippingLabel')->name('sigep.newShippingLabel');
            Route::any('/vendedor/sigep/frete-facil/nova-etiqueta-devolucao', 'SigepController@newReturnLabel')->name('sigep.newReturnLabel');
            Route::post('/vendedor/sigep/frete-facil/disponibilidade-de-etiqueta', 'SigepController@tagAvailability')->name('sigep.tagAvailabilitys');
            Route::post('/vendedor/sigep/frete-facil/gerar-etiqueta', 'SigepController@generateTag')->name('sigep.generateTag');
            Route::get('/vendedor/sigep/frete-facil/etiqueta-gerada/{tagID}', 'SigepController@successLabel')->name('sigep.successLabel');
            Route::get('/vendedor/sigep/frete-facil/codigo-gerado/{tagID}', 'SigepController@successLabelReverso')->name('sigep.successLabelReverso');
            Route::get('/vendedor/sigep/frete-facil/visualizarEtiquetaCorreiosFreteFacil', 'SigepController@visualizarEtiquetaCorreiosFreteFacil')->name('sigep.visualizarEtiquetaCorreiosFreteFacil');
            Route::get('/vendedor/sigep/frete-facil/buscaClienteCorreios', 'SigepController@buscaClienteCorreios')->name('sigep.buscaClienteCorreios');

            /**  CONTROLE VENDAS  */
            Route::get('/vendedor/vendas', 'SaleController@index')->name('sale.index');

            ## Rota Any Busca
            Route::any('/vendedor/vendas-search', 'SaleController@index')->name('sale.searchSales');

            Route::get('/vendedor/vendas/detalhes/{id}', 'SaleController@salesDetails')->name('sale.salesDetails');
            Route::get('/vendedor/vendas/crm', 'SaleController@crm')->name('sale.crm');
            Route::post('/vendedor/vendas/crm/novo', 'SaleController@crmNew')->name('sale.crmNew');
            Route::post('/vendedor/vendas/crm/detalhes/salvar', 'SaleController@crmDetailsSave')->name('sale.crmDetailsSave');
            Route::post('/vendedor/vendas/crm/detalhes/fechar', 'SaleController@crmDetailsClose')->name('sale.crmDetailsClose');
            Route::get('/vendedor/vendas/crm/detalhes/pauser', 'SaleController@crmDetailsPause')->name('sale.crmDetailsPause');
            Route::get('/vendedor/vendas/crm/detalhes/reabrir', 'SaleController@crmDetailsReopen')->name('sale.crmDetailsReopen');
            Route::get('/vendedor/vendas/crm/detalhes/{id}', 'SaleController@crmDetails')->name('sale.crmDetails');
            Route::get('/vendedor/vendas/relatorio', 'SaleController@generalSales')->name('sale.generalSales');

            Route::get('/vendedor/vendas/bling/new', 'BlingController@blingSendRequest')->name('sale.blingSendRequest');

            ## Rota Any Busca
            Route::any('/vendedor/vendas/clientes', 'SaleController@customers')->name('sale.customers');

            Route::get('/vendedor/vendas/clientes-detalhes/{id}', 'SaleController@customersDetails')->name('sale.customersDetails');
            Route::any('/vendedor/vendas/crm-search', 'SaleController@crm')->name('sale.searchCrm');
            Route::any('/vendedor/vendas/relatorio-search', 'SaleController@searchGeneralSales')->name('sale.searchGeneralSales');

            /** ROTA POST  */
            Route::post('/vendedor/vendas/rastreio-salvar', 'SaleController@traceSaveSales')->name('sale.traceSaveSales');
            Route::post('/vendedor/vendas/nota-fiscal-salvar', 'SaleController@invoiceSaveSales')->name('sale.invoiceSaveSales');
            Route::post('/vendedor/vendas/estornar/', 'SaleController@salesReverse')->name('sale.salesReverse');
            /** ROTA PUT  */

            /** ROTA DELETE  */

            /** CONTROLE FINANCEIRO  */
            Route::get('/vendedor/financeiro/recebive', 'FinancialController@billsReceive')->name('financial.billsReceive');
            ## Rota Any Busca
            Route::any('/vendedor/financeiro/recebive-search', 'FinancialController@billsReceive')->name('financial.searchBillsReceive');

            Route::get('/vendedor/financeiro/faturas', 'FinancialController@openInvoice')->name('financial.openInvoice');
            Route::get('/vendedor/financeiro/faturas/detalhes', 'FinancialController@openInvoiceDetails')->name('financial.openInvoiceDetails');
            Route::get('/vendedor/financeiro/faturas/detalhes/imprimir', 'FinancialController@openInvoiceDetailsPrinter')->name('financial.openInvoiceDetailsPrinter');

            Route::get('/vendedor/financeiro/payment', 'FinancialController@openInvoicePayment')->name('financial.openInvoicePayment');
            Route::post('/vendedor/financeiro/payment/update', 'FinancialController@updateInvoicePayment')->name('financial.updateInvoicePayment');
            Route::get('/vendedor/financeiro/payment/finalizar', 'FinancialController@finishInvoicePayment')->name('financial.finishInvoicePayment');
            Route::get('/vendedor/financeiro/mercado-pago', 'FinancialController@mercadoPago')->name('financial.mercadoPago');
            Route::get('/vendedor/financeiro/conta-bancaria', 'FinancialController@bankAccount')->name('financial.bankAccount');
            Route::post('/vendedor/financeiro/conta-bancaria/nova', 'FinancialController@newDigitalAccount')->name('financial.newDigitalAccount');

            Route::get('/vendedor/financeiro/mercado-pago/config', 'FinancialController@mercadoPagoConfig')->name('financial.mercadoPagoConfig');

            /* ANY */
            Route::any('/vendedor/financeiro/faturas-search', 'FinancialController@searchOpenInvoice')->name('financial.searchOpenInvoice');

            /** ROTA POST  */
            Route::post('/vendedor/mercado-pago/notification', 'FinancialController@notificationMp')->name('financial.notificationMp');
            Route::post('/vendedor/financeiro/payment/confirmed', 'FinancialController@InvoicePaymentConfirmed')->name('financial.InvoicePaymentConfirmed');
            Route::post('/vendedor/financeiro/faturas/cancelar', 'FinancialController@openInvoiceCancel')->name('financial.openInvoiceCancel');
            /** ROTA PUT  */

            /** ROTA DELETE  */
            Route::delete('/vendedor/financeiro/mercado-pago/remove', 'FinancialController@destroyMercadoPago')->name('financial.destroyMercadoPago');


            /** CONTROLE DE ROTAS FINANCEIRO PAGARME */ /////////
            Route::get('/financeiro/saque-redirect/{id}', 'FinancialController@plunderRedirect')->name('financial.plunderRedirect');
            Route::get('/financeiro/saque-recebedor/{type}', 'FinancialController@plunderRecebedor')->name('financial.plunderRecebedor');
            Route::post('/financeiro/saque-recebedores/confirmar', 'FinancialController@plunderRecebedorConfirm')->name('financial.plunderRecebedorConfirm');

            Route::get('/financeiro/saque-recebedores/relatorio', 'FinancialController@plunderRecebedorReport')->name('financial.plunderRecebedorReport');

            Route::put('/financeiro/saque-recebedores/cancelar', 'FinancialController@plunderRecebedorCancel')->name('financial.plunderRecebedorCancel');

            /**  CONTROLE AFILIADOS */
            ## Rota Any Busca
            Route::any('/vendedor/afiliados', 'AffiliateController@index')->name('affiliate.index');

            Route::get('/vendedor/afiliados/afiliados-detalhes/{id}', 'AffiliateController@affiliateDetails')->name('affiliate.affiliateDetails');
            Route::get('/vendedor/afiliados/vendas-detalhes/{id}', 'AffiliateController@affiliateDetailsSales')->name('affiliate.affiliateDetailsSales');
            Route::get('/vendedor/afiliados/lista-de-afiliados', 'AffiliateController@affiliateList')->name('affiliate.affiliateList');

            Route::get('/vendedor/afiliados/produtos-vendidos-afiliados', 'AffiliateController@productSold')->name('affiliate.productSold');
            ## Rota Any Busca
            Route::any('/vendedor/afiliados/produtos-vendidos-afiliados-search', 'AffiliateController@productSold')->name('affiliate.searchProductSold');

            /** ROTA ANY */
            Route::any('/vendedor/afiliados/lista-de-afiliados-search', 'AffiliateController@searchAffiliateList')->name('affiliate.searchAffiliateList');


            /** ROTA POST  */

            /** ROTA PUT  */

            /** ROTA DELETE  */

            /**  CONFIGURAÇÕES DO SISTEMA */

            Route::get('/settings', 'SystemController@settings')->name('system.settinsg');
            Route::get('/vendedor/sistema/configuracao-marketplace', 'SystemController@configMkp')->name('system.configMkp');
            Route::get('/vendedor/sistema/configuracao-transportadora', 'SystemController@shippingConfig')->name('system.shippingConfig');
            Route::get('/vendedor/sistema/relatorio/permissoes', 'SystemController@userPerm')->name('system.userPerm');
            Route::get('/vendedor/sistema/relatorio/config-envios', 'SystemController@configShipping')->name('system.configShipping');

            Route::get('/vendedor/sistema/correios', 'SystemController@correios')->name('system.correios');
            Route::get('/vendedor/sistema/frete-facil', 'SystemController@easyShipping')->name('system.easyShipping');
            Route::get('/vendedor/sistema/tabela-fixa', 'SystemController@fixedTable')->name('system.fixedTable');
            Route::get('/vendedor/sistema/dhl', 'SystemController@dhl')->name('system.dhl');
            Route::get('/vendedor/sistema/fedex', 'SystemController@fedex')->name('system.fedex');

            Route::get('/vendedor/sistema/mercado-livre/login', 'MercadoLivreController@loginMercadoLivre')->name('marketplace.loginMercadoLivre');
            Route::get('/vendedor/sistema/achaddo/login', 'AchaddoController@loginAchaddo')->name('marketplace.loginAchaddo');
            Route::get('/vendedor/sistema/afiliados/login', 'AffiliateController@loginAffiliate')->name('marketplace.loginAffiliate');
            Route::get('/vendedor/sistema/woocommerce/login', 'WoocommerceController@loginWoocommerce')->name('marketplace.loginWoocommerce');
            Route::get('/vendedor/sistema/magento/login', 'MagentoController@loginMagento')->name('marketplace.loginMagento');
            Route::get('/vendedor/sistema/aliexpress/login', 'AliexpressController@loginAliexpress')->name('marketplace.loginAliexpress');
            Route::get('/vendedor/sistema/redirect/login', 'RedirectController@loginRedirect')->name('marketplace.loginRedirect');
            Route::get('/vendedor/sistema/tabela-fixa/export', 'SystemController@fixedTableExport')->name('system.fixedTableExport');
            Route::get('/vendedor/sistema/dropify/login', 'DropifyController@loginDropify')->name('marketplace.loginDropify');
            Route::get('/vendedor/sistema/bling/login', 'BlingController@loginBling')->name('marketplace.loginBling');
            Route::get('/vendedor/sistema/dslite/login', 'DsliteController@loginDslite')->name('marketplace.loginDslite');
            Route::get('/vendedor/sistema/facily/login', 'FacilyController@loginFacily')->name('marketplace.loginFacily');
            Route::get('/vendedor/sistema/monetizze/login', 'MonetizzeController@loginMonetizze')->name('marketplace.loginMonetizze');
            Route::get('/vendedor/sistema/braip/login', 'BraipController@loginBraip')->name('marketplace.loginBraip');
            Route::get('/vendedor/sistema/eduzz/login', 'EduzzController@loginEduzz')->name('marketplace.loginEduzz');

            //// Importação Produtos Woocommerce
            Route::post('/vendedor/sistema/woocommerce/import', 'WoocommerceController@importWoocommerce')->name('marketplace.importWoocommerce');
            Route::post('/vendedor/sistema/woocommerce/atualizar-estoque', 'WoocommerceController@updateStockWoocommerce')->name('marketplace.updateStockWoocommerce');
            Route::post('/vendedor/sistema/woocommerce/atualizar-valores', 'WoocommerceController@updatePriceWoocommerce')->name('marketplace.updatePriceWoocommerce');

            //// Importação Produtos Magento
            Route::post('/vendedor/sistema/magento/import', 'MagentoController@importMagento')->name('marketplace.importMagento');
            Route::post('/vendedor/sistema/magento/atualizar-estoque', 'MagentoController@updateStockMagento')->name('marketplace.updateStockMagento');
            Route::post('/vendedor/sistema/magento/atualizar-valores', 'MagentoController@updatePriceMagento')->name('marketplace.updatePriceMagento');

            /// IMportação Produtos Dropify
            Route::post('/vendedor/sistema/dropify/import', 'DropifyController@importDropify')->name('marketplace.importDropify');
            Route::post('/vendedor/sistema/dropify/atualizar-estoque', 'DropifyController@updateStockDropify')->name('marketplace.updateStockDropify');
            Route::post('/vendedor/sistema/dropify/atualizar-valores', 'DropifyController@updatePriceDropify')->name('marketplace.updatePriceDropify');

            /// IMportação Produtos Dslite
            Route::post('/vendedor/sistema/dslite/import', 'DsliteController@importDslite')->name('marketplace.importDslite');
            Route::post('/vendedor/sistema/dslite/atualizar-estoque', 'DsliteController@updateStockDslite')->name('marketplace.updateStockDslite');
            Route::post('/vendedor/sistema/dslite/atualizar-valores', 'DsliteController@updatePriceDslite')->name('marketplace.updatePriceDslite');

            /// IMportação Produtos Bling
            Route::post('/vendedor/sistema/bling/import', 'BlingController@importBling')->name('marketplace.importBling');
            Route::post('/vendedor/sistema/bling/atualizar-estoque', 'BlingController@updateStockBling')->name('marketplace.updateStockBling');
            Route::post('/vendedor/sistema/bling/atualizar-valores', 'BlingController@updatePriceBling')->name('marketplace.updatePriceBling');

            /** ROTA POST  */
            Route::post('/vendedor/sistema/achaddo/novo', 'AchaddoController@newAchaddo')->name('marketplace.newAchaddo');
            Route::post('/vendedor/sistema/afiliados/novo', 'AffiliateController@newAffiliate')->name('marketplace.newAffiliate');
            Route::post('/vendedor/sistema/redirect/novo', 'RedirectController@newRedirect')->name('marketplace.newRedirect');
            Route::post('/vendedor/sistema/achaddo/liberar', 'AchaddoController@releaseAchaddo')->name('marketplace.releaseAchaddo');
            Route::post('/vendedor/sistema/afiliados/liberar', 'AffiliateController@releaseAffiliate')->name('marketplace.releaseAffiliate');
            Route::post('/vendedor/sistema/woocommerce/liberar', 'WoocommerceController@releaseWoocommerce')->name('marketplace.releaseWoocommerce');
            Route::post('/vendedor/sistema/magento/liberar', 'MagentoController@releaseMagento')->name('marketplace.releaseMagento');
            Route::post('/vendedor/sistema/aliexpress/liberar', 'AliexpressController@releaseAliexpress')->name('marketplace.releaseAliexpress');

            Route::get('/vendedor/sistema/aliexpress/deletar', 'AliexpressController@releaseAliexpressDelete')->name('marketplace.releaseAliexpressDelete');
            Route::get('/vendedor/sistema/dropify/deletar', 'DropifyController@releaseDropifyDelete')->name('marketplace.releaseDropifyDelete');
            Route::get('/vendedor/sistema/dslite/deletar', 'DsliteController@releaseDsliteDelete')->name('marketplace.releaseDsliteDelete');
            Route::get('/vendedor/sistema/bling/deletar', 'BlingController@releaseBlingDelete')->name('marketplace.releaseBlingDelete');
            Route::get('/vendedor/sistema/monetizze/deletar', 'MonetizzeController@releaseMonetizzeDelete')->name('marketplace.releaseMonetizzeDelete');
            Route::get('/vendedor/sistema/eduzz/deletar', 'EduzzController@releaseEduzzDelete')->name('marketplace.releaseEduzzDelete');
            Route::get('/vendedor/sistema/braip/deletar', 'BraipController@releaseBraipDelete')->name('marketplace.releaseBraipDelete');
            Route::get('/vendedor/sistema/monetizze/deletar', 'MonetizzeController@releaseMonetizzeDelete')->name('marketplace.releaseMonetizzeDelete');

            Route::any('/vendedor/sistema/configuracao/addCorreios', 'SystemController@addCorreios')->name('system.addCorreios');
            Route::post('/vendedor/sistema/tabela-fixa/import', 'SystemController@fixedTableImport')->name('system.fixedTableImport');
            Route::post('/vendedor/sistema/tabela-fixa/nova', 'SystemController@fixedTableNew')->name('system.fixedTableNew');

            Route::post('/vendedor/sistema/dropify/nova', 'DropifyController@saveDropify')->name('system.saveDropify');
            Route::post('/vendedor/sistema/dslite/nova', 'DsliteController@saveDslite')->name('system.saveDslite');
            Route::post('/vendedor/sistema/bling/nova', 'BlingController@saveBling')->name('system.saveBling');
            Route::post('/vendedor/sistema/facily/nova', 'FacilyController@saveFacily')->name('system.saveFacily');
            Route::post('/vendedor/sistema/monetizze/nova', 'MonetizzeController@saveMonetizze')->name('system.saveMonetizze');
            Route::post('/vendedor/sistema/braip/nova', 'BraipController@saveBraip')->name('system.saveBraip');
            Route::post('/vendedor/sistema/eduzz/nova', 'EduzzController@saveFacily')->name('system.saveEduzz');

            Route::post('/vendedor/sistema/woocommerce/test', 'WoocommerceController@testWoocommerce')->name('marketplace.testWoocommerce');

            /** ROTA ANY  */
            Route::any('/vendedor/sistema/mercado-livre/categorias', 'MercadoLivreController@categoryMercadoLivre')->name('marketplace.categoryMercadoLivre');

            /** ROTA PUT  */
            Route::put('/vendedor/sistema/achaddo/pausar', 'AchaddoController@pauseAchaddo')->name('marketplace.pauseAchaddo');
            Route::put('/vendedor/sistema/mercado-livre/destroy', 'MercadoLivreController@destroyMercadoLivre')->name('marketplace.destroyMercadoLivre');
            Route::put('/vendedor/sistema/woocommerce/destroy', 'WoocommerceController@destroyWoocommerce')->name('marketplace.destroyWoocommerce');
            Route::put('/vendedor/sistema/redirect/destroy', 'RedirectController@destroyRedirect')->name('marketplace.destroyRedirect');
            Route::put('/vendedor/sistema/magento/destroy', 'MagentoController@destroyMagento')->name('marketplace.destroyMagento');
            Route::put('/vendedor/sistema/mercado-livre/categorias/nova', 'MercadoLivreController@categoryMercadoLivreDo')->name('marketplace.categoryMercadoLivre.do');
            Route::put('/vendedor/sistema/mercado-livre/configuracao/nova', 'MercadoLivreController@configMercadoLivreDo')->name('marketplace.configMercadoLivre.do');
            Route::put('/vendedor/sistema/tabela-fixa/disable', 'SystemController@fixedTableDisable')->name('system.fixedTableDisable');

            ## Donwload arquivos
            Route::get('/vendedor/download/tabela-frete', 'SystemController@downloadShippingTable')->name('system.downloadShippingTable');
            Route::get('/vendedor/download/tabela-modelo-produtos-completa', 'SystemController@downloadCompleteProductsModel')->name('system.downloadCompleteProductsModel');
            Route::get('/vendedor/download/tabela-modelo-produtos-valor-estoque', 'SystemController@downloadStockValueModel')->name('system.downloadStockValueModel');

            /** ROTA DELETE  */
        }
    );

    Route::group([
        'domain' =>  config('multisite.backend'),
        'namespace' => 'Affiliate',
        'middleware' => 'demoMode'
    ], function () {


        //////////////////////////////////////////////////////
        //////////** CONTROLE DE ROTAS AFILIADOS  *//////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////

        Route::get('/afiliados', 'AdminController@index')->name('affiliate.affiliateIndex');
        Route::get('/afiliados/pendingRefundAffiliate', 'AdminController@pendingRefundAffiliate')->name('affiliate.pendingRefundAffiliate');
        Route::get('/afiliados/amountReceivedAffiliate', 'AdminController@amountReceivedAffiliate')->name('affiliate.amountReceivedAffiliate');
        Route::get('/afiliados/balanceAvailableAffiliate', 'AdminController@balanceAvailableAffiliate')->name('affiliate.balanceAvailableAffiliate');

        Route::any('/afiliados/saque-recebedores/relatorio', 'AdminController@plunderRecebedorReport')->name('affiliate.plunderRecebedorReport');
        Route::get('/afiliados/saque-recebedor/{type}', 'AdminController@plunderRecebedor')->name('affiliate.plunderRecebedor');

        Route::any('/afiliados/meu-catalogo', 'AdminController@myCatalog')->name('affiliate.myCatalog');

        Route::get('/afiliados/produtos/{id}', 'AdminController@myProduct')->name('affiliate.myProduct');

        Route::get('/afiliados/lojas-catalogo', 'AdminController@catalogStore')->name('affiliate.catalogStore');
        ## Rota Any Busca
        Route::any('/afiliados/catalogo-search', 'AdminController@catalogStore')->name('affiliate.searchIndex');

        Route::get('/afiliados/pedidos', 'AdminController@order')->name('affiliate.order');
        ## Rota Any Busca
        Route::any('/afiliados/pedidos-search', 'AdminController@order')->name('affiliate.searchOrder');

        Route::get('/afiliados/pedidos/detalhes/{id}', 'AdminController@orderDetails')->name('affiliate.orderDetails');
        Route::get('/afiliados/recebiveis', 'AdminController@receivableAffiliate')->name('affiliate.receivablesAffiliate');
        Route::get('/afiliados/ferramentas', 'AdminController@appliance')->name('affiliate.appliance');
        Route::get('/afiliados/minha-conta', 'AdminController@myAccount')->name('affiliate.myAccount');
        Route::any('/afiliados/courses', 'AdminController@courses')->name('affiliate.courses');
        Route::get('/afiliados/redes-sociais', 'AdminController@socialMidia')->name('affiliate.socialMidia');
        Route::get('/afiliados/feed-de-dados', 'AdminController@feedData')->name('affiliate.feedData');

        /** ROTA POST  */

        /** ROTA PUT  */
        Route::any('/afiliados/adicionar-ajax/', 'AdminController@addCatalogAjax')->name('affiliate.addCatalogAjax');
        Route::put('/afiliados/adicionar/', 'AdminController@addCatalog')->name('affiliate.addCatalog');
        Route::put('/afiliados/prova-social-page/', 'AdminController@socialProof')->name('affiliate.socialProof');
        Route::put('/afiliados/pausar/', 'AdminController@pauseCatalog')->name('affiliate.pauseCatalog');

        /** ROTA DELETE  */
    });


    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS MARKETPLACE  */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    Route::group([
        'domain' =>  config('multisite.backend'),
        'namespace' => 'Marketplace',
        'middleware' => 'demoMode'
    ], function () {

        /** CONFIGURAÇÕES CAIXA DE EMAILS */

        Route::get('/store', 'AdminController@index')->name('marketplace.marketplaceIndex');
        Route::get('/store/loja-pausada', 'AdminController@storePaused')->name('marketplace.storePaused');

        Route::get('/store/signatureStore', 'AdminController@signatureStore')->name('marketplace.signatureStore');
        Route::get('/store/assinaturas', 'AdminController@subscriptions')->name('marketplace.subscriptions');
        Route::get('/store/faturas', 'AdminController@invoice')->name('marketplace.invoice');
        Route::get('/store/faturas/viewPix', 'AdminController@invoiceViewPix')->name('marketplace.invoiceViewPix');
        Route::get('/store/faturas/viewBoleto', 'AdminController@invoiceViewBoleto')->name('marketplace.invoiceViewBoleto');

        Route::get('/store/fatura/detalhes/{id}', 'AdminController@invoiceDetails')->name('marketplace.invoiceDetails');
        Route::get('/store/fatura/detalhes/imprimir/{id}', 'AdminController@invoiceDetailsPrinter')->name('marketplace.invoiceDetailsPrinter');
        Route::get('/store/fatura/criar-pdf', 'AdminController@invoiceCreatePDF')->name('marketplace.invoiceCreatePDF');
        Route::get('/store/novo-plano/{id}', 'AdminController@newPlane')->name('marketplace.newPlane');
        Route::get('/store/update-plano/{id}', 'AdminController@updatePlane')->name('marketplace.updatePlane');
        Route::get('/store/novo-plano/sucesso/{id}', 'AdminController@successPlane')->name('marketplace.successPlane');

        ## Rota Pagamento
        Route::post('/store/novo-plano/pagamento', 'AdminController@paymentPlane')->name('marketplace.paymentPlane');

        Route::get('/store/novo-plano/resultado/{id}/{type}', 'AdminController@paymentResult')->name('marketplace.paymentResult');
        Route::get('/store/minha-conta', 'AdminController@myAccount')->name('marketplace.myAccount');

        Route::any('/store/meu-catalogo', 'AdminController@myCatalog')->name('marketplace.myCatalog');
        Route::put('/store/meu-catalogo/atualizar-produtos', 'AdminController@myCatalogUpdate')->name('marketplace.myCatalogUpdate');

        Route::get('/store/carrinhos-ativos', 'AdminController@activeCarts')->name('marketplace.activeCarts');
        Route::get('/store/automacao/', 'AdminController@automationMkp')->name('marketplace.automationMkp');
        Route::post('/store/automacao/salvar', 'AdminController@automationMkpSave')->name('marketplace.automationSave');

        Route::get('/store/pedidos', 'AdminController@requests')->name('marketplace.requests');
        Route::get('/store/pedidos/detalhes/{id}', 'AdminController@requestsDetails')->name('marketplace.requestsDetails');
        Route::get('/store/recebiveis', 'AdminController@receivables')->name('marketplace.receivables');
        Route::get('/store/caixa-de-emails', 'AdminController@mailbox')->name('marketplace.mailbox');
        Route::get('/store/redes-sociais', 'AdminController@socialNetworks')->name('marketplace.socialNetworks');
        Route::get('/store/feed-de-dados', 'AdminController@dataFeed')->name('marketplace.dataFeed');
        Route::get('/store/sitemaps', 'AdminController@sitemaps')->name('marketplace.sitemaps');
        Route::get('/store/dominio', 'AdminController@domain')->name('marketplace.domain');
        Route::get('/store/dominio/principal', 'AdminController@domainPrincipal')->name('marketplace.domainPrincipal');
        Route::get('/store/banners', 'AdminController@banners')->name('marketplace.banners');
        Route::get('/store/loja-de-temas', 'AdminController@storeThemes')->name('marketplace.storeThemes');
        Route::get('/store/loja-de-temas/compra/{id}', 'AdminController@storeThemesBuy')->name('marketplace.storeThemesBuy');
        Route::get('/store/loja-de-temas/finalizar-pedido/{id}', 'AdminController@storeThemesFinalizeOrder')->name('marketplace.storeThemesFinalizeOrder');

        ## Integração
        Route::any('/store/aceite-de-termos', 'AdminController@termsRedirectAccept')->name('marketplace.termsRedirectAccept');
        Route::any('/store/integracao', 'AdminController@integration')->name('marketplace.integration');
        Route::any('/store/integracao/calShipping', 'AdminController@calShipping')->name('marketplace.calShipping');
        Route::get('/store/integracao/produto/{id}', 'AdminController@integrationProducts')->name('marketplace.integrationProducts');
        Route::any('/store/integracao/pedidos', 'AdminController@integrationRequests')->name('marketplace.integrationRequests');
        Route::post('/store/integracao/credito', 'AdminController@storeAddCredit')->name('marketplace.storeAddCredit');
        Route::post('/store/integracao/envio-bling', 'AdminController@productRegisterBling')->name('marketplace.productRegisterBling');

        Route::get('/store/produto-redirect/categorias-ml', 'AdminController@productCategoryArray')->name('marketplace.productCategoryArrayMl');
        Route::get('/store/produto-redirect/subcategorias-ml', 'AdminController@productSubcategoryArray')->name('marketplace.productSubcategoryArrayMl');
        Route::get('/store/produto-redirect/subcategorias-ml2', 'AdminController@productSubcategoryArray2')->name('marketplace.productSubcategoryArrayMl2');

        Route::post('/store/integracao/envio-mercado-livre', 'AdminController@productRegisterML')->name('marketplace.productRegisterML');
        Route::post('/store/integracao/update-mercado-livre', 'AdminController@productRegisterMlUpdate')->name('marketplace.productRegisterMlUpdate');

        Route::put('/store/integracao/pause/Mercado-livre', 'AdminController@productPauseMl')->name('marketplace.productPauseMl');
        Route::put('/store/integracao/active/Mercado-livre', 'AdminController@productActiveMl')->name('marketplace.productActiveMl');

        Route::delete('/store/integracao/deletar/bling', 'AdminController@productDeleteBling')->name('marketplace.productDeleteBling');
        Route::delete('/store/integracao/deletar/mercado-livre', 'AdminController@productDeleteMl')->name('marketplace.productDeleteMl');


        Route::get('/store/atendimento/', 'AdminController@attendance')->name('marketplace.attendance');
        Route::get('/store/atendimento/detalhes/{id}', 'AdminController@attendanceDetails')->name('marketplace.attendanceDetails');

        Route::get('/store/configuracao/', 'AdminController@storeConfig')->name('marketplace.storeConfig');
        Route::put('/store/configuracao/prova-social', 'AdminController@socialProof')->name('marketplace.socialProof');
        Route::get('/store/woocommerce/', 'AdminController@woocommerce')->name('marketplace.woocommerce');
        Route::post('/store/woocommerce/config', 'AdminController@woocommerceConfig')->name('marketplace.woocommerceConfig');
        Route::put('/store/woocommerce/destroy', 'AdminController@woocommerceDestroy')->name('marketplace.woocommerceDestroy');

        Route::post('/store/woocommerce/export', 'WooController@woocommerceSendProducts')->name('marketplace.woocommerceSendProducts');
        Route::post('/store/woocommerce/atualizar-preco', 'WooController@woocommerceUpdatePrice')->name('marketplace.woocommerceUpdatePrice');
        Route::post('/store/woocommerce/atualizar-catalogo', 'WooController@woocommerceUpdateCatalog')->name('marketplace.woocommerceUpdateCatalog');

        /** ROTA POST  */
        Route::post('/store/novo-plano/salvar', 'AdminController@savePlane')->name('marketplace.savePlane');
        Route::post('/store/update-plano/salvar', 'AdminController@updatePlaneDo')->name('marketplace.updatePlaneDo');
        Route::put('/store/cancelar-plano', 'AdminController@cancelPlane')->name('marketplace.cancelPlane');
        Route::post('/store/banners/ativar', 'AdminController@bannersActive')->name('marketplace.bannersActive');
        Route::post('/store/banners/new', 'AdminController@bannersNew')->name('marketplace.bannersNew');
        Route::post('/store/novo-plano/payment', 'AdminController@paymentPlanePayment')->name('marketplace.paymentPlanePayment');

        /** ROTA PUT  */
        Route::put('/store/redes-sociais/update', 'AdminController@socialNetworksUpdate')->name('marketplace.socialNetworksUpdate');
        Route::put('/store/facebook-pixel/update', 'AdminController@facebookPixelUpdate')->name('marketplace.facebookPixelUpdate');
        Route::put('/store/tags/update', 'AdminController@tagsUpdate')->name('marketplace.tagsUpdate');
        Route::put('/store/google-analytics/update', 'AdminController@googleAnalyticsUpdate')->name('marketplace.googleAnalyticsUpdate');
        Route::put('/store/rastreio-url/update', 'AdminController@crawlUrlUpdate')->name('marketplace.crawlUrlUpdate');
        Route::put('/store/cores-tema/update', 'AdminController@corsThemeUpdate')->name('marketplace.corsThemeUpdate');
        Route::put('/store/tema/update', 'AdminController@themeUpdate')->name('marketplace.themeUpdate');
        Route::put('/store/tema-configuracao/update', 'AdminController@themeConfigUpdate')->name('marketplace.themeConfigUpdate');
        Route::put('/store/configuracao/update', 'AdminController@storeConfigUpdate')->name('marketplace.storeConfigUpdate');
        Route::put('/store/url-config/update', 'AdminController@urlConfigUpdate')->name('marketplace.urlConfigUpdate');


        Route::put('/store/banners/remove', 'AdminController@bannersRemove')->name('marketplace.bannersRemove');

        /** ROTA DELETE  */
        Route::delete('/store/url-config/remove', 'AdminController@urlConfigRemove')->name('marketplace.urlConfigRemove');
        Route::delete('/store/name-config/update', 'AdminController@nameUpdate')->name('marketplace.nameUpdate');
        Route::get('/store/remover-card/{cardID}', 'AdminController@removeCard')->name('marketplace.removeCard');
    });

    Route::group([
        'domain' =>  config('multisite.backend'),
        'namespace' => 'Form',
        'middleware' => 'demoMode'
    ], function () {


        //////////////////////////////////////////////////////
        /** CONTROLE DE ROTAS DO USUARIOS */
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////
        // // Redirecinar Painel Usuário ///
        Route::get('/usuarios', 'UserController@index')->name('user.index');
        Route::get('/usuarios/editar', 'UserController@edit')->name('user.edit');

        /** ROTA POST  */
        Route::post('/usuarios/novo', 'UserController@create')->name('user.create');
        Route::post('/usuarios/adicionar-tokens', 'UserController@createTokens')->name('user.createTokens');
        Route::delete('/usuarios/deletar-token', 'UserController@deleteTokens')->name('user.deleteTokens');

        /** ROTA PUT  */
        Route::put('/usuarios/editar/salvar', 'UserController@update')->name('user.update');
        Route::put('/usuarios/editar/salvar/senha', 'UserController@updatePassword')->name('user.updatePassword');
        Route::put('/usuarios/editar-shop/salvar', 'UserController@updateShop')->name('user.updateShop');

        /** ROTA DELETE  */
        Route::delete('/usuarios/deletar', 'UserController@destroy')->name('user.destroy');
    });



    Route::group([
        'domain' =>  config('multisite.backend'),
        'namespace' => 'Multinivel',
        'middleware' => 'demoMode'
    ], function () {



        //////////////////////////////////////////////////////
        /** CONTROLE DE ROTAS AFILIADOS  */
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////

        Route::get('/multinivel', 'MultinivelController@multinivelIndex')->name('multinivel.multinivelIndex');
        Route::get('/multinivel/recebiveis', 'MultinivelController@receivables')->name('multinivel.receivables');
        Route::get('/multinivel/lojas-ativas', 'MultinivelController@activeStores')->name('multinivel.activeStores');
        Route::get('/multinivel/vendedores-ativos', 'MultinivelController@activeSellers')->name('multinivel.activeSellers');

        Route::get('/multinivel/faturas', 'MultinivelController@invoices')->name('multinivel.invoices');

        Route::any('/multinivel/saque-recebedores/relatorio', 'MultinivelController@plunderRecebedorReport')->name('multinivel.plunderRecebedorReport');
        Route::get('/multinivel/saque-recebedor/{type}', 'MultinivelController@plunderRecebedor')->name('multinivel.plunderRecebedor');

        Route::get('/multinivel/awards', 'MultinivelController@awards')->name('multinivel.awards');
        Route::get('/multinivel/marketing', 'MultinivelController@marketing')->name('multinivel.marketing');
        Route::get('/multinivel/metricas', 'MultinivelController@metrics')->name('multinivel.metrics');
        Route::get('/multinivel/publicidade', 'MultinivelController@publicity')->name('multinivel.publicity');

        Route::get('/multinivel/balanceAvailableMultinivel', 'MultinivelController@balanceAvailableMultinivel')->name('multinivel.balanceAvailableMultinivel');
        Route::get('/multinivel/amountReceivedMultinivel', 'MultinivelController@amountReceivedMultinivel')->name('multinivel.amountReceivedMultinivel');
        Route::get('/multinivel/pendingRefundMultinivel', 'MultinivelController@pendingRefundMultinivel')->name('multinivel.pendingRefundMultinivel');

        /** ROTA POST  */
        Route::post('/ativar/parceria', 'MultinivelController@activatePartnership')->name('multinivel.activatePartnership');

        /** ROTA PUT  */


        /** ROTA DELETE  */
    });
} else if (config('multisite.developers') === URL::to('/')) {

    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS DEVELOPERS */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    Route::group([
        'domain' =>  config('multisite.developers'),
        'namespace' => 'Developers',
        'middleware' => 'demoMode'
    ], function () {


        /** ROTA GET  */
        Route::get('/', 'DevelopersController@index')->name('developers.index');
        Route::get('/api-fornecedor/v1', 'DevelopersController@supplierApi')->name('developers.supplierApi');
        Route::get('/api-marketplace/v1', 'DevelopersController@marketplaceApi')->name('developers.marketplaceApi');
        Route::get('/desenvolvedor-temas/v1', 'DevelopersController@themeDeveloper')->name('developers.themeDeveloper');
        Route::get('/credenciais', 'DevelopersController@credentials')->name('developers.credentials');
        Route::get('/duvidas', 'DevelopersController@doubts')->name('developers.doubts');
        Route::get('/duvidas/detalhes/{id}', 'DevelopersController@doubtsDetails')->name('developers.doubtsDetails');

        /** ROTA POST  */

        /** ROTA PUT  */

        /** ROTA DELETE  */

        /** ROTA ANY */
        Route::any('/duvidas/search', 'DevelopersController@doubts')->name('developers.doubtsSearch');
    });
} else if (config('multisite.help') === URL::to('/')) {


    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS CENTRAL DE AJUDA */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    Route::group([
        'domain' =>  config('multisite.help'),
        'namespace' => 'Help',
        'middleware' => 'demoMode'
    ], function () {

        /** ROTA GET  */
        Route::get('/', 'HelpController@index')->name('help.index');
        Route::get('/fale-conosco', 'HelpController@contact')->name('help.contact');
        Route::get('/detalhes/{id}', 'HelpController@helpDetails')->name('help.Details');
        Route::any('/resultado', 'HelpController@search')->name('help.search');

        /** ROTA POST  */
        Route::post('/fale-conosco/enviar', 'HelpController@contactSend')->name('help.contactSend');
        /** ROTA PUT  */

        /** ROTA DELETE  */

        /** ROTA ANY */
        Route::any('/{cat}', 'HelpController@search')->name('help.searchNew');
        Route::any('/{cat}/{slug}', 'HelpController@helpDetails')->name('help.searchNewResult');
    });
} else if (config('multisite.link') === URL::to('/')) {

    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS MINI LINKS */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    Route::group([
        'domain' =>  config('multisite.link'),
        'namespace' => 'Link',
        'middleware' => 'demoMode'
    ], function () {

        ## Redirecinar Painel Usuário
        Route::get('/', 'AdminController@indexLink')->name('minilink.index');

        ## Checkout Afiliado
        Route::get('/c/{token}', 'AdminController@checkoutLink')->name('minilink.checkoutLink');
        ## Pagina do Produto
        Route::get('/p/{token}', 'AdminController@pageLink')->name('minilink.pageLink');

        ## Link Cadastro Multinivel
        Route::get('/m/{token}', 'AdminController@registerLink')->name('minilink.registerLink');

        ## ROTAS MONETIZZE
        Route::get('/monetizze/obrigado/{id}', 'MonetizzeController@checkoutThanks')->name('monetizze.checkoutThanks');
        Route::get('/monetizze/{id}/{slug}', 'MonetizzeController@products')->name('monetizze.products');
        Route::any('/monetizze/rastreio/{id}/{slug}', 'MonetizzeController@tracking')->name('monetizze.Tracking');
        Route::get('/monetizze/pedidos/{id}/{slug}', 'MonetizzeController@orderList')->name('monetizze.orderList');
        Route::post('/monetizze/pedidos/{id}/{slug}/entrar', 'MonetizzeController@siteLogin')->name('monetizze.siteLogin');
        Route::get('/monetizze/recuperar-senha/{id}/{slug}', 'MonetizzeController@recoverPassword')->name('monetizze.recoverPassword');

        ## ROTAS BRAIP
        Route::get('/braip/obrigado/{id}', 'BraipController@checkoutThanks')->name('braip.checkoutThanks');
        Route::get('/braip/{id}/{slug}', 'BraipController@products')->name('braip.products');
        Route::any('/braip/rastreio/{id}/{slug}', 'BraipController@tracking')->name('braip.Tracking');
        Route::get('/braip/pedidos/{id}/{slug}', 'BraipController@orderList')->name('braip.orderList');
        Route::post('/braip/pedidos/{id}/{slug}/entrar', 'BraipController@siteLogin')->name('braip.siteLogin');
        Route::get('/braip/recuperar-senha/{id}/{slug}', 'BraipController@recoverPassword')->name('braip.recoverPassword');

        ## ROTAS EDUZZ
        Route::get('/eduzz/obrigado/{id}', 'EduzzController@checkoutThanks')->name('eduzz.checkoutThanks');
        Route::get('/eduzz/{id}/{slug}', 'EduzzController@products')->name('eduzz.products');
        Route::any('/eduzz/rastreio/{id}/{slug}', 'EduzzController@tracking')->name('eduzz.Tracking');
        Route::get('/eduzz/pedidos/{id}/{slug}', 'EduzzController@orderList')->name('eduzz.orderList');
        Route::post('/eduzz/pedidos/{id}/{slug}/entrar', 'EduzzController@siteLogin')->name('eduzz.siteLogin');
        Route::get('/eduzz/recuperar-senha/{id}/{slug}', 'EduzzController@recoverPassword')->name('eduzz.recoverPassword');

        ## Rota de Token
        Route::get('/{token}', 'AdminController@routeLink')->name('minilink.routeLink');
    });
} else if (config('multisite.site') === URL::to('/')) {


    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS LOJA VIRTUAIS PREMIUM */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    Route::group([
        'domain' =>  config('multisite.site'),
        'namespace' => 'StorePremium',
        'middleware' => 'demoMode'
    ], function () {

        # ROTA GET #
        Route::get('/', 'WebController@home')->name('premium.home');
        Route::get('/calShipping', 'WebController@calculeteShipping')->name('premium.calculeteShipping');
        Route::any('/installment', 'WebController@installment')->name('premium.installment');
        Route::get('/pedido-detalhes/{slug}/{slug_product}/{orderID}', 'WebController@orderDetails')->name('premium.orderDetails');
        Route::get('/pedido-lista/{slug}/{slug_product}', 'WebController@orderList')->name('premium.orderList');
        Route::get('/rastreio/{slug}/{slug_product}', 'WebController@orderTracking')->name('premium.orderTracking');
        Route::get('/tracking/{slug}/{slug_product}', 'WebController@orderTrackingTo')->name('premium.orderTrackingTo');
        Route::get('/recuperar-senha/{slug}/{slug_product}', 'WebController@recoverPassword')->name('premium.recoverPassword');
        Route::get('/chamado/{slug}/{slug_product}', 'WebController@orderCalled')->name('premium.orderCalled');
        Route::delete('/cancelamento/{slug}/{slug_product}', 'WebController@orderDelete')->name('premium.orderDelete');
        Route::get('/deslogar/{slug}/{slug_product}/sair', 'WebController@siteLogout')->name('premium.siteLogout');

        Route::get('/calculatedVariation', 'WebController@calculatedVariation')->name('premium.calculatedVariation');
        Route::get('/imageAliexpress', 'WebController@imageAliexpress')->name('premium.imageAliexpress');
        Route::get('/atributo-variacao', 'WebController@calculatedAttributeVariation')->name('premium.calculatedAttributeVariation');

        # ROTA ANY
        Route::any('/produto/{slug}/{slug_product}', 'WebController@product')->name('premium.product');
        Route::any('/checkout/{slug}/{slug_product}', 'WebController@checkout')->name('premium.checkout');
        Route::any('/options/{slug}/{slug_product}', 'WebController@optionsProduct')->name('premium.optionsProduct');

        # ROTA POST #
        Route::post('/pedido/{slug}/novo/{slug_product}', 'WebController@createOrder')->name('premium.createOrder');
        Route::post('/logar/{slug}/{slug_product}/entrar', 'WebController@siteLogin')->name('premium.siteLogin');
    });
} else if (config('multisite.dashboard') === URL::to('/')) {


    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS LOJA DASHBOARD */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////

    Route::group([
        'domain' =>  config('multisite.dashboard'),
        'namespace' => 'Dashboard',
        'middleware' => 'demoMode'
    ], function () {

        // // Redirecinar Painel Usuário ///
        Route::get('/', 'WebController@index')->name('dashboard.index');
        Route::get('/admin', 'WebController@index')->name('dashboard.index2');

        Route::get('/admin/logout', 'WebController@logout')->name('dashboard.logout');

        Route::get('/admin/home', 'AdminController@home')->name('dashboard.home');
        Route::get('/admin/home/advanceValueDashboard', 'AdminController@advanceValueDashboard')->name('dashboard.advanceValueDashboard');
        Route::get('/admin/home/pendingRefundDashboard', 'AdminController@pendingRefundDashboard')->name('dashboard.pendingRefundDashboard');
        Route::get('/admin/home/amountReceivedDashboard', 'AdminController@amountReceivedDashboard')->name('dashboard.amountReceivedDashboard');
        Route::get('/admin/home/salesValueDashboard', 'AdminController@salesValueDashboard')->name('dashboard.salesValueDashboard');

        Route::get('/admin/minha-conta', 'AdminController@myAccount')->name('dashboard.myAccount');
        Route::any('/admin/fornecedores', 'AdminController@providers')->name('dashboard.providers');

        ## Rota Pedidos
        Route::any('/admin/pedidos', 'OrdersController@requests')->name('dashboard.requests');
        Route::post('/admin/pedidos-detalhes/arquivar', 'OrdersController@requestsDetailsArchive')->name('dashboard.requestsDetailsArchive');
        Route::put('/admin/pedidos-detalhes/arquivar-lista', 'OrdersController@requestsDetailsArchiveList')->name('dashboard.requestsDetailsArchiveList');
        Route::get('/admin/pedidos-detalhes/{id}', 'OrdersController@requestsDetails')->name('dashboard.requestsDetails');

        Route::any('/admin/pedidos-achaddo', 'OrdersController@requestsAchaddo')->name('dashboard.requestsAchaddo');
        Route::any('/admin/pedidos-afiliados', 'OrdersController@requestsAffiliate')->name('dashboard.requestsAffiliate');
        Route::get('/admin/pedidos-redirect', 'OrdersController@requestsRedirect')->name('dashboard.requestsRedirect');
        Route::post('/admin/pedidos-postback/new', 'OrdersController@requestsPostBackNew')->name('dashboard.requestsPostBackNew');
        Route::post('/admin/pedidos-reembolso/new', 'OrdersController@requestsBankData')->name('dashboard.requestsBankData');

        Route::any('/admin/produtos', 'CatalogController@products')->name('dashboard.products');
        Route::any('/admin/produtos/lixeira', 'CatalogController@productsTrash')->name('dashboard.productsTrash');
        Route::any('/admin/produtos-ativos', 'CatalogController@productsActive')->name('dashboard.productsActive');
        Route::get('/admin/produtos-loja/{id}', 'CatalogController@productsSearchStore')->name('dashboard.products.searchStore');

        Route::put('/admin/produto-detalhes/atualizar', 'CatalogController@productEditTo')->name('catalog.updateProduct.to');
        Route::put('/admin/produto-detalhes/pausar', 'CatalogController@productPause')->name('dashboard.productPause');
        Route::put('/admin/produto-detalhes/lixeira', 'CatalogController@productTrashProduct')->name('dashboard.productTrashProduct');
        Route::get('/admin/produto-detalhes/productCategoryArray', 'CatalogController@productCategoryArray')->name('dashboard.productCategoryArray');
        Route::get('/admin/produto-detalhes/productSubcategoryArray', 'CatalogController@productSubcategoryArray')->name('dashboard.productSubcategoryArray');


        Route::get('/admin/produto-detalhes/update-dropify/{shop_id}/{id}', 'CatalogController@productUpdateDropify')->name('dashboard.productUpdateDropify');
        Route::get('/admin/produto-detalhes/update-dslite/{shop_id}/{id}', 'CatalogController@productUpdateDslite')->name('dashboard.productUpdateDslite');
        Route::get('/admin/produto-detalhes/update-ml/{shop_id}/{id}', 'CatalogController@productUpdateMl')->name('dashboard.productUpdateMl');
        Route::get('/admin/produto-detalhes/update-bling/{shop_id}/{id}', 'CatalogController@productUpdateMl')->name('dashboard.productUpdateBling');

        Route::get('/admin/produto-detalhes/{shop_id}/{id}', 'CatalogController@productEdit')->name('dashboard.productEdit');
        Route::post('/admin/produto-detalhes/update-feeds', 'CatalogController@productEditFeeds')->name('dashboard.productEditFeeds');

        ## Rota MarketPlace
        Route::get('/admin/produto-redirect', 'MarketplaceController@productRedirect')->name('dashboard.productRedirect');
        Route::any('/admin/produto-redirect-search', 'MarketplaceController@productRedirect')->name('dashboard.productRedirectSearch');

        ## Landing Page
        Route::get('/admin/produto-redirect/landing-page/{id}', 'MarketplaceController@productRedirectLandingPage')->name('dashboard.productRedirectLandingPage');
        Route::post('/admin/produto-redirect/landing-page/salvar', 'MarketplaceController@productRedirectLandingPageSave')->name('dashboard.productRedirectLandingPageSave');

        Route::get('/admin/produto-redirect/detalhes/{id}', 'MarketplaceController@productRedirectDetails')->name('dashboard.productRedirectDetails');
        Route::any('/admin/produto-redirect/integracao/calShipping', 'MarketplaceController@calShipping')->name('dashboard.calShipping');
        Route::post('/admin/produto-redirect/cadastro/mercadolivre', 'MarketplaceController@productRegisterML')->name('dashboard.productRegisterML');
        Route::post('/admin/produto-redirect/cadastro/mercadolivre-update', 'MarketplaceController@productRegisterMlUpdate')->name('dashboard.productRegisterMlUpdate');
        Route::post('/admin/produto-redirect/cadastro/bling', 'MarketplaceController@productRegisterBling')->name('dashboard.productRegisterBling');

        ## Rotas Monetizze / Braip / Eduzz
        Route::post('/admin/produto-redirect/cadastro/monetizze', 'MarketplaceController@productRegisterMonetizze')->name('dashboard.productRegisterMonetizze');
        Route::post('/admin/produto-redirect/cadastro/braip', 'MarketplaceController@productRegisterBraip')->name('dashboard.productRegisterBraip');
        Route::post('/admin/produto-redirect/cadastro/eduzz', 'MarketplaceController@productRegisterEduzz')->name('dashboard.productRegisterEduzz');

        Route::post('/admin/produto-redirect/cadastro/monetizze/update', 'MarketplaceController@productMonetizzeUpdate')->name('dashboard.productMonetizzeUpdate');
        Route::post('/admin/produto-redirect/cadastro/braip/update', 'MarketplaceController@productBraipUpdate')->name('dashboard.productBraipUpdate');
        Route::post('/admin/produto-redirect/cadastro/eduzz/update', 'MarketplaceController@productEduzzUpdate')->name('dashboard.productEduzzUpdate');

        Route::get('/admin/produto-redirect/categorias-ml', 'MarketplaceController@productCategoryArray')->name('dashboard.productCategoryArrayMl');
        Route::get('/admin/produto-redirect/subcategorias-ml', 'MarketplaceController@productSubcategoryArray')->name('dashboard.productSubcategoryArrayMl');
        Route::get('/admin/produto-redirect/subcategorias-ml2', 'MarketplaceController@productSubcategoryArray2')->name('dashboard.productSubcategoryArrayMl2');

        Route::put('/admin/produto-redirect/cadastro/canal', 'MarketplaceController@productRegisterChannel')->name('dashboard.productRegisterChannel');
        Route::put('/admin/produto-redirect/pausar/canal', 'MarketplaceController@productPauseChannel')->name('dashboard.productPauseChannel');
        Route::put('/admin/produto-redirect/deletar/canal', 'MarketplaceController@productDeleteChannel')->name('dashboard.productDeleteChannel');

        Route::delete('/admin/produto-redirect/delete/bling', 'MarketplaceController@productDeleteBling')->name('dashboard.productDeleteBling');
        Route::delete('/admin/produto-redirect/delete/mercado-livre', 'MarketplaceController@productDeleteMl')->name('dashboard.productDeleteMl');
        Route::delete('/admin/produto-redirect/delete/monetizze', 'MarketplaceController@productDeleteMonetizze')->name('dashboard.productDeleteMonetizze');
        Route::delete('/admin/produto-redirect/delete/briap', 'MarketplaceController@productDeleteBraip')->name('dashboard.productDeleteBraip');
        Route::delete('/admin/produto-redirect/delete/eduzz', 'MarketplaceController@productDeleteEduzz')->name('dashboard.productDeleteEduzz');

        Route::put('/admin/produto-redirect/pause/Mercado-livre', 'MarketplaceController@productPauseMl')->name('dashboard.productPauseMl');
        Route::put('/admin/produto-redirect/active/Mercado-livre', 'MarketplaceController@productActiveMl')->name('dashboard.productActiveMl');

        ## Rota Afiliate
        Route::get('/admin/afiliados', 'AffiliateController@affiliates')->name('dashboard.affiliates');

        Route::any('/admin/afiliados/cursos-e-treinamentos', 'AffiliateController@coursesTraining')->name('dashboard.coursesTraining');

        Route::get('/admin/afiliados/cursos-e-treinamentos/novo', 'AffiliateController@coursesTrainingNew')->name('dashboard.coursesTrainingNew');
        Route::post('/admin/afiliados/cursos-e-treinamentos/save', 'AffiliateController@coursesTrainingSave')->name('dashboard.coursesTrainingSave');
        Route::get('/admin/afiliados/cursos-e-treinamentos/editar/{id}', 'AffiliateController@coursesTrainingEdit')->name('dashboard.coursesTrainingEdit');
        Route::post('/admin/afiliados/cursos-e-treinamentos/editar/save', 'AffiliateController@coursesTrainingEditSave')->name('dashboard.coursesTrainingEditSave');
        Route::get('/admin/afiliados/cursos-e-treinamentos/deletar', 'AffiliateController@coursesTrainingDelete')->name('dashboard.coursesTrainingDelete');

        ## Rota Clientes
        Route::any('/admin/clientes', 'ClientsController@customers')->name('dashboard.customers');
        Route::get('/admin/cliente-detalhe/{id}', 'ClientsController@customersDetails')->name('dashboard.customersDetails');
        Route::post('/admin/cliente-detalhe/update', 'ClientsController@customersDetailsUpdate')->name('dashboard.customersDetailsUpdate');
        Route::delete('/admin/cliente-detalhe/delete', 'ClientsController@customersDetailsDelete')->name('dashboard.customersDetailsDelete');
        Route::get('/admin/cliente-store/{id}', 'ClientsController@customersStore')->name('dashboard.customersStore');
        Route::get('/admin/cliente-pedidos/{id}', 'ClientsController@customersRequests')->name('dashboard.customerscustomersRequests');

        ## Rotas Shops
        Route::any('/admin/shops', 'ShopsController@shops')->name('dashboard.shops');
        Route::get('/admin/shops-faturas/{id}', 'ShopsController@shopsInvoices')->name('dashboard.shopsInvoices');
        Route::get('/admin/shops-pedidos/{id}', 'ShopsController@shopsOrdered')->name('dashboard.shopsOrdered');
        Route::get('/admin/shops/edit/{id}', 'ShopsController@shopsEdit')->name('dashboard.shopsEdit');
        Route::get('/admin/shops/pausar/{id}', 'ShopsController@shopsPaused')->name('dashboard.shopsPaused');
        Route::get('/admin/shops/ativar/{id}', 'ShopsController@shopsActive')->name('dashboard.shopsActive');
        Route::get('/admin/shops/automacao/{id}', 'ShopsController@shopsAutomation')->name('dashboard.shopsAutomation');
        Route::get('/admin/shops/excluir/{id}', 'ShopsController@shopsExcluir')->name('dashboard.shopsExcluir');

        ## Rota Stores
        Route::any('/admin/stores', 'StoresController@stores')->name('dashboard.stores');
        Route::get('/admin/stores/planos', 'StoresController@storesPlans')->name('dashboard.storesPlans');
        Route::get('/admin/stores/planos/editar/{id}', 'StoresController@storesPlansEdit')->name('dashboard.storesPlansEdit');
        Route::put('/admin/stores/planos/update/{id}', 'StoresController@storesPlansUpdate')->name('dashboard.storesPlansUpdate');

        Route::post('/admin/stores/planos/novo', 'StoresController@storesPlansNews')->name('dashboard.storesPlansNews');
        Route::delete('/admin/stores/planos/excluir', 'StoresController@storesPlansDelete')->name('dashboard.storesPlansDelete');

        Route::get('/admin/stores/planos/newPagamer/{id}', 'StoresController@plansNewPagamer')->name('dashboard.plansNewPagamer');

        Route::any('/admin/stores/assinaturas', 'StoresController@storesSubscriptions')->name('dashboard.storesSubscriptions');

        Route::get('/admin/stores/loja-de-temas', 'StoresController@storesThemes')->name('dashboard.storesThemes');

        Route::post('/admin/stores/loja-de-temas/novo', 'StoresController@storesThemesNew')->name('dashboard.storesThemesNew');
        Route::delete('/admin/stores/loja-de-temas/excluir', 'StoresController@storesThemesDelete')->name('dashboard.storesThemesDelete');

        Route::get('/admin/stores/paginas-configuracao', 'StoresController@configurationPages')->name('dashboard.configurationPages');
        Route::post('/admin/stores/paginas-configuracao/edite/{id}', 'StoresController@configurationPagesEdite')->name('dashboard.configurationPagesEdite');

        Route::get('/admin/stores/paginas-perguntas-e-respostas', 'StoresController@questionsList')->name('dashboard.questionsList');
        Route::post('/admin/stores/paginas-perguntas-e-respostas/edite/{id}', 'StoresController@questionsPagesEdite')->name('dashboard.questionsPagesEdite');

        ## Referencia APIs tipoPublicacao
        Route::get('/admin/configuracao-api', 'ReferenceApiController@settingsApi')->name('dashboard.settingsApi');
        Route::get('/admin/referencia-api', 'ReferenceApiController@referenceApi')->name('dashboard.referenceApi');
        Route::get('/admin/referencia-api/categorias-bling', 'ReferenceApiController@syncCategoryBling')->name('dashboard.syncCategoryBling');

        Route::get('/admin/MlenviosPais', 'ReferenceApiController@MlenviosPais')->name('dashboard.MlenviosPais');
        Route::get('/admin/tipoPublicacao', 'ReferenceApiController@tipoPublicacao')->name('dashboard.tipoPublicacao');
        Route::get('/admin/usuarioTesteMl', 'ReferenceApiController@usuarioTesteMl')->name('dashboard.usuarioTesteMl');


        Route::get('/admin/stores/central-de-ajuda', 'StoresController@helpCenter')->name('dashboard.helpCenter');
        Route::get('/admin/stores/central-de-ajuda/novo', 'StoresController@helpCenterNew')->name('dashboard.helpCenterNew');
        Route::post('/admin/stores/central-de-ajuda/novo-salvar', 'StoresController@helpCenterNewSave')->name('dashboard.helpCenterNewSave');
        Route::get('/admin/stores/central-de-ajuda/editar/{id}', 'StoresController@helpCenterEdit')->name('dashboard.helpCenterEdit');
        Route::delete('/admin/stores/central-de-ajuda/editar/excluir', 'StoresController@helpCenterEditDelete')->name('dashboard.helpCenterEditDelete');
        Route::post('/admin/stores/central-de-ajuda/editar/salvar', 'StoresController@helpCenterEditSave')->name('dashboard.helpCenterEditSave');

        ## Rota Configurações Home
        Route::get('/admin/configuracoes', 'ConfigGeralController@settings')->name('dashboard.settings');
        Route::post('/admin/configuracoes/update-tax', 'ConfigGeralController@settingsUptadeTax')->name('dashboard.settingsUptadeTax');
        Route::post('/admin/configuracoes/update-multinivel', 'ConfigGeralController@settingsUpdateMultinivel')->name('dashboard.settingsUpdateMultinivel');
        Route::get('/admin/configuracoes/criar-conta-bancaria', 'ConfigGeralController@createBankAccount')->name('dashboard.createBankAccount');
        Route::post('/admin/configuracoes/pagina/{id}', 'ConfigGeralController@configurationPage')->name('dashboard.configurationPage');

        Route::any('/admin/traducao', 'ConfigGeralController@translations')->name('dashboard.translations');

        Route::get('/admin/traducao/novo', 'ConfigGeralController@translationsNew')->name('dashboard.translationsNew');

        ## Rota Central de Ajuda
        Route::any('/admin/central-de-ajuda/categorias', 'ConfigGeralController@helpCategories')->name('dashboard.helpCategories');
        Route::any('/admin/central-de-ajuda/descritivos', 'ConfigGeralController@helpDescriptive')->name('dashboard.helpDescriptive');
        Route::get('/admin/central-de-ajuda/descritivos/novo', 'ConfigGeralController@helpDescriptiveNew')->name('dashboard.helpDescriptiveNew');
        Route::get('/admin/central-de-ajuda/descritivos/editar/{id}', 'ConfigGeralController@helpDescriptiveEdit')->name('dashboard.helpDescriptiveEdit');
        Route::post('/admin/central-de-ajuda/descritivos/editar/save', 'ConfigGeralController@helpDescriptiveEditSave')->name('dashboard.helpDescriptiveEditSave');

        Route::post('/admin/central-de-ajuda/descritivos/salvar', 'ConfigGeralController@helpDescriptiveSave')->name('dashboard.helpDescriptiveSave');
        Route::post('/admin/central-de-ajuda/categorias/novo', 'ConfigGeralController@helpCategoriesNew')->name('dashboard.helpCategoriesNew');
        Route::get('/admin/central-de-ajuda/categorias/deletar/{id}', 'ConfigGeralController@helpCategoriesDelete')->name('dashboard.helpCategoriesDelete');
        Route::delete('/admin/central-de-ajuda/descritivos/deletar', 'ConfigGeralController@helpDescriptiveDelete')->name('dashboard.helpDescriptiveDelete');

        ## Rota Documentação API
        Route::any('/admin/documentacao-api/categorias', 'ConfigGeralController@documentationCategories')->name('dashboard.documentationCategories');
        Route::any('/admin/documentacao-api/descritivos', 'ConfigGeralController@documentationDescriptive')->name('dashboard.documentationDescriptive');
        Route::get('/admin/documentacao-api/descritivos/novo', 'ConfigGeralController@documentationDescriptiveNew')->name('dashboard.documentationDescriptiveNew');
        Route::get('/admin/documentacao-api/descritivos/editar/{id}', 'ConfigGeralController@documentationDescriptiveEdit')->name('dashboard.documentationDescriptiveEdit');

        ## Rota Webservice Correios
        Route::get('/admin/webservice-correios/gerenciar-tabela', 'CorreiosOfflineController@webServiceCorreios')->name('dashboard.webServiceCorreios');
        Route::get('/admin/webservice-correios/gerenciar-tabela/list', 'CorreiosOfflineController@webServiceCorreiosList')->name('dashboard.webServiceCorreiosList');
        Route::get('/admin/webservice-correios/gerenciar-tabela/update', 'CorreiosOfflineController@webServiceCorreiosUpdate')->name('dashboard.webServiceCorreiosUpdate');
        Route::get('/admin/webservice-correios/gerenciar-tabela/create', 'CorreiosOfflineController@webServiceCorreiosCreate')->name('dashboard.webServiceCorreiosCreate');


        Route::post('/admin/documentacao-api/descritivos/editar/save', 'ConfigGeralController@documentationDescriptiveEditSave')->name('dashboard.documentationDescriptiveEditSave');
        Route::delete('/admin/documentacao-api/descritivos/deletar', 'ConfigGeralController@documentationDescriptiveDelete')->name('dashboard.documentationDescriptiveDelete');
        Route::post('/admin/documentacao-api/descritivos/salvar', 'ConfigGeralController@documentationDescriptiveSave')->name('dashboard.documentationDescriptiveSave');
        Route::post('/admin/documentacao-api/categorias/nova', 'ConfigGeralController@documentationCategoriesNew')->name('dashboard.documentationCategoriesNew');
        Route::get('/admin/documentacao-api/categorias/deletar/{id}', 'ConfigGeralController@documentationCategoriesDelete')->name('dashboard.documentationCategoriesDelete');

        Route::get('/admin/usuarios', 'UsersController@users')->name('dashboard.users');
        Route::any('/admin/usuarios-search', 'UsersController@users')->name('dashboard.usersSearch');

        ## Newslleter
        Route::any('/admin/usuarios/newsletter', 'UsersController@usersNewsletter')->name('dashboard.usersNewsletter');
        Route::get('/admin/usuarios/newsletter/novo', 'UsersController@usersNewsletterNew')->name('dashboard.usersNewsletterNew');

        Route::get('/admin/usuarios/newsletter/novo', 'UsersController@usersNewsletterNew')->name('dashboard.usersNewsletterNew');

        Route::post('/admin/usuarios/newsletter/imageUpload', 'UsersController@imageUpload')->name('dashboard.imageUpload');

        Route::post('/admin/usuarios/newsletter/salvar', 'UsersController@usersNewsletterSave')->name('dashboard.usersNewsletterSave');
        Route::post('/admin/usuarios/newsletter/send', 'UsersController@usersNewsletterSend')->name('dashboard.usersNewsletterSend');
        Route::post('/admin/usuarios/newsletter/pause', 'UsersController@usersNewsletterPause')->name('dashboard.usersNewsletterPause');
        Route::post('/admin/usuarios/newsletter/Close', 'UsersController@usersNewsletterClose')->name('dashboard.usersNewsletterClose');

        Route::delete('/admin/usuarios/newsletter/deletar', 'UsersController@usersNewsletterDelete')->name('dashboard.usersNewsletterDelete');

        Route::post('/admin/usuarios/newsletter/sendTest', 'UsersController@usersNewsletterSendTest')->name('dashboard.usersNewsletterSendTest');
        Route::get('/admin/usuarios/newsletter/adicionar-lista', 'UsersController@usersNewsletterAddList')->name('dashboard.usersNewsletterAddList');
        Route::get('/admin/usuarios/newsletter/teste-email', 'UsersController@usersNewsletterTest')->name('dashboard.usersNewsletterTest');
        Route::post('/admin/usuarios/newsletter/adicionar-lista/save', 'UsersController@usersNewsletterAddListSave')->name('dashboard.usersNewsletterAddListSave');
        Route::post('/admin/usuarios/newsletter/adicionar-lista/remove', 'UsersController@usersNewsletterAddListRemove')->name('dashboard.usersNewsletterAddListRemove');

        ## Whatsapp
        Route::any('/admin/usuarios/whatsapp', 'UsersController@usersWhatsapp')->name('dashboard.usersWhatsapp');
        Route::any('/admin/usuarios/whatsapp/novo', 'UsersController@usersWhatsappNew')->name('dashboard.usersWhatsappNew');

        Route::get('/admin/usuarios-detalhes/acesso-direto', 'UsersController@directAccess')->name('dashboard.directAccess');
        Route::get('/admin/usuarios-detalhes/{id}', 'UsersController@usersDetails')->name('dashboard.usersDetails');
        Route::post('/admin/usuarios/update', 'UsersController@usersUpdate')->name('dashboard.usersUpdate');
        Route::delete('/admin/usuarios/delete', 'UsersController@usersDelete')->name('dashboard.usersDelete');

        Route::get('/admin/departamento', 'AdminController@departaments')->name('dashboard.departaments');
        Route::any('/admin/departamento-search', 'AdminController@departaments')->name('dashboard.departamentsSearch');
        Route::get('/admin/departamento/edite/{id}', 'AdminController@departamentsEdit')->name('dashboard.departamentsEdit');

        Route::get('/admin/categoria/{id}', 'AdminController@category')->name('dashboard.category');
        Route::get('/admin/categoria/edite/{id}', 'AdminController@categoryEdit')->name('dashboard.categoryEdit');
        Route::get('/admin/subcategoria/{id}', 'AdminController@subCategory')->name('dashboard.subCategory');
        Route::get('/admin/subcategoria/edite/{id}', 'AdminController@subCategoryEdit')->name('dashboard.subCategoryEdit');
        Route::any('/admin/aprovar-produtos', 'AdminController@approveProducts')->name('dashboard.approveProducts');


        Route::get('/admin/relacionar-categorias', 'AdminController@relateCategories')->name('dashboard.relateCategories');
        Route::get('/admin/relacionar-categorias/edite/categoria', 'AdminController@productCategoryArray')->name('dashboard.productCategoryArray');
        Route::get('/admin/relacionar-categorias/edite/subcategoria', 'AdminController@productSubcategoryArray')->name('dashboard.productSubcategoryArray');
        Route::get('/admin/relacionar-categorias/edite/{id}', 'AdminController@relateCategoriesEdit')->name('dashboard.relateCategoriesEdit');

        Route::any('/admin/juntar-categorias', 'AdminController@joinCategories')->name('dashboard.joinCategories');
        Route::get('/admin/juntar-categorias/edite/{id}', 'AdminController@joinCategoriesEdit')->name('dashboard.joinCategoriesEdit');
        Route::put('/admin/juntar-categorias/save', 'AdminController@joinCategoriesSave')->name('dashboard.joinCategoriesSave');

        Route::any('/admin/relacionar-categorias-search', 'AdminController@relateCategories')->name('dashboard.relateCategoriesSearch');
        Route::any('/admin/produtos-loja-search/{id}', 'AdminController@productsSearchStoreSearch')->name('dashboard.products.searchStoreSearch');
        Route::any('/admin/auto-complete-google', 'AdminController@autoCompleteGoogle')->name('dashboard.autoCompleteGoogle');
        Route::any('/admin/auto-complete-ml', 'AdminController@autoCompleteCategoryML')->name('dashboard.autoCompleteCategoryML');

        Route::any('/admin/categoria-search/{id}', 'AdminController@categorySearch')->name('dashboard.categorySearch');

        Route::post('/admin/departamento-new', 'AdminController@departamentNew')->name('dashboard.departamentNew');
        Route::post('/admin/categoria-new', 'AdminController@categoryNew')->name('dashboard.categoryNew');
        Route::post('/admin/subcategoria-new', 'AdminController@subCategoryNew')->name('dashboard.subCategoryNew');

        Route::put('/admin/departamento-update', 'AdminController@departamentUpdate')->name('dashboard.departamentUpdate');
        Route::put('/admin/categoria-update', 'AdminController@categoryUpdate')->name('dashboard.categoryUpdate');
        Route::put('/admin/subcategoria-update', 'AdminController@subCategoryUpdate')->name('dashboard.subCategoryUpdate');
        Route::put('/admin/departamento-delete', 'AdminController@departamentDelete')->name('dashboard.departamentDelete');
        Route::put('/admin/categoria-delete', 'AdminController@categoryDelete')->name('dashboard.categoryDelete');
        Route::put('/admin/subcategoria-delete', 'AdminController@subCategoryDelete')->name('dashboard.subCategoryDelete');

        Route::put('/admin/relacionar-categorias/update', 'AdminController@relateCategoriesUpdate')->name('dashboard.relateCategoriesUpdate');
        Route::put('/admin/relacionar-categorias/update-lista', 'AdminController@relateCategoriesUpdateList')->name('dashboard.relateCategoriesUpdateList');

        Route::put('/admin/aprovar-produtos/list', 'AdminController@approveProductsList')->name('dashboard.approveProductsList');
        Route::put('/admin/aprovar-produtos/listAll', 'AdminController@approveProductsListAll')->name('dashboard.approveProductsListAll');
        Route::put('/admin/produtos-ativos/pausar', 'AdminController@productsActivePause')->name('dashboard.productsActivePause');

        Route::get('/admin/datatable-produtos', 'TableController@products')->name('dashboard.datatable.products');

        ## Rota Web Aberta
        Route::get('/recuperar-senha', 'WebController@loginRecover')->name('dashboard.loginRecover');
        Route::get('/alterar-senha/{token}', 'WebController@changePassword')->name('dashboard.changePassword');
        Route::post('/nova-senha', 'WebController@newPassword')->name('dashboard.newPassword.do');
        Route::put('/salvar-senha', 'WebController@salvePassword')->name('dashboard.savePassword.do');
        Route::post('/admin/login', 'WebController@login')->name('dashboard.login.do');
    });
} else if (config('multisite.checkout') === URL::to('/')) {


    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS CKECKOUT */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////

    Route::group([
        'domain' =>  config('multisite.checkout'),
        'namespace' => 'Checkout',
        'middleware' => 'demoMode'
    ], function () {


        ///////////////////////////////////
        //// CHECOUT WOOCOMMERCE  /////////
        ///////////////////////////////////
        ## GET
        Route::get('/cart/{shopId}', 'CheckoutController@shoppingCart')->name('checkout.shoppingCart');
        Route::get('/api', 'CheckoutController@apiMkt')->name('checkout.apiMkt');
        Route::get('/checkout/deletar/{shopId}/{id}', 'CheckoutController@deleteCart')->name('checkout.deleteCart');
        Route::get('/calcular-frete/{shopId}', 'CheckoutController@calculateShipping')->name('checkout.calculateShipping');
        Route::get('/rastreio/{shopId}', 'CheckoutController@tracking')->name('checkout.tracking');
        Route::get('/pedidos-detalhes/{shopId}/{orderMasterID}', 'CheckoutController@requestsDetails')->name('checkout.requestsDetails');
        Route::get('/pedidos-lista/{shopId}', 'CheckoutController@requestsList')->name('checkout.requestslist');
        Route::get('/ShippingCheckout', 'CheckoutController@affiliateShippingCheckout')->name('checkout.affiliateShippingCheckout');

        Route::get('/{shopId}/{productId}', 'CheckoutController@addCart')->name('checkout.addCart');

        ## ANY
        Route::any('/affiliate/installment', 'CheckoutController@installment')->name('checkout.installment');

        ## POST
        Route::post('/mkp/{shopId}', 'CheckoutController@marketPlace')->name('checkout.marketPlace');
        Route::post('/pedido-novo/{shopId}', 'CheckoutController@newOrder')->name('checkout.newOrder');

        ///////////////////////////////////
        //// CHECOUT PRODUTOS DIRECT  /////
        ///////////////////////////////////
        Route::get('/{shopId}/{productSlug}', 'CheckoutController@affiliate')->name('checkout.affiliate');


        Route::post('/{shopId}/{productSlug}/payment', 'CheckoutController@affiliatePayment')->name('checkout.affiliatePayment');
        Route::get('/{shopId}/{productSlug}/detalhes/{orderID}', 'CheckoutController@affiliateDetails')->name('checkout.affiliateDetails');
    });
} else if (config('multisite.touchbuy') === URL::to('/')) {


    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS TOUCHBUY */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////

    Route::group([
        'domain' =>  config('multisite.touchbuy'),
        'namespace' => 'Touchbuy',
        'middleware' => 'demoMode'
    ], function () {


        Route::get('/', 'HomeController@login')->name('touch.login'); /// Login Loja
        Route::get('/home', 'TouchController@home')->name('touch.home');
    });
} else if (config('multisite.travel') === URL::to('/')) {


    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS VIAGENS */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////

    Route::group([
        'domain' =>  config('multisite.travel'),
        'namespace' => 'Travel',
        'middleware' => 'demoMode'
    ], function () {


        Route::get('/', 'TravelController@index')->name('travel.index');
    });
} else {


    //////////////////////////////////////////////////////
    /** CONTROLE DE ROTAS DO MARKETPLACE */
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    $url = URL::current();

    Route::group([
        'domain' =>  parse_url($url, PHP_URL_HOST),
        'namespace' => 'StoreMarketplace',
        'middleware' => 'demoMode'
    ], function () {

        // // Redirecinar Painel Usuário ///
        Route::get('/', 'WebController@index')->name('mkp.index');
        Route::get('/shop', 'WebController@shop')->name('mkp.shop');
        Route::get('/shop/{filter}', 'WebController@shop')->name('mkp.shopFilter');
        Route::get('/lista-de-desejos', 'WebController@wishlist')->name('mkp.wishlist');
        Route::get('/quem-somos', 'WebController@aboutUs')->name('mkp.aboutUs');
        Route::get('/mi/{product_id}', 'WebController@miniLink')->name('mkp.miniLink');

        Route::any('/loja/{slug}', 'WebController@stores')->name('mkp.stores');

        Route::get('/departamentos', 'WebController@pageDepartaments')->name('mkp.pageDepartaments');
        Route::get('/sem-resultado', 'WebController@noResults')->name('mkp.noResults');
        Route::get('/produto/variacao/calculo', 'WebController@calculatedVariation')->name('mkp.calculatedVariation');
        Route::get('/produto/atributo-variacao/calculo', 'WebController@calculatedAttributeVariation')->name('mkp.calculatedAttributeVariation');
        Route::get('/produto/carregar/image', 'WebController@imageAliexpress')->name('mkp.imageAliexpress');
        Route::get('/carrinho', 'WebController@cart')->name('mkp.cart');
        Route::get('/checkout', 'WebController@checkout')->name('mkp.checkout');
        Route::get('/payment', 'WebController@payment')->name('mkp.payment');

        Route::any('/payment/reprocess/{orderID}', 'WebController@paymentReprocess')->name('mkp.paymentReprocess');

        Route::get('/concluido', 'WebController@concluded')->name('mkp.concluded');
        Route::get('/login', 'WebController@login')->name('mkp.login');
        Route::get('/cadastro', 'WebController@register')->name('mkp.register');
        Route::get('/recuperar-senha', 'WebController@recoverPassword')->name('mkp.recoverPassword');
        Route::get('/alterar-senha/{token}', 'WebController@changePassword')->name('mkp.changePassword');
        Route::get('/central-de-ajuda', 'WebController@helpCenter')->name('mkp.help');
        Route::get('/removeCart', 'WebController@removeCart')->name('mkp.removeCart');
        Route::get('/calculateShipping', 'WebController@calculateShipping')->name('mkp.calculateShipping');
        Route::get('/calculateShippingCheckout', 'WebController@calculateShippingCheckout')->name('mkp.calculateShippingCheckout');
        Route::get('/error', 'WebController@error')->name('mkp.error');
        Route::get('/pagina/{slug}', 'WebController@pages')->name('mkp.pages');
        Route::get('/confirmar/email/{token}', 'WebController@confirmEmail')->name('mkp.confirmEmail');
        Route::get('/produto/remove-wishlist/{id}', 'WebController@removeWishlist')->name('mkp.removeWishlist');
        Route::get('/produto/{product_id}/{slug}', 'WebController@product')->name('mkp.product');

        /// Feed de dados ///
        Route::get('/feeds/google-feed.xml', 'WebController@feedGoogle')->name('mkp.feedGoogle');
        Route::get('/feeds/facebook-feed.xml', 'WebController@feedFacebook')->name('mkp.feedFacebook');
        Route::get('/xml/sitemaps.xml', 'WebController@sitemaps')->name('mkp.sitemaps');

        // ANY
        Route::post('/addCart', 'WebController@addCart')->name('mkp.addCart');

        //  Post
        Route::post('/addCupom', 'WebController@addCupom')->name('mkp.addCupom');
        Route::post('/login/entrar', 'WebController@loginDo')->name('mkp.login.do');
        Route::post('/cadastro/novo', 'WebController@registerNew')->name('mkp.registerNew');
        Route::post('/pedido/novo', 'WebController@newOrder')->name('mkp.newOrder');
        Route::post('/pedido/finalizar', 'WebController@finishOrder')->name('mkp.finishOrder');
        Route::post('/pedido/reprocess', 'WebController@orderReprocess')->name('mkp.orderReprocess');
        Route::post('/solicitar-token/novo', 'WebController@tokenNew')->name('mkp.tokenNew');
        Route::post('/solicitar-senha/nova', 'WebController@passwordNew')->name('mkp.passwordNew');
        Route::post('/carrinho/atualizar', 'WebController@updateCart')->name('mkp.updateCart');
        Route::post('/assinar-newsletter', 'WebController@signNewsletter')->name('mkp.signNewsletter');
        Route::post('/produto/add-wishlist', 'WebController@addWishlist')->name('mkp.addWishlist');

        //** Rotas PUT */
        Route::put('/salvar-senha/nova', 'WebController@passwordNewDo')->name('mkp.passwordNewDo');

        Route::post('/chamados-novo/do', 'UserController@registerNewDo')->name('mkp.calledNewDo');
        Route::post('/chamados-resposta/do', 'UserController@responseNewDo')->name('mkp.responseNewDo');
        Route::post('/meus-dados/update', 'UserController@myDateUpdate')->name('mkp.myDateUpdate');
        Route::post('/endereco/update', 'UserController@myAddress')->name('mkp.myAddress');

        /// Get
        Route::get('/minha-conta', 'UserController@myAccount')->name('mkp.myAccount');


        Route::get('/pedidos', 'UserController@orders')->name('mkp.orders');
        Route::get('/pedidos-detalhes/confirmar-cancelamento', 'UserController@confirmCancellation')->name('mkp.confirmCancellation');
        Route::get('/pedidos-detalhes/confirmar-cancelamento/to', 'UserController@confirmCancellationTo')->name('mkp.confirmCancellationTo');
        Route::get('/pedidos-detalhes/acompanhar-entrega', 'UserController@trackDelivery')->name('mkp.trackDelivery');
        Route::get('/pedidos-detalhes/ver-pix', 'UserController@paymentPix')->name('mkp.paymentPix');
        Route::get('/pedidos-detalhes/rastrerObjetoCorreios', 'UserController@rastrerObjetoCorreios')->name('mkp.rastrerObjetoCorreios');
        Route::get('/pedidos-detalhes/rastrerObjetoAliexpress', 'UserController@rastrerObjetoAliexpress')->name('mkp.rastrerObjetoAliexpress');
        Route::get('/pedidos-detalhes/{id}', 'UserController@requestsDetails')->name('mkp.requestsDetails');

        ## Rastreamento de Pedidos
        Route::get('/pedidos-detalhes/carregar-rastreio', 'TrackingController@correiosRastreamento')->name('mkp.correiosRastreamento');

        Route::get('/meus-dados', 'UserController@myDate')->name('mkp.myDate');
        Route::get('/endereco', 'UserController@address')->name('mkp.address');
        Route::get('/chamados', 'UserController@called')->name('mkp.called');
        Route::get('/chamados-detalhes/{id}', 'UserController@calledDetails')->name('mkp.calledDetails');
        Route::get('/chamados-novo', 'UserController@calledNew')->name('mkp.calledNew');
        Route::get('/chamados-fechar', 'UserController@calledClose')->name('mkp.calledClose');

        Route::get('/usuario/deslogar', 'UserController@logout')->name('mkp.logout');

        /// Pagina Não Localizada
        Route::get('/{slug}', 'WebController@index')->name('mkp.indexErro');
    });
}